<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


 // ===== Roles and Permissions APIs ==========

    //---- Roles-----
    Route::post('/role', [
      'uses' => 'RolesController@addrole'
      //'middleware' => 'auth.jwt'
    ]);
    Route::put('/roles/{id}', [
      'uses' => 'RolesController@updateRole',
      //'middleware' => 'auth.jwt'
    ]);
    Route::get('/roles', [
      'uses' => 'RolesController@getRole'
    ]);
    Route::delete('/roles/{id}', [
      'uses' => 'RolesController@deleteRole'
    ]);
    //---- Permissions-----
    Route::post('/permission', [
      'uses' => 'RolesController@addPermission'
    ]);
    Route::put('/permissions/{id}', [
      'uses' => 'RolesController@updatePermission'
    ]);
    Route::get('/permissions', [
      'uses' => 'RolesController@getPermission'
    ]);
    Route::delete('/permissions/{id}', [
      'uses' => 'RolesController@deletePermission'
    ]);

    // ---- Attaching Roles and permissions
    Route::post('/attachRole', [
      'uses' => 'RolesController@attachRoleToUser'
    ]);

    Route::post('/attachPermission', [
      'uses' => 'RolesController@attachPermissionToRole'
    ]);


 // ===== End of Roles and Permissions APIs ==========

  // ===== Users APIs ==========
  Route::post('/signup', [
    'uses' => 'UserController@signup'
  ]);
  Route::post('/addUser', [
    'uses' => 'UserController@addUser'
  ]);
  Route::post('/user/signin', [
    'uses' => 'UserController@signin'
  ]);
  Route::get('/register/confirm/{token}', [
    'uses' => 'UserController@confirmEmail'
  ]);

  Route::put('/users/{id}', [
    'uses' => 'UserController@updateUser'
  ]);
  Route::get('/users', [
    'uses' => 'UserController@getUser'
  ]);
  Route::get('/user/{id}', [
    'uses' => 'UserController@getOneUser'
  ]);
  Route::delete('/users/{id}', [
    'uses' => 'UserController@deleteUser'
  ]);
  // =====End of  Users APIs ==========

  // ===== START OF PASSWORD RESET ==========

  Route::post('password/email', 'Auth\ForgotPasswordController@getResetPassword');

  // =====END OF PASSWORD RESET ==========

  // ===== CLUBS APIs ==========

  Route::post('/addClub', [
    'uses' => 'ClubController@addClub'
  ]);

  Route::post('/club/switch', [
    'uses' => 'ClubController@switchClubStatus'
  ]);

  Route::put('/clubs/{id}', [
    'uses' => 'ClubController@updateClub'
  ]);
  Route::get('/clubs', [
    'uses' => 'ClubController@getClub'
  ]);
  Route::get('/club/{id}', [
    'uses' => 'ClubController@getOneClub'
  ]);
  Route::delete('/clubs/{id}', [
    'uses' => 'ClubController@deleteClub'
  ]);
  // =====End of  CLUBS APIs ==========


  // ===== Branches APIs ==========

  Route::post('/addBranch', [
    'uses' => 'BranchController@addBranch'
  ]);

  Route::post('/branch/admin', [
    'uses' => 'BranchController@addBranchAdmin'
  ]);

  Route::post('/branch/switch', [
    'uses' => 'BranchController@switchBranchStatus'
  ]);

  Route::put('/branches/{id}', [
    'uses' => 'BranchController@updateBranch'
  ]);
  Route::get('/branches', [
    'uses' => 'BranchController@getBranch'
  ]);
  Route::get('/branche/{id}', [
    'uses' => 'BranchController@getOneBranch'
  ]);
  Route::delete('/branches/{id}', [
    'uses' => 'BranchController@deleteBranch'
  ]);
  // =====End of  Branches APIs ==========


  // ===== Branch Advs APIs ==========

  Route::post('/add/branch/adv', [
    'uses' => 'BranchAdvController@store'
  ]);

  Route::post('/branch/adv/switch', [
    'uses' => 'BranchAdvController@switchAdvStatus'
  ]);

  Route::put('/branch/adv/{id}', [
    'uses' => 'AdvController@update'
  ]);
  Route::get('/branch/advs', [
    'uses' => 'AdvController@getAdv'
  ]);
  Route::get('/branch/adv/{id}', [
    'uses' => 'AdvController@getOneAdv'
  ]);
  Route::delete('/branch/adv/{id}', [
    'uses' => 'AdvController@destroy'
  ]);
  // =====End of Branch Advs APIs ==========


  // ===== Courts APIs ==========

  Route::post('/addCourt', [
    'uses' => 'CourtController@addCourt'
  ]);

  Route::post('/court/switch', [
    'uses' => 'CourtController@switchCourtStatus'
  ]);

  Route::put('/courts/{id}', [
    'uses' => 'CourtController@updateCourt'
  ]);
  Route::get('/courts', [
    'uses' => 'CourtController@getCourt'
  ]);
  Route::get('/court/{id}', [
    'uses' => 'CourtController@getOneCourt'
  ]);
  Route::delete('/courts/{id}', [
    'uses' => 'CourtController@deleteCourt'
  ]);
  // =====End of  Courts APIs ==========

  // ===== Trainers APIs ==========

  Route::post('/addTrainer', [
    'uses' => 'TrainerController@addTrainer'
  ]);

  Route::post('/trainer/switch', [
    'uses' => 'CourtController@switchTrainerStatus'
  ]);

  Route::put('/trainers/{id}', [
    'uses' => 'TrainerController@updateTrainer'
  ]);
  Route::get('/trainers', [
    'uses' => 'TrainerController@getTrainer'
  ]);
  Route::get('/trainer/{id}', [
    'uses' => 'TrainerController@getOneTrainer'
  ]);
  Route::delete('/trainers/{id}', [
    'uses' => 'TrainerController@deleteTrainer'
  ]);
  // =====End of  Trainers APIs ==========

  // ===== System Sports APIs ==========

  Route::post('/addSport', [
    'uses' => 'SportController@addSport'
  ]);

  Route::get('/sports', [
    'uses' => 'SportController@getSport'
  ]);

  // =====End of System Sports APIs ==========

  // ===== Advs APIs ==========

  Route::post('/addAdv', [
    'uses' => 'AdvController@store'
  ]);

  Route::post('/adv/switch', [
    'uses' => 'AdvController@switchAdvStatus'
  ]);

  Route::put('/adv/{id}', [
    'uses' => 'AdvController@update'
  ]);
  Route::get('/advs', [
    'uses' => 'AdvController@getAdv'
  ]);
  Route::get('/adv/{id}', [
    'uses' => 'AdvController@getOneAdv'
  ]);
  Route::delete('/adv/{id}', [
    'uses' => 'AdvController@destroy'
  ]);
  // =====End of  Advs APIs ==========
