<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ===== Page Routing  ==========

Route::get('/403','PagesController@notPermited');
Route::get('/404','PagesController@notfound');
Route::get('/info/{subtab}','PagesController@terms');

Route::group(['middleware' => 'authAndSystemAdmin'], function()
{
    Route::get('/club-list', 'PagesController@systemClubs');
    Route::get('/home-advs', 'PagesController@homeAdvs');

    //testing routes
    Route::get('/profile', 'PagesController@userprofile');
    Route::get('/calender', 'PagesController@calender');
});

Route::group(['middleware' => 'authAndIsAdmin'], function()
{

    Route::get('/system-member', 'PagesController@systemMember');

});


Route::get('/importExportpage', ['as'=>'importExportpage', 'uses'=>'ItemController@importExport']);
Route::get('/downloadExcel/{type}', 'ItemController@downloadExcel');
Route::get('/downloaderrorssheet/{patch}', 'ItemController@downloadErrorsSheet');
Route::post('/importExcel', 'ItemController@importExcel');
Route::post('/import', 'ItemController@import');
Route::get('/status', ['as'=>'status', 'uses'=>'ItemController@status']);

Route::get('/myaccount', 'PagesController@myaccount')->middleware('auth');

// ===== End Page Routing  ==========


 // ===== Roles and Permissions APIs ==========
 Route::group(['middleware' => 'authAndSystemAdmin'], function()
 {
   //---- Roles-----
   Route::post('/role', [
     'uses' => 'RolesController@addrole'
     //'middleware' => 'auth.jwt'
   ]);
   Route::put('/roles/{id}', [
     'uses' => 'RolesController@updateRole',
     //'middleware' => 'auth.jwt'
   ]);
   Route::get('/roles', [
     'uses' => 'RolesController@getRole'
   ]);
   Route::delete('/roles/{id}', [
     'uses' => 'RolesController@deleteRole'
   ]);
   //---- Permissions-----
   Route::post('/permission', [
     'uses' => 'RolesController@addPermission'
   ]);
   Route::put('/permissions/{id}', [
     'uses' => 'RolesController@updatePermission'
   ]);
   Route::get('/permissions', [
     'uses' => 'RolesController@getPermission'
   ]);
   Route::delete('/permissions/{id}', [
     'uses' => 'RolesController@deletePermission'
   ]);

   // ---- Attaching Roles and permissions
   Route::post('/attachRole', [
     'uses' => 'RolesController@attachRoleToUser'
   ]);

   Route::post('/attachPermission', [
     'uses' => 'RolesController@attachPermissionToRole'
   ]);

 });


 // ===== End of Roles and Permissions APIs ==========



// ===== Users  ==========

Route::group(['middleware' => 'auth'], function()
{

      Route::put('/users/{id}', [
        'uses' => 'UserController@updateUser'
      ]);
      Route::post('/updateUserPic/{id}', [
        'uses' => 'UserController@updateUserPic'
      ]);
});


Route::post('/reset/user/password', [
  'uses' => 'UserController@resetUserPassword'
]);
Route::get('/register/confirm/{token}', [
  'uses' => 'UserController@confirmEmail'
]);
Route::post('/resendVerifyEmail', [
  'uses' => 'UserController@resendVerifyEmail'
]);



Route::group(['middleware' => 'authAndIsAdmin'], function()
{
      Route::post('/addUser', [
        'uses' => 'UserController@addUser'
      ]);
      Route::get('/users', [
        'uses' => 'UserController@getUser'
      ]);
      Route::get('/user/{id}', [
        'uses' => 'UserController@getOneUser'
      ]);
      Route::delete('/users/{id}', [
        'uses' => 'UserController@deleteUser'
      ]);
});


Route::post('/signup', [
  'uses' => 'UserController@signup'
]);

Route::post('/user/signin', [
  'uses' => 'UserController@signin'
]);

// =====End of  Users  ==========



// ===== START OF PASSWORD RESET ==========

// Route::post('password/email', 'Auth\ForgotPasswordController@getResetPassword');

// =====END OF PASSWORD RESET ==========

// ===== CLUBS  ==========
Route::group(['middleware' => 'authAndSystemAdmin'], function()
{
  Route::post('/addClub', [
    'uses' => 'ClubController@addClub'
  ]);

  Route::post('/club/switch', [
    'uses' => 'ClubController@switchClubStatus'
  ]);

  Route::put('/clubs/{id}', [
    'uses' => 'ClubController@updateClub'
  ]);
  Route::get('/clubs', [
    'uses' => 'ClubController@getClub'
  ]);
  Route::get('/club/{id}', [
    'uses' => 'ClubController@getOneClub'
  ]);
  // modify here to delete only the branch
  Route::delete('/clubs/{id}', [
    'uses' => 'ClubController@deleteClub'
  ]);
});


Route::get('/club-page/{branch_id}/{tabName}/{subTabName}', [
  'uses' => 'PagesController@viewBranch'
]);


// =====End of  CLUBS  ==========


// ===== Booking  ==========

Route::post('/getCalenderData', [
  'uses' => 'BookingController@getCalenderData'
]);

Route::post('/getCalenderResources', [
  'uses' => 'BookingController@getCalenderResources'
]);

Route::post('/getCalenderEvents', [
  'uses' => 'BookingController@getCalenderEvents'
]);

Route::post('/getAvailableTrainers', [
  'uses' => 'BookingController@getAvailableTrainers'
]);

Route::post('/setBooking', [
  'uses' => 'BookingController@setBooking'
]);

Route::post('/getBranchCourts', [
  'uses' => 'BookingController@getBranchCourts'
]);

Route::post('/buildUserBookingsTable', [
  'uses' => 'BookingController@buildUserBookingsTable'
]);

Route::post('/getBookingReceipt', [
  'uses' => 'BookingController@getBookingReceipt'
]);

Route::post('/cancelUserBooking', [
  'uses' => 'BookingController@cancelUserBooking'
]);

Route::post('/feedEnterCashModal', [
  'uses' => 'BookingController@feedEnterCashModal'
]);

Route::post('/submitCashPayment', [
  'uses' => 'BookingController@submitCashPayment'
]);
Route::post('/refundThisBooking', [
  'uses' => 'BookingController@refundThisBooking'
]);

Route::get('/booking/success/{refId}', [
  'uses' => 'PagesController@bookingSuccess'
]);

Route::get('request/booking',
 'BookingController@requestReturn')->name('requestReturn');

// ===== End of Booking  ==========


// ===== Payment  ==========

Route::group(['middleware' => 'authAndIsAdmin'], function()
{

        Route::post('/buildMonthPaymentsTable', [
          'uses' => 'PaymentController@buildMonthPaymentsTable'
        ]);

        Route::post('/submitDeposit', [
          'uses' => 'PaymentController@submitDeposit'
        ]);

        Route::post('/uploadBankReceipt', [
          'uses' => 'PaymentController@uploadBankReceipt'
        ]);

        Route::post('/feedReceiptImageModal', [
          'uses' => 'PaymentController@feedReceiptImageModal'
        ]);





});
Route::get('/download-reciept/{file_name}', [
  'uses' => 'PaymentController@getDownload'
]);
// ===== End of Payment  ==========

// ===== Branches  ==========
Route::group(['middleware' => 'authAndIsAdmin'], function()
{
      Route::post('/addBranch', [
        'uses' => 'BranchController@addBranch'
      ]);

      Route::post('/branch/admin', [
        'uses' => 'BranchController@addBranchAdmin'
      ]);

      Route::put('/branch/admin/{id}', [
        'uses' => 'BranchController@updateBranchAdminInfo'
      ]);

      Route::post('/branch/switch', [
        'uses' => 'BranchController@switchBranchStatus'
      ]);

      Route::put('/branches/{id}', [
        'uses' => 'BranchController@updateBranch'
      ]);

      Route::put('/branchesBasic/{id}', [
        'uses' => 'BranchController@updateBranchBasicInfo'
      ]);

      Route::get('/branches', [
        'uses' => 'BranchController@getBranch'
      ]);
      Route::get('/branch/{id}/{tabName}', [
        'uses' => 'PagesController@getOneBranch'
      ]);

      Route::get('/branch/{id}/{tabName}/{subTabName}', [
        'uses' => 'PagesController@getOneBranch'
      ]);

      Route::delete('/branches/{id}', [
        'uses' => 'BranchController@deleteBranch'
      ]);

      Route::post('/reset/admin/password', [
        'uses' => 'BranchController@resetAdminPassword'
      ]);

      Route::post('/searchAllMembers', [
        'uses' => 'BranchController@searchAllMembers'
      ]);

      Route::post('/feedUpdateMemberForm', [
        'uses' => 'BranchController@feedUpdateMemberForm'
      ]);

      Route::put('/updateSheetMember/{member_id}', [
        'uses' => 'BranchController@updateSheetMember'
      ]);

      Route::post('/assignMemberToClub', [
        'uses' => 'BranchController@assignMemberToClub'
      ]);

      Route::post('/checkOrAttachMember', [
        'uses' => 'BranchController@checkOrAttachMember'
      ]);

      Route::post('/buildServiceSportCard', [
        'uses' => 'BranchController@buildServiceSportCard'
      ]);

      Route::post('/UpdateJoinedMemberStatus', [
        'uses' => 'BranchController@UpdateJoinedMemberStatus'
      ]);

      Route::post('/buildBookingsTable', [
        'uses' => 'BranchController@buildBookingsTable'
      ]);

      Route::post('/buildBookingsCalender', [
        'uses' => 'BranchController@buildBookingsCalender'
      ]);
      Route::post('/getRelCourtsAndTrainers', [
        'uses' => 'BranchController@getRelCourtsAndTrainers'
      ]);

      Route::get('/getMembers/getdata/{branch_id}', 'BranchController@getBranchMembers')->name('getMembers/getdata/{branch_id}');

      Route::get('/getMembersRequests/getrequests/{branch_id}', 'BranchController@getBranchRequests')->name('getBranchRequests/getrequests/{branch_id}');

      Route::get('/getJoinedMembers/getmembers/{branch_id}', 'BranchController@getJoinedMembers')->name('getJoinedMembers/getmembers/{branch_id}');


});

Route::post('/checkOrAttachMember2', [
  'uses' => 'BranchController@checkOrAttachMember2'
])->middleware('auth');

Route::post('/checkOrAttachMember3', [
  'uses' => 'BranchController@checkOrAttachMember3'
])->middleware('auth');


// ===== End of  Branches  ==========
Route::group(['middleware' => 'authAndIsAdmin'], function()
{
// ===== MEMBERS   ==========
      Route::get('/member/{id}/{tabName}', [
        'uses' => 'PagesController@getOneMember'
      ]);

      Route::put('/branch/member/{id}', [
        'uses' => 'BranchController@updateBranchMember'
      ]);

      Route::post('/branch/member/delete', [
        'uses' => 'BranchController@branchMemberDelete'
      ]);

});


// ===== End of  MEMBERS  ==========


// ===== APPLICATION  ==========
Route::group(['middleware' => 'authAndIsAdmin'], function()
{

      Route::post('/confirmApplication', [
        'uses' => 'ApplicationController@confirmApplication'
      ]);

      Route::post('/cancelApplication', [
        'uses' => 'ApplicationController@cancelApplication'
      ]);

});

Route::post('/reportRequestToAdmin', [
  'uses' => 'ApplicationController@reportRequestToAdmin'
]);
// ===== END OF APPLICATION  ==========


  // ===== SERVICES  ==========
Route::group(['middleware' => 'authAndIsAdmin'], function()
{
        Route::post('/addService', [
          'uses' => 'ServiceController@addService'
        ]);

        Route::put('/services/{id}', [
          'uses' => 'ServiceController@updateService'
        ]);
        Route::get('/services', [
          'uses' => 'ServiceController@getService'
        ]);
        Route::get('/service/{id}', [
          'uses' => 'ServiceController@getOneService'
        ]);
        Route::delete('/services/{id}', [
          'uses' => 'ServiceController@deleteService'
        ]);
  });
  // ===== END OF SERVICES  ==========

  // ===== SERVICES SPORTS ==========
  Route::group(['middleware' => 'authAndIsAdmin'], function()
  {
        Route::post('/service/sport/switch', [
          'uses' => 'ServiceSportController@switchServiceStatus'
        ]);

        Route::post('/service/sport/add', [
          'uses' => 'ServiceSportController@addServiceSport'
        ]);

        Route::put('/service/sport/{id}', [
          'uses' => 'ServiceSportController@updateService'
        ]);
    });
  // ===== END OF SPORTS  ==========


// ===== Branch Advs APIs ==========
Route::group(['middleware' => 'authAndIsAdmin'], function()
{


        Route::post('/add/branch/adv', [
          'uses' => 'BranchAdvController@store'
        ]);

        Route::put('/branch/adv/{id}', [
          'uses' => 'BranchAdvController@update'
        ]);
        Route::get('/branch/advs', [
          'uses' => 'BranchAdvController@getAdv'
        ]);
        Route::get('/branch/adv/{id}', [
          'uses' => 'BranchAdvController@getOneAdv'
        ]);
        Route::post('/branch/adv/delete', [
          'uses' => 'BranchAdvController@destroy'
        ]);

        Route::post('/sortBranchAdvs', [
          'uses' => 'BranchAdvController@sortAdvs'
        ]);
});
// =====End of Branch Advs APIs ==========



  // ===== Courts APIs ==========
  Route::group(['middleware' => 'authAndIsAdmin'], function()
  {
          Route::post('/addCourt', [
            'uses' => 'CourtController@addCourt'
          ]);

          Route::put('/courts/{id}', [
            'uses' => 'CourtController@updateCourt'
          ]);
          Route::get('/courts', [
            'uses' => 'CourtController@getCourt'
          ]);
          Route::get('/court/{id}', [
            'uses' => 'CourtController@getOneCourt'
          ]);
          Route::post('/court/delete', [
            'uses' => 'CourtController@deleteCourt'
          ]);
    });
  // =====End of  Courts APIs ==========

  // ===== Trainers APIs ==========

  Route::group(['middleware' => 'authAndIsAdmin'], function()
  {

        Route::post('/addTrainer', [
          'uses' => 'TrainerController@addTrainer'
        ]);

        Route::put('/trainers/{id}', [
          'uses' => 'TrainerController@updateTrainer'
        ]);
        Route::get('/trainers', [
          'uses' => 'TrainerController@getTrainer'
        ]);
        Route::get('/trainer/{id}', [
          'uses' => 'TrainerController@getOneTrainer'
        ]);
        Route::post('/trainer/delete', [
          'uses' => 'TrainerController@deleteTrainer'
        ]);
    });
  // =====End of  Trainers APIs ==========

// ===== Advs APIs ==========
Route::group(['middleware' => 'authAndSystemAdmin'], function()
{

        Route::post('/addAdv', [
          'uses' => 'AdvController@store'
        ]);

        Route::post('/updateSponsorLogos', [
          'uses' => 'AdvController@updateSponsorLogos'
        ]);

        Route::post('/removeSponsorLogo', [
          'uses' => 'AdvController@removeSponsorLogo'
        ]);

        Route::post('/addSidebarAdv', [
          'uses' => 'AdvController@store'
        ]);

        Route::put('/adv/{id}', [
          'uses' => 'AdvController@update'
        ]);

        Route::put('/sidebar/adv/{id}', [
          'uses' => 'AdvController@update'
        ]);

        Route::get('/advs', [
          'uses' => 'AdvController@getAdv'
        ]);
        Route::get('/adv/{id}', [
          'uses' => 'AdvController@getOneAdv'
        ]);

        Route::post('/adv/delete', [
          'uses' => 'AdvController@destroy'
        ]);

        Route::post('/sortAdvs', [
          'uses' => 'AdvController@sortAdvs'
        ]);

        Route::post('/sortSidebarAdvs', [
          'uses' => 'AdvController@sortAdvs'
        ]);

        Route::post('/updateWelcomeNote', [
          'uses' => 'SettingController@updateWelcomeNote'
        ]);
});

// =====End of  Advs APIs ==========


Route::get('/{unlocatedUrl}',[
  'uses' => 'PagesController@notfound'
]);

Route::get('/{unlocatedUrl}/{second}',[
  'uses' => 'PagesController@notfound'
]);

Route::get('/{unlocatedUrl}/{second}/{third}',[
  'uses' => 'PagesController@notfound'
]);

Route::get('/{unlocatedUrl}/{second}/{third}/{fourth}',[
  'uses' => 'PagesController@notfound'
]);


// Route::get('/vue', function () {
//     return view('vuetest', ['name' => 'James']);
// });
