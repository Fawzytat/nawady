<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advs', function (Blueprint $table) {
          $table->increments('id');
          $table->boolean('isActive')->default(0);
          $table->string('type'); // wide , long , sponsor logo
          $table->string('position')->nullable();
          $table->string('url')->nullable();
          $table->string('link')->nullable();
          $table->date('startdate')->nullable();
          $table->date('enddate')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advs');
    }
}
