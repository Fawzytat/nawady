<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            $table->foreign('booking_id')->references('id')->on('bookings');
            $table->string('type');
            $table->string('status')->default("pending");
            $table->string('migs_ReceiptNo')->nullable();
            $table->string('migs_CardType')->nullable();
            $table->string('migs_CardNum')->nullable();
            $table->string('migs_TransactionNo')->nullable();
            $table->string('fawry_payment_flag')->nullable();
            $table->string('fawrybillingAcctNum')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
