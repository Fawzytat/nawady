<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_sport', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('services');
            $table->integer('sport_id')->unsigned();
            $table->foreign('sport_id')->references('id')->on('sports');
            $table->boolean('isActive')->default(0);
            $table->string('slot_duration')->nullable();
            $table->string('slot_price')->nullable();
            $table->string('morning_from')->nullable();//<===== time
            $table->string('morning_to')->nullable();//<===== time
            $table->string('afternoon_from')->nullable();//<===== time
            $table->string('afternoon_to')->nullable();//<===== time
            $table->string('evening_from')->nullable();//<===== time
            $table->string('evening_to')->nullable();//<===== time
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_sport');
    }
}
