<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');//
            $table->integer('member_id')->unsigned();
            $table->foreign('member_id')->references('id')->on('members');//
            $table->integer('branch_id')->unsigned();
            $table->foreign('branch_id')->references('id')->on('branches');//
            $table->integer('court_id')->unsigned();
            $table->foreign('court_id')->references('id')->on('courts');//
            $table->integer('service_sport_id')->unsigned();
            $table->foreign('service_sport_id')->references('id')->on('service_sport');//

            $table->boolean('isPaid')->default('0');//
            $table->string('confirmed')->default('0');//
            $table->string('status')->default('upcoming');//
            $table->boolean('passLimit')->default('0');//
            $table->string('booking_type'); // <=== Cash , credit_card , fawry , gzira_card
            $table->string('total_time'); // <=== from to as text
            $table->date('day');//
            $table->time('from');//
            $table->time('to');//
            $table->string('total_price'); // <=== Price//
            // trainer
            $table->boolean('withTrainer')->default('0');//
            $table->integer('trainer_id')->nullable();//
            //fawry
            $table->string('fawrybillingAcctNum')->nullable();
            $table->string('messageSignature')->nullable();
            $table->string('payment_code')->nullable();
            $table->string('transaction_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
