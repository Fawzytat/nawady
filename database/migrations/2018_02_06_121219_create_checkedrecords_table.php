<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckedrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkedrecords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patch');
            $table->boolean('isValid');
            $table->string('position');
            $table->string('membership_id')->nullable();
            $table->string('membership_name')->nullable();
            $table->string('error');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkedrecords');
    }
}
