<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('branch_id')->unsigned();
          $table->foreign('branch_id')->references('id')->on('branches');
          $table->string('name');
          $table->string('short_name');
          $table->boolean('isActive')->default(0);
          $table->integer('max_book_per_day')->nullable()->default(1);
          $table->string('max_duration_per_booking')->nullable()->default(1);
          $table->string('cancel_limit')->nullable()->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
