<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_sport_id')->unsigned();
            $table->foreign('service_sport_id')->references('id')->on('service_sport');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('avatarURL')->nullable();
            $table->string('phone')->nullable();
            $table->string('price_per_session')->nullable();
            $table->string('daily_from')->nullable();//<===== time
            $table->string('daily_to')->nullable();//<===== time
            $table->string('days_off')->nullable();//<===== JSON of dates
            $table->boolean('isActive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers');
    }
}
