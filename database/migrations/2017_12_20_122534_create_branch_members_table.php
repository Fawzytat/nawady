<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_members', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('member_id')->unsigned();
          $table->foreign('member_id')->references('id')->on('members');
          $table->integer('branch_id')->unsigned();
          $table->foreign('branch_id')->references('id')->on('branches');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_members');
    }
}
