<?php

namespace Illuminate\Auth\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('contact@nawady.services', 'Nawady system')
            ->line('We received a request to change your password on Nawady Services.')
            ->action('Click here to change your password', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('If you didn’t request a password change, you can ignore this message and continue to use your
             current password. Someone probably typed in your email address by accident..')
             ->line('Your friends at Nawady services');
    }
}
