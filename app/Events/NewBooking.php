<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Booking;
use App\User;
use App\Branch;

class NewBooking
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $booking;
    public $club_admin;
    public $system_admin;

    public function __construct(Booking $booking)
    {
        $branch = Branch::where("id",$booking->branch_id)->first();
        $this->booking = $booking;
        $this->club_admin = User::where("id",$branch->admin_id)->first();
        $this->system_admin = User::with(['roles' => function($q){
                                  $q->where('name', 'sys_admin');
                              }])->first();

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
