<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckedRecord extends Model
{  
    protected $table = 'checkedrecords';
}
