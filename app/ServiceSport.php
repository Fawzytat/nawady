<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceSport extends Model
{
   protected $table = 'service_sport';


   public function service()
   {
     return $this->belongsTo('App\Service');
   }

   public function sport()
   {
     return $this->hasOne('App\Sport','id','sport_id');
   }

   public function court()
   {
     return $this->hasMany('App\Court');
   }

   public function trainer()
   {
     return $this->hasMany('App\Trainer');
   }

   public function toggleActive()
     {
         $this->isActive= !$this->isActive;
         return $this;
     }

}
