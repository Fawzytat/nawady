<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('without_spaces', function($attr, $value)
        {
          return preg_match('/^\S*$/u', $value);
        });

        Validator::extend('strong_password', function ($attribute, $value, $parameters, $validator) {
           // Contain at least one uppercase/lowercase letters, one number and one special char
           return preg_match('/^(?=.*[a-z|A-Z])(?=.*[A-Z])(?=.*\d).{8,}$/', (string)$value);
       }, 'Please make a strong password');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
