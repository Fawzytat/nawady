<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Validator;
use Config;
use Excel;
use App\Item;
use App\InfectedRecord;
use App\CheckedRecord;
use Illuminate\Support\Facades\Artisan;

use Illuminate\Support\Facades\Log;
//use Maatwebsite\Excel\Facades\Excel;


use App\Flag;

class ValidateManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validate:excelfile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This validates an excel file';

    protected $chunkSize = 100;
    /**
     * Create a new command instance.
     *
     * @return void
     */



    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle()
     {
         $file = Flag::where('imported','=','0')->where('validated','=','0')->orderBy('created_at', 'DESC')->first();

         $file_path = Config::get('filesystems.disks.local.root') . '/' .$file->file_name;
         //Log::debug($file->file_name);
         //Log::debug($file->rows_validated);


         // let's first count the total number of rows
         Excel::load($file_path, function($reader) use($file) {
             $objWorksheet = $reader->getActiveSheet();
             $file->total_rows = $objWorksheet->getHighestRow() - 1; //exclude the heading
             $file->save();
         });
         // working to this point
         //now let's import the rows, one by one while keeping track of the progress
         Excel::filter('chunk')
             ->selectSheetsByIndex(0)
             ->load($file_path)
             ->chunk($this->chunkSize, function($result) use ($file) {
                 $rows = $result->toArray();
                 $counter = 0;

                 $checked_records=[];
                 $checked_members_ids=[];
                 $stored_checked_records =  CheckedRecord::where('patch',$file->id)
                 ->pluck('membership_id')->toArray();

                 foreach ($rows as $row)
                 {
                   $counter++;

                   if(empty($row['membership_id']) || empty($row['membership_name']) || empty($row['record_id']) )
                   {
                       array_push($checked_records, [
                           'patch' => $file->id,
                           'position' => $row['record_id'],
                           'error' => 'empty',
                           'membership_id' => 'none',
                           'isValid' => 0
                                   ]);
                       array_push($checked_members_ids, $row['membership_id'] );

                   }
                   if(!empty($row['membership_id']) && !empty($row['record_id']))
                   {

                       if (in_array($row['membership_id'], $stored_checked_records) || in_array($row['membership_id'], $checked_members_ids) )
                       {
                         array_push($checked_records, [
                             'patch' => $file->id,
                             'position' => $row['record_id'],
                             'error' => 'repeated',
                             'membership_id' => $row['membership_id'],
                             'isValid' => 0
                                     ]);
                         array_push($checked_members_ids, $row['membership_id'] );
                       }
                       else
                       {

                         array_push($checked_records, [
                             'patch' => $file->id,
                             'position' => $row['record_id'],
                             'error' => 'none',
                             'membership_id' => $row['membership_id'],
                             'isValid' => 1
                                     ]);
                         array_push($checked_members_ids, $row['membership_id'] );
                       }

                   }

                 }

                 if($checked_records && !empty($checked_records))
                 {
                  CheckedRecord::insert($checked_records);
                 }




                  $file = $file->fresh(); //reload from the database
                  $file->rows_validated = $file->rows_validated + $counter;
                  $file->save();



             }
         );

         $file->validated =1;
         $file->save();
         Artisan::call('cache:clear');
     }
}
