<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Validator;
use Config;
use Excel;
use App\Member;
use App\Item;
use App\CheckedRecord;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
//use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


use App\Flag;

class ImportManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:excelfile {--branch=} {--patch=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This imports an excel file';

    protected $chunkSize = 100;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */



     public function handle()
     {
        $branch_id = $this->option('branch');
        $patch_id = $this->option('patch');

        // IN CASE OF LOOKING FOR JOINED MEMBERS
        // $members = Member::whereHas('branch', function($q) use($branch_id) {
        //     $q->where('branch_id', $branch_id);
        // })->get();


        $file = Flag::where('id','=', $patch_id)
                     ->orderBy('created_at', 'DESC')
                     ->first();
        $file->import_started = 1;
        $file->save();

         $file_path = Config::get('filesystems.disks.local.root') . '/' .$file->file_name;


         // // working to this point
         // //now let's import the rows, one by one while keeping track of the progress
         Excel::filter('chunk')
             ->selectSheetsByIndex(0)
             ->load($file_path)
             ->chunk($this->chunkSize, function($result) use ($file , $branch_id) {
                 $rows = $result->toArray();
                 $counter = 0;

                 $members = Member::where('branch_id',$branch_id)->get();
                 $membership_ids = $members->pluck('membershipId')->toArray();

                 $checked_members_ids=[];
                 $dataArray=[];
                 foreach ($rows as $row)
                 {

                   if(!empty($row['membership_id']) && !empty($row['membership_name']))
                   {
                     if( ! in_array( $row['membership_id'] ,$membership_ids ) && ! in_array( $row['membership_id'] ,$checked_members_ids ) )
                      {

                        array_push($dataArray, [
                            'membershipId' => $row['membership_id'] ,
                            'membershipName' => $row['membership_name'],
                            'branch_id' => $branch_id
                        ]);

                        array_push($checked_members_ids, $row['membership_id'] );


                      }

                      $counter++;
                   }
                 }
                 if(isset($dataArray) && !empty($dataArray))
                 {
                   Member::insert($dataArray);
                 }
                 // IN CASE OF LOOKING FOR JOINED MEMBERS
                 //if(isset($dataArray))
                 //{
                   //$inserted_ids = [];
                   //foreach($dataArray as $data)
                   //{
                        //$inserted_ids[] = DB::table('members')->insertGetId($data);
                   //}
                   // foreach($inserted_ids as $key => $value)
                   // {
                   //   $member = Member::where('id', $value )->first();
                   //   $member->branch()->syncWithoutDetaching($branch_id);
                   // }
                 //}



                    $file = $file->fresh(); //reload from the database
                    $file->rows_imported = $file->rows_imported + $counter;
                    $file->save();


             }
         );

         $file->imported =1;
         $file->save();
         CheckedRecord::where('patch',$file->id)->delete();
         Artisan::call('cache:clear');
     }



}
