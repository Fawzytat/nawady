<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      date_default_timezone_set('Africa/Cairo');

         // $schedule->command('queue:work',[
         //   '--tries' => 3
         //   ])->everyTenMinutes();
      $schedule->call('\App\Http\Controllers\ComandsController@advertReminder')->dailyAt('13:00');
      $schedule->call('\App\Http\Controllers\ComandsController@CheckBookingStatus')->dailyAt('21:00');

      //$schedule->call('\App\Http\Controllers\ComandsController@logMyName')->everyMinute();


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
