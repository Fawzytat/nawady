<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Court extends Model
{
  public function Branch()
  {
    return $this->belongsTo('App\Branch');
  }

  public function serviceSport()
  {
    return $this->belongsTo('App\ServiceSport');
  }


  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }
}
