<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchAdv extends Model
{
  public function branch()
  {
    return $this->belongsTo('App\Branch');
  }


  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }
}
