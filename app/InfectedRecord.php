<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfectedRecord extends Model
{
    protected $table = 'infectedrecords';
}
