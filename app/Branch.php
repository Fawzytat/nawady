<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function courts()
    {
      return $this->hasMany('App\Court');
    }

    public function services()
    {
      return $this->hasMany('App\Service');
    }

    public function advs()
    {
      return $this->hasMany('App\BranchAdv')->orderBy('position');
    }

    public function club()
    {
      return $this->belongsTo('App\Club');
    }

    public function member()
    {
      return $this->belongsToMany('App\Member', 'branch_members', 'branch_id', 'member_id');
    }

    public function toggleActive()
      {
          $this->isActive= !$this->isActive;
          return $this;
      }
}
