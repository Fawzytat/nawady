<?php

namespace App\Utils;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Booking;
use App\Branch;
use App\Sport;
use App\Service;
use App\ServiceSport;

class Helpers
{



//------------------- FILE UPLOAD HELPER -------------------------
  public static function upload( $content=null , $savePath=null ){
    if ($content) {
      if(!$savePath){
        return 'can`t find the path that where to save the file please use';
      }
      $max_size = 10;//<=========10MB
      $file_size = round(($content->getClientSize()/1024)/1024,3);//<=========== get the uploaded file size before uploading
      $file_type = $content->getClientOriginalExtension();
      $original_name = $content->getClientOriginalName();
      if ($max_size < $file_size) {
        $upload_response['status'] = false;
        $upload_response['size'] = $file_size;
        $upload_response['size'] = $file_size;
        $upload_response['message'] = 'can`t upload file becouse of the size , max uploaded file is 10MB';
          return $upload_response;
      }
      $images_ext = ['png','jpg','jpeg'];//<========== allowed extensions for images
      $files_ext = ['doc','docx','xlsx','xlsm','xltx','xltm','xls','pdf'];//<========== allowed extensions for Files
      if (in_array($file_type , $images_ext)) { //<========= is image? YES let`s upload images with multi qualities
          $img = Image::make($content->getRealPath());
          $img->save(storage_path('../public/storage/app/public/'.$savePath.'/'.$content->hashName()));
          $img->save(storage_path('../public/storage/app/public/60/'.$savePath.'/'.$content->hashName()),60);
          $img->save(storage_path('../public/storage/app/public/20/'.$savePath.'/'.$content->hashName()),20);
        $upload_response['pathToSave'] = $savePath.'/'.$content->hashName();
      }elseif (in_array($file_type , $files_ext)) { //<========= is file? YES let`s upload the file
        $upload_response['pathToSave'] = $content->store($savePath );
      }else{
        $upload_response['status'] = false;
        $upload_response['message'] = 'file extension not supported';
      }
      $upload_response['status'] = true;
      $upload_response['size'] = $file_size;
      $upload_response['type'] = $file_type;
      $upload_response['message'] = 'uploaded with success';
        return (object)$upload_response;
    }
    return 'no files';
  }
//-------------------END OF FILE UPLOAD HELPER -------------------------
//-------------------USER REGISTER HELPER -------------------------



      public static function SignUpUser($request,$verify_status) {

          $validator = Validator::make($request->toArray(), [
            'username' => 'required|without_spaces|string|max:255|unique:users',
            'phone' => 'required|digits:11',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|strong_password|string|min:6|confirmed',
            'membershipId' => 'sometimes|required|string',
            'membershipName' => 'sometimes|required|string',
            'club_id' => 'sometimes|required|string'
         ]);

          if ($validator->fails())
          {
            return [ "validation" => "failed" ,"validator" => $validator];
          }

          if ($request->file('avatar')) {//<============if file attached
            $upload_file = Helpers::upload($request->file('avatar'),'avatars');
          }
          $user = new User([
             'username' => $request->username,
             'email' => $request->email,
             'phone' => $request->phone,
             'address' => $request->address,
             'avatarURL' => isset($upload_file)?$upload_file->pathToSave:"none",
             'verified' => $verify_status,
             'password' => bcrypt($request->password),
         ]);
         $user->save();

         return $user;
      }


//-------------------END OF USER REGISTER HELPER -------------------------

  public static function generateCode($code_type)
  {
    if ($code_type == "booking_code")
    {
      $code = Helpers::generateRandomString(6,"digits");
      if (Helpers::bookingCodeExists($code)) {
          $code =  Helpers::generateRandomString();
      }
      return $code;
    }

    if ($code_type == "payment_code")
    {
      $code = Helpers::generateRandomString(4,"string");
      return $code;
    }

  }

//-------------------generate Uni code Number HELPER -------------------------
    public static function generateRandomString($length ,$type) {
      if($type == "digits")
      {
        $characters = '0123456789';
      }
      if($type == "string")
      {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      }
       $charactersLength = strlen($characters);
       $code = '';
       for ($i = 0; $i < $length; $i++) {
           $code .= $characters[rand(0, $charactersLength - 1)];
       }
       return $code;
   }

   public static function bookingCodeExists($code) {
        return Booking::where("booking_id",$code)->exists();
    }

//-------------------END generate Uni code Number HELPER -------------------------

//-------------------SET DEFAULT BRANCH SETTING -------------------------
        public static function setDefaultBranchSetting($branch) {
          $service =  new Service;
          $service->branch_id = $branch->id;
          $service->isActive = 1;
          $service->max_book_per_day = 1;
          $service->max_duration_per_booking = 1;
          $service->cancel_limit = 0;
          $service->short_name = "court_booking";
          $service->name = "COURT BOOKING";
          $service->save();

          $service_sport1 =  new ServiceSport;
          $service_sport1->service_id = $service->id;
          $service_sport1->sport_id = 27;
          $service_sport1->isActive = 1;

          $service_sport1->slot_duration = 30;
          $service_sport1->slot_duration_h = "00:30";
          $service_sport1->slot_price = 100;
          $service_sport1->morning_from = "08:00:00";
          $service_sport1->morning_to = "11:00:00";
          $service_sport1->afternoon_from = "12:00:00";
          $service_sport1->afternoon_to = "17:00:00";
          $service_sport1->evening_from = "18:00:00";
          $service_sport1->evening_to = "23:00:00";

          $service_sport1->save();


          $service_sport2 =  new ServiceSport;
          $service_sport2->service_id = $service->id;
          $service_sport2->sport_id = 22;
          $service_sport2->isActive = 1;
          $service_sport2->slot_duration = 30;
          $service_sport2->slot_duration_h = "00:30";
          $service_sport2->slot_price = 100;
          $service_sport2->morning_from = "08:00:00";
          $service_sport2->morning_to = "11:00:00";
          $service_sport2->afternoon_from = "12:00:00";
          $service_sport2->afternoon_to = "17:00:00";
          $service_sport2->evening_from = "18:00:00";
          $service_sport2->evening_to = "23:00:00";
          $service_sport2->save();
        }
//------------------- END SET DEFAULT BRANCH SETTING -------------------------

      public static function getUserBranches($user)
      {
          $branches = [];
            foreach ($user->members as $member)
            {
                foreach ($member->branch->where('isActive',1) as $member_branch)
                {
                    $branches[] = $member_branch;
                }
            }
          return $branches;
      }

      public static function getRemainingSports($branch)
      {
        $sports = Sport::all();
        $selected_sports = [];
        foreach ($branch->services->where('short_name','court_booking')->first()->serviceSport as $branch_service_sport) {
          array_push($selected_sports,$branch_service_sport->sport_id);
        }
        $sports_filtered = $sports->whereNotIn('id', $selected_sports);

        return $sports_filtered;
      }




      //-------------------------------------

        public static function calculateMonthBookings($month,$year,$branch_id)
        {

          date_default_timezone_set('Africa/Cairo');
          $branch = Branch::where('id',$branch_id)->first();
          $branch_name = $branch->name ;
          $club_name = $branch->club->name;
          // QUERIES
         $online_bookings = Booking::where('branch_id',$branch_id)
           ->whereMonth('day', $month)
           ->whereYear('day', $year)
           ->where('booking_type','cc')
           ->where('confirmed',1)
           ->get();
         $total_refund = Booking::where('branch_id',$branch_id)
          ->whereMonth('day', $month)
          ->whereYear('day', $year)
          ->where('booking_type','cc')
          ->where('status','refunded')
          ->sum('total_price');
         $cash_bookings = Booking::where('branch_id',$branch_id)
          ->whereMonth('day', $month)
          ->whereYear('day', $year)
          ->where('booking_type','cash')
          ->get();
         // ALL NEEDED CALCULATIONS
         $online_bookings_total = $online_bookings->sum('total_price');
         $cash_bookings_total = $cash_bookings->sum('total_price');
         $bank_fees = ( 3 / 100) * $online_bookings_total;
         $countOnlineTotal = count($online_bookings);
         $countCashTotal = count($cash_bookings);
         $countAllTotal = $countOnlineTotal + $countCashTotal;
         $final_online_total = $online_bookings_total - ( $bank_fees + $total_refund);
         // CREATING COLLECTION
          $collection = collect([
              'branch_name' => $branch_name,
              'club_name' => $club_name,
              'bank_fees' => $bank_fees,
              'total_refund' => $total_refund,
              'online_bookings_total' => $online_bookings_total,
              'final_online_total' => $final_online_total,
              'cash_bookings_total' => $cash_bookings_total,
              'cash_online_total' => $cash_bookings_total + $online_bookings_total,
              'countOnlineTotal' => $countOnlineTotal,
              'countCashTotal' => $countCashTotal,
              'countAllTotal' => $countAllTotal,
              'month' => $month,
              'year' => $year,
          ]);
          return $collection;
        }


        public static function getPreviousMonths($given_month)
        {
          $months = ["01","02","03","04","05","06","07","08","09","10","11","12"];
          $previousMonths = [];
           if (in_array($given_month, $months))
           {
             $key = array_search($given_month, $months);
             for( $i=($key-1); $i>=0; $i--)
             // to get the current month too
             //for( $i=$key; $i>=0; $i--)
             {
               array_push($previousMonths,$months[$i]);
             }
           }
           return $previousMonths;
        }

        public static function getMonthString($m) {
        switch ($m) {
            case "01":
                $m = "January";
                break;
            case "02":
                $m = "Febuary";
                break;
            case "03":
                $m = "March";
                break;
            case "04":
                $m = "April";
                break;
            case "05":
                $m = "May";
                break;
            case "06":
                $m = "June";
                break;
            case "07":
                $m = "July";
                break;
            case "08":
                $m = "August";
                break;
            case "09":
                $m = "September";
                break;
            case "10":
                $m = "October";
                break;
            case "11":
                $m = "November";
                break;
            case "12":
                $m = "December";
                break;
            default:
                $m = false;
                break;
        }
        return $m;
        }
}
