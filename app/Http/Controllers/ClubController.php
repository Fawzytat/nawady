<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Court;
use App\Service;
use App\ServiceSport;
use App\Branch;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;
use Illuminate\Http\Request;

class ClubController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addClub(Request $request)
    {
      // if (!$user = JWTAuth::parseToken()->authenticate()) {
      //      return response()->json(['error' => 'Authentication failed'], 404);
      //   }

      // $validator = Validator::make($request->toArray(), [
      //   'club_name' => 'required|string',
      //   'branch_name' => 'required|string',
      //   'club_status' => 'required',
      //   'branch_status' => 'required'
      // ]);
      //
      //  if ($validator->fails()) {
      //
      //    return response()->json(['errors'=>$validator->errors()],401);
      //  }

       if ($request->file('club_avatar')) {//<============if file attached
         $upload_file = Helpers::upload($request->file('club_avatar'),'avatars');
       }
       //----------- (1) CREATING NEW CLUB  ---------------
       $club = new Club;
       $club->name = $request->club_name;
       if($request->club_status == "true")
       {
          $club->isActive = 1;
       }
       if($request->club_status == "false")
       {
         $club->isActive = 0;
       }
       $club->logoUrl = isset($upload_file)?$upload_file->pathToSave:"none";
       $club->save();
       //-------------------------

       //----------- (2) ATTACHING BRANCH TO THIS CLUB  ---------------
       $branch = new Branch;
       $branch->name = $request->branch_name;
       if($request->branch_status == "true")
       {
          $branch->isActive = 1;
       }
       if($request->branch_status == "false")
       {
         $branch->isActive = 0;
       }
       $branch->type = "primary";
       $club->branches()->save($branch);
       //-------------------------


       //----------- (3) SET DEFAULT BRANCH SETTING  ---------------

       Helpers::setDefaultBranchSetting($branch);

       //-------------------------



       if ($request['isWeb'])
       {

         $tabName = "branchinfo";
         return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
           'status' => 'BRANCH updated',
           'tabName' => $tabName
           ]);
       }

       return response()->json([
           'club' => $club,
           'branch' => $branch,
           'message' => 'Club added succefully '
       ], 200);
    }


    public function switchClubStatus(Request $request)
    {
      $club = Club::where('id', $request->club_id)->first();
      if (!$club)
      {
        return response()->json([
            'message' => 'Club Not found '
        ], 401);
      }

      $club->toggleActive()->save();

      return response()->json([
          'club_status' => $club->isActive,
          'message' => 'Club status switched succefully '
      ], 200);

    }


    //------------------- CLUBS MODEL CRUD  -------------------------
    /**
     * GET ALL CLUBS
     *
     *
     */
     public function getClub()
     {
       $clubs = Club::with('branches')->get();

       $response = [
         'clubs' => $clubs
       ];
       return response()->json($response , 200);
     }

     /**
      * GET ONE Club
      *
      *
      */
      public function getOneClub($id)
      {
        $club = Club::where('id',$id);
        $response = [
          'club' => $club
        ];
        return response()->json($response , 200);
      }


    /**
     * Club UPDATE
     *
     *
     */
     public function updateClub(Request $request, $id)
       {
           $club = Club::find($id);
           if(!$club)
           {
             return response()->json(['message' => 'club not found '] , 404);
           }

           if ($request->file('avatar')) {//<============if file attached
             $upload_file = Helpers::upload($request->file('avatar'),'avatars');
             $club->logoUrl = $upload_file->pathToSave ;
           }

           $club->name = $request->name;
           if($request->club_status == "true")
           {
              $club->isActive = 1;
           }
           if($request->club_status == "false")
           {
             $club->isActive = 0;
           }
           //$club->email = $request->email;



           $club->save();

            if ($request['isWeb'])
            {
              return redirect('/club-list')->with('status','branch updated');
            }


           return response()->json([
             'club' => $club,
             'message' => 'club Updated succefully '] , 200);
       }
     /**
      * CLUBS DELETE
      *
      *
      */

      public function deleteClub($id)
      {
          $club = Club::find($id);
          $club->delete();
          return response()->json(['Message' => 'club deleted succefully'] , 200);
      }

    //-------------------END OF Club MODEL CRUD  -------------------------
}
