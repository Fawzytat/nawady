<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminCreated;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Court;
use App\Trainer;
use App\BookedTrainer;
use App\Service;
use App\ServiceSport;
use App\Branch;
use App\Application;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Omnipay\Omnipay;
use Illuminate\Support\Facades\Event;
use App\Events\NewBooking;
use App\Notifications\NewBookingAdmin;
use App\Notifications\NewBookingSAdmin;
use App\Notifications\BookingCanceled;
use App\Notifications\BookingRefunded;
class BookingController extends Controller
{


    public function getCalenderData(Request $request)
    {
        $courts_array = [];
        $dimmed_slots = [];
        $booked_slots = [];
        $current_user_bookings = 0;
        $user_verified = "not_verified";
        $user_auth = "guest";
        $service_sport = ServiceSport::where('id',$request->service_sport_id)->first();
        $courts = ServiceSport::find($service_sport->id)->court->where('isActive',1);
        $courts_count = count($service_sport->court->where('isActive',1));
        $max_book_per_day = $service_sport->service->max_book_per_day;
        $max_slots_per_booking = $service_sport->service->max_duration_per_booking;
        $duration_allowed = ( $service_sport->service->max_duration_per_booking * $service_sport->slot_duration );
        date_default_timezone_set('Africa/Cairo');
        //--- Substracting last slot if exceeded evening_to time  ---
        $evening_to = $service_sport->evening_to;
        $diffrence_in_min = (strtotime($service_sport->evening_to) - strtotime($service_sport->morning_from)) / 60;
        $extra_min = $diffrence_in_min % $service_sport->slot_duration;
        if($extra_min != 0)
        {
          $evening_to = date('H:i:s', strtotime($service_sport->evening_to) - 60 * $extra_min);
        }
        //--- End of  Substracting last slot if exceeded evening_to time  ---
        if(Auth::user()) { $user_auth = "auth"; }
        if( Auth::user() && count( Auth::user()->members))
        {
           $branches = Helpers::getUserBranches(Auth::user());
           if(count( collect($branches)) && count( collect($branches)->where('id', $request->branch_id ) ))
           {
             $user_verified = "verified";
           }

           $current_user_bookings = count(Booking::where('user_id',Auth::user()->id)
           ->where('service_sport_id',$request->service_sport_id)
           ->where('confirmed',1)
           ->where('status','!=','canceled')
           ->where('day',$request->day_date)->get());
        }

        foreach ($courts as $key => $court)
        {
          $courts_array[] = (object) array('id' => $court->id , 'title' => $court->name);
          $dimmed_slots[] = (object) array('id' => $key , 'resourceId' => $court->id ,
                                           //'start' => date("H:i:s",strtotime($service_sport->morning_to)+($service_sport->slot_duration * 60)),
                                           'start' => $service_sport->morning_to,
                                           'end' => $service_sport->afternoon_from,
                                           'rendering' => 'background',
                                           'color' => '#797979');
          $dimmed_slots[] = (object) array('id' => $key , 'resourceId' => $court->id ,
                                          //'start' => date("H:i:s",strtotime($service_sport->afternoon_to)+($service_sport->slot_duration * 60)) ,
                                          'start' => $service_sport->afternoon_to,
                                          'end' => $service_sport->evening_from,
                                          'rendering' => 'background',
                                          'color' => '#797979');

          if ( date('Y-m-d',strtotime($request->day_date)) >= $court->closing_from &&
              date('Y-m-d',strtotime($request->day_date)) <= $court->closing_to )
          {
            $dimmed_slots[] = (object) array('id' => $key , 'resourceId' => $court->id ,
                                            'start' => $service_sport->morning_from ,
                                            'end' => $evening_to,
                                            'rendering' => 'background',
                                            'color' => '#797979');
          }

          if( date('Y-m-d',strtotime($request->day_date)) == date('Y-m-d'))
          {
            $dimmed_slots[] = (object) array('id' => $key , 'resourceId' => $court->id ,
                                            'start' => $service_sport->morning_from ,
                                            'end' => date('H:i:s'),
                                            'rendering' => 'background',
                                            'color' => '#797979');
          }

        }
        $related_bookings = Booking::where("service_sport_id",$request->service_sport_id)
                                    ->where("day",$request->day_date)
                                    ->where("confirmed",1)
                                    ->where('status','!=','canceled')
                                    ->where('status','!=','refunded')
                                    ->get();

        foreach ($related_bookings as $key => $booking)
        {
          $booked_slots[] = (object) array('id' => "booked".$key , 'resourceId' => $booking->court_id ,
                                          'start' => $booking->from,
                                          'end' => $booking->to,
                                          'rendering' => 'background',
                                          'color' => '#832B2B');
        }


        return response()->json([
            'myMinTime' => $service_sport->morning_from,
            'myMaxTime' => $evening_to,
            'scrollTime' => '08:00',
            'user_auth' => "$user_auth",
            'user_verified' => $user_verified,
            'max_book_per_day' => $max_book_per_day,
            'current_user_bookings' => $current_user_bookings,
            'max_slots_per_booking' => $max_slots_per_booking,
            'slotDuration' => $service_sport->slot_duration_h,
            'slotDuration_m' => $service_sport->slot_duration,
            'slot_price' => $service_sport->slot_price,
            'duration_allowed' => $duration_allowed,
            'slotLabelInterval' => $service_sport->slot_duration_h,
            'courts' => json_encode($courts_array),
            'courts_count' => $courts_count,
            'dimmed_slots' => json_encode($dimmed_slots),
            'booked_slots' => json_encode($booked_slots),
            'calender_title' => ''.strtoupper($service_sport->sport->name).' AVAILABILITY CALENDAR',
            'price_duration' => '
              Slot Price '.$service_sport->slot_price.'  EGP
              <span class="duration">
              Duration: '.$service_sport->slot_duration.' Minutes
              </span>
            '
        ], 200);

    }


    public function setBooking(Request $request)
    {
      $auth_user = Auth::user();
      $booking = new Booking;
      $booking->user_id = Auth::user()->id;
      $booking->member_id = Auth::user()->members->where('branch_id',$request->branch_id)->first()->id;
      $booking->branch_id = $request->branch_id;
      $booking->court_id = $request->court_id;
      $booking->service_sport_id = $request->service_sport_id;
      $booking->total_price = $request->booking_total;
      $booking->day = $request->date;
      $booking->from = $request->time_from;
      $booking->to = $request->time_to;
      $booking->transaction_token = str_random(60);
      $booking->total_time = $request->total_time;
      $booking->isPaid = 0;
      if($request->trainer_id && $request->trainer_id != '')
      {
        $booking->withTrainer = 1;
        $booking->trainer_id = $request->trainer_id;
      }
      if($request->booking_type == "cash")
      {
        $booking->booking_type = "cash";
        $booking->confirmed = 1;
        $booking->status = "upcoming";
        $booking->payment_code = Helpers::generateCode("payment_code");
      }
      if($request->booking_type == "cc")
      {
        $booking->booking_type = "cc";
      }
      $booking->booking_id = Helpers::generateCode("booking_code");
      $booking->save();

      if($request->trainer_id && $request->trainer_id != '')
      {
      $booked_trainer = new BookedTrainer;
      $booked_trainer->trainer_id = $request->trainer_id;
      $booked_trainer->service_sport_id = $request->service_sport_id;
      $booked_trainer->day = $request->date;
      $booked_trainer->from = $request->time_from;
      $booked_trainer->to = $request->time_to;
      $booked_trainer->booking_id = $booking->id;
      $booked_trainer->save();
      }

      $branch = Branch::where("id",$booking->branch_id)->first();
      $club_admin = User::where("id",$branch->admin_id)->first();
      $system_admin = User::with(['roles' => function($q){
                                $q->where('name', 'sys_admin');
                            }])->first();
      if($request->booking_type == "cash")
      {
      $club_admin->notify(new NewBookingAdmin($booking));
      $auth_user->notify(new NewBookingSAdmin($booking));
      }
      if($request->booking_type == "cc")
      {
        $url = $this->sendPaymentRequest($booking->id,round(floatval($booking->total_price)),$booking->transaction_token);
        if($request->ajax()) {
          return response()->json([
              'url' => $url,
          ], 200);
        }
      }
      else
      {
        if($request->ajax()) {
          return response()->json([
              'booking_id' => $booking->booking_id,
          ], 200);
        }
      }



    }

    public function sendPaymentRequest($id,$total_price,$transaction_token)
    {
      $gateway = Omnipay::create('Migs_ThreeParty');
      $gateway->setMerchantId(config('sysconfig.migs.migs_config_egp.merchand_id'));
      $gateway->setMerchantAccessCode(config('sysconfig.migs.migs_config_egp.access_code'));
      $gateway->setSecureHash(config('sysconfig.migs.migs_config_egp.hash_secret'));
      $response = $gateway->purchase(['amount' => $total_price,
                                      'description'=>Auth::user()->email,
                                      'currency' => 'EGP',
                                      'returnUrl'=>route('requestReturn',
                                                        ['booking_id'=>$id,
                                                         'transaction_token'=>$transaction_token
                                                        ]),
                                      'transactionId'=>$id
                                    ])->send();
      if ($response->isRedirect()){
        //$response->redirect();
         $url = $response->getRedirectUrl();
         Log::debug($url);
         return $url;
       }
      else { Log::debug($response->getMessage()); }
    }


    public function requestReturn(Request $request)
    {
     //Log::debug($request);
     if ($request->booking_id&&$request->transaction_token) {
       $booking = Booking::where([
         'id'=>$request->booking_id,
         'transaction_token'=>$request->transaction_token
       ])->first();
       $booking->isPaid = 1;
       $booking->confirmed = 1;
       $booking->status = "upcoming";
       $booking->save();
       $branch = Branch::where("id",$booking->branch_id)->first();
       $club_admin = User::where("id",$branch->admin_id)->first();
       $system_admin = User::with(['roles' => function($q){
                                 $q->where('name', 'sys_admin');
                             }])->first();
       $club_admin->notify(new NewBookingAdmin($booking));
       $auth_user = Auth::user();
       $auth_user->notify(new NewBookingSAdmin($booking));


       $payment = new Payment;
       $payment->booking_id = $booking->id;
       $payment->migs_ReceiptNo = $request->vpc_ReceiptNo;
       $payment->migs_CardType = $request->vpc_Card;
       $payment->migs_CardNum = $request->vpc_CardNum;
       $payment->migs_TransactionNo = $request->vpc_TransactionNo;
       $payment->Type = "cc";
       $payment->status = $request->vpc_Message; //approved
       $payment->save();

     }else{
       if($request['is_api']){
         $data['status'] = false;
         $data['message']='record not found';
         return response()->json($data,400);
       } else{
         abort(404);
       }
     }
     return redirect('/booking/success/'.$booking->booking_id.'');
    }




    public function getAvailableTrainers(Request $request)
    {

      $day = $request->day;
      $date = $request->date;
      $from = $request->time_from;
      $to = $request->time_to;
      $available_trainers = [];
      $not_available_trainers = [];

      $related_booked_trainers = BookedTrainer::where('day',$date)
      ->where('service_sport_id',$request->service_sport_id)
      ->where('from','<=',$from)
      ->where('to','>=',$to)
      ->get();
      foreach ($related_booked_trainers as $related_booked_trainer ) {
        array_push($not_available_trainers, $related_booked_trainer->trainer_id);
      }

      $trainers = Trainer::where('service_sport_id',$request->service_sport_id)
      ->where('isActive',1)
      ->whereNotIn('id',$not_available_trainers)
      ->get();

      foreach ($trainers as $trainer)
      {
        if($from > $trainer->daily_from && $to < $trainer->daily_to)
        {
          $trainer_days_off = collect(json_decode($trainer->days_off));
          if (!($trainer_days_off->contains($day)))
          {
            array_push($available_trainers, $trainer);
          }
        }
      }
      if($request->ajax()) {
          return view('includes.trainers-list', [
            'trainers' => collect($available_trainers)
            ])->render();
      }
    }

    public function getBranchCourts(Request $request)
    {
      $courts_list ='<option value="" selected>Select Court…</option>';
      $service = Service::where("branch_id",$request->branch_id)
      ->where("short_name","court_booking")->first();
      if ($service){
        $courts = $service->courts;
        foreach ($courts as $court) {
          $courts_list.='<option value="'.$court->id.'">'.$court->name.'</option>';
        }
      }
      return response()->json([
          'courts_list' => $courts_list
      ], 200);
    }

    public function getAllRelatedBookings($request)
    {
      $bookings = Booking::where('confirmed',1)
      ->where('user_id',$request->user_id)
      ->where(function ($query) use ($request) {
        if($request->branch_id != '')
        {
         $query->where('branch_id', '=', $request->branch_id);
        }
         if($request->court_id != '')
         {
          $query->where('court_id', '=', $request->court_id);
         }
         if($request->status != '')
         {
          $query->where('status', '=', $request->status);
         }
      })->orderBy('day', 'DESC')->orderBy('from', 'DESC');
      return $bookings;

    }

    public function buildUserBookingsTable(Request $request)
    {
      $bookings = $this->getAllRelatedBookings($request);

      return Datatables::of($bookings)
      ->addColumn('booking_id', function(Booking $booking) {
        return '<a data-id="'.$booking->id.'" onclick="showReceipt(this)" class="text-success anchor_link">#'.$booking->booking_id.'</a>';
      })->addColumn('court', function(Booking $booking) {
        $court = Court::where('id',$booking->court_id)->first();
        return '<h6>'.$court->name.'</h6>';
      })->addColumn('club', function(Booking $booking) {
        $branch = Branch::where('id',$booking->branch_id)->first();
        $club_name = $branch->club->name;
        return '<h6>'.$club_name." - " .$branch->name.'</h6>';
      })->addColumn('date', function(Booking $booking) {
        return '<h6>'.$booking->day.'</h6>';
      })->addColumn('time', function(Booking $booking) {
        return '<h6>'.$booking->total_time.'</h6>';
      })->addColumn('trainer', function(Booking $booking) {
        if($booking->trainer_id)
        {$trainer = Trainer::where('id',$booking->trainer_id)->first();
        return '<h6>'.$trainer->name.'</h6>';}
        else{return '<h6>None</h6>';}
      })->addColumn('status', function(Booking $booking) {
        return '<h6>'.$booking->status.'</h6>';
      })->addColumn('total', function(Booking $booking) {
        return '<h6>'.$booking->total_price.' EGP</h6>';
      })->addColumn('action', function(Booking $booking) {
        if($booking->status != "done" && $booking->status != "canceled")
        {
          date_default_timezone_set('Africa/Cairo');
          $cancel_limit_in_hours = Service::where('branch_id',$booking->branch_id)->first()->cancel_limit;
          $max_cancel_date = date('Y-m-d H:i:s', strtotime($booking->day.''.$booking->from) - 60 * 60 * $cancel_limit_in_hours);
          if( date('Y-m-d H:i:s') < $max_cancel_date)
          {
            return '<a data-id="'.$booking->id.'" onclick="cancelBooking(this)" class="text-primary anchor_link"> Cancel </a>';
          }
          else
          {
            // return '<h6>'.date('Y-m-d H:i:s').'</h6>';
            //'.date('Y-m-d H:i:s').' Cancel '.$max_cancel_date.'
            return '<a> Cancel </a>';
          }
        }
        else {
          return ' - ';
        }
      })->rawColumns(['booking_id', 'club', 'court', 'date', 'time', 'trainer', 'status', 'total', 'action'])
      ->make(true);
    }

    public function getBookingReceipt(Request $request)
    {
      $booking = Booking::where('id',$request->booking_id)->first();
      $branch = Branch::where("id",$booking->branch_id)->first();
      $branch_name = $branch->name;
      $club = Club::where("id",$branch->club->id)->first();
      $club_name = Club::where("id",$branch->club->id)->first()->name;
      $court_name = Court::where("id",$booking->court_id)->first()->name;
      if($booking->withTrainer)
      {
        $trainer_name = Trainer::where("id",$booking->trainer_id)->first()->name;
      }
      if($request->ajax()) {
          return view('includes.booking-receipt', [
            'booking' => $booking,
            'club' => $club,
            'club_name' => $club_name,
            'branch_name' => $branch_name,
            'court_name' => $court_name,
            'trainer_name' => isset($trainer_name)?$trainer_name:"none"
            ])->render();
      }
    }

    public function cancelUserBooking(Request $request)
    {
      $booking = Booking::find($request->booking_id);
      $booking->status = "canceled";
      $booking->save();
      if($booking->booked_trainers)
      {
      $booking->booked_trainers->delete();
      }

      $branch = Branch::where("id",$booking->branch_id)->first();
      $club_admin = User::where("id",$branch->admin_id)->first();
      $system_admin = User::with(['roles' => function($q){
                                $q->where('name', 'sys_admin');
                            }])->first();
      $club_admin->notify(new BookingCanceled($booking));
      $system_admin->notify(new BookingCanceled($booking));

      return redirect('/myaccount');
    }

    public function feedEnterCashModal(Request $request)
    {
      $booking = Booking::where('id',$request->booking_id)->first();
      $member = Member::where("id",$booking->member_id)->first();
      if($request->ajax()) {
          return view('includes.cash-payment-data', [
            'booking' => $booking,
            'membershipName' =>$member->membershipName
            ])->render();
      }
    }

    public function submitCashPayment(Request $request)
    {
      $booking = Booking::where('id',$request->booking_id)->first();
      if($booking->payment_code == $request->payment_code)
      {
        $booking->status = "done";
        $booking->isPaid = 1;
        $booking->save();
      }
      else
      {
        return response()->json(['errors'=>['payment_code' =>'Wrong Payment Code!']],401);
      }
    }

    public function refundThisBooking(Request $request)
    {
      $booking = Booking::where('id',$request->booking_id)->first();
      $booking->status = "refunded";
      $booking->save();


      $Booking_member = User::where("id",$booking->user_id)->first();
      $Booking_member->notify(new BookingRefunded($booking));
      return redirect()->back();
    }

    public function destroy(Booking $booking)
    {
        //
    }
}
