<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminCreated;
use App\Notifications\MembershipReport;

use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Court;
use App\Service;
use App\ServiceSport;
use App\Branch;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class ApplicationController extends Controller
{

  public function reportRequestToAdmin(Request $request)
  {
    Log::debug($request);
    $application = new Application;
    $application->user_id = Auth::user()->id;
    $application->membershipId = $request->member_id;
    $application->reason = $request->reason;
    $application->membershipName = $request->member_name;
    $application->branch_id = $request->branch_id;
    $application->status = "pending";
    $application->save();

    $branch = Branch::where('id', $request->branch_id)->first();
    $admin = User::where('id', $branch->admin_id)->first();
    if(isset($admin))
    {
        $admin->notify(new MembershipReport($request->member_id, $request->member_name));
    }

    return response()->json([
      'Message' => 'Data sent to club admin.'
      ] , 201);


  }


  public function cancelApplication(Request $request)
  {
    $application = Application::where('id',$request->req_id)->first();
    $application->delete();
    return response()->json([
      'Message' => 'Request removed succefully.'
      ] , 201);
  }


  public function confirmApplication(Request $request)
  {

   $application = Application::where('id',$request->req_id)->first();
   if(!$application)
   {
     Log::info("case1");
     return response()->json([
       'Message' => 'Request not found , please refresh the page and try again '
       ] , 401);
   }
   $user = User::where('id',$application->user_id)->first();
   $branch = Branch::where('id',$application->branch_id)->first();
   $membership_id = $application->membershipId;

   $exists = DB::table('members')->where('membershipId',$membership_id)->where('branch_id',$branch->id)->count() > 0;
   if($exists)
   {
     Log::info("case2");
         $member = Member::where('membershipId',$membership_id)->where('branch_id',$branch->id)->first();
         if ($member->user_id)
         {
           return response()->json([
             'Message' => 'This Membership id is assigned to a user'
             ] , 401);
         }
         else
         {
           Log::info("case3");
             //---------- (4) ASSIGN MEMBER TO BRANCH ----------------
             $member->branch()->syncWithoutDetaching($branch->id);
             $member->membershipName = $application->membershipName;
             $member->save();
             $application->delete();
             return response()->json([
               'Message' => 'Member Assigned succefully.'
               ] , 201);
             //--------------------------
         }
   }
   else
   {
     Log::info("case4");
     $member = new Member;
     $member->membershipName = $application->membershipName;
     $member->membershipId = $application->membershipId;
     $member->branch_id = $application->branch_id;
     $member->user_id = $application->user_id;
     $member->isActive = 1;
     $member->save();

     $member->branch()->syncWithoutDetaching($branch->id);
     $application->delete();
     return response()->json([
       'Message' => 'Member Added Assigned succefully.'
       ] , 201);
   }

  }

}
