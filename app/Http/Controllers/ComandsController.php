<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdExpiry;
use App\Notifications\BranchAdExpiry;
use App\Adv;
use App\BranchAdv;
use App\User;
use App\Booking;

class ComandsController extends Controller
{
    public function runQueueWorker()
    {
      Artisan::call('queue:work', [
        '--tries' => 2
      ]);
    }

    public function advertReminder()
    {
      date_default_timezone_set('Africa/Cairo');
      $advs = Adv::all();
      $branch_advs = BranchAdv::all();
      $system_admin = User::with(['roles' => function($q){
                                $q->where('name', 'sys_admin');
                            }])->first();
      $date = date('Y-m-d');
      foreach ($advs as $adv)
      {
        $deadline = date( 'Y-m-d' ,strtotime ('-7 day' , strtotime ( $adv->enddate )))  ;
        //Log::debug($deadline);
        if ( $date > $deadline && $date < date( 'Y-m-d' ,strtotime($adv->enddate)))
        {
          $system_admin->notify(new AdExpiry($adv));
        }
      }

      foreach ($branch_advs as $branch_adv)
      {
        $deadline = date( 'Y-m-d' ,strtotime ('-7 day' , strtotime ( $branch_adv->enddate )))  ;
        if ( $date > $deadline && $date < date( 'Y-m-d' ,strtotime($branch_adv->enddate)))
        {
          $system_admin->notify(new BranchAdExpiry($branch_adv));
        }
      }

    }

    public function CheckBookingStatus()
    {
      date_default_timezone_set('Africa/Cairo');
      $date = date('Y-m-d');
      $bookings = Booking::all();
      foreach ($bookings as $booking) {
        if($booking->booking_type == "cc")
        {
          if($date > date( 'Y-m-d' , strtotime( $booking->day)))
          {
            $booking->status = "done";
            $booking->save();
          }
        }
        if($booking->booking_type == "cash")
        {
          if($date > date( 'Y-m-d' , strtotime( $booking->day)))
          {
            if($booking->status == "upcoming" && $booking->isPaid == 0)
            {
              $booking->status = "not done";
              $booking->save();
            }
          }
        }
      }

    }


    public function logMyName()
    {
      Log::info('My name is tat');
    }
}
