<?php

namespace App\Http\Controllers;

use App\BranchAdv;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Role;
use App\Branch;
use App\Permission;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;
use DateTime;

class BranchAdvController extends Controller
{
  /**
   * GET ALL Advs
   *
   *
   */
   public function getAdv()
   {
     $branchAdv = BranchAdv::with('branches')->get();

     $response = [
       'Advs' => $branchAdv
     ];
     return response()->json($response , 200);
   }

  /**
   * GET ONE Club
   *
   *
   */
   public function getOneAdv($id)
   {
     $branchAdv = BranchAdv::where('id',$id);
     $response = [
       'adv' => $branchAdv
     ];
     return response()->json($response , 200);
   }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if ($request['isWeb'])
      {
          $validator = Validator::make($request->toArray(), [
            'link' => 'url',
         ]);

          if ($validator->fails()) {
            $tabName = "branchinfo";
            return redirect('/branch/'.$request->branch_id.'/'.$tabName)->with([
              'tabName' => $tabName
              ])->withErrors($validator);
          }
      }
      else
      {
          $validator = Validator::make($request->toArray(), [
            'link' => 'url',
         ]);

          if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()],401);
          }
      }


      $branch = Branch::find($request->branch_id);
      if(!$branch)
      {
        return response()->json(['message' => 'branch not found '] , 404);
      }

      $last_adv_pos = BranchAdv::max('position');

       $branchAdv = new BranchAdv;
       $branchAdv->branch_id = $branch->id;
       if ($request->file('advPic')) {//<============if file attached
         $upload_file = Helpers::upload($request->file('advPic'),'advs');
         $branchAdv->url = isset($upload_file)?$upload_file->pathToSave:"none";
       }

       $branchAdv->link = $request->link;
       $branchAdv->position = ($last_adv_pos + 1) ;

       if ($request->has('dateFrom'))
       {
         //$start_date = date('Y-m-d',strtotime($request->dateFrom));
         $branchAdv->startdate =  $request->dateFrom;
       }
       if($request->has('dateTo'))
       {
          $branchAdv->enddate = $request->dateTo;
       }
       if($request->adv_status == "true")
       {
          $branchAdv->isActive = 1;
       }
       if($request->adv_status == "false")
       {
         $branchAdv->isActive = 0;
       }

       $branchAdv->save();
       $branch = $branchAdv->branch();
       if ($request['isWeb'])
       {
         $tabName = "branchinfo";
         return redirect('/branch/'.$branchAdv->branch->id.'/'.$tabName)->with([
           'status' => 'branch updated',
           'branch' => $branch,
           'tabName' => $tabName
           ]);
       }
       return response()->json([
           'Adv' => $branchAdv,
           'message' => 'Adv added succefully '
       ], 200);
    }



    public function switchAdvStatus(Request $request)
    {
      $branchAdv = BranchAdv::where('id', $request->adv_id)->first();
      if (!$branchAdv)
      {
        return response()->json([
            'message' => 'Adv Not found '
        ], 401);
      }

      $branchAdv->toggleActive()->save();

      return response()->json([
          'Adv_status' => $branchAdv->isActive,
          'message' => 'Adv status switched succefully '
      ], 200);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BranchAdv  $branchAdv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $branchAdv = BranchAdv::find($id);
      if ($request['isWeb'])
      {
          $validator = Validator::make($request->toArray(), [
            'link'.$id => 'url',
         ]);

          if ($validator->fails()) {
            $tabName = "branchinfo";
            return redirect('/branch/'.$branchAdv->branch->id.'/'.$tabName)->with([
              'tabName' => $tabName
              ])->withErrors($validator);
          }
      }
      else
      {
          $validator = Validator::make($request->toArray(), [
            'link' => 'required|url',
         ]);

          if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()],401);
          }
      }




       if(!$branchAdv)
       {
         return response()->json(['message' => 'branch adv not found '] , 404);
       }

       $branchAdv->link = $request['link'.$id];

       if ($request->file('advPic')) {//<============if file attached
         $upload_file = Helpers::upload($request->file('advPic'),'advs');
         $branchAdv->url = isset($upload_file)?$upload_file->pathToSave:"none";
       }
       if($request->adv_status == "true")
       {
          $branchAdv->isActive = 1;
       }
       if($request->adv_status == "false")
       {
         $branchAdv->isActive = 0;
       }


       if ($request->has('dateFrom'))
       {
         $branchAdv->startdate =  $request->dateFrom;
       }
       if($request->has('dateTo'))
       {
          $branchAdv->enddate = $request->dateTo;
       }

       $branchAdv->save();
       $branch = $branchAdv->branch();
       if ($request['isWeb'])
       {
         $tabName = "branchinfo";
         return redirect('/branch/'.$branchAdv->branch->id.'/'.$tabName)->with([
           'status' => 'branch updated',
           'branch' => $branch,
           'tabName' => $tabName
           ]);
       }

       return response()->json([
         'adv' => $branchAdv,
         'message' => 'branch Updated succefully '
         ] , 200);
    }

    public function sortAdvs(Request $request)
    {

      $order = $request->only('data');
      $order_parsed = parse_str($order['data'],$str);
      $branch_adv = $str['branchAd'];
      $counter = 1;
      foreach ($branch_adv as $key => $adv_id) {
        $branchAdv = BranchAdv::find($adv_id);
        $branchAdv->position = $counter;
        $branchAdv->save();
        $counter++;
      }
      return response()->json(['Message' => 'success'] , 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adv  $adv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $branchAdv = BranchAdv::find($request->adv_id);
      $branchAdv->delete();

      $branch = $branchAdv->branch();
      if ($request['isWeb'])
      {
        $tabName = "branchinfo";
        return redirect('/branch/'.$branchAdv->branch->id.'/'.$tabName)->with([
          'status' => 'Advert deleted',
          'branch' => $branch,
          'tabName' => $tabName
          ]);
      }
      return response()->json(['Message' => 'Adv deleted succefully'] , 200);
    }
}
