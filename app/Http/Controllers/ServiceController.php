<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use App\Court;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Branch;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;


class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function addService(Request $request)
     {
       // All this function needs edit
       $validator = Validator::make($request->toArray(), [
         'name' => 'required|string',
         'status' => 'required',

       ]);

        if ($validator->fails()) {

          return response()->json(['errors'=>$validator->errors()],401);
        }


        $service = new service;
        $service->name = $request->service_name;
        $service->service_sport_id = $request->service_sport_id;
        $service->isActive = $request->service_status;
        $service->closing_from = $request->closing_from;
        $service->closing_to = $request->closing_to;
        $service->save();

        return response()->json([
            'service' => $service,
            'message' => 'service added succefully '
        ], 200);
     }


    public function switchServiceStatus(Request $request)
    {
      $service = service::where('id', $request->service_id)->first();
      if (!$service)
      {
        return response()->json([
            'message' => 'service Not found '
        ], 401);
      }

      $service->toggleActive()->save();

      return response()->json([
          'service_status' => $service->isActive,
          'message' => 'service status switched succefully '
      ], 200);

    }

    //------------------- serviceS MODEL CRUD  -------------------------
    /**
     * GET ALL serviceS
     *
     *
     */
     public function getservice()
     {
       $services = service::all();

       $response = [
         'services' => $services
       ];
       return response()->json($response , 200);
     }

     /**
      * GET ONE service
      *
      *
      */
      public function getOneservice($id)
      {
        $service = service::where('id',$id);
        $response = [
          'service' => $service
        ];
        return response()->json($response , 200);
      }


    /**
     * service UPDATE
     *
     *
     */
     public function updateService(Request $request, $id)
       {
         $service = service::find($id);
         $branch = Branch::where('id', $service->branch_id)->first();

         if(!$service)
         {
           return response()->json(['message' => 'service not found '] , 404);
         }
         else
         {
           $validator = Validator::make($request->toArray(), [
             'bookingPerDay' => 'required|numeric|min:1',
             'durationPerBooking' => 'required|numeric|min:1',
             'cancelationLimit' => 'required|numeric'
          ]);

           if ($validator->fails())
          {
             if ($request['isWeb'])
             {
               $tabName = "setting";
               return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                 'tabName' => $tabName
                 ])->withErrors($validator);
             }
             return response()->json(['errors'=>$validator->errors()],401);
           }
           else
           {
             $service->max_book_per_day = $request->bookingPerDay;
             $service->max_duration_per_booking = $request->durationPerBooking;
             if( !$request->cancelationLimit || $request->cancelationLimit == "" )
             {
                $service->cancel_limit = 0;
             }
             else
             {
                $service->cancel_limit = $request->cancelationLimit;
             }


             if($request->service_status == "true")
             {
                $service->isActive = 1;
             }
             if($request->service_status == "false")
             {
               $service->isActive = 0;
             }

             $service->save();

             if ($request['isWeb'])
             {
               $tabName = "setting";
               return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                 'status' => 'Service updated',
                 'tabName' => $tabName
                 ]);
             }

             return response()->json([
               'service' => $service,
               'message' => 'service Updated succefully '] , 200);
             }
           }






       }





     /**
      * service DELETE
      *
      *
      */

      public function deleteservice($id)
      {
          $service = service::find($id);
          $service->delete();
          return response()->json(['Message' => 'service deleted succefully'] , 200);
      }

    //-------------------END OF service MODEL CRUD  -------------------------
}
