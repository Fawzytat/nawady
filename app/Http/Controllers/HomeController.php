<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Branch;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Auth::user()->hasRole('sys_admin'))
      {
        return redirect('/club-list');
      }
      else if (Auth::user()->hasRole('club_admin'))
      {
        $branch = Branch::where('admin_id',Auth::user()->id)->first();
        return redirect('/branch/'.$branch->id.'/branchinfo'); //<== booking page by default
      }
      else if (Auth::user()->hasRole('member'))
      {
        return redirect()->back();
      }

    }

    public function adminIndex()
    {
        return view('admin.index');
    }
}
