<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use App\Utils\Json;
use App\User;
use App\Adv;
use Illuminate\Http\Request;
use App\Utils\Helpers;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    use Notifiable;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */


   public $sponser_logos;


    public function __construct()
    {
        $this->middleware('guest');
        $this->sponser_logos =  Adv::where('type','sponser_logo')
                              ->where('isActive',1)->orderBy('position')->get();
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email',[
          'logos' =>  $this->sponser_logos
        ]);
    }
          /**
       * Send a reset link to the given user.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      // public function getResetPassword(Request $request)
      // {
      //     //$this->validate($request, ['email' => 'required|email']);
      //     $validator = Validator::make($request->toArray(), [
      //       'email' => 'required|email'
      //     ]);
      //
      //      if ($validator->fails()) {
      //
      //        return response()->json(['errors'=>$validator->errors()],401);
      //      }
      //
      //         $user = User::where('email', $request->input('email'))->first();
      //
      //         if (!$user) {
      //             return response()->json(Json::response(null, trans('passwords.user')), 400);
      //         }
      //
      //         $token = $this->broker()->createToken($user);
      //         $newPassword = Helpers::generateRandomString(10);
      //         $user->password = bcrypt($newPassword);
      //         $user->save();
      //
      //         $user->notify(new ResetPasswordNotification($newPassword));
      //         // return response()->json(Json::response([
      //         //   'message' => 'Check your Email with the new password'
      //         // ]));
      //         return redirect('/')->with('status','Check your Email with the new password');
      //
      // }
}
