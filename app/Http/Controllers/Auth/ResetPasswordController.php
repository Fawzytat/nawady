<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\ResetHelper\Json;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Adv;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public $sponser_logos;


      public function __construct()
      {
          $this->middleware('guest');
          $this->sponser_logos =  Adv::where('type','sponser_logo')
                                ->where('isActive',1)->orderBy('position')->get();
      }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            [
              'token' => $token,
              'email' => $request->email,
              'logos' =>  $this->sponser_logos
             ]
        );
    }


}
