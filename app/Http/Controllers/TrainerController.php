<?php

namespace App\Http\Controllers;

use App\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Court;
use App\Branch;
use App\Permission;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;


class TrainerController extends Controller
{
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
   public function addTrainer(Request $request)
   {

     // $validator = Validator::make($request->toArray(), [
     //   'name' => 'required|string',
     //   'status' => 'required',
     //   'service_sport_id' => 'required'
     // ]);
     //
     //  if ($validator->fails()) {
     //
     //    return response()->json(['errors'=>$validator->errors()],401);
     //  }

      if ($request->file('avatar')) {//<============if file attached
        $upload_file = Helpers::upload($request->file('avatar'),'avatars');
      }

      $trainer = new Trainer;
      $trainer->name = $request->trainer_name;
      $trainer->service_sport_id = $request->service_sport_id;
      //$trainer->isActive = $request->trainer_status;
      if($request->trainer_status == "true")
      {
         $trainer->isActive = 1;
      }
      if($request->trainer_status == "false")
      {
        $trainer->isActive = 0;
      }
      $trainer->avatarURL = isset($upload_file)?$upload_file->pathToSave:"none";
      $trainer->email = $request->email;
      $trainer->phone = $request->phone;
      $trainer->price_per_session = $request->price_per_session;
      $trainer->daily_from = $request->coachWTF;
      $trainer->daily_to = $request->coachWTT;
      $trainer->days_off = json_encode($request->offDays);
      $trainer->save();

      $branch = $trainer->serviceSport->service->branch;

      if ($request['isWeb'])
      {
        $tabName = "setting";
        $subTabName = $trainer->serviceSport->id;
        return redirect('/branch/'.$branch->id.'/'.$tabName.'/'.$subTabName)->with([
          'status' => 'Service updated',
          'tabName' => $tabName
          ]);
      }


      return response()->json([
          'trainer' => $trainer,
          'message' => 'trainer added succefully '
      ], 200);
   }


  public function switchTrainerStatus(Request $request)
  {
    $trainer = Trainer::where('id', $request->trainer_id)->first();
    if (!$trainer)
    {
      return response()->json([
          'message' => 'trainer Not found '
      ], 401);
    }

    $trainer->toggleActive()->save();

    return response()->json([
        'trainer_status' => $trainer->isActive,
        'message' => 'trainer status switched succefully '
    ], 200);

  }

  //------------------- trainerS MODEL CRUD  -------------------------
  /**
   * GET ALL trainerS
   *
   *
   */
   public function getTrainer()
   {
     $trainers = Trainer::all();

     $response = [
       'trainers' => $trainers
     ];
     return response()->json($response , 200);
   }

   /**
    * GET ONE trainer
    *
    *
    */
    public function getOneTrainer($id)
    {
      $trainer = Trainer::where('id',$id);
      $response = [
        'trainer' => $trainer
      ];
      return response()->json($response , 200);
    }


  /**
   * trainer UPDATE
   *
   *
   */
   public function updateTrainer(Request $request, $id)
     {

       Log::debug($request);
       //  $validator = Validator::make($request->toArray(), [
       //    'name' => 'required|string',
       //    'status' => 'required',
       // ]);
       //
       //
       //  if ($validator->fails()) {
       //
       //    return response()->json(['errors'=>$validator->errors()],401);
       //  }



         $trainer = Trainer::find($id);
         $branch = $trainer->serviceSport->service->branch;

         if(!$trainer)
         {
           return response()->json(['message' => 'trainer not found '] , 404);
         }

         $trainer->name = $request->coachName;

         if($request->trainer_status == "true")
         {
            $trainer->isActive = 1;
         }
         if($request->trainer_status == "false")
         {
           $trainer->isActive = 0;
         }

         if ($request->file('avatar')) {//<============if file attached
           $upload_file = Helpers::upload($request->file('avatar'),'avatars');
         }
         if ($request->file('avatar')){$trainer->avatarURL = $upload_file->pathToSave;}



         $trainer->email = $request->coachEmail;
         $trainer->phone = $request->coachPhone;
         $trainer->price_per_session = $request->coachPrice;
         $trainer->daily_from = $request->coachWTF;
         $trainer->daily_to = $request->coachWTT;
         $trainer->days_off = json_encode($request->offDays);

         $trainer->save();

         if ($request['isWeb'])
         {
           $tabName = "setting";
           $subTabName = $trainer->serviceSport->id;
           return redirect('/branch/'.$branch->id.'/'.$tabName.'/'.$subTabName)->with([
             'status' => 'Service updated',
             'tabName' => $tabName
             ]);
         }

         return response()->json([
           'trainer' => $trainer,
           'message' => 'trainer Updated succefully '] , 200);
     }



   /**
    * trainer DELETE
    *
    *
    */

    public function deleteTrainer(Request $request)
    {
        $trainer = Trainer::find($request->trainer_id);
        $trainer->delete();

        $branch = $trainer->serviceSport->service->branch;
        if ($request['isWeb'])
        {
          $tabName = "setting";
          $subTabName = $trainer->serviceSport->sport->first()->id;
          return redirect('/branch/'.$trainer->serviceSport->service->branch->id.'/'.$tabName.'/'.$subTabName)->with([
            'status' => 'Trainer deleted',
            'branch' => $branch,
            'tabName' => $tabName
            ]);
        }

        return response()->json(['Message' => 'trainer deleted succefully'] , 200);
    }

  //-------------------END OF trainer MODEL CRUD  -------------------------
}
