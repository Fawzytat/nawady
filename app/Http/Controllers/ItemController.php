<?php

namespace App\Http\Controllers;

use App\Item;
use App\CheckedRecord;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Log;

use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\Process\Process as Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Artisan;
use App\Flag;
use Validator;
use Redirect;
use Config;
use Session;
use DB;


class ItemController extends Controller
{


	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function import(Request $request)
	{
			//ini_set('memory_limit', '-1');
			ini_set('memory_limit','512M');
			if($request->has('validate'))
				{
					if ($request->hasFile('import_file')) {
						$excel_file = $request->file('import_file');
					 $validator = Validator::make($request->all(), [
							 'import_file' => 'required'
					 ]);

					 $validator->after(function($validator) use ($excel_file) {
							 if ($excel_file->guessClientExtension()!=='xlsx') {
									 $validator->errors()->add('field', 'File type is invalid - only xlsx is allowed');
							 }
					 });

					 if ($validator->fails()) {
							 return back()->withErrors($validator);
					 }
					 return	$this->validateSheet($request,$excel_file);
					}

					else
					{
						 return back()->withErrors(['emptyfile'=>'Please Choose a file ']);
					}

        }


			if($request->has('import'))
				{
        return  $this->importSheet($request);
        }


	}

	public function importSheet($request)
	{
		Artisan::call('cache:clear');

		// $process = new Process('php ../artisan import:excelfile --branch='.$request->import_branch_id.' --patch='.$request->import_patch_id.'');
		// $process->start();
		//Artisan::call('import:excelfile');
		Artisan::call('import:excelfile', [
			'--branch' => $request->import_branch_id, '--patch' => $request->import_patch_id
		]);

		Session::flash('message', 'Hold on tight. Your file is being processed');
		//return redirect('/importExportpage');
		return redirect('/branch/'.$request->import_branch_id.'/members/sheet');
	}

	public function validateSheet($request,$excel_file)
	{
		try {
				$fname = md5(rand()) . '.xlsx';
				$full_path = Config::get('filesystems.disks.local.root');
				$excel_file->move( $full_path, $fname );
				$flag_table = Flag::firstOrNew(['file_name'=>$fname]);
				$flag_table->imported = 0; //file was not imported
				$flag_table->validated = 0; //file was not validated
				$flag_table->save();
		}catch(\Exception $e){
				return back()->withErrors($e->getMessage()); //don't use this in production ok ?
		}

		Artisan::call('cache:clear');

		try {
				Artisan::call('validate:excelfile');
				} catch (Exception $e) {
				Response::make($e->getMessage(), 500);
				}

		// $process = new Process('php ../artisan validate:excelfile');
		// $process->start();

		Session::flash('message', 'Hold on tight. Your file is being processed');
		return redirect('/branch/'.$request->import_branch_id.'/members/sheet');
		//working to this point
	}

	public function status(Request $request)
	{
			$flag_table = DB::table('flag')
											->orderBy('created_at', 'desc')
											->first();
			if(empty($flag_table)) {
					return response()->json(['msg' => 'done']); //nothing to do
			}
			if($flag_table->imported == 1) {
					return response()->json(['msg' => 'done_importing']);
			}

			if($flag_table->imported == 0 && $flag_table->validated == 1 && $flag_table->checked == 0 && $flag_table->import_started == 0  )
			{

						 $infected_records = CheckedRecord::where('patch',$flag_table->id)->where('isValid',0)->get();
						 if(count($infected_records) > 0)
						 {
							 $validation_status = "you have ".count($infected_records)." infected records";
							 $flagtable = Flag::where('id',$flag_table->id)->first();
							 $flagtable->checked = 1;
							 $flagtable->save();
							 return response()->json([
								 'msg' => 'done_validation',
								 'patch' => $flag_table->id,
								 'validation_status' => $validation_status,
								 'no_of_records' => count($infected_records)
							 ]);
						 }
						 else
						 {
							 $validation_status = "valid";


							 $flagtable = Flag::where('id',$flag_table->id)->first();
							 $flagtable->checked = 1;
							 $flagtable->save();
							 return response()->json([
								 'msg' => 'done_validation',
								 'patch' => $flag_table->id,
								 'validation_status' => $validation_status,
								 'no_of_records' => 0
							 ]);
						 }


			}

			else
			{
				if( $flag_table->import_started == 1)
				{
						if($flag_table->rows_validated == $flag_table->total_rows )
						{
							$status = ( $flag_table->rows_imported ? $flag_table->rows_imported : 0 ) . ' excel rows have been imported out of a total of ' . $flag_table->total_rows;
							return response()->json(['msg' => $status]);
						}

				}

				else
				{
						if($flag_table->checked == 1)
						{
							return response()->json(['msg' => 'done']);
						}
						else {
							$status = ( $flag_table->rows_validated ? $flag_table->rows_validated : 0 ) . ' excel rows have been validated out of a total of ' . $flag_table->total_rows;
							return response()->json(['msg' => $status]);
						}


				}

			}
	}


  /**
     * Return View file
     *
     * @var array
     */
	public function importExport()
	{
		return view('importExport');
	}


	/**
     * File Export Code
     *
     * @var array
     */
	public function downloadExcel(Request $request, $type)
	{
		$data = Item::get()->toArray();
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);
	}

	public function downloadErrorsSheet(Request $request ,$patch)
	{

		$data = CheckedRecord::where('patch',$patch)
		->where('isValid',0)
		->get(['position', 'membership_id', 'error'])->toArray();

		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download("xlsx");
	}


	/**
     * Import file into database Code
     *
     * @var array
     */
	public function importExcel(Request $request)
	{


		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			Log::debug($path);

			$data = Excel::load($path, function($reader) {

			})->get();


			if(!empty($data) && $data->count()){


				foreach ($data->toArray() as $key => $value)
				{
					if(!empty($value)){
						foreach ($value as $v) {
							$insert[] = [
                'title' => $v['title'],
                'description' => $v['description']
                          ];
						}
					}
				}


				if(!empty($insert)){
					Item::insert($insert);

					return back()->with('success','Insert Record successfully.');
				}


			}


		}


		return back()->with('error','Please Check your file, Something is wrong there.');
	}


}
