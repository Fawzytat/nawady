<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\ServiceSport;
use App\Court;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Branch;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;


class ServiceSportController extends Controller
{


  public function switchServiceStatus(Request $request)
  {
    $service = ServiceSport::where('id', $request->service_sport_id)->first();
    if (!$service)
    {
      return response()->json([
          'message' => 'service Not found '
      ], 401);
    }

    $service->toggleActive()->save();

    return response()->json([
        'service_status' => $service->isActive,
        'message' => 'service status switched succefully '
    ], 200);

  }


  public function updateService(Request $request, $id)
    {

        $service = ServiceSport::find($id);
        $branch = Branch::where('id', $service->service->branch_id)->first();
        if(!$service)
        {
          return response()->json(['message' => 'service sport not found '] , 404);
        }
        $service->slot_duration = $request->slotDuration;
        $slotDuration = date('H:i', mktime(0,$request->slotDuration));
        $service->slot_duration_h = $slotDuration;
        $service->slot_price = $request->priceSlot;
        $service->morning_from = $request->morningWTF;
        $service->morning_to = $request->morningWTT;
        $service->afternoon_from = $request->afternoonWTF;
        $service->afternoon_to = $request->afternoonWTT;
        $service->evening_from = $request->eveningWTF;
        $service->evening_to = $request->eveningWTT;

        $service->save();

        if ($request['isWeb'])
        {
          $tabName = "setting";
          $subTabName = $service->id;
          return redirect('/branch/'.$branch->id.'/'.$tabName.'/'.$subTabName)->with([
            'status' => 'Service Sport updated',
            'tabName' => $tabName
            ]);
        }

        return response()->json([
          'service' => $service,
          'message' => 'service Updated succefully '] , 200);
    }


    public function addServiceSport(Request $request)
    {
      $service_sport =  new ServiceSport;
      $service_sport->service_id = $request->service_id;
      $service_sport->sport_id = $request->sport_id;
      $service_sport->isActive = 1;
      $service_sport->slot_duration = 30;
      $service_sport->slot_duration_h = "00:30";
      $service_sport->slot_price = 100;
      $service_sport->morning_from = "08:00:00";
      $service_sport->morning_to = "11:00:00";
      $service_sport->afternoon_from = "12:00:00";
      $service_sport->afternoon_to = "17:00:00";
      $service_sport->evening_from = "18:00:00";
      $service_sport->evening_to = "23:00:00";
      $service_sport->save();
      return back();
    }


}
