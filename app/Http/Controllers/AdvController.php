<?php

namespace App\Http\Controllers;

use App\Adv;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;


class AdvController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       if ($request['isWeb'])
       {
           $validator = Validator::make($request->toArray(), [
             'link' => 'url',
          ]);

           if ($validator->fails()) {

             return redirect('/home-advs')->withErrors($validator);
           }
       }
       else
       {
           $validator = Validator::make($request->toArray(), [
             'link' => 'url',
          ]);

           if ($validator->fails()) {
             return response()->json(['errors'=>$validator->errors()],401);
           }
       }

       if($request->has('adv_type'))
       {$last_adv_pos = Adv::where('type','sidebar')->max('position');}
       else
        { $last_adv_pos = Adv::where('type','carousel')->max('position');}

        $Adv = new Adv;
        if ($request->file('advPic')) {//<============if file attached
          $upload_file = Helpers::upload($request->file('advPic'),'advs');
          $Adv->url = isset($upload_file)?$upload_file->pathToSave:"none";
        }
        $Adv->link = $request->link;
        if ($request->has('adv_type')){$Adv->type = "sidebar";}
        else{$Adv->type = "carousel";}

        $Adv->position = ($last_adv_pos + 1) ;
        if ($request->has('dateFrom')){$Adv->startdate =  $request->dateFrom;}
        if($request->has('dateTo')){$Adv->enddate = $request->dateTo;}
        if($request->adv_status == "true"){$Adv->isActive = 1;}
        if($request->adv_status == "false"){$Adv->isActive = 0;}
        $Adv->save();

        if ($request['isWeb'])
        {
          return redirect('home-advs')->with([
            'status' => 'Advert added',
            ]);
        }
        return response()->json([
            'Adv' => $Adv,
            'message' => 'Adv added succefully '
        ], 200);
     }

     public function updateSponsorLogos(Request $request)
     {
       if ($request->file('sponsorlogo')) {//<============if file attached
         $Adv = new Adv;
         $upload_file = Helpers::upload($request->file('sponsorlogo'),'advs');
         $Adv->url = isset($upload_file)?$upload_file->pathToSave:"none";
           $Adv->type = "sponser_logo";
           $Adv->isActive = 1;
           $Adv->save();
       }
       return back();
     }

     public function removeSponsorLogo(Request $request)
     {
      $Adv =  Adv::where('id',$request->logo_id)->first();
      $Adv->delete();
      return redirect('/home-advs');
     }


     /**
      * GET ALL Advs
      *
      *
      */
      public function getAdv()
      {
        $Advs = Adv::with('branches')->get();

        $response = [
          'Advs' => $Advs
        ];
        return response()->json($response , 200);
      }

     /**
      * GET ONE Club
      *
      *
      */
      public function getOneAdv($id)
      {
        $Adv = Adv::where('id',$id);
        $response = [
          'adv' => $Adv
        ];
        return response()->json($response , 200);
      }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adv  $adv
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
       $adv = Adv::find($id);
       if ($request['isWeb'])
       {
           $validator = Validator::make($request->toArray(), [
             'link'.$id => 'url',
          ]);

           if ($validator->fails()) {

             return redirect('/home-advs')->withErrors($validator);
           }
       }
       else
       {
           $validator = Validator::make($request->toArray(), [
             'link' => 'url',
          ]);

           if ($validator->fails()) {
             return response()->json(['errors'=>$validator->errors()],401);
           }
       }



        $adv = Adv::find($id);
        if(!$adv)
        {
          return response()->json(['message' => ' adv not found '] , 404);
        }
        $adv->link = $request['link'.$id];
        //$adv->link = $request->link;
        if ($request->file('advPic')) {//<============if file attached
          $upload_file = Helpers::upload($request->file('advPic'),'advs');
          $adv->url = isset($upload_file)?$upload_file->pathToSave:"none";
        }


        if($request->adv_status == "true")
        {
           $adv->isActive = 1;
        }
        if($request->adv_status == "false")
        {
          $adv->isActive = 0;
        }
        if ($request->has('dateFrom'))
        {
          $adv->startdate =  $request->dateFrom;
        }
        if($request->has('dateTo'))
        {
           $adv->enddate = $request->dateTo;
        }

        $adv->save();

        if ($request['isWeb'])
        {
          $tabName = "branchinfo";
          return redirect('/home-advs')->with([
            'status' => 'Adv updated'
            ]);
        }
        return response()->json([
          'adv' => $adv,
          'message' => 'branch Updated succefully '] , 200);
     }

     /**
      * Switch adv status
      *
      *
      */


     public function switchAdvStatus(Request $request)
     {
       $adv = Adv::where('id', $request->adv_id)->first();
       if (!$adv)
       {
         return response()->json([
             'message' => 'Adv Not found '
         ], 401);
       }

       $adv->toggleActive()->save();

       return response()->json([
           'Adv_status' => $adv->isActive,
           'message' => 'Adv status switched succefully '
       ], 200);

     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adv  $adv
     * @return \Illuminate\Http\Response
     */
     public function sortAdvs(Request $request)
     {

       $order = $request->only('data');
       $order_parsed = parse_str($order['data'],$str);
       $advert = $str['adv'];
       $counter = 1;
       foreach ($advert as $key => $adv_id) {
         $adv = Adv::find($adv_id);
         $adv->position = $counter;
         $adv->save();
         $counter++;
       }
       return response()->json(['Message' => 'success'] , 200);

     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Adv  $adv
      * @return \Illuminate\Http\Response
      */
     public function destroy(Request $request)
     {
       $adv = Adv::find($request->adv_id);
       $adv->delete();

       if ($request['isWeb'])
       {

         return redirect('/home-advs')->with([
           'status' => 'Advert deleted'
           ]);
       }
       return response()->json(['Message' => 'Adv deleted succefully'] , 200);
     }
}
