<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;


class RolesController extends Controller
{

  //============== Roles Functions ===============
    public function addrole(Request $request)
    {
      //this code only if we need to retrieve the current users like (Auth::user;) !
      //$user = JWTAuth::parseToken()->toUser();
      //This code is not needed when using the auth.jwt middleware!
      // if (!$user = JWTAuth::parseToken()->authenticate()) {
      //      return response()->json(['error' => 'Authentication failed'], 404);
      //   }
      $role = new Role();
      $role->name         = $request->name;
      $role->display_name = $request->display_name; // optional
      $role->description  = $request->description; // optional
      $role->save();

     return response()->json(['role' => $role] , 201);
    }
    public function getRoles()
    {
      $roles = Role::all();
      $response = [
        'roles' => $roles
      ];
      return response()->json($response , 200);
    }


    public function updateRole(Request $request, $id)
      {
          $role = Role::find($id);
          if(!$role)
          {
            return response()->json(['message' => 'role not found '] , 404);
          }
          $role->name         = $request->name;
          $role->display_name = $request->display_name; // optional
          $role->description  = $request->description; // optional

          $role->save();
          return response()->json(['role' => $role] , 200);
      }

    public function deleteRole($id)
    {
        $role = Role::find($id);
        $role->delete();
        return response()->json(['Message' => 'role deleted succefully'] , 200);
    }

//============== End of Roles Functions ===============

//============== Permission Functions ===============
    public function addper(Request $request)
    {

      $permision = new Permission();
      $permision->name         = $request->name;
      $permision->display_name = $request->display_name; // optional
      // Allow a user to...
      $permision->description  = $request->description; // optional
      $permision->save();
      return response()->json(['permision' => $permision] , 201);
    }

    public function getPermissions()
    {
      $permisions = Permission::all();
      $response = [
        'permisions' => $permisions
      ];
      return response()->json($response , 200);
    }


    public function updatePermission(Request $request, $id)
      {
          $permision = Permission::find($id);
          if(!$permision)
          {
            return response()->json(['message' => 'permision not found '] , 404);
          }
          $permision->name         = $request->name;
          $permision->display_name = $request->display_name; // optional
          // Allow a user to...
          $permision->description  = $request->description; // optional
          $permision->save();
          return response()->json(['permision' => $permision] , 200);
      }

    public function deletePermission($id)
    {
        $permision = Permission::find($id);
        $permision->delete();
        return response()->json(['Message' => 'permision deleted succefully'] , 200);
    }
//============== End of Permission Functions ===============


//============== Attaching Functions ===============
    public function attachPermissionToRole(Request $request)
    {
      $role = Role::where('id',$request->role_id)->first();
      $permision = Permission::where('id',$request->permision_id)->first();
      $role->attachPermission($permision);

      return response()->json(['Message' => 'Permission attched succefully'] , 200);
    }

    public function attachRoleToUser(Request $request)
    {
      $user = User::where('id',$request->user_id)->first();
      $role = Role::where('id',$request->role_id)->first();
      $user->attachRole($role);

      return response()->json(['Message' => 'Role attched succefully'] , 200);
    }

  //============== End of Attaching Functions ===============

}
