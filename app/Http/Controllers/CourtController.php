<?php

namespace App\Http\Controllers;

use App\Court;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Branch;
use App\Permission;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;

class CourtController extends Controller
{
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
   public function addCourt(Request $request)
   {
     Log::debug($request);
      $court = new Court;
      $court->name = $request->court_name;
      $court->service_sport_id = $request->service_sport_id;

      if($request->court_status == "true")
      {
         $court->isActive = 1;
      }
      if($request->court_status == "false")
      {
        $court->isActive = 0;
      }
      $court->closing_from = $request->dateFrom;
      $court->closing_to = $request->dateTo;
      $court->save();

      $branch = $court->serviceSport->service->branch;

      if ($request['isWeb'])
      {
        $tabName = "setting";
        $subTabName = $court->service_sport_id;
        return redirect('/branch/'.$branch->id.'/'.$tabName.'/'.$subTabName)->with([
          'status' => 'Service updated',
          'tabName' => $tabName
          ]);
      }


      return response()->json([
          'court' => $court,
          'message' => 'court added succefully '
      ], 200);
   }


  public function switchCourtStatus(Request $request)
  {
    $court = Court::where('id', $request->court_id)->first();
    if (!$court)
    {
      return response()->json([
          'message' => 'court Not found '
      ], 401);
    }

    $court->toggleActive()->save();

    return response()->json([
        'court_status' => $court->isActive,
        'message' => 'court status switched succefully '
    ], 200);

  }

  //------------------- COURTS MODEL CRUD  -------------------------
  /**
   * GET ALL COURTS
   *
   *
   */
   public function getCourt()
   {
     $courts = Court::all();

     $response = [
       'courts' => $courts
     ];
     return response()->json($response , 200);
   }

   /**
    * GET ONE COURT
    *
    *
    */
    public function getOnecourt($id)
    {
      $court = Court::where('id',$id);
      $response = [
        'court' => $court
      ];
      return response()->json($response , 200);
    }


  /**
   * COURT UPDATE
   *
   *
   */
   public function updateCourt(Request $request, $id)
     {
       //  $validator = Validator::make($request->toArray(), [
       //    'name' => 'required|string',
       //    'status' => 'required',
       // ]);
       //
       //  if ($validator->fails()) {
       //
       //    return response()->json(['errors'=>$validator->errors()],401);
       //  }


         $court = Court::find($id);
         $branch = $court->serviceSport->service->branch;

         if(!$court)
         {
           return response()->json(['message' => 'court not found '] , 404);
         }

         $court->name = $request->courtName;
         //$court->isActive = $request->court_status;
         if($request->court_status == "true")
         {
            $court->isActive = 1;
         }
         if($request->court_status == "false")
         {
           $court->isActive = 0;
         }
         $court->closing_from = $request->dateFrom;
         $court->closing_to = $request->dateTo;

         $court->save();

         if ($request['isWeb'])
         {
           $tabName = "setting";
           $subTabName = $court->service_sport_id;
           return redirect('/branch/'.$branch->id.'/'.$tabName.'/'.$subTabName)->with([
             'status' => 'Service updated',
             'tabName' => $tabName
             ]);
         }


         return response()->json([
           'court' => $court,
           'message' => 'court Updated succefully '] , 200);
     }



   /**
    * court DELETE
    *
    *
    */

    public function deleteCourt(Request $request)
    {
        $court = Court::find($request->court_id);
        $court->delete();

        $branch = $court->serviceSport->service->branch;
        if ($request['isWeb'])
        {
          $tabName = "setting";
          $subTabName = $court->serviceSport->sport->first()->id;
          return redirect('/branch/'.$court->serviceSport->service->branch->id.'/'.$tabName.'/'.$subTabName)->with([
            'status' => 'Advert deleted',
            'branch' => $branch,
            'tabName' => $tabName
            ]);
        }

        return response()->json(['Message' => 'court deleted succefully'] , 200);
    }

  //-------------------END OF court MODEL CRUD  -------------------------
}
