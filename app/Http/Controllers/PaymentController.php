<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Payment;
use App\Deposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Court;
use App\Trainer;
use App\BookedTrainer;
use App\Service;
use App\ServiceSport;
use App\Branch;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Config;
use Response;
use Illuminate\Support\Facades\Storage;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function buildMonthPaymentsTable(Request $request)
     {
       $payment_months = [];
       if(date("Y") == $request->year){$previous_months = Helpers::getPreviousMonths(date('m'));}
       else{$previous_months = Helpers::getPreviousMonths(12);}

       foreach ($previous_months as $previous_month) {
          $month_booking_data = Helpers::calculateMonthBookings($previous_month,$request->year,$request->branch_id);
          $payment_months[] = $month_booking_data;
       }
       return Datatables::of(collect($payment_months))
       ->addColumn('club_name', function($payment_month) {
         return '<th><h5>'.ucfirst($payment_month['club_name']).' - '.ucfirst($payment_month['branch_name']).'</h5></th>';
       })->addColumn('month', function($payment_month) use($request){
          return '<th><a class="anchor_link text-primary"><h5>'.Helpers::getMonthString($payment_month['month']).'  '.$request->year.'</h5></a></th>';
       })->addColumn('total_booking', function($payment_month) {
         return '
         <th>
             <h5>'.$payment_month['countAllTotal'].' booking</h5>
             <a class="sm">'.$payment_month['countOnlineTotal'].' online</a><br><br>
             <a class="sm">'.$payment_month['countCashTotal'].' cash</a>
         </th>
         ';
       })->addColumn('revenue', function($payment_month) {
         return '
         <th>
             <h5>'.$payment_month['cash_online_total'].' EGP</h5>
             <a class="sm">'.$payment_month['online_bookings_total'].' EGP online</a><br><br>
             <a class="sm">'.$payment_month['cash_bookings_total'].' EGP cash</a>
         </th>
         ';
       })->addColumn('bank_fees', function($payment_month) {
         return '<th><h5>'.$payment_month['bank_fees'].'</h5></th>';
       })->addColumn('total_refund', function($payment_month) {
         return '<th><h5>'.$payment_month['total_refund'].'</h5></th>';
       })->addColumn('total_amount', function($payment_month) {
         return '<th><h5>'.$payment_month['final_online_total'].'</h5></th>';
       })->addColumn('recieipt', function($payment_month) use($request) {
         $deposit = Deposit::where('branch_id', $request->branch_id)
         ->where('month',$payment_month['month'])
         ->where('year',$payment_month['year'])
         ->first();
         if($deposit && $deposit->status == "paid")
         {
           if(!$deposit->recieipt_url)
           {
             if(Auth::user()->hasRole('sys_admin')){
               return '
               <th>
               <a href="#" data-toggle="modal"
               data-branch="'.$request->branch_id.'" data-month="'.$payment_month['month'].'" data-year="'.$payment_month['year'].'"
                data-target="#viewReceiptModal" onclick="feedUploadModal(this);" >Upload Receipt</a>
               </th>
               ';
             }
             else{return ' - ';}
           }
           else
           {
             //$file_path = Config::get('filesystems.disks.local.root') . '/' .$deposit->recieipt_url;
             //$url = Storage::url($deposit->recieipt_url);
             $file_name = $deposit->recieipt_url;
             return '
             <th><a  href="/download-reciept/'.$file_name.'"
             class="anchor_link text-primary">View Receipt</a></th>
             ';
           }
         }
         else{return ' - ';}

       })->addColumn('status', function($payment_month) use($request) {
         $deposit = Deposit::where('branch_id', $request->branch_id)
         ->where('month',$payment_month['month'])
         ->where('year',$payment_month['year'])
         ->first();
         if(!$deposit || $deposit->status != "paid")
         {
           if(Auth::user()->hasRole("sys_admin"))
           {
             return '
             <div class="form-group pl-5 pr-5">
               <select class="custom-select monthPaymentSelect border-0">
                 <option value="due" class="font-weight-bold" selected>Due</option><option value="paid"
                 data-branch="'.$request->branch_id.'" data-month="'.$payment_month['month'].'" data-year="'.$payment_month['year'].'"
                 class="font-weight-bold">paid</option>
               </select>
             </div>
             ';
           }else{
             return '<th><h5>Due</h5></th>';
           }
         }
         else
          {
           return '<th><h5>Paid</h5></th>';
          }
       })->rawColumns(['club_name', 'month','total_booking','revenue','bank_fees','total_refund','total_amount','recieipt','status'])
       ->make(true);

     }

      public function getDownload($filename)
       {
         $file_path = Config::get('filesystems.disks.local.root') . '/' .$filename;
         //$file = Storage::url('app/'.$filename);
         // $headers = array(
         //   'Content-Type: application/jpeg',
         // );
         return Response::download($file_path);
       }


   public function submitDeposit(Request $request){
      $deposit = Deposit::where('branch_id', $request->branch_id)
      ->where('month',$request->month)
      ->where('year',$request->year)
      ->first();
      if(!$deposit)
      {
        $deposit = new Deposit;
        $deposit->branch_id = $request->branch_id;
        $deposit->month = $request->month;
        $deposit->year = $request->year;
        $deposit->status = "paid";
        $deposit->save();
      }
      else
      {
        $deposit->status = "paid";
        $deposit->save();
      }

      return redirect()->back();
   }

   public function uploadBankReceipt(Request $request){
      $deposit = Deposit::where('branch_id', $request->branch_id)
      ->where('month',$request->month)
      ->where('year',$request->year)
      ->first();
      if($deposit)
      {
        if ($request->file('payment_reciept')) {//<============if file attached
            // $upload_file = Helpers::upload($request->file('payment_reciept'),'receipts');
            // $deposit->recieipt_url = $upload_file->pathToSave;
            // $deposit->save();
            $payment_reciept = $request->file('payment_reciept');
            $ext = $payment_reciept->guessClientExtension();
            $fname = md5(rand()).'.'.$ext;
    				$full_path = Config::get('filesystems.disks.local.root');
    				$payment_reciept->move( $full_path, $fname );
            $deposit->recieipt_url = $fname;
            $deposit->save();
          }
      }
      return redirect()->back();
   }

   public function feedReceiptImageModal(Request $request)
   {
     $deposit = Deposit::where('branch_id', $request->branch_id)
     ->where('month',$request->month)
     ->where('year',$request->year)
     ->first();
     if($deposit)
     {
       if($request->ajax()) {
           return view('includes.recieipt-image', [
             'image' => $deposit->recieipt_url
             ])->render();
       }
     }

   }


}
