<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use App\Notifications\EmailConfirmation;
use App\Notifications\AccountCreated;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Branch;
use App\Permission;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Utils\Helpers;
use Illuminate\Support\Facades\Event;
//use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Support\Facades\Artisan;
use App\Events\NewSignUp;
use Carbon\Carbon;
use App\Jobs\SendVerificationEmail;

class UserController extends Controller
{

  use Notifiable;
  /**
   * USER SIGN UP VIA REGISTERATION PAGE
   *
   *
   */

   public function signup(Request $request)
   {
    Log::info("Request fired");
    $user = Helpers::SignUpUser($request, false); // <===== Second parameter for verify_status
    if ($user["validation"] == "failed")
    {
      return response()->json(['errors'=>$user["validator"]->errors()],401);
    }

     //Event::fire(new NewSignUp($user));
    $user->notify(new EmailConfirmation($user));
    //  $verification_email = (new SendVerificationEmail($user))
    //                         ->delay(Carbon::now()->addSeconds(2));

    //  dispatch($verification_email);
    $role = Role::where('name', 'member' )->first();
    if (!$role)
    {
      return response()->json([
        'Message' => 'Assigned role not found.'
        ] , 401);
    }
    $user->attachRole($role);

      return response()->json([
        'message' => 'Please check your email address to verify your account.'
        ] , 201);

     Log::info("Request Ended");
   }

   public function resendVerifyEmail(Request $request)
   {
     $user = User::where('id',$request->user_id)->first();
     $user->notify(new EmailConfirmation($user));
   }

   /**
    * ADDING USER MANUALLY BY CLUB ADMIN
    *
    *
    */

    public function addUser(Request $request)
    {
        $password = $request->password;
        $tabName = "members";
              // ------------ CHECK IF MEMBER EXISTS -----------------
        $exists = count(Member::where('membershipId',$request->membershipId)->where('branch_id',$request->branch_id)->get());
        if($exists)
        {
          $member = Member::where('membershipId',$request->membershipId)->first();
          $branch = Branch::where('id',$request->branch_id)->first();

          $attached = DB::table('branch_members')
          ->whereMemberId($member->id)
          ->whereBranchId($branch->id)
          ->count() > 0;

          if ($attached)
          {
            return redirect('/branch/'.$request->branch_id.'/'.$tabName)->with([
              'tabName' => $tabName
              ])->withErrors(['membershipId' =>'Membership id must be unique in this branch']);
          }
        }
        else
        {
          //----------- (1) CREATING NEW USER TO USERS TABLE  ---------------
           $user = Helpers::SignUpUser($request, true); // <===== 2nd param for verify_status
           if ($user["validation"] == "failed")
           {
             if($request['isWeb'])
             {

               return redirect('/branch/'.$request->branch_id.'/'.$tabName)->with([
                 'tabName' => $tabName
                 ])->withErrors($user["validator"]);
             }
             else
             {
               return response()->json(['errors'=>$user["validator"]->errors()],401);
             }

           }
           //--------------------------
           //---------- (2) ATTACHING MEMBER ROLE TO THIS USER----------------
           $role = Role::where('name', 'member' )->first();
           if (!$role)
           {
             return response()->json([
               'Message' => 'Assigned role not found.'
               ] , 401);
           }
           $user->attachRole($role);
           //---------- (3) CREATING NEW MEMBER ACCOUNT & ATTACH USER TO IT----------------
           $member = new Member;
           $member->user_id = $user->id;
           $member->membershipId = $request->membershipId;
           $member->branch_id = $request->branch_id;
           $member->isActive = 1;
           $member->membershipName = $request->membershipName;
           $member->save();
           //--------------------------
           //---------- (4) ASSIGN MEMBER TO CLUB ----------------
           $member->branch()->syncWithoutDetaching($request->branch_id);
           //--------------------------
           //---------- (5) SEND USER NOTIFICATION ----------------
           $user->notify(new AccountCreated($user,$password,$member));
           //--------------------------
           //---------- RETURN MESSAGE ----------------
           if ($request['isWeb'])
           {
             $tabName = "members";
             return redirect('/branch/'.$request->branch_id.'/'.$tabName)->with([
               'status' => 'Member added successfully ',
               'tabName' => $tabName
               ]);
           }

           return response()->json([
             'Message' => 'User Created succefully.'
             ] , 201);
           //--------------------------
        }

    }

   /**
    * USER SIGN IN
    *
    *
    */

   public function signin(Request $request){


     $validator = Validator::make($request->toArray(), [
       'username' => 'required|string|without_spaces',
       'password' => 'required|string|min:6',
     ]);

      if ($validator->fails()) {

        return response()->json(['errors'=>$validator->errors()],401);
      }

     $credentials = $request->only('username','password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'error' => 'Invalid Credentials !'
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'error' => 'Could not create token!'
            ], 500);
        }

        $user = Auth::user();
        //check if we need to return user
        return response()->json([
            'token' => $token ,
            'user' => $user,
            'role' => $user->roles,
            'message' => 'user signed in succefully '
        ], 200);
   }

   /**
    * CONFIRM EMAIL
    *
    *
    */

   public function confirmEmail($token)
   {
       User::whereToken($token)->firstOrFail()->confirmEmail();
       return redirect('/')->with('status','your email confirmed you can login now');
   }
   //------------------- USER MODEL CRUD  -------------------------
   /**
    * GET ALL USERS
    *
    *
    */
    public function getUser()
    {
      $users = User::with('roles')->get();

      $response = [
        'users' => $users
      ];
      return response()->json($response , 200);
    }

    /**
     * GET ONE USER
     *
     *
     */
     public function getOneUser($id)
     {
       $user = User::where('id',$id);
       $response = [
         'user' => $user
       ];
       return response()->json($response , 200);
     }


   /**
    * USER UPDATE
    *
    *
    */
    public function updateUser(Request $request, $id)
      {
          $user = User::find($id);
          $tabName ="info";
          if(!$user)
          {
            return response()->json(['message' => 'user not found '] , 404);
          }

          $validator = Validator::make($request->toArray(), [
         'username' => 'sometimes|required|without_spaces|string|max:255|unique:users,username,'.$user->id,
         'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
         'phone' => 'required|digits:11',
         ]);

          if ($validator->fails())
          {
            if ($request['isWeb'])
            {
              if (Auth::user()->hasRole('sys_admin'))
              {
                  return redirect('/member/'.$request->member_id.'/'.$tabName.'')->with([
                    'status' => 'Validation Errors'
                    ])->withErrors($validator);
              }
              return redirect('/myaccount')->with([
                'status' => 'Validation Errors'
                ])->withErrors($validator);
            }
            else
            {
               return response()->json(['errors'=>$validator->errors()],401);
            }
          }

          if ($request->file('avatar')) {//<============if file attached
            $upload_file = Helpers::upload($request->file('avatar'),'avatars');
          }

          if ($request->has('email')) { $user->email = $request->email; }
          if ($request->has('phone')) { $user->phone = $request->phone; }
          if ($request->has('address')) { $user->address = $request->address; }
          if ($request->has('password')) {  $user->password = bcrypt($request->password);}
          if ($request->file('avatar')){$user->avatarURL = $upload_file->pathToSave;}
          $user->save();

          if ($request['isWeb'])
          {
            if (Auth::user()->hasRole('sys_admin'))
            {
                return redirect('/member/'.$request->member_id.'/'.$tabName.'')->with('status','Profile updated');
            }
            return redirect('/myaccount')->with('status','Profile updated');
          }
          return response()->json([
            'user' => $user,
            'message' => 'user Updated succefully '] , 200);
      }

    


      public function updateUserPic(Request $request, $id)
        {
          $user = User::find($id);
          if ($request->file('userImage')) {//<============if file attached
            $upload_file = Helpers::upload($request->file('userImage'),'avatars');
          }
          if ($request->file('userImage')){$user->avatarURL = $upload_file->pathToSave;}
          $user->save();
        }

    /**
     * USER DELETE
     *
     *
     */

     public function resetUserPassword(Request $request)
       {
           $tabName ="info";
           if(Auth::user()->hasRole('sys_admin'))
           {
              $user = User::where('id',$request->user_id)->first();
           }
           else {$user = Auth::user(); }

           if (password_verify( $request->old_password , $user->password))
           {

              $validator = Validator::make($request->toArray(), [
              'password' => 'required|strong_password|string|min:6|confirmed'
              ]);
              if ($validator->fails())
                  {
                    if (Auth::user()->hasRole('sys_admin'))
                    {
                        return redirect('/member/'.$user->id.'/'.$tabName.'')->with([
                          'status' => 'Validation Errors'
                          ])->withErrors($validator);
                    }
                    return redirect('/myaccount')->with([
                      'status' => 'Validation Errors'
                      ])->withErrors($validator);
                  }
                  $user->password = bcrypt($request->password);
                  $user->save();
                  if(Auth::user()->hasRole('sys_admin'))
                  {
                    return redirect('/member/'.$user->id.'/'.$tabName.'')->with('status','Profile updated');
                  }
                  else
                  {
                    return redirect('/myaccount')->with([
                      'status' => 'success',
                      ]);
                  }
              }

           else
           {
             if(Auth::user()->hasRole('sys_admin'))
             {
               return redirect('/member/'.$user->id.'/'.$tabName.'')
               ->withErrors([
                 'old_password'=>'Wrong password',
                 'formShow' => 'd_show'
               ]);
             }
             else
             {
               return redirect('/myaccount')->with([
                 'status' => 'failed'
                 ])->withErrors([
                   'old_password'=>'Wrong password',
                   'formShow' => 'd_show'
                 ]);
             }

           }
       }

     public function deleteUser($id)
     {
         $user = User::find($id);

         $user->delete();
         return response()->json(['Message' => 'user deleted succefully'] , 200);
     }

   //-------------------END OF USER MODEL CRUD  -------------------------
}
