<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adv;
use App\Club;
use App\User;
use App\Sport;
use App\Member;
use App\Branch;
use App\Booking;
use App\Trainer;
use App\Court;
use App\BranchAdv;
use App\Setting;
use App\Application;
use App\ServiceSport;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;

class PagesController extends Controller
{
      public $carousels;
      public $sponser_logos;
      public $sidebar_images;
      public $clubs;
      public $setting;

      public function __construct()
      {
          $this->sponser_logos =  Adv::where('type','sponser_logo')
                                ->where('isActive',1)->orderBy('position')->get();
          $this->carousels =  Adv::where('type','carousel')
                                ->where('isActive',1)
                                ->orderBy('position')->get();

          $this->sidebar_images =  Adv::where('type','sidebar')
                                ->where('isActive',1)
                                ->orderBy('position')->get();
          $this->clubs =  Club::where('isActive',1)->get();
          $this->setting =  Setting::first();
      }


      public function welcome()
      {
        $user = Auth::user();
        $branches= [];
        if($user)
          {
          $branches = Helpers::getUserBranches($user);
          }
        $setting =  Setting::first();
        return view('welcome', [
          'logos' =>  $this->sponser_logos,
          'carousels' =>  $this->carousels,
          'sidebar_images' =>  $this->sidebar_images,
          'clubs' =>  $this->clubs,
          'setting' => $setting,
          'branches' => $branches,
          'all_branches' => Branch::all()
        ]);
      }


      public function systemClubs()
      {
          $clubs= Club::all();
          return view('system.club-list', [
            'logos' =>  $this->sponser_logos,
            'clubs' =>  $clubs
          ]);
      }

      public function branchDashboard()
      {
        $branch = Branch::where('admin_id', Auth::user()->id )->first();
        $branch_id = $branch->id;
        $admin = Auth::user();
        $sports = Sport::all();
          return view('branch',[
            'logos' =>  $this->sponser_logos,
            'branch' => $branch,
            'admin' => $admin,
            'tabName' => isset($tabName)?$tabName:"booking",
            'subTabName' => isset($subTabName)?$subTabName:"add_member",
            'totalsheetmembers' => count(Member::where('branch_id',$branch_id)->get()),
            'totalbranchrequests' => count(Application::where('branch_id',$branch_id)->get()),
            'sports' => $sports
          ]);
      }

      public function myaccount()
      {
        $user = Auth::user();
        $branches = [];
        if($user){$branches = Helpers::getUserBranches($user);}
        if(Auth::user()->hasRole('sys_admin'))
        {
            return redirect('/club-list');
        }
        if(Auth::user()->hasRole('club_admin'))
        {
          $branch = Branch::where('admin_id', Auth::user()->id )->first();
          $branch_id = $branch->id;
          return redirect('/branch/'.$branch_id.'/booking');
        }
        return view('myaccount',[
          'logos' =>  $this->sponser_logos,
          'branches' => $branches
        ]);
      }


      /**
       * GET ONE BRANCH
       *
       *
       */
       public function getOneBranch(Request $request ,$id, $tabName=null , $subTabName=null)
       {
         $branch = Branch::where('id',$id)->first();
         if(!$branch)
         {
           return redirect('/404');
         }
         $admin = User::where('id', $branch->admin_id )->first();
         if(!$admin)
         {
           if (! Auth::user()->hasRole('sys_admin'))
           {
             return redirect('/403');
           }
         }
         if(isset($subTabName) && $subTabName !="sheet")
         {
            $current_service_sport = ServiceSport::where('id',$subTabName)->first();
            if(!$current_service_sport)
            {
              return redirect('/404');
            }
         }
         else
         {
           $current_service_sport = $branch->services->where('short_name','court_booking')->first()->serviceSport->first();
         }

         $sports_filtered = Helpers::getRemainingSports($branch);
         $month_booking_data = Helpers::calculateMonthBookings(date('m'),date('Y'),$branch->id);
         $monthBooking = json_decode($month_booking_data);
         // $response = [
         //   'branch' => $branch
         // ];
         if ($request['isWeb'])
         {
           return view('branch', [
             'logos' =>  $this->sponser_logos,
             'branch' => $branch,
             'admin' => $admin,
             'tabName' => isset($tabName)?$tabName:"booking",
             'subTabName' => isset($subTabName)?$subTabName:"default",
             'totalsheetmembers' => count(Member::where('branch_id',$id)->get()),
             'totalbranchrequests' => count(Application::where('branch_id',$id)->get()),
             'sports' => $sports_filtered,
             'online_bookings_total' => $monthBooking->online_bookings_total,
             'cash_bookings_total' => $monthBooking->cash_bookings_total,
             'total_refund' => $monthBooking->total_refund,
             'bank_fees' => $monthBooking->bank_fees,
             'final_online_total' => $monthBooking->final_online_total,
             'current_service_sport' => $current_service_sport
             ]);
         }
         return response()->json($response , 200);
       }


       public function getOneMember(Request $request ,$id, $tabName=null)
       {
         $user = User::where('id',$id)->first();
         if(!$user)
         {
           return redirect('/404');
         }
         $branches = [];
         if($user){$branches = Helpers::getUserBranches($user);}
         return view('system.system-member', [
           'user' => $user,
           'logos' =>  $this->sponser_logos,
           'tabName' => isset($tabName)?$tabName:"info",
           'branches' => $branches
           ]);
       }

       public function viewBranch(Request $request , $branch_id ,$tabName=null , $subTabName=null)
       {
         $branch = Branch::where('id',$branch_id)->first();
         if(!$branch || $branch->isActive == 0)
         {
           return redirect('/404');
         }
         $user = Auth::user();
         $branches= [];
         if($user){$branches = Helpers::getUserBranches($user);}
         foreach (collect($branches) as $user_branch ) {
           if(! $user_branch->isActive )
           {
             return redirect('/404');
           }
         }
         $branch_admin = User::where('id',$branch->admin_id)->first();
         $branchAdvs = BranchAdv::where('branch_id',$branch_id)
                                 ->where('isActive' ,1)
                                 ->orderBy('position')->get();
         if ($request['isWeb'])
         {
           return view('branch-page', [
             'logos' =>  $this->sponser_logos,
             'branch' => $branch,
             'tabName' => isset($tabName)?$tabName:"court-booking",
             'subTabName' => isset($subTabName)?$subTabName:"default",
             'branchAdvs' => $branchAdvs,
             'branches'=> $branches,
             'branch_admin' => $branch_admin
             ]);
         }
       }

       public function homeAdvs()
       {
         $sponser_logos =  Adv::where('type','sponser_logo')->get();
         $carousels =  Adv::where('type','carousel')->orderBy('position')->get();
         $sidebar_images =  Adv::where('type','sidebar')->orderBy('position')->get();

           return view('home_advs', [
             'logos' =>  $sponser_logos,
             'advs' => $carousels,
             'sidebar_images' => $sidebar_images,
             'setting' => $this->setting
             ]);


       }


       public function userprofile()
       {
         $user = User::where('id',59)->first();
         $booking = Booking::all();
         return view('profile', [
           'logos' =>  $this->sponser_logos,
           'user' => $user,
           'bookings' => $booking
           ]);

       }

       public function calender()
       {
         $sponser_logos =  Adv::where('type','sponser_logo')->get();
         return view("test2", [
           'logos' =>  $sponser_logos
         ]);
       }

       public function bookingSuccess($booking_id)
       {
         $sponser_logos =  Adv::where('type','sponser_logo')->get();
         $booking = Booking::where('booking_id',$booking_id)->first();
         $branch = Branch::where("id",$booking->branch_id)->first();
         $service_sport = ServiceSport::where('id',$booking->service_sport_id)->first();
         $cancel_limit_in_hours = $service_sport->service->cancel_limit;
         $branch_name = $branch->name;
         $club_name = Club::where("id",$branch->club->id)->first()->name;
         $court_name = Court::where("id",$booking->court_id)->first()->name;
         if($booking->withTrainer)
         {
           $trainer_name = Trainer::where("id",$booking->trainer_id)->first()->name;
         }
         return view("booking-success", [
           'logos' =>  $sponser_logos,
           'booking' => $booking,
           'club_name' => $club_name,
           'branch_name' => $branch_name,
           'court_name' => $court_name,
           'trainer_name' => isset($trainer_name)?$trainer_name:"none",
           'cancel_limit_in_hours' => $cancel_limit_in_hours
         ]);
       }

       public function notFound(){

            return view('errors.404', [
              'logos' =>  $this->sponser_logos
            ]);
       }

       public function notPermited(){

            return view('errors.403', [
              'logos' =>  $this->sponser_logos
            ]);
       }

       public function serviceIsDown(){

            return view('errors.503', [
              'logos' =>  $this->sponser_logos
            ]);
       }



       public function terms($subtab)
       {
            if($subtab == "terms")
            {
              $current_tab = "terms";
            }
            if($subtab == "privacy")
            {
              $current_tab = "privacy";
            }
            if($subtab == "cancel")
            {
              $current_tab = "cancel";
            }
            return view('terms', [
              'logos' =>  $this->sponser_logos,
              'current_tab' => $current_tab
            ]);
       }





}
