<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminCreated;
use App\User;
use App\Role;
use App\Member;
use App\Club;
use App\Court;
use App\Trainer;
use App\Service;
use App\ServiceSport;
use App\Branch;
use App\Application;
use App\Booking;
use App\Permission;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class BranchController extends Controller
{





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function addBranch(Request $request)
     {
        //----------- (1) VALIDATING REQUEST  ---------------
             // $validator = Validator::make($request->toArray(), [
             //   'branch_name' => 'required|string',
             //   'branch_status' => 'required',
             //   'club_id' => 'required'
             // ]);
             //
             //  if ($validator->fails()) {
             //
             //    return response()->json(['errors'=>$validator->errors()],401);
             //  }
        //-----------------------
        //----------- (2) CREATING NEW BRANCH  ---------------

        $branch = new Branch;
        $branch->name = $request->branch_name;
        $branch->club_id = $request->club_id;

        if($request->branch_status == "true")
        {
           $branch->isActive = 1;
        }
        if($request->branch_status == "false")
        {
          $branch->isActive = 0;
        }

        $branch->type = "secondary";
        $branch->save();
        //-----------------------
        //----------- (3) SET DEFAULT BRANCH SETTING  ---------------
            /**
             * Create default service .
             * Create default 2 sports.
             */
        Helpers::setDefaultBranchSetting($branch);

        //-------------------------
        if ($request['isWeb'])
        {
          return redirect('/club-list')->with('status','branch updated');
        }

        return response()->json([
            'branch' => $branch,
            'message' => 'Branch added succefully '
        ], 200);
     }


    public function switchBranchStatus(Request $request)
    {
      $branch = Branch::where('id', $request->branch_id)->first();
      if (!$branch)
      {
        return response()->json([
            'message' => 'Branch Not found '
        ], 401);
      }

      $branch->toggleActive()->save();

      return response()->json([
          'Branch_status' => $branch->isActive,
          'message' => 'Branch status switched succefully '
      ], 200);

    }

    //------------------- BranchS MODEL CRUD  -------------------------
    /**
     * GET ALL BRANCHS
     *
     *
     */
     public function getBranch()
     {
       $branchs = Branch::all();

       $response = [
         'branchs' => $branchs
       ];
       return response()->json($response , 200);
     }




    /**
     * Branch UPDATE
     *
     *
     */
     public function updateBranch(Request $request, $id)
       {
         $branch = Branch::find($id);
         if(!$branch)
         {
           return response()->json(['message' => 'branch not found '] , 404);
         }

          $validator = Validator::make($request->toArray(), [
         'branch_email' => 'required|string|email|max:255',
         'branch_phone' => 'required'
         ]);

          if ($validator->fails()) {

            if ($request['isWeb'])
            {
              $tabName = "branchinfo";
              return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                'status' => 'user updated',
                'tabName' => $tabName
                ])->withErrors($validator);
            }
            else
            {
               return response()->json(['errors'=>$validator->errors()],401);
            }
          }




           $branch->email = $request->branch_email;
           $branch->phone = $request->branch_phone;
           $branch->address = $request->address;
           $branch->website = $request->website;
           $branch->map_url = $request->map_url;

           $branch->save();

           if ($request['isWeb'])
           {
             $tabName = "branchinfo";
             return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
               'status' => 'BRANCH updated',
               'tabName' => $tabName
               ]);
           }

           return response()->json([
             'branch' => $branch,
             'message' => 'branch Updated succefully '] , 200);
       }

       public function updateBranchBasicInfo(Request $request, $id)
         {




             $branch = Branch::find($id);
             if(!$branch)
             {
               return response()->json(['message' => 'branch not found '] , 404);
             }

             $branch->name = $request->branch_name;
             if($request->branch_status == "true")
             {
                $branch->isActive = 1;
             }
             if($request->branch_status == "false")
             {
               $branch->isActive = 0;
             }



             $branch->save();

             if ($request['isWeb'])
             {
               return redirect('/club-list')->with('status','branch updated');
             }

             return response()->json([
               'branch' => $branch,
               'message' => 'branch Updated succefully '] , 200);
         }

     /**
      * ADD BRANCH ADMIN
      *
      *
      */

      public function addBranchAdmin(Request $request)
        {

           $password = $request->password;
           $branch = Branch::find($request->branch_id);

          //----------- (1) CREATING NEW USER TO USERS TABLE  ---------------
           $user = Helpers::SignUpUser($request, true); // <===== 2nd param for verify_status
           if ($user["validation"] == "failed")
           {
             if ($request['isWeb'])
             {
               $tabName = "branchinfo";
               return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                 'status' => 'user updated',
                 'tabName' => $tabName
                 ])->withErrors($user["validator"]);
             }
             else
             {
                return response()->json(['errors'=>$user["validator"]->errors()],401);
             }

           }
           //--------------------------
           //---------- (2) ATTACHING MEMBER ROLE TO THIS USER----------------
           $role = Role::where('name', 'club_admin' )->first();
           if (!$role)
           {
             return response()->json([
               'Message' => 'Assigned role not found.'
               ] , 401);
           }
           $user->attachRole($role);
              //--------------------------

              //---------- (3) ASSIGN Admin TO Branch ----------------

            if(!$branch)
            {
              return response()->json(['message' => 'branch not found '] , 404);
            }
            $branch->admin_id = $user->id;
            $branch->save();
            //--------------------------

            //---------- (4) SEND USER NOTIFICATION ----------------
            $user->notify(new AdminCreated($user,$password,$branch));
            //--------------------------
            //---------- RETURN MESSAGE ----------------
            if ($request['isWeb'])
            {
              $tabName = "branchinfo";
              return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                'status' => 'user updated',
                'tabName' => $tabName
                ]);
            }
            return response()->json([
              'Message' => 'Branch Admin added Created succefully.'
              ] , 201);
            //--------------------------

        }
        public function resetAdminPassword(Request $request)
          {

            $user = User::where('id', $request->admin_id )->first();
            $branch =Branch::where('admin_id',$request->admin_id)->first();
            $tabName = "branchinfo";

            if (password_verify( $request->old_password , $user->password)) {

              $request->validate([
                  'password' => 'required|strong_password|string|min:6|confirmed'
                ]);
              $user->password = bcrypt($request->password);
              $user->save();
              return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                'status' => 'success',
                'tabName' => $tabName,
                ]);
            }
            else {
              Log::debug($request);

              return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                'status' => 'failed',
                'tabName' => $tabName,
                ])->withErrors(['old_password'=>'Wrong password']);
            }
          }


        /**
         * UPDATE BRANCH ADMIN INFO
         *
         *
         */

        public function updateBranchAdminInfo(Request $request, $id)
          {

              $admin = User::find($id);
              $branch = Branch::where('admin_id', $id)->first();

              $validator = Validator::make($request->toArray(), [
                'email' => 'required|string|email|max:255|unique:users,email,'.$admin->id,
                'phone' => 'required',
                'username'=> 'required|without_spaces|string|max:255|unique:users,username,'.$admin->id
              ]);

               if ($validator->fails()) {
                 if ($request['isWeb'])
                 {
                   $tabName = "branchinfo";
                   return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                     'status' => 'user updated',
                     'tabName' => $tabName
                     ])->withErrors($validator);
                 }
                 else
                 {
                    return response()->json(['errors'=>$validator->errors()],401);
                 }

               }


              if(!$admin)
              {
                return response()->json(['message' => 'User not found '] , 404);
              }

              $admin->email = $request->email;
              $admin->phone = $request->phone;
              $admin->username = $request->username;

              $admin->save();

              if ($request['isWeb'])
              {
                $tabName = "branchinfo";
                return redirect('/branch/'.$branch->id.'/'.$tabName)->with([
                  'status' => 'user updated',
                  'tabName' => $tabName
                  ]);
              }

              return response()->json([
                'branch' => $branch,
                'message' => 'branch Updated succefully '] , 200);
          }

       public function searchAllMembers(Request $request)
       {
        // $members = Member::whereHas('user', function($query) use($request) {
        //                 $query->Where('username', 'like', '%' . $request->querydata . '%')
        //                 ->orWhere('email', 'like', '%' . $request->querydata . '%') ;
        //                 })->paginate(5);
        $users = User::where(function($query) use($request) {
                        $query->Where('username', 'like', '%' . $request->querydata . '%')
                        ->orWhere('email', 'like', '%' . $request->querydata . '%') ;
                        })->paginate(5);

        $branch = Branch::where('id',$request->branch_id)->first();
          if ($request->ajax()) {
              return view('includes.member', [
                'users' => $users,
                'branch' => $branch,
                ])->render();
          }
       }

       public function feedUpdateMemberForm(Request $request)
       {
        $member = Member::where('id',$request->member_id)->first();
        $branch = Branch::where('id',$request->branch_id)->first();
          if ($request->ajax()) {
              return view('includes.sheet-member', [
                'member' => $member,
                'branch' => $branch
                ])->render();
          }
       }

       public function updateSheetMember(Request $request , $member_id)
       {
         $validator = Validator::make($request->toArray(), [
           'membershipId' => 'required',
           'membershipName' => 'required',
         ]);
          if ($validator->fails()) {
               return response()->json(['errors'=>$validator->errors()],401);
          }
          else
          {
            $member = Member::find($member_id);
            $searched_member_exist = Member::where('membershipId',$request->membershipId)
            ->where('branch_id',$request->branch_id)->first();
            if($searched_member_exist)
            {
              return response()->json(['errors'=>['membershipId' =>'Membership ID Exist already ! ']],401);
            }
            else
            {
              $member->membershipName = $request->membershipName;
              $member->membershipId = $request->membershipId;
              $member->save();
            }

          }


       }


       public function assignMemberToClub(Request $request)
       {

        $member = Member::where('id',$request->member_id)->first();
        $branch = Branch::where('id',$request->branch_id)->first();

        $exists = DB::table('branch_members')
        ->whereMemberId($member->id)
        ->whereBranchId($branch->id)
        ->count() > 0;
        if ($exists)
        {
          return response()->json([
            'Message' => 'Already Assigned.'
            ] , 401);
        }
        else {
          //---------- (4) ASSIGN MEMBER TO CLUB ----------------
          $member->branch()->syncWithoutDetaching($request->branch_id);
          $member->user_id = $member->user->id;
          return response()->json([
            'Message' => 'Member Assigned succefully.'
            ] , 201);
          //--------------------------
        }

        //---------- RETURN MESSAGE ----------------
        return response()->json([
          'Message' => 'Member Assigned succefully.'
          ] , 201);

       }




       public function checkOrAttachMember(Request $request)
       {
         $validator = Validator::make($request->toArray(), [
           'member_name' => 'required',
           'member_id' => 'required',
         ]);

          if ($validator->fails()) {
               Log::debug($validator->errors());
               return response()->json(['errors'=>$validator->errors()],401);
          }

        $exists = count(Member::where('membershipId',$request->member_id)->where('branch_id',$request->branch_id)->get());
        if($exists)
        {
          $member = Member::where('membershipId',$request->member_id)->first();
          $branch = Branch::where('id',$request->branch_id)->first();

          $attached = DB::table('branch_members')
          ->whereMemberId($member->id)
          ->whereBranchId($branch->id)
          ->count() > 0;

          if ($attached)
          {
            return response()->json([
              'error' => 'Already Assigned.'
              ] , 404);
          }
          else {
            //---------- (4) ASSIGN MEMBER TO CLUB ----------------
            $member->branch()->syncWithoutDetaching($request->branch_id);
            $member->membershipName = $request->member_name;
            $member->isActive = 1;
            $member->branch_id = $branch->id;
            $member->user_id = Auth::user()->id;
            $member->save();

            return response()->json([
              'Message' => 'Member Assigned succefully.'
              ] , 201);

          }
        }
        else
        {
          return response()->json([
            'error' => 'MEMBER NOT EXIST'
            ] , 404);
        }


       }

       public function checkOrAttachMember2(Request $request)
       {
         $validator = Validator::make($request->toArray(), [
           'member_name' => 'required',
           'member_id' => 'required',
         ]);

          if ($validator->fails()) {
               return response()->json(['errors'=>$validator->errors()],401);
          }

        $exists = count(Member::where('membershipId',$request->member_id)
                                ->where('branch_id',$request->branch_id)->get());
        if($exists)
        {
          Log::info('Membership id exsts');
          $member = Member::where('membershipId',$request->member_id)->
                    where('branch_id',$request->branch_id)->first();
          $branch = Branch::where('id',$request->branch_id)->first();

          $attached = DB::table('branch_members')
          ->whereMemberId($member->id)
          ->whereBranchId($branch->id)
          ->count() > 0;

          if ($attached)
          {
            Log::info('already assigned');
            return response()->json([
              'error' => 'Already Assigned.'
              ] , 404);
          }
          else {
            //--- Check if in requests and delete it
            $applications = Application::where('user_id', Auth::user()->id)
            ->where('branch_id',$branch->id)
            ->get();
            if($applications)
            {
              Log::info('Requests found');
              foreach ($applications as $application) {
                $application->delete();
              }
            }

            //---------- (4) ASSIGN MEMBER TO CLUB ----------------
            $member->branch()->syncWithoutDetaching($request->branch_id);
            $member->membershipName = $request->member_name;
            $member->isActive = 1;
            $member->branch_id = $branch->id;
            $member->user_id = Auth::user()->id;
            $member->save();

            return response()->json([
              'Message' => 'Member Assigned succefully.'
              ] , 201);

          }
        }
        else
        {
          Log::info('Membership-ID not found in this branch');
          return response()->json(['errors'=>['member_id' =>'Membership ID doesnt exist in this branch ! <br /> <a href="#"
          data-reason="Membership was not found!" onclick="reportRequestToAdmin(this);">Report to the club admin</a> ']],401);
        }


       }

       public function checkOrAttachMember3(Request $request)
       {
         $validator = Validator::make($request->toArray(), [
           'member_name' => 'required',
           'member_id' => 'required',
         ]);

          if ($validator->fails()) {
               return response()->json(['errors'=>$validator->errors()],401);
          }

        $exists = count(Member::where('membershipId',$request->member_id)
                                ->where('branch_id',$request->branch_id)->get());
        if($exists)
        {
          $member = Member::where('membershipId',$request->member_id)->
                    where('branch_id',$request->branch_id)->first();
          $branch = Branch::where('id',$request->branch_id)->first();

          $attached = DB::table('branch_members')
          ->whereMemberId($member->id)
          ->whereBranchId($branch->id)
          ->count() > 0;

          if ($attached)
          {
            return response()->json([
              'error' => 'Already Assigned.'
              ] , 404);
          }
          else {
            //--- Check if in requests and delete it
            $applications = Application::where('membershipId', $member->membershipId)
            ->where('branch_id',$branch->id)
            ->get();
            foreach ($applications as $application) {
              $application->delete();
            }
            //---------- (4) ASSIGN MEMBER TO CLUB ----------------
            $member->branch()->syncWithoutDetaching($request->branch_id);
            $member->membershipName = $request->member_name;
            $member->isActive = 1;
            $member->branch_id = $branch->id;
            $member->user_id = $request->user_id;
            $member->save();

            return response()->json([
              'Message' => 'Member Assigned succefully.',
              'user_id' => $member->user_id
              ] , 201);

          }
        }
        else
        {
          return response()->json(['errors'=>['member_id' =>'Membership ID doesnt exist in this branch !']],401);
        }


       }


       public function getBranchMembers($branch_id)
       {
           $branch = Branch::where('id', $branch_id )->first();

           $members = Member::where('branch_id',$branch->id)->get();

           return Datatables::of($members)
                  ->addColumn('edit', function(Member $member) use($branch) {
                    return '<button type="button" href="#sheetMemberModal"
                    data-id="'.$member->id.'"
                    data-branch = "'.$branch->id.'"
            	      data-toggle="modal"
                    id="#sheet_member_edit-'.$member->id.'"
                    class="feed_member_modal btn btn-link"><span class="icon-edit"></span></button>';
                })->addColumn('status', function(Member $member) {
                        if($member->user_id && $member->user_id != "")
                        {
                            return '<span class="glyphicon glyphicon-one-fine-dot"></span>';
                        }

                })->rawColumns(['edit', 'status'])->make(true);

       }

      public function getBranchRequests($branch_id)
      {
        $branch = Branch::where('id', $branch_id )->first();
        $applications = Application::where('branch_id',$branch->id)->get();

        return Datatables::of($applications)
                          ->addColumn('actions', function(Application $application) {
                            return '<input data-reqid="'.$application->id.'" id="btn_confirm_req_'.$application->id.'" value="CONFIRM"
                            class="btn btn-default my-3" onclick="confirmReqClick(this)"></input>
                            <input  data-reqid="'.$application->id.'" id="btn_cancel_req_'.$application->id.'" value="CANCEL"
                            class="btn btn-default my-3" onclick="cancelReqClick(this)"></input>';
                        })->addColumn('avatar', function(Application $application) {
                           if(!$application->user->avatarURL || $application->user->avatarURL == "none")
                           {
                             return '<div class="thumb user-thumb"><img src="/images/user.jpg"></div>';
                           }
                           else {
                             return '<div class="thumb user-thumb"><img src="../../../storage/app/public/'.$application->user->avatarURL.'"></div>';
                           }
                         })->addColumn('reason', function(Application $application) {
                            return $application->reason;
                        })->rawColumns(['actions', 'avatar','reason'])->make(true);
      }




      public function getJoinedMembers($branch_id)
      {
        $branch = Branch::where('id', $branch_id )->first();
        $members = $branch->member();
        return Datatables::of($members)
        ->addColumn('avatar', function(Member $member) {
           if(!$member->user->avatarURL || $member->user->avatarURL == "none")
           {
             return '<div class="thumb user-thumb"><img src="/images/user.jpg"></div>';
           }
           else {
             return '<div class="thumb user-thumb"><img src="../../../storage/app/public/'.$member->user->avatarURL.'"></div>';
           }
        })->addColumn('bookings', function(Member $member) {
           return '0 booking';
        })->addColumn('date', function(Member $member) {
           return $member->created_at;
        })->addColumn('status', function(Member $member) {
          if($member->isActive)
          {
          return '<div class="material-switch"><input data-memberid="'.$member->member_id.'" onclick="UpdateMemberActiveStatus(this)"
             type="checkbox" id="updateMember_'.$member->member_id.'" checked ><label for="updateMember_'.$member->member_id.'" class="label-default"></label></div>';
          }else
          {
            return '<div class="material-switch"><input data-memberid="'.$member->member_id.'" onclick="UpdateMemberActiveStatus(this)"
               type="checkbox" id="updateMember_'.$member->member_id.'"><label for="updateMember_'.$member->member_id.'" class="label-default"></label></div>';
          }

        })->addColumn('actions', function(Member $member) {
           return '<button data-memberid="'.$member->user->id.'" onclick="ViewProfile(this)" id="btn_view_profile_'.$member->user->id.'"
           class="btn btn-default my-3">VIEW PROFILE</button>';
        })->rawColumns(['actions', 'avatar', 'bookings', 'date', 'status'])->make(true);
      }


      public function UpdateJoinedMemberStatus(Request $request)
      {
        $member = Member::where('id',$request->member_id)->first();

        if (!$member)
        {
          return response()->json([
              'message' => 'Member Not found '
          ], 401);
        }

        $member->toggleActive()->save();

        return response()->json([
            'Member_status' => $member->isActive,
            'message' => 'Member status switched succefully '
        ], 200);
      }

      public function updateBranchMember(Request $request , $id)
      {
        $member = Member::where('id',$id)->first();
        $tabName = "clubs";
        $membershipId =  $request['membershipId'.$id];

        //Log::debug($request->branch_id);
        if ($request['isWeb'])
        {

        $exists = DB::table('members')
        ->where('membershipId',$membershipId)
        ->where('branch_id',$request->branch_id)
        ->where('id','!=',$id)
        ->count() > 0;


        if ($exists)
          {
          return redirect('/member/'.$member->user->id.'/'.$tabName)->withErrors(
              ['membershipId'.$id =>'Membership id must be unique in this branch']
          );

          }
          else
          {
            $validator = Validator::make($request->toArray(), [
              'phone'.$id => 'required|digits:11'
           ]);

            if ($validator->fails()) {
              return redirect('/member/'.$member->user->id.'/'.$tabName)->withErrors($validator);
            }

            else
            {
              $member->membershipId = $membershipId;
              $member->membershipName = $request->name;
              $member->save();

              $user = $member->user;
              $user->phone = $request['phone'.$id];
              $user->save();
              return redirect('/member/'.$member->user->id.'/'.$tabName);
            }
          }

        }


      }


      public function branchMemberDelete(Request $request)
      {
        $tabName = "clubs";
        $member = Member::where('id',$request->member_id)
                         ->where('branch_id',$request->branch_id)
                         ->first();

        $user_id = $member->user_id;
        // here it search for all the members in the branch member table with same id and delete
        $member->branch()->detach($request->branch_id);

        $member->user_id = null ;
        $member->isActive = 0 ;
        $member->save();

        return redirect('/member/'.$user_id.'/'.$tabName);

      }

      public function getRelCourtsAndTrainers(Request $request)
      {
        $courts_list ='<option value="" selected>Select Court…</option>';
        $trainers_list ='<option value="" selected>Select Trainer…</option>';
        $courts = Court::where("service_sport_id",$request->service_sport_id)->get();
        $trainers = Trainer::where("service_sport_id",$request->service_sport_id)->get();
        foreach ($courts as $court) {
          $courts_list.='<option value="'.$court->id.'">'.$court->name.'</option>';
        }
        foreach ($trainers as $trainer) {
          $trainers_list.='<option value="'.$trainer->id.'">'.$trainer->name.'</option>';
        }

        return response()->json([
            'courts_list' => $courts_list,
            'trainers_list' => $trainers_list,
        ], 200);
      }

      public function getAllRelatedBookings($request)
      {
        $bookings = Booking::where('branch_id', $request->branch_id )
        ->where('confirmed',1)
        ->where(function ($query) use ($request) {
          if($request->current_month != '')
          {
           $query->whereMonth('day', '=', $request->current_month);
          }
          else
          {
            $query->whereMonth('day', '=', date("m")); // get data of the current month
          }
           if($request->service_sport_id != '')
           {
            $query->where('service_sport_id', '=', $request->service_sport_id);
           }
           if($request->trainer_id != '')
           {
            $query->where('trainer_id', '=', $request->trainer_id);
           }
           if($request->court_id != '')
           {
            $query->where('court_id', '=', $request->court_id);
           }
           if($request->payment_type != '')
           {
            $query->where('booking_type', '=', $request->payment_type);
           }
           if($request->status != '')
           {
            $query->where('status', '=', $request->status);
           }
        })
        ->orderBy('day', 'DESC')->orderBy('from', 'DESC');
        return $bookings;
      }

      public function buildBookingsTable(Request $request)
      {
        $bookings = $this->getAllRelatedBookings($request);
        return Datatables::of($bookings)
        ->addColumn('booking_id', function(Booking $booking) {
          return '#'.$booking->booking_id.'';
        })->addColumn('member', function(Booking $booking) {
          $member = Member::where('id',$booking->member_id)->first();
          return $member->membershipName;
        })->addColumn('sport', function(Booking $booking) {
          $service_sport = ServiceSport::where('id',$booking->service_sport_id)->first();
          return $service_sport->sport->name;
        })->addColumn('court', function(Booking $booking) {
          $court = Court::where('id',$booking->court_id)->first();
          return $court->name;
        })->addColumn('date', function(Booking $booking) {
          return $booking->day;
        })->addColumn('time', function(Booking $booking) {
          return $booking->total_time;
        })->addColumn('trainer', function(Booking $booking) {
          if($booking->trainer_id)
          {$trainer = Trainer::where('id',$booking->trainer_id)->first();
          return $trainer->name;}
          else{return 'None';}
        })->addColumn('payment', function(Booking $booking) {
          if($booking->booking_type == "cash"){return 'Cash';}
          if($booking->booking_type == "cc"){return 'Credit Card';}
        })->addColumn('status', function(Booking $booking) {
          if($booking->booking_type == "cc" && $booking->status == "canceled")
          {
            return '<select class="custom-select refund_select border-0">
                        <option data-id="'.$booking->id.'" value="cancel" class="font-weight-bold" selected>Canceled</option>
                        <option data-id="'.$booking->id.'" value="refund" class="font-weight-bold">Refunded</option>
                      </select>';
          }
          return $booking->status;
        })->addColumn('total', function(Booking $booking) {
          return $booking->total_price.' EGP';
        })->addColumn('action', function(Booking $booking) {
          if ( $booking->booking_type =="cash" && $booking->status =="upcoming")
          {
             return ' <div class="dropdown booking-code-menu">
                        <i class="fas fa-ellipsis-v fa-lg dropdown-toggle"
                         type="button" id="dropdownMenu'.$booking->id.'"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                        <div class="dropdown-menu  bg-secondary" aria-labelledby="dropdownMenu'.$booking->id.'">
                            <button class="dropdown-item text-white" type="button" data-toggle="modal"
                            data-id="'.$booking->id.'" onclick="feedCashModal(this);" data-target="#cashPaymentCode">Enter Code</button>
                        </div>
                      </div>';
          }

        })->rawColumns(['booking_id', 'sport', 'court', 'member', 'date', 'time', 'trainer', 'payment', 'status', 'total', 'action'])
        ->make(true);
      }


      public function buildBookingsCalender(Request $request)
      {
        $related_bookings = [];
        $bookings = $this->getAllRelatedBookings($request)->get();
        foreach ($bookings as $booking) {
          $court = Court::where('id',$booking->court_id)->first();
          $member = Member::where('id',$booking->member_id)->first();
          $related_bookings[] = (object) array(
            'title' => $booking->total_time .'-    Court: '.$court->name .'  -    Member: '.$member->membershipName.'  -    Payment Method: '.$booking->booking_type.'  -    Booking Status: '.$booking->status.'  -    Amount: '.$booking->total_price.' EGP' ,
            'start' => $booking->day
          );
        }

        if($request->current_month != '')
        {
        $day = date("Y-".$request->current_month."-01");
        }
        else
        {
        $day = date("Y-m-d");
        }
        return response()->json([
            'bookings' => json_encode($related_bookings),
            'day' => isset($day)?$day:date("Y-m-d")
        ], 200);
      }

      public function buildServiceSportCard(Request $request)
      {
        $current_service_sport = ServiceSport::where('id',$request->current_service_sport_id)->first();
          if ($request->ajax()) {
              return view('branch.sport-container', [
                'current_service_sport' => $current_service_sport
                ])->render();
          }
      }



     /**
      * BRANCH DELETE
      *
      *
      */

      public function deleteBranch($id)
      {
          $branch = Branch::find($id);
          $branch->delete();
          return response()->json(['Message' => 'branch deleted succefully'] , 200);
      }

    //-------------------END OF BRANCH MODEL CRUD  -------------------------
}
