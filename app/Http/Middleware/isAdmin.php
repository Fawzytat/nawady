<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

class isAdmin
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      if (! (Auth::user()->hasRole('sys_admin') || Auth::user()->hasRole('club_admin')) ) {
        return redirect ('/403');
      }

        return $next($request);
    }
}
