<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }

    public function serviceSport()
    {
      return $this->belongsTo('App\ServiceSport');
    }

}
