<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function branch()
    {
      return $this->belongsToMany('App\Branch', 'branch_members', 'member_id', 'branch_id');
    }

    public function toggleActive()
      {
          $this->isActive= !$this->isActive;
          return $this;
      }
}
