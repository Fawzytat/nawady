<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\ResetPasswordNotification;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'phone', 'address' ,'avatarURL', 'password','verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
  *
  * Boot the model.
  *
  */
  // This is only for the email confirmation and it is saved in DB
  // But for the Authintication we use JWT and it is not saved in DB

    // public function sendPasswordResetNotification($token)
    //   {
    //       $this->notify(new ResetPasswordNotification($token, $this->username));
    //   }



    public static function boot()
    {
      parent::boot();

      static::creating(function ($user) {
          $user->token = str_random(40);
      });
    }

        /**
     * Confirm the user.
     *
     * @return void
     */
    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;

        $this->save();
    }

    public function members()
    {
      return $this->hasMany('App\Member');
    }

    public function application()
    {
      return $this->hasMany('App\Application');
    }
}
