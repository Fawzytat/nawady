<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adv extends Model
{
  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }
}
