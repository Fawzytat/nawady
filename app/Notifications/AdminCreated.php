<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;
use App\Member;

class AdminCreated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $user;
    protected $password;
    protected $branch;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user ,$password,$branch )
    {
        $this->user = $user;
        $this->password = $password;
        $this->branch = $branch;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/');
        return (new MailMessage)
                    ->from('contact@nawady.services', 'Nawady system')
                    ->subject('Club added successfully!')
                    ->markdown('email.admin-created', [
                      'user' => $this->user ,
                      'url' => $url,
                      'password' => $this->password,
                      'branch' => $this->branch
                    ]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
