<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Adv;


class AdExpiry extends Notification
{
    use Queueable;

    protected $adv;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Adv $adv)
    {
        $this->adv = $adv;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from('info@nawady.services', 'Nawady Services Sysem')
                    ->subject('Advert expiry warning')
                    ->line('One of your system advertisments is about to expire , Kindly check .')
                    ->action('Go to Adverts Dashboard', url('/home-advs'))
                    ->line('Your friends at Nawady services,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
