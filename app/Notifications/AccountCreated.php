<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;
use App\Member;

class AccountCreated extends Notification
{
    use Queueable;
    protected $user;
    protected $password;
    protected $member;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user ,$password , $member )
    {
        $this->user = $user;
        $this->password = $password;
        $this->member = $member;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Welcome to S-Team.')
                    ->action(' Go to home page ', url('/'))
                    ->line('Your user name : '.$this->user->username.'')
                    ->line('Your password : '.$this->password.' ')
                    ->line('Your Membership name : '.$this->member->membershipName.'')
                    ->line('Your Membership ID : '.$this->member->membershipId.'');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
