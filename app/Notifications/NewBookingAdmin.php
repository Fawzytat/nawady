<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Booking;
use App\Branch;
use App\User;
use App\Member;
use App\Trainer;
use App\Court;
use App\ServiceSport;


class NewBookingAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $booking;
    public $branch;
    public $sport_name;
    public $court_name;
    public $member;
    public $trainer = "";


    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->branch = Branch::where('id',$booking->branch_id)->first();
        $this->sport_name = ServiceSport::where('id',$booking->service_sport_id)->first()->sport->name;
        $this->court_name = Court::where('id',$booking->court_id)->first()->name;
        $this->member = Member::where('id',$booking->member_id)->first();
        if($booking->withTrainer)
        {
          $this->trainer = Trainer::where("id",$booking->trainer_id)->first();
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->from(''.$this->booking->user->email.'', 'Nawady system - '.$this->booking->user->username.'')
                ->subject('New Booking Request')
                ->markdown('email.booking.admin', [
                  'booking' => $this->booking,
                  'branch' => $this->branch,
                  'sport_name' => $this->sport_name,
                  'court_name' => $this->court_name,
                  'member' => $this->member,
                  'trainer' => $this->trainer
                ]);
    }

    public function routeNotificationForMail($notification)
   {
       return $this->booking->user->email;
   }
   // @component('mail::button', ['url' => ''])
   // Button Text Dude
   // @endcomponent

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
