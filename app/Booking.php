<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
  public function payment()
  {
    return $this->hasOne('App\Payment');
  }

  public function booked_trainers()
  {
    return $this->hasOne('App\BookedTrainer');
  }


  public function booked_slots()
  {
    return $this->hasMany('App\Slot');
  }

  public function court()
  {
    return $this->hasOne('App\Court');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

}
