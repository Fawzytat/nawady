<?php

namespace App\Listeners;

use App\Events\NewBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notifiable;
use App\Notifications\NewBookingAdmin;
use App\Notifications\NewBookingSAdmin;

class SendBookingNotifications implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBooking  $event
     * @return void
     */
    public function handle(NewBooking $event)
    {
        $event->club_admin->notify(new NewBookingAdmin($event->booking));
        //$event->system_admin->notify(new NewBookingSAdmin($event->booking));
    }
}
