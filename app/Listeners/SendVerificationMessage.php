<?php

namespace App\Listeners;

use App\Events\NewSignUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notifiable;
use App\Notifications\EmailConfirmation;

class SendVerificationMessage implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSignUp  $event
     * @return void
     */
    public function handle(NewSignUp $event)
    {
        $event->user->notify(new EmailConfirmation($event->user));

    }
}
