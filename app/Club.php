<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{



  public function branches()
  {
    return $this->hasMany('App\Branch');
  }

  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }
}
