<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
  public function branch()
  {
    return $this->belongsTo('App\Branch');
  }

  public function serviceSport()
  {
    return $this->hasMany('App\ServiceSport');
  }

  public function courts()
 {
     return $this->hasManyThrough('App\Court', 'App\ServiceSport');
 }

 public function trainers()
{
    return $this->hasManyThrough('App\Trainer', 'App\ServiceSport');
}


  public function sport()
  {
    return $this->belongsToMany('App\Sport', 'service_sport', 'service_id', 'sport_id')
                ->withPivot('id', 'slot_duration','isActive','slot_price','morning_from','morning_to','afternoon_from','afternoon_to','evening_from','evening_to');
  }

  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }
}
