<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
  public function toggleActive()
    {
        $this->isActive= !$this->isActive;
        return $this;
    }



    public function service()
    {
      return $this->belongsToMany('App\Service', 'service_sport', 'sport_id', 'service_id');
    }


    public function courts()
    {
      return $this->hasMany('App\Court');
    }
}
