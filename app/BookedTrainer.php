<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookedTrainer extends Model
{
  protected $table = 'booked_trainers';

  public function booking()
  {
    return $this->belongsTo('App\Booking');
  }
}
