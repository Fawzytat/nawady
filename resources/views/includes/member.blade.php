
@if(isset($users))
    <div id="load" style="position: relative;">
    @foreach($users as $user)

    @if(! $user->hasRole('sys_admin') && ! $user->hasRole('club_admin'))
     {!! Form::open([ 'url' => 'assignMemberToClub' ,
                     'method' => 'post',
                     'id' => 'assign_member_'.$user->id.''

     ]) !!}

       {{ csrf_field() }}

       <input type="hidden" name="member_id" value="{{ $user->id }}" />
       <input type="hidden" name="branch_id" value="{{ $branch->id }}" />
      <div class="d-flex flex-wrap flex-xl-nowrap assigned-members">
        <div class="thumb">
          @if(! $user->avatarURL || $user->avatarURL == "none")
          <img src="/images/user.jpg">
          @else
          <img src="{{ asset('storage/app/public/').'/'.$user->avatarURL }}">
          @endif
        </div>
        <div class="d-flex justify-content-center align-items-center flex-wrap flex-xl-nowrap">
          <p> {{ $user->username }} </p>
          <p> {{ $user->email }} </p>

          @if(count($user->members->where('branch_id',$branch->id)))
              <p class="pull-right">
             Already Assigned to this branch
              </p>
          @else
          <a data-id="{{ $user->id }}"
             id="{{ 'assign_member_submit_'.$user->id }}"
             onclick="submitAssignForm(this)"
             class="btn btn-default">ASSIGN
           </a>
           <p class="pull-right" style="display:none;" id="{{ 'assignedSuccefully_'.$user->id }}">
             Assigned Succefully
           </p>
           @endif

        </div>
      </div>

      {!! Form::close() !!}
    @endif
    @endforeach
    </div>

    {{ $users->links() }}
@endif
