
        <div class="modal fade" id="cashPaymentCode" tabindex="-1" role="dialog" aria-labelledby="cashPaymentCodeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-uppercase" id="cashPaymentCodeLabel">cash payment reference code</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            {!! Form::open([ 'url' => '/submitCashPayment' ,
                            'method' => 'post',
                            'id' => 'ConfirmPaymentCodeForm'
            ]) !!}

              {{ csrf_field() }}

          <div id="refrence_info"></div>
          </div>
          <div class="modal-footer">
            <button type="button" id="ConfirmPaymentCodesubmit" class="btn btn-primary btn-block">Done</button>
          </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>
