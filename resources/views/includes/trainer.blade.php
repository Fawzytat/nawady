<!-- RESERVE A TRAINER Modal -->
<div class="modal fade dont_clear_me" id="reserveTrainer" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
        	<div class="modal-header">
                <h5 class="modal-title">RESERVE A TRAINER</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div id="trainers_list"></div>
            </div>
        </div>
    </div>
</div>


<!-- <h6>Slots trainer needed in:</h6>
<ul class="nav nav-tabs">
<li class="nav-item">
<a class="nav-link active" href="#">11 AM</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">11:30 AM</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">12 PM</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">12:30 PM</a>
</li>
</ul> -->
