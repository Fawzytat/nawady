<div class="modal fade" id="Booking_receipt_modal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">

        <div class="modal-content">
        	<div class="modal-header card-header bg-primary position-relative">
              <h5 class="text-white text-left">booking Invoice</h5>
              <a href="javascript:window.print()" class="print-link pull-right text-white position-absolute h2" style="right:35px; top:25px;">
                <span class="icon-print"></span>
              </a>
            </div>
            <div class="modal-body">
              <div id="booking_data"></div>
            </div>
            </div>

    </div>
</div>
