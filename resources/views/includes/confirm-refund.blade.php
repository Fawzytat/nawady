<!-- confirmRefundModal -->
<div class="modal fade bd-example-modal-lg" id="confirmRefundModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open([ 'url' => '/refundThisBooking' ,
                      'method' => 'post'
      ]) !!}

        {{ csrf_field() }}
      <div class="modal-body text-center">
        <input type="hidden" name="booking_id" id="bookingToRefundId" value="">
        <p class="lead font-weight-bold">
          Confirm changeing this booking status to refunded?
        </p>
        <i>this action can't be undo!</i>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-outline-primary border-0" data-dismiss="modal">cancel</button>
        <button type="submit" class="btn btn-outline-primary border-0">Confirm Refund</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- confirmRefundModal -->
