<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <div class="row"><h5 class="modal-title">MEMBER REGISTERATION</h5></div>
              <div class="row">

              </div>


                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <p class="text-primary text-center">Welcome to Nawady plateform registration.
Once your account is ready, visit your club branch page to verify your MemberShip.</p>
                <form method="post" id="RegisterForm" name="RegisterForm" action="/signup" enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="form-group" id="register-username">
                        <span class="input">
                          <input class="input__field" type="text" id="username" value="{{ old('username') }}" autofocus name="username" required/>
                          <label class="input__label" for="username">
                            <span class="input__label-content">*  Username</span>
                        </label>
                        </span>
                        <span class="help-block"><strong id="register-errors-username"></strong></span>
                    </div>
                    <div class="form-group" id="register-password">
                        <span class="input">
                          <input class="input__field" type="password" id="password" value="{{ old('password') }}" autofocus name="password" required/>
                          <label class="input__label" for="password">
                            <span class="input__label-content">*  Password</span>
                        </label>
                        </span>
                        <span class="help-block"><strong id="register-errors-password"></strong></span>
                    </div>
                    <div class="form-group" id="register-password-confirm">
                        <span class="input">
                          <input class="input__field" type="password" id="password_confirmation" name="password_confirmation" required/>
                          <label class="input__label" for="password_confirmation">
                            <span class="input__label-content">*  Retype Password</span>
                        </label>
                        <span class="help-block"><strong id="register-errors-password-confirm"></strong></span>
                        </span>
                    </div>
                    <div class="form-group" id="register-email">
                        <span class="input">
                          <input class="input__field" type="email" id="email" value="{{ old('email') }}" autofocus name="email" required />
                          <label class="input__label" for="email">
                            <span class="input__label-content">*  Email</span>
                        </label>
                        </span>
                        <span class="help-block"><strong id="register-errors-email"></strong></span>
                    </div>
                    <div class="form-group" id="register-phone">
                        <span class="input">
                          <input class="input__field" type="text" id="phone" value="{{ old('phone') }}" autofocus name="phone" required/>
                          <label class="input__label" for="phone">
                            <span class="input__label-content">*  Mobile</span>
                        </label>
                        </span>
                        <span class="help-block"><strong id="register-errors-phone"></strong></span>
                    </div>
                    <div class="form-group d-flex justify-content-between align-items-center" id="register-avatar">
                       <label class="inline">Upload Profile Picture</label>
                       <span class="btn btn-default btn-file">
                                CHOOSE FILE <input type="file" id="avatar"  name="avatar" />
                            </span>
                    </div>

            </div>
            <div class="modal-footer">
                <span class="register_success"><strong>Please check your email address to verify your account.</strong></span>
                <button type="submit" class="btn btn-block btn-primary" id="register-form-submit" name="register-form-submit" value="register">REGISTER</button>
                  </form>
            </div>
        </div>
    </div>
</div>
