<!-- RESERVE A TRAINER Modal -->
<div class="modal fade" id="Booking_cancel_modal" tabindex="-1" role="dialog" aria-labelledby="slotsErrorModalLabel" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open([ 'url' => '/cancelUserBooking' ,
                            'method' => 'post'
            ]) !!}

              {{ csrf_field() }}
            <div class="modal-body">
                <input type="hidden" name="booking_id" id="Booking_cancel_id" />
            	  <h5 class="text-center"> Are you sure you want to cancel this booking? </h5>
                <h7 class="text-center"> You will get notified when refund is processed  </h7>
        				<div class="w-100">
        					<button type="button" data-dismiss="modal" class="btn btn-primary btn-block mt-5 mb-3">DON'T CANCEL</button>
                  <button type="submit" class="btn btn-primary btn-block mt-5 mb-3">CANCEL BOOKING</button>
        				</div>
            </div>
              {!! Form::close() !!}
        </div>
    </div>
</div>
