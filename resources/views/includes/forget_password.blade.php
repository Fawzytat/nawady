<!-- Forget password modal -->
<div class="modal fade" id="forgotPwModal" tabindex="-1" role="dialog" aria-labelledby="forgotPwModal" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Retrive Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/password/email">
                  {{ csrf_field() }}
                    <div class="form-group">
                        <span class="input">
                          <input class="input__field" type="email" name="email" />
                          <label class="input__label" for="email">
                            <span class="input__label-content">Registered Email</span>
                        </label>
                        </span>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-block btn-primary">Reset Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
