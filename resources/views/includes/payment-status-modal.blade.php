<!-- Status Modal -->
<div class="modal fade bd-example-modal-lg" id="paymentStatusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title text-uppercase" id="statusModalLabel">paid status confirmation</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        {!! Form::open([ 'url' => '/submitDeposit' ,
                        'method' => 'post'
        ]) !!}

          {{ csrf_field() }}

        <input type="hidden" name="branch_id" id="payment_branch_id">
        <input type="hidden" name="month" id="payment_month">
        <input type="hidden" name="year" id="payment_year">
        <p class="lead font-weight-bold">
          Confirm changeing this payment status to paid?
        </p>
        <i>this action will send confirmation to club admin. <br> this action can't be undo!</i>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-outline-primary border-0" data-dismiss="modal">cancel</button>
        <button type="submit" class="btn btn-outline-primary border-0">Confirm Payment</button>

        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- End Status Modal -->
