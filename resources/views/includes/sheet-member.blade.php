@if(isset($member))
      <div class="modal-header">
          <h5 class="modal-title"> EDIT MEMBER DATA </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
       {!! Form::open([ 'url' => '/updateSheetMember/'.$member->id.'',
                       'method' => 'put',
                       'id' => 'updateSheetMember'

       ]) !!}

         {{ csrf_field() }}

        <input type="hidden" id="member_id" name="member_id" value="{{ $member->id }}" />
        <input type="hidden" id="branch_id" name="branch_id" value="{{ $branch->id }}" />
        <div class="form-group" id="sheet-membershipName">
            <span class="input input--filled">
              <input class="input__field" type="text" id="membershipName" value="{{ $member->membershipName }}" autofocus name="membershipName" required/>

              <label class="input__label" for="text">
                <span class="input__label-content">membership Name</span>
            </label>
            </span>
            <span class="help-block"><strong id="updateSheetMember-errors-membershipName"></strong></span>
        </div>

        <div class="form-group" id="sheet-membershipId">
            <span class="input input--filled">
                <input class="input__field" type="text"  id="membershipId" value="{{ $member->membershipId }}" autofocus name="membershipId" required/>
                <label class="input__label" for="text">
                    <span class="input__label-content">membership ID</span>
                </label>
            </span>
            <span class="help-block"><strong id="updateSheetMember-errors-membershipId"></strong></span>
        </div>


      </div>
      <div class="modal-footer">
          <button  class="btn btn-block btn-primary" id="updateSheetMember_submit" name="updateSheetMember_submit" value="Update">Update</button>
      </div>

      {!! Form::close() !!}
@endif

<script>
$('#updateSheetMember_submit').click(function(e){

e.preventDefault();
clearAndResetLoginInputs();
 var updateSheetMember = $("#updateSheetMember");
 var member_id = $('#member_id').val();
 var formData = updateSheetMember.serialize();


        $.ajax({
            url:'/updateSheetMember/'+member_id+'',
            type:'PUT',
            data:formData,
            success:function(data){
              window.location.reload();
            },
            error: function (data) {
               appendAllLoginErrors(data);
            }
        });
    });
</script>
