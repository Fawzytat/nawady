
  @if(Auth::user())

  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="slide-nav">
      <a href="#" class="nav-link">
          <div class="user-controls">
              <div class="d-flex align-items-center">
                  <div class="thumb user-thumb">
                      @if(!Auth::user()->avatarURL || Auth::user()->avatarURL == "none")
                      <img src="/images/user.jpg">
                      @else
                      <img src="{{ asset('storage/app/public/').'/'.Auth::user()->avatarURL }}">
                      @endif
                  </div>
                      @if(Auth::user()->hasRole('sys_admin'))
                        <p>{{ Auth::user()->username }}</p>
                      @elseif(Auth::user()->hasRole('club_admin'))
                        <p>{{ Auth::user()->username }}</p>
                      @else
                        <p>{{ Auth::user()->username }}</p>
                      @endif
              </div>
          </div>
      </a>
      @if(Auth::user()->hasRole('sys_admin'))
      <a href="/club-list" class="nav-link">CLUBS</a>
      <a href="/home-advs" class="nav-link">HOMEPAGE ADVS</a>
      @elseif(Auth::user()->hasRole('club_admin'))
      <a href="/myaccount" class="nav-link">My Account</a>
      @else
      <a href="/myaccount" class="nav-link">My Account</a>
      @endif
      <!-- <a href="#" class="nav-link">Logout</a> -->
      <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          LOGOUT
      </a>
  </nav>

  @endif

<header class="container-fluid bg-primary">
	<div class="d-flex d-md-none justify-content-center">
		@foreach($logos as $logo)
		<a class="nav-link" target="_blank" href="{{ $logo->link }}"><img src="{{ asset('storage/app/public/').'/'.$logo->url }}"></a>
		@endforeach
	</div>
	<ul class="nav">
		<li class="nav-item">
			<a class="nav-link logo" href="/">
			<img src="/images/logo.png" alt="steam logo">
			</a>
		</li>
		<li class="nav-item sponsors d-none d-md-flex">
			@foreach($logos as $logo)
			<a class="nav-link" href="{{ $logo->link }}"><img src="{{ asset('storage/app/public/').'/'.$logo->url }}"></a>
			@endforeach
		</li>


		@guest
		<li class="nav-item login-item">
			<a class="nav-link" href="#loginModal" data-toggle="modal"><span class="icon-user"></span></a>
		</li>
		@else

		<li class="nav-item user-controls d-none d-md-flex">
			<!-- HOMEPAGE ADS -->
			@if(Auth::user()->hasRole('sys_admin'))
			<a class="nav-link" href="/home-advs">
				HOMEPAGE ADS
			</a>
			@endif
			<!-- HOMEPAGE ADS -->

			<!-- LOGOUT LINK -->
			<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">	LOGOUT</a>
			<!-- END LOGOUT LINK -->



                  <!--  NAME AND TITLE -->
                  <a class="nav-link" href="/myaccount">
                      <div class="d-flex align-items-center">
                        @if(Auth::user()->hasRole('sys_admin'))
                          <p>{{ Auth::user()->username }}</p>
                        @elseif(Auth::user()->hasRole('club_admin'))
                          <p>{{ Auth::user()->username }}</p>
                        @else
                        <p>{{ Auth::user()->username }}</p>
                        @endif
                          <div class="thumb user-thumb">
                            @if(Auth::user()->avatarURL == "none")
                            <img src="/images/user.jpg">
                            @else
                            <img src="{{ asset('storage/app/public/').'/'.Auth::user()->avatarURL }}">
                            @endif
                          </div>
                      </div>
                  </a>
                  <!-- END NAME AND TITLE -->

                  <!-- HAMBURGER MENU -->
                  @if(Auth::user())
                  <li class="nav-item d-block d-md-none">
                      <button class="hamburger hamburger--arrow-r" type="button" id="showBtn">
                          <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                          </span>
                      </button>
                  </li>
                  @endif

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>
			<!-- END HAMBURGER MENU -->
		</li>


		@endguest
	</ul>
</header>
