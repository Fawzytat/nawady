<!-- View Receipt Modal -->
<div class="modal fade" id="viewUploadedReceiptModal" tabindex="-1" role="dialog" aria-labelledby="viewUploadedReceiptModal"
 aria-hidden="true">
<div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
<div class="modal-content">
<div class="modal-header">
  <h5 class="modal-title text-uppercase ml-0" id="viewUploadedReceiptModalLabel">bank receipt</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
<div id="rec_img_container"></div>
</div>
<div class="modal-footer">
  <button type="button" data-dismiss="modal" class="btn btn-primary btn-block">Close</button>
</div>
</div>
</div>
</div>
<!-- End View Receipt Modal -->
