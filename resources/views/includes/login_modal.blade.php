<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">MEMBER LOGIN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="LoginForm" name="LoginForm"  >
                  {{ csrf_field() }}
                  <div class="form-group" id="login-username">
                      <span class="input">
                        <input class="input__field" type="text" id="login_username" name="username" required/>
                        <label class="input__label" for="username">
                          <span class="input__label-content">Username</span>
                      </label>
                      </span>
                      <span class="help-block"><strong id="login-errors-username"></strong></span>
                  </div>
                  <div class="form-group" id="login-password">
                      <span class="input">
                        <input class="input__field" type="password" id="login_password" name="password" required/>
                        <label class="input__label" for="password">
                          <span class="input__label-content">Password</span>
                      </label>
                      </span>
                      <span class="help-block"><strong id="login-errors-password"></strong></span>
                  </div>

                  <a href="{{ route('password.request') }}" >Forgot Password?</a>
                <!-- <a href="#forgotPwModal" data-toggle="modal">Forgot Password?</a> -->
            </div>
            <div class="modal-footer">
                <button  type="button" id="login_form_submit" value="login" name="login-form-submit" class="btn btn-block btn-primary">Login</button>

                <div class="w-100 py-3 d-flex justify-content-between">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="staySignedIn">
                        <label class="custom-control-label" for="staySignedIn">Stay signed in</label>
                    </div>
                    <a href="#">Need Help?</a>
                </div>
                <div class="w-100 py-3 d-flex justify-content-center">
                  <a href="#registerModal" data-toggle="modal">Create an account</a>
                </div>

                </form>
            </div>

        </div>
    </div>
</div>
