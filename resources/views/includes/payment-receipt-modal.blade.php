<!-- View Receipt Modal -->
<div class="modal fade" id="viewReceiptModal" tabindex="-1" role="dialog" aria-labelledby="viewReceiptModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
<div class="modal-content">
<div class="modal-header">
  <h5 class="modal-title text-uppercase ml-0" id="viewReceiptModalLabel">upload bank receipt</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
    {!! Form::open([ 'url' => '/uploadBankReceipt' ,
                     'method' => 'post',
                     'enctype' => 'multipart/form-data'

     ]) !!}

       {{ csrf_field() }}

  <div class="form-group inline mt-3">
    <input type="hidden" name="branch_id" id="payment_r_branch_id">
    <input type="hidden" name="month" id="payment_r_month">
    <input type="hidden" name="year" id="payment_r_year">
    <label class="inline text-capitalize">upload receipt</label>
    <span class="btn btn-default btn-file text-uppercase">
      choose file
      <input type="file" name="payment_reciept">
    </span>
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary btn-block">Save</button>
  {!! Form::close() !!}
</div>
</div>
</div>
</div>
<!-- End View Receipt Modal -->
