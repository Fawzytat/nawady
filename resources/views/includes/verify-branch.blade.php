
<div class="modal fade" id="memberAddClubModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div id="beforeReporting">
                  <div id="first_message">
                    <p>
                      You have to be a member in this club to book courts, if you are a member please add your membership ID
                    </p>

                  </div>
                  <div id="second_message" style="display:none;">
                    <span class="help-block"><strong>This membership-ID is already assigned to other member. Please enter other valid membership-ID </strong></span>
                    <p>or <a href="#" data-reason="Membership-ID is already taken!" onclick="reportRequestToAdmin(this);">Report to the club admin</a></p>
                  </div>

                  <form method="post" id="attachMemberForm" name="attachMemberForm">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ $branch->id }}" name="branch_id" id="attachFormBranchId" />
                     <div class="form-group" id="attach-member-name">
                          <span class="input input-invalid">
                            <input class="input__field" type="text" name="member_name" required />
                            <label class="input__label" for="member_name">
                              <span class="input__label-content">Member Name</span>
                          </label>
                          </span>
                          <span class="help-block"><strong id="attach-errors-member-name"></strong></span>
                      </div>
                      <div class="form-group" id="attach-member-id">
                          <span class="input">
                            <input class="input__field" type="text" name="member_id" required />
                            <label class="input__label" for="member_id">
                              <span class="input__label-content">Membership ID</span>
                          </label>
                          </span>
                          <span class="help-block"><strong id="attach-errors-member-id"></strong></span>
                      </div>
                      <div class="w-100 d-flex flex-row-reverse">
                    <button  id="attach_form_submit" class="btn btn-link">SUBMIT</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">CANCEL</button>
                    </div>
                  </form>

                </div>

                <div id="reported_message" style="display:none;">
                  <p>
                    Your data are sent to the club admin to check , you will be notified once he confirms your data.
                  </p>

                  <div class="d-flex flex-column align-items-center my-5">
                    <p>For Follow up</p>
                    <a href="tel:{{ (isset($branch_admin->phone)?$branch_admin->phone : '') }}"><span class="icon-phone"></span>CALL BOOKING CENTER</a>
                    <p>{{ (isset($branch_admin->phone)?$branch_admin->phone : '') }}</p>
                  </div>
                  <div class="w-100 d-flex justify-content-center">
                    <button type="button" class="btn btn-link" onclick="page_location_reload();" data-dismiss="modal">ok</button>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>
