<div class="card-body d-print">
  <div class="thumb">
      @if(! $club->logoUrl || $club->logoUrl == "none")
      <img src="/images/club.png">
      @else
      <img src="{{ asset('storage/app/public/').'/'.$club->logoUrl }}">
      @endif
  </div>
  <div class="booking-status">
    <h5>Booking ID  <span>#{{ $booking->booking_id }}</span></h5>
    <h6> Your booking details </h6>
    <p>Club:<span>{{ $club_name }}</span></p>
    <p>Branch:<span>{{ $branch_name  }}</span></p>
    <p>Court:<span>{{ $court_name }}</span></p>
    <p>Time:<span>{{ $booking->total_time }}</span></p>
    <p>Date:<span>{{ date('D',strtotime($booking->day)) }}, {{ $booking->day }}</span></p>
    @if($booking->withTrainer)
    <p>Trainer:<span>{{ $trainer_name }}</span></p>
    @endif
    @if($booking->booking_type == "cash")
    <p>Payment Code:<span>{{ $booking->payment_code }}</span></p>
    @endif
    @if($booking->booking_type == "cc")
    <p>Transaction No<span>{{ $booking->payment->migs_TransactionNo }}</span></p>
    <p>Card No<span>{{ $booking->payment->migs_CardNum }}</span></p>
    <p>Receipt No<span>{{ $booking->payment->migs_ReceiptNo }}</span></p>
    @endif
    <p>Total:<span>{{ $booking->total_price }} EGP</span></p>
    @if(Auth::user()->hasRole('member') || Auth::user()->hasRole('member'))
    <h6> Name : <span>{{ Auth::user()->members->where('branch_id',$booking->branch_id)->first()->membershipName }}</span> </h6>
    <h6> Address : <span>{{ Auth::user()->address }}</span> </h6>
    @endif
  </div>
</div>
<div class="w-100">
<button type="button" data-dismiss="modal"  class="btn btn-primary btn-block mt-5 mb-3">DONE</button>
</div>
