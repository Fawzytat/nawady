<!-- RESERVE A TRAINER Modal -->
<div class="modal fade dont_clear_me" id="slotsErrorModal" tabindex="-1" role="dialog" aria-labelledby="slotsErrorModalLabel" aria-hidden="true">
    <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            	  <h5 class="text-center" id="slots_error_message"></h5>
        				<div class="w-100">
        					<button type="button" data-dismiss="modal" class="btn btn-primary btn-block mt-5 mb-3">OK</button>
        				</div>
            </div>
        </div>
    </div>
</div>
