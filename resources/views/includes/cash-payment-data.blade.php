  <div class="reference-info">
              <input type="hidden" name="booking_id" value="{{ $booking->id }}">
              <small>
                <b>Booking ID:</b> {{ $booking->booking_id }}
              </small>
              <br>
              <small>
                <b>Members Name:</b> {{ $membershipName }}
              </small>
  </div>
  <div class="form-group" id="cash-payment-code">
    <span class="input">
      <input type="text" name="payment_code" class="input__field">
      <label for="" class="input__label">
        <span class="input__label-content text-capitalize">Enter Code</span>
      </label>
    </span>
    <span class="help-block"><strong id="cash-payment-error"></strong></span>
  </div>
