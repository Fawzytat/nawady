<div class="col-lg-6">
  <div class="card">
    <div class="card-header bg-failure">
      <span class="icon-error">×</span>
      <h1>We Are Sorry</h1>
    </div>
    <div class="card-body">
      <h2 class="card-title">Your booking could not be processed</h2>
      <p>Please make sure you entered the wright credit card number or use another card for payment</p>
      <div class="d-flex flex-row-reverse">
        <a href="#" class="btn btn-link">TRY AGAIN</a>
        <a href="#" class="btn btn-link btn-delete">CANCEL</a>
      </div>
    </div>
  </div>
</div>
