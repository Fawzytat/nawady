@if(count($trainers) == 0)
  <p class="text-center"> There is no available trainers </p>
@else
<h6>Select Trainer:</h6>

  <form id="trainers_list_form">

    @foreach($trainers as $trainer)
      <div class="custom-control custom-radio">
      <input type="radio"
       trainer-price="{{ $trainer->price_per_session }}" trainer-id="{{ $trainer->id }}"
       trainer-name="{{ $trainer->name }}" value="{{ $trainer->price_per_session }}"
       name="trainer" id="{{ 'trainer_'.$trainer->id }}"
        class="custom-control-input trainer_radio">
      <label class="custom-control-label"  for="{{ 'trainer_'.$trainer->id }}">
      <div class="d-flex align-items-center">
        <div class="thumb">
          @if(! $trainer->avatarURL || $trainer->avatarURL == "none")
          <img src="/images/user.jpg">
          @else
          <img src="{{ asset('storage/app/public/').'/'.$trainer->avatarURL }}">
          @endif
        </div>
        <div>
          <p> {{ $trainer->name }}</p>
          <small>{{ $trainer->price_per_session }} EGP</small>
        </div>
      </div>
      </label>
      </div>
    @endforeach
    @endif
      <div class="w-100">
      <button type="button" data-dismiss="modal"  class="btn btn-primary btn-block mt-5 mb-3">DONE</button>
      </div>
</form>

<script>
$(function(){
  $('.trainer_radio').click(function(){
    if ($(this).is(':checked'))
    {
      var trainer_price = parseInt($(this).attr('trainer-price'));
      var trainer_id = parseInt($(this).attr('trainer-id'));
      var current_price = parseInt($("#booking_total").html());
      var no_of_slots = parseInt($("#current_no_of_slots").val());
      if($("#selected_trainer_price").val() == "")
      {
        var current_selected_price = 0;
      }
      else
      {
          var current_selected_price = $("#selected_trainer_price").val();
      }
      var new_price = ((trainer_price * no_of_slots ) + current_price) - current_selected_price;
      $("#booking_total").html("");
      $("#current_booking_total").val("");
      $("#selected_trainer_id").val("");
      $("#selected_trainer_price").val("");
      $("#booking_total").html(new_price);
      $("#current_booking_total").val(new_price);
      $("#selected_trainer_id").val(trainer_id);
      $("#selected_trainer_price").val(trainer_price);

      $("#reserve_btn").hide();
      $("#booked_trainer_data").html($(this).attr('trainer-name'));
      $("#booked_trainer_data").show();
      $("#booked_trainer_clear").show();
      console.log(new_price);
    }
  });
});
</script>
