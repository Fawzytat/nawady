@extends('layouts.main')
@section('content')

<!-- =================  LOGIN MODAL ===================== -->
      @include('includes.login_modal')
<!-- ================= END LOGIN MODAL ===================== -->
<!-- =================  REGISTER MODAL ===================== -->
      @include('includes.register_modal')
<!-- ================= END REGISTER MODAL ===================== -->
<!-- =================  FORGET PASSWORD MODAL ===================== -->
      @include('includes.forget_password')
<!-- ================= END FORGET PASSWORD MODAL ===================== -->



<section class="container-fluid">
    <div class="row">
        @include('partials.flash')
    </div>
    <div class="row">
        <div class="col-lg-4">
           @if(Auth::user())
             <div class="card d-none d-md-flex">
                 <h1>{{ $setting->welcome_note }}</h1>
                 @if(Auth::user()->hasRole('sys_admin'))
                 <a href="/club-list"  class="btn btn-primary">VIEW CLUB LISTING</a>
                 @endif
                 @if(Auth::user()->hasRole('club_admin'))
                 <a href="{{'/branch/'.$all_branches->where('admin_id', Auth::user()->id )->first()->id.'/booking'}}"  class="btn btn-primary">VIEW MY CLUB</a>
                 @endif
                 @if(Auth::user()->hasRole('member'))
                 <a href="/myaccount"  class="btn btn-primary">VIEW MY ACCOUNT</a>
                 @endif
             </div>
           @else
             <div class="card d-none d-md-flex">
                 <h1>{{ $setting->welcome_note }}</h1>
                 <a href="#loginModal" data-toggle="modal" class="btn btn-primary">LOGIN / REGISTER</a>
             </div>
           @endif

            <div class="owl-carousel owl-theme ads-carousel">
                @foreach($carousels as $carousel)
                @if($carousel->enddate)
                  @if( date("Y-m-d") < date('Y-m-d',strtotime($carousel->enddate)) && date("Y-m-d") > date('Y-m-d',strtotime($carousel->startdate)))
                  <a href="{{ $carousel->link }}" target="_blank"><img src="{{ asset('storage/app/public/').'/'.$carousel->url }}"></a>
                  @endif
                @else
                <a href="{{ $carousel->link }}" target="_blank"><img src="{{ asset('storage/app/public/').'/'.$carousel->url }}"></a>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-5 order-md-3 order-lg-2 owl-carousel owl-theme clubs-carousel">
          @foreach($clubs->chunk(9) as $chunk)
            <div class="row">
              @foreach($chunk as $club)
              @if(count($club->branches->where('isActive',1)))
              <div class="col-4 club-thumb">
                <!-- MEMBER PROCESSING -->
                    @if(Auth::user())
                        @if( count( Auth::user()->members) && count( collect($branches) ) )
                            @if( count( collect($branches)->whereIn('id', $club->branches->pluck('id')->toArray())->where('isActive',1) ) )
                               <a href="/club-page/{{ collect($branches)->whereIn('id', $club->branches->pluck('id')->toArray())->where('isActive',1)->first()->id }}/court-booking/default">
                            @else
                            <a href="/club-page/{{ $club->branches->first()->id }}/court-booking/default">
                            @endif
                        @else
                          <a href="/club-page/{{ $club->branches->where('isActive',1)->first()->id }}/court-booking/default">
                        @endif
                    @else
                    <a href="/club-page/{{ $club->branches->where('isActive',1)->first()->id }}/court-booking/default">
                    @endif

                      <div class="thumb-wrap">
                        @if(Auth::user())
                          @if( count( Auth::user()->members) )
                            @if( count( collect($branches)->whereIn('id', $club->branches->pluck('id')->toArray())->where('isActive',1) ) )
                            <span class="icon-user"></span>
                            @endif
                          @endif
                        @endif
                        <div class="thumb">
                            @if(! $club->logoUrl || $club->logoUrl == "none")
                            <img src="/images/club.png">
                            @else
                            <img src="{{ asset('storage/app/public/').'/'.$club->logoUrl }}">
                            @endif

                        </div>
                      </div>
                        <h2>{{ $club->name }}</h2>
                    </a>



                <!-- MEMBER PROCESSING -->
              </div>
              @endif
              @endforeach
            </div>
            @endforeach
        </div>
        <div class="col-lg-3 order-md-2 order-lg-3">
            <div class="ads-space ads-space-sm">
                @foreach($sidebar_images as $sidebar_image)
                  @if($sidebar_image->enddate)
                    @if( date("Y-m-d") < date('Y-m-d',strtotime($sidebar_image->enddate)) && date("Y-m-d") > date('Y-m-d',strtotime($sidebar_image->startdate)) )
                      <a href="{{ $sidebar_image->link }}" target="_blank"><img src="{{ asset('storage/app/public/').'/'.$sidebar_image->url }}" alt=""></a>
                    @endif
                  @else
                  <a href="{{ $sidebar_image->link }}" target="_blank"><img src="{{ asset('storage/app/public/').'/'.$sidebar_image->url }}" alt=""></a>
                  @endif
                @endforeach

            </div>
        </div>
    </div>
</section>

@endsection

@section('customScript')
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
    <script src="js/register.js"></script>
    <script src="js/login.js"></script>
@endsection
