<!-- =================  payment-status-modal ===================== -->
       @include('includes.payment-status-modal')
<!-- ================= payment-status-modal ===================== -->

<!-- =================  confirm-refund ===================== -->
       @include('includes.payment-receipt-modal')
<!-- ================= confirm-refund ===================== -->
<!-- =================  confirm-refund ===================== -->
       @include('includes.payment-receipt-uploaded-modal')
<!-- ================= confirm-refund ===================== -->



<div class="card mb-5" id="payment-reporting-card">
        <!-- Payment Reporting head -->
        <div class="row mb-3" id="payment-reporting-card-header">
            <div class="col-12">
                <h1 class="text-dark text-capitalize">Current Month: 01 {{ date('M') }} - {{ date('d M Y') }}</h1>
            </div>
            <div class="col-12">
                <div class="row" id="payment-reporting-card-header-content">
                    <div class="col-3">
                    <h4>online booking</h4>
                    <p>&#8203; &#8203; Total : {{ $online_bookings_total }} EGP</p>
                    <p>- Refund : {{ $total_refund }} EGP</p>
                    <p>- Bank Fees : (3%): {{ $bank_fees }} EGP</p>
                    <p>Total: {{ $final_online_total }} EGP</p>
                </div>
                <div class="col-3">
                    <h4>Cash booking</h4>
                    <p> &#8203; </p>
                    <p> &#8203; </p>
                    <p> &#8203;</p>
                    <p>total: {{ $cash_bookings_total }} EGP</p>
                </div>
                <div class="col-3">
                    <p> &#8203; </p>
                    <p> &#8203; </p>
                    <p> &#8203; </p>
                    <p> &#8203;</p>
                    <h4>Final Total: {{ $final_online_total + $cash_bookings_total}} EGP</p>
                </div>
                </div>
            </div>
        </div>
        <!-- End Payment Reporting head -->

        <!-- payment years -->
        <div class="row mt-4 mb-4" id="payment-reporting-card-links">
            <div class="d-flex justify-content-start ml-3">
                <a data-year="2018" class="payment_year anchor_link active" >2018</a>
                <a data-year="2017" class="payment_year anchor_link">2017</a>

            </div>
        </div>
        <!-- End payment years -->


        <div>

            <input type="hidden" id="current_year" value="{{ date('Y') }}" />
            <table class="table table-bordered" id="month-payments-table">
                 <thead>
                     <tr>
                       <th>Club</th>
                       <th>Month</th>
                       <th>Total booking</th>
                       <th>Revenue</th>
                       <th>Bank fees (3%)</th>
                       <th>Total refund</th>
                       <th>Total amount</th>
                       <th>Recieipt</th>
                       <th>Status</th>
                     </tr>
                 </thead>
             </table>
        </div>





</div>
