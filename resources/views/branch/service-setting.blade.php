<ul class="nav nav-subtab" role="tablist">
	@foreach($branch->services as $service)
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="{{'#'.$service->short_name}}" role="tab" aria-selected="true">{{ $service->name }}</a>
	</li>
	@endforeach
</ul>

<div class="tab-content">
	@foreach($branch->services as $service)
	<div class="tab-pane fade active show" id="{{$service->short_name}}" role="tabpanel">
		<!-- ======  SPORTS  MODAL  ======= -->
		<div class="modal fade" id="addSportModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title ml-0">ADD SPORT</h4>
					</div>
					<div class="modal-body">
						{!! Form::open([ 'url' => '/service/sport/add' , 'method' => 'post' ]) !!} {{ csrf_field() }}
						<input type="hidden" name="service_id" value="{{ $service->id }}" />
						<select class="custom-select" name="sport_id" placeholder="Choose Sport…">
							@foreach($sports as $sport)
							<option value="{{ $sport->id }}">{{ $sport->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary btn-block">ADD</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<!-- ======  SPORTS  MODAL  ======= -->
		<!-- ========================== START OF SERVICE SETTING ============================== -->
		<div class="card">
			<h2>Service Settings</h2>
			{!! Form::open([ 'url' => '/services/'.$service->id.'' , 'method' => 'put' , 'id' => 'serviceSettings_'.$service->id.'' ,
			'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap' , 'enctype' => 'multipart/form-data' ]) !!} {{ csrf_field()
			}}
			<div class="form-group inline order-3 order-md-1">
				<label class="inline">Service Active</label>
				<div class="material-switch">
					<input type="hidden" value="" name="service_status" id="{{ 'serviceActive_'.$service->id }}" /> @if($service->isActive)
					<input onclick="handleServiceClick(this)" data-serviceid="{{ $service->id }}" id="{{ 'serviceActive'.$service->id }}" type="checkbox"
					    checked disabled="true"> @else
					<input onclick="handleServiceClick(this)" id="{{ 'serviceActive'.$service->id }}" data-serviceid="{{ $service->id }}" type="checkbox"
					    disabled="true"> @endif
					<label for="{{ 'serviceActive'.$service->id }}" class="label-default"></label>
				</div>
			</div>
			<div class="form-group order-4 order-md-2 {{ $errors->has('bookingPerDay') ? ' has-error' : '' }}">
				<span class="input input--filled">
					<input class="input__field" name="bookingPerDay" value="{{ $service->max_book_per_day }}" disabled="disabled" type="tel"
					    required>
					<label class="input__label" for="bookingPerDay">
						<span class="input__label-content"> Max bookings allowed for member (per day)</span>
					</label>
				</span>
				@if ($errors->has('bookingPerDay'))
				<span class="help-block">
					<strong>{{ $errors->first('bookingPerDay') }}</strong>
				</span>
				@endif
			</div>
			<div class="form-group order-5 order-md-3 {{ $errors->has('durationPerBooking') ? ' has-error' : '' }}">
				<span class="input input--filled">
					<input class="input__field" name="durationPerBooking" value="{{ $service->max_duration_per_booking }}" disabled="disabled"
					    type="text" required>
					<label class="input__label" for="durationPerBooking">
						<span class="input__label-content"> Max No. of slots allowed for member (per booking) </span>
					</label>
				</span>
				@if ($errors->has('durationPerBooking'))
				<span class="help-block">
					<strong>{{ $errors->first('durationPerBooking') }}</strong>
				</span>
				@endif
			</div>
			<div class="form-group order-6 order-md-4 {{ $errors->has('cancelationLimit') ? ' has-error' : '' }}">
				<span class="input input--filled">
					<input class="input__field" name="cancelationLimit" value="{{ $service->cancel_limit }}" disabled="disabled" type="text"
					    required>
					<label class="input__label" for="cancelationLimit">
						<span class="input__label-content"> Booking Cancellation within (in hours)</span>
					</label>
				</span>
				@if ($errors->has('cancelationLimit'))
				<span class="help-block">
					<strong>{{ $errors->first('cancelationLimit') }}</strong>
				</span>
				@endif
			</div>
			<div class="btns-wrap order-2 order-md-5">
				<button type="submit" class="btn btn-primary btn-save d-none" data-form-target="{{ 'serviceSettings_'.$service->id }}">save</button>
				<button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
			</div>
			<button type="button" class="btn btn-link order-1 order-md-6" data-form-target="{{ 'serviceSettings_'.$service->id }}">
				<span class="icon-edit"></span>
			</button>
			{!! Form::close() !!}
		</div>
		<!-- ========================== END OF SERVICE SETTING  ============================== -->

		<!-- ========================== START OF SPORTS LIST  BUTTONS ============================== -->
		<ul class="nav sports-subtab" role="tablist">
			@foreach($service->sport as $key => $sport)
			<!-- NO CHANGES HERE  -->
			 @if(!empty($subTabName) && $subTabName == 'default')
			<li class="nav-item @if($key == 0) active show @endif" data-toggle="tooltip" title="{{ $sport->name }}"
				 data-servicesportid="{{$sport->pivot->id}}" onclick="updateSPcard(this)">
				<a class="nav-link @if($key == 0) active show @endif" data-toggle="tab" href="{{'#sport'.$sport->pivot->id}}" role="tab"
				    aria-selected="false">
					@else
					<li class="nav-item {{ !empty($subTabName) && $subTabName == $sport->pivot->id ? 'show active' : '' }}"
						data-toggle="tooltip" title="{{ $sport->name }}" data-servicesportid="{{$sport->pivot->id}}"
						onclick="updateSPcard(this)">
						<a class="nav-link {{ !empty($subTabName) && $subTabName == $sport->pivot->id ? 'show active' : '' }}"
							data-toggle="tab" href="{{'#sport'.$sport->pivot->id}}" role="tab" aria-selected="false">
							
				@endif

							<span class="h5">{{ $sport->name }}</span>

						</a>
					</li>
			@endforeach
					<a href="#addSportModal" data-toggle="modal">ADD SPORT</a>
					<input type="hidden" id="current_service_sport_id" value="{{ $current_service_sport->id }}" />
		</ul>
		<!-- ========================== END OF SPORTS LIST BUTTONS ============================== -->
		<!-- ========================== START OF SPORTS LIST  CONTENT============================== -->
		<div class="tab-content">
			<!-- ===================== START OF SINGLE SPORT TAB ========================= -->
			@foreach($service->sport as $key => $sport)
			 @if(!empty($subTabName) && $subTabName == 'default')
			<div class="tab-pane fade @if($key == 0) active show @endif" id="{{'sport'.$sport->pivot->id}}" role="tabpanel">
		   @else
				<div class="tab-pane fade {{ !empty($subTabName) && $subTabName == $sport->pivot->id ? 'show active' : '' }}" id="{{'sport'.$sport->pivot->id}}"
				    role="tabpanel">
		   @endif
			   <!-- endforeach -->
					<!-- ========  SPORT SWITCHER ======= -->
					<!-- current service sport id -->

					<div class="form-group inline">
						<label class="inline">Sport Active</label>
						<div class="material-switch">
							@if($sport->pivot->isActive)
							<input id="{{ 'sport_'.$sport->pivot->id.'_active' }}" onclick="serviceSportSwitch(this)" data-serviceid="{{ $sport->pivot->id }}"
							    name="sport_status" type="checkbox" checked> @else
							<input id="{{ 'sport_'.$sport->pivot->id.'_active' }}" onclick="serviceSportSwitch(this)" data-serviceid="{{ $sport->pivot->id }}"
							    name="sport_status" type="checkbox"> @endif
							<label for="{{ 'sport_'.$sport->pivot->id.'_active' }}" class="label-default"></label>
						</div>
					</div>
					<!-- ======== END OF SPORT SWITCHER ======= -->
				</div>
				@endforeach

						<!-- ============== COURTS AND TRAINERS CONTAINER ==================== -->
						<div class="card mb-5" id="sport_container_card"></div>
						<!-- ============== END OF COURTS AND TRAINERS CONTAINER ==================== -->

				<!-- ================= END OF SINGLE SPORT TAB =================== -->
			</div>
			<!-- ========================== END OF SPORTS LIST CONTENT============================== -->
		</div>
		@endforeach
	</div>
