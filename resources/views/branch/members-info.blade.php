<ul class="nav nav-subtab" role="tablist">
  <li class="nav-item">
    <a class="nav-link {{ !empty($subTabName) && $subTabName == 'registered' ? 'active' : '' }}" data-toggle="tab" href="#registeredMembers" role="tab" aria-selected="false">REGISTERD CLUB MEMBERS <span class="badge">{{ count($branch->member) }}</span><span class="badge-alt">{{ $totalbranchrequests }}</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ !empty($subTabName) && $subTabName == 'sheet' ? 'active' : '' }}" data-toggle="tab" href="#membersSheet" role="tab" aria-selected="false">MEMBERS SHEET <span class="badge">{{ $totalsheetmembers }}</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ !empty($subTabName) && $subTabName == 'default' ? 'active' : '' }}" data-toggle="tab" href="#addMember" role="tab"  aria-selected="true">ADD/ASSIGN MEMBER</a>
  </li>
</ul>

<div class="tab-content">
	<div class="tab-pane fade {{ !empty($subTabName) && $subTabName == 'registered' ? 'show active' : '' }}" id="registeredMembers" role="tabpanel">
	  	<div class="card mb-5">
		    <h2>MEMBERS REQUESTS</h2>
        <table class="table table-bordered" id="branch-members-requests">
             <thead>
                 <tr>
                     <th></th>
                     <th> Membership ID</th>
                     <th> Member’s Name</th>
                     <th> failure reason</th>
                     <th></th>
                 </tr>
             </thead>
         </table>

		</div>
		<div class="card mb-5">
		    <h2>JOIND MEMBERS</h2>
        <table class="table table-bordered" id="joined-members-table">
             <thead>
                 <tr>
                     <th></th>
                     <th>Membership ID</th>
                     <th>Member’s Name</th>
                     <th>Bookings</th>
                     <th>date joined</th>
                     <th>Active/deactive</th>
                     <th>Actions</th>
                 </tr>
             </thead>
         </table>
		</div>
	 </div>

  <!-- =============== MEMBERS SHEET TAB  =================== -->
	 <div class="tab-pane fade {{ !empty($subTabName) && $subTabName == 'sheet' ? 'show active' : '' }}" id="membersSheet" role="tabpanel">
	  	<div class="card mb-5">
        <div class="row">

  		  		@if ($message = Session::get('success'))
  					<div class="alert alert-success" role="alert">
  						{{ Session::get('success') }}
  					</div>
  				   @endif


  				@if($message = Session::get('error'))
  					<div class="alert alert-danger text-center" role="alert">
  						{{ Session::get('error') }}
  					</div>
  				@endif

          @if(!empty($errors) && count($errors)>0))
          <div class="col-md-12">
            <div class="alert alert-danger text-center" role="alert">
              @foreach ($errors->all() as $error)
                  <p> {{ $error }} </p>
              @endforeach
            </div>
          </div>
          @endif

  				@if(Session::has('message'))
          <div class="col-md-12">
            <div class="alert alert-success text-center" role="alert">
    							<p id="status"><?php echo Session::get('message'); ?></p>
             </div>
          </div>
          @endif


        </div>

	  		<div class="row">
	  			<div class="col-md-8">
	  				<div>
					    <h2>MEMBERS</h2>


					   <!--  <div class="search">
					    	<input type="text" id="searcMemberNameID" name="searchMembers" placeholder="Search member name/ID…">
					    	<span class="icon-search"></span>
					    </div> -->

              <div class="modal fade" id="sheetMemberModal" tabindex="-1" role="dialog" aria-labelledby="sheetMemberModal" aria-hidden="true">
                  <div class="modal-dialog d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
                      <div id="members_feed_result" class="modal-content">
                        @include('includes.sheet-member')
                      </div>
                  </div>
              </div>

               <table class="table table-bordered" id="members-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Member’s Name</th>
                            <th>Membership ID</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

			  		</div>

	  			</div>

	  			<div class="col-md-4">
              <form action="{{ URL::to('import') }}"
              class="form-horizontal" method="post"
              enctype="multipart/form-data" id="import_form_action">

                <h2>Select/Update Members’ Sheet</h2>
    	  				<div class="form-group inline my-3" id="choose_file_div">
    	                   <label class="inline">Choose XLSX file</label>
    	                   <span class="btn btn-default btn-file">
    	                         BROWSE FILE <input name="import_file" type="file" class="browse_importer" >
    	                    </span>
    	           </div>

                 {{ csrf_field() }}
                 <input type="hidden" name="import_patch_id" id="import_patch_id" />
                 <input type="hidden" name="import_branch_id" id="import_branch_id" value="{{ $branch->id }}" />

               <input type="submit" id="btn_checker" name="validate" disabled value="CHECK FILE BEFORE IMPORTING"
               class="btn btn-default my-3"></input>

    					 <input type="submit" id="btn_importer" name="import" value="START IMPORTING"
               class="btn btn-default my-3" style="display:none;"></input>

               <a href="#" id="errors_sheet_btn" style="display:none;" download>Download Errors Sheet</a>
              </form>



                  <!-- ======= IMPORTING INFORMATION =========== -->
                  <ul class="guide">
        						<li>
        							- File type uploaded shall be “xlsx” file
        						</li>
        						<li>
        							- Max records allowed at a time for upload are 10,000 records
        						</li>
        						<li>
        							- Membership-ID should be unique
        						</li>
                    <li>
        							- 1st column Must be ( record_id ) <br />
                      - 2nd column Must be ( membership_id ) <br />
                      - 3rd column Must be ( membership_name ) <br />
        						</li>
        					</ul>
                  <!-- ======= END OF  IMPORTING INFORMATION =========== -->
	  			</div>


	  		</div>
		</div>
	 </div>

<!-- =============== END OF MEMBERS SHEET TAB  =================== -->

	 <div class="tab-pane fade {{ !empty($subTabName) && $subTabName == 'default' ? 'show active' : '' }}" id="addMember" role="tabpanel" aria-labelledby="home-tab">

    @if(Auth::user()->hasRole('sys_admin'))
      <div class="card mb-5">
		    <h2>ASSIGN MEMBER TO CLUB</h2>
		    <div class="search">
		    	<input type="hidden" id="current_branch_id" name="branch_id" value="{{ $branch->id }}" />
          <input id="memberSearch" name="searchMembers" type="text"
           onchange="searchAllMembers(this);" placeholder="Search member Username/Email…"
            onpaste="this.onchange();" oninput="this.onchange();"/>

          <span class="icon-search"></span>
		    </div>

        <div id="members_search_result">
           @include('includes.member')
        </div>
		    </div>
     @endif

		<div class="card mb-5">

			<h2>ADD MEMBER</h2>
      {!! Form::open([ 'url' => '/addUser' ,
                      'method' => 'post'
      ]) !!}

        {{ csrf_field() }}
        <input type="hidden" value="{{ $branch->id }}" name="branch_id" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
							<span class="input">
					              <input class="input__field" name="username" value="{{ old('username') }}" type="text">
					              <label class="input__label" for="username">
					                <span class="input__label-content">User Name</span>
							      </label>
					        </span>
                  @if ($errors->has('username'))
                      <span class="help-block">
                          <strong>{{ $errors->first('username') }}</strong>
                      </span>
                  @endif
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					        <span class="input">
					              <input class="input__field" name="email" value="{{ old('email') }}" type="email">
					              <label class="input__label" for="email">
					                <span class="input__label-content">Email</span>
							      </label>
					        </span>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
				    	</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
					        <span class="input">
					              <input class="input__field" name="password" type="password">
					              <label class="input__label" for="password">
					                <span class="input__label-content">Password</span>
							      </label>
					        </span>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
				    	</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
					        <span class="input">
					              <input class="input__field" name="password_confirmation" type="password">
					              <label class="input__label" for="password_confirmation">
					                <span class="input__label-content">Retype Password</span>
							      </label>
					        </span>

				    	</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('membershipName') ? ' has-error' : '' }}">
							<span class="input">
					              <input class="input__field" name="membershipName" value="{{ old('membershipName') }}" type="text">
					              <label class="input__label" for="membershipName">
					                <span class="input__label-content">Member Name</span>
							      </label>
					        </span>
                  @if ($errors->has('membershipName'))
                      <span class="help-block">
                          <strong>{{ $errors->first('membershipName') }}</strong>
                      </span>
                  @endif
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('membershipId') ? ' has-error' : '' }}">
							<span class="input">
					              <input class="input__field" value="{{ old('membershipId') }}" name="membershipId" type="text">
					              <label class="input__label" for="membershipId">
					                <span class="input__label-content">Member ID</span>
							      </label>
					      </span>
                @if ($errors->has('membershipId'))
                    <span class="help-block">
                        <strong>{{ $errors->first('membershipId') }}</strong>
                    </span>
                @endif
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
					        <span class="input">
					              <input class="input__field" value="{{ old('phone') }}" name="phone" type="tel">
					              <label class="input__label" for="phone">
					                <span class="input__label-content">Phone</span>
							      </label>
					        </span>
                  @if ($errors->has('phone'))
                      <span class="help-block">
                          <strong>{{ $errors->first('phone') }}</strong>
                      </span>
                  @endif
				    	</div>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-primary btn-block">ADD MEMBER</button>
					</div>
				</div>

			{!! Form::close() !!}

		</div>
	 </div>
</div>
