 <!-- =================  cash-payment-modal ===================== -->
        @include('includes.cash-payment-modal')
 <!-- ================= cash-payment-modal ===================== -->

 <!-- =================  confirm-refund ===================== -->
        @include('includes.confirm-refund')
 <!-- ================= confirm-refund ===================== -->

<ul class="nav nav-subtab" role="tablist">

  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#courtBooking" role="tab" aria-selected="true">COURT BOOKING</a>
  </li>
</ul>

<div class="tab-content mb-5">
	<div class="tab-pane fade active show" id="courtBooking" role="tabpanel">
		<div class="card">
		    <h2>Filter Booking Details</h2>
		    <form class="mt-4">
		    	<div class="row">
		    		<div class="col-md-3 mb-2">
		    			<div class="form-group">
			    			<select class="custom-select" id="monthSelect" name="monthSelect" placeholder="Select Month…">
                  <option value='' selected>Select Month…</option>
                  <option value='{{ date("m", strtotime("Janaury")) }}'>Janaury</option>
                  <option value='{{ date("m", strtotime("February")) }}'>February</option>
                  <option value='{{ date("m", strtotime("March")) }}'>March</option>
                  <option value='{{ date("m", strtotime("April")) }}'>April</option>
                  <option value='{{ date("m", strtotime("May")) }}'>May</option>
                  <option value='{{ date("m", strtotime("June")) }}'>June</option>
                  <option value='{{ date("m", strtotime("July")) }}'>July</option>
                  <option value='{{ date("m", strtotime("August")) }}'>August</option>
                  <option value='{{ date("m", strtotime("September")) }}'>September</option>
                  <option value='{{ date("m", strtotime("October")) }}'>October</option>
                  <option value='{{ date("m", strtotime("November")) }}'>November</option>
                  <option value='{{ date("m", strtotime("December")) }}'>December</option>
				          </select>
		    			</div>
		    		</div>
		    		<div class="col-md-3 mb-2">
		    			<div class="form-group">
			    			<select class="custom-select" id="sportSelect" name="sportSelect" placeholder="Select Sport…">
			    				      <option value="" selected>Select Sport…</option>
                        @foreach($branch->services->where('short_name','court_booking')->first()->serviceSport as $service_sport)
                        <option value="{{ $service_sport->id }}">{{ $service_sport->sport->name }}</option>
                        @endforeach
				              </select>
			          </div>
		    		</div>
		    		<div class="col-md-3 mb-2">
		    			<div class="form-group">
			    			<select class="custom-select" id="courtSelect" name="courtSelect" placeholder="Select Court…">
			    				<option value="" selected>Select Court…</option>
				        </select>
			          </div>
		    		</div>
		    		<div class="col-md-3 mb-2">
              <div class="form-group">
			    	  		<select class="custom-select" id="coachSelect" name="coachSelect" placeholder="With Coach…">
			    				      <option value="" selected>With Coach…</option>
				          </select>
			          </div>
		    		</div>
		    		<div class="col-md-3 mb-2">
              <div class="form-group">
			    			<select class="custom-select" id="paymentSelect" name="paymentSelect" placeholder="Select Payment Method…">
			    				      <option value="" selected>Select Payment Method…</option>
				                <option value="cash">Cash</option>
				                <option value="cc">Credit Card</option>
				              </select>
			          </div>
		    		</div>
            <div class="col-md-3 mb-2">
              <div class="form-group">
                <select class="custom-select" id="statusSelect" name="statusSelect" placeholder="Select Status…">
                        <option value="" selected>Select Status </option>

                        <option value="upcoming">upcoming</option>
                        <option value="canceled">canceled</option>
                        <option value="refunded">refunded</option>
                        <option value="done">done</option>
                        <option value="not done">not done</option>
                      </select>
                </div>
            </div>
		    		<div class="col-md-3 ml-auto mb-2">
		    			<div class="d-flex filter-btns">
		    				<button type="button" id="GoFilterBtn" class="btn btn-default">GO</button>
		    				<button type="button" id="GoClearBtn" class="btn btn-cancel">CLEAR</button>
                <div id="example_wrapper"></div>
		    			</div>
		    		</div>
		    	</div>
		    </form>
		    <div class="d-flex align-items-center mt-5">
		    	<h2 class="mb-0">Select View</h2>
		    	<ul class="nav nav-view">
            <li class="nav-item">
		    			<a href="#tableView" class="nav-link active" data-toggle="tab"><span class="icon-table"></span></a>
		    		</li>
            <li class="nav-item">
		    			<a href="#calenderView" onclick="triggerCalenderLoad();" class="nav-link" data-toggle="tab"><span class="icon-calendar"></span></a>
		    		</li>
		    	</ul>
		    </div>

		    <div class="tab-content px-4 pt-4">
          <div class="tab-pane fade active show" id="tableView">
            <div>
        		    <h2>Bookings</h2>
                <input type="hidden" id="current_month" value="{{ date('m') }}" />
                <table class="table table-bordered" id="current-bookings-table">
                     <thead>
                         <tr>
                             <th>Booking ID</th>
                             <th>Member Name</th>
                             <th>Sport</th>
                             <th>Court Name</th>
                             <th>Date</th>
                             <th>Time</th>
                             <th>Trainer</th>
                             <th>Payment</th>
                             <th>Booking Status</th>
                             <th>Total</th>
                             <th></th>
                         </tr>
                     </thead>
                 </table>
        		</div>
          </div>
          <div class="tab-pane fade" id="calenderView">
		    		<div id="bookingCalender" class="mt-4 calender-view"></div>
		    	</div>
		    </div>
		</div>
	</div>
</div>
