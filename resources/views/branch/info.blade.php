<div class="card">
    <h2 class="card-title"> Branch Contact Info</h2>
    <!-- <form id="branchContactInfo" class="d-flex align-items-center flex-wrap flex-xl-nowrap"> -->
      {!! Form::open([ 'url' => '/branches/'.$branch->id.'' ,
                      'method' => 'put' ,
                       'id' => 'branchContactInfo' ,
                       'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap' ,
                       'enctype' => 'multipart/form-data'
      ]) !!}

        {{ csrf_field() }}
      <input type="hidden" id="BranchId" value="{{ $branch->id }}" />
    	<div class="form-group {{ $errors->has('branch_email') ? ' has-error' : '' }} " >
	        <span class="input input--filled">
	              <input class="input__field" name="branch_email" value="{{ $branch->email }}" disabled="disabled" type="email">
                <label class="input__label" for="branch_email">
	                <span class="input__label-content">Email *</span>
			      </label>
	        </span>
          @if ($errors->has('branch_email'))
              <span class="help-block">
                  <strong>{{ $errors->first('branch_email') }}</strong>
              </span>
          @endif
    	</div>
    	<div class="form-group {{ $errors->has('branch_phone') ? ' has-error' : '' }} " >
	        <span class="input input--filled">
	              <input class="input__field" name="branch_phone" value="{{ $branch->phone }}" disabled="disabled" type="tel">
                <label class="input__label" for="branch_phone">
	                <span class="input__label-content">Phone *</span>
			      </label>
	        </span>
          @if ($errors->has('branch_phone'))
              <span class="help-block">
                  <strong>{{ $errors->first('branch_phone') }}</strong>
              </span>
          @endif
    	</div>
    	<div class="form-group">
	        <span class="input input--filled">
	              <input class="input__field" name="address" value="{{ $branch->address }}" disabled="disabled" type="text">
	              <label class="input__label" for="address">
	                <span class="input__label-content">Address *</span>
			      </label>
	        </span>
    	</div>
    	<div class="form-group">
	        <span class="input input--filled">
	              <input class="input__field" name="website" value="{{ $branch->website }}" disabled="disabled" type="text">
	              <label class="input__label" for="website">
	                <span class="input__label-content">Website</span>
			      </label>
	        </span>
    	</div>
    	<div class="form-group">
	        <span class="input input--filled">
	              <input class="input__field" name="map_url" value="{{ $branch->map_url }}" disabled="disabled" type="text">
	              <label class="input__label" for="map_url">
	                <span class="input__label-content">Google Maps</span>
			      </label>
	        </span>
    	</div>
    	<div class="btns-wrap">
    		<button type="submit"  class="btn btn-primary btn-save d-none" data-form-target="branchContactInfo">save</button>
    		<button type="button"  class="btn btn-primary btn-cancel d-none">cancel</button>
    	</div>
	    	<button type="button" id="BranchInfo-form-edit" class="btn btn-link" data-form-target="branchContactInfo"><span class="icon-edit"></span></button>
      {!! Form::close() !!}
</div>


<!-- ==================== BRANCH ADMIN INFO ===================== -->

<div class="row">
  @if($admin)
	<div class="col-md-6">
    <div class="card">
       <h2 class="card-title">Branch Admin Info</h2>

         {!! Form::open([ 'url' => '/branch/admin/'.$branch->admin_id.'' ,
                         'method' => 'put' ,
                          'id' => 'branchAdminInfo' ,
                          'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap' ,
                          'enctype' => 'multipart/form-data'
         ]) !!}

           {{ csrf_field() }}


         <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="email" value="{{ $admin ? $admin->email : 'N/A' }}" disabled="disabled" required type="email">
                   <label class="input__label" for="email">
                     <span class="input__label-content">Email *</span>
               </label>
             </span>
             @if ($errors->has('email'))
                 <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                 </span>
             @endif
         </div>
         <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="phone" value="{{ $admin ? $admin->phone : 'N/A' }}" disabled="disabled" required type="tel">
                   <label class="input__label" for="phone">
                     <span class="input__label-content">Phone *</span>
               </label>
             </span>
             @if ($errors->has('phone'))
                 <span class="help-block">
                     <strong>{{ $errors->first('phone') }}</strong>
                 </span>
             @endif
         </div>
         <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="username" value="{{ $admin ? $admin->username : 'N/A' }}" disabled="disabled" required type="text">
                   <label class="input__label" for="username">
                     <span class="input__label-content">Username *</span>
               </label>
             </span>
             @if ($errors->has('username'))
                 <span class="help-block">
                     <strong>{{ $errors->first('username') }}</strong>
                 </span>
             @endif
         </div>

         <div class="btns-wrap">
           <button type="submit" class="btn btn-primary btn-save d-none" data-form-target="branchAdminInfo">save</button>
           <button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
         </div>
         <button type="button" class="btn btn-link" data-form-target="branchAdminInfo"><span class="icon-edit"></span></button>
       </form>
   </div>
	</div>
	<div class="col-md-6">
		<div class="card">
		    <h2 class="card-title">Change Password</h2>

          {!! Form::open([ 'url' => '/reset/admin/password' ,
                          'method' => 'post' ,
                           'id' => 'branchAdminChangePW' ,
                           'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap' ,

          ]) !!}

            {{ csrf_field() }}
          <input type="hidden" name="admin_id" value="{{ $admin->id }}"/>
          <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
			        <span class="input input--filled">
			              <input class="input__field" name="old_password"  disabled="disabled" type="password">
			              <label class="input__label" for="old_password">
			                <span class="input__label-content">Old Password</span>
					      </label>
			        </span>
              @if ($errors->has('old_password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('old_password') }}</strong>
                  </span>
              @endif
		    	</div>
		    	<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
			        <span class="input input--filled">
			              <input class="input__field" name="password" disabled="disabled" type="password">
			              <label class="input__label" for="password">
			                <span class="input__label-content">New Password</span>
					      </label>
			        </span>
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
		    	</div>
		    	<div class="form-group">
			        <span class="input input--filled">
			              <input class="input__field" name="password_confirmation" disabled="disabled" type="password">
			              <label class="input__label" for="password_confirmation">
			                <span class="input__label-content">Rewrite New Password</span>
					      </label>
			        </span>
		    	</div>
		    	<div class="btns-wrap">
		    		<button type="submit" class="btn btn-primary btn-save d-none" data-form-target="branchAdminChangePW">save</button>
		    		<button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
		    	</div>
		    	<button type="button" class="btn btn-link" data-form-target="branchAdminChangePW"><span class="icon-edit"></span></button>
		     {!! Form::close() !!}
		</div>
	</div>

  @else
  <div class="col-md-12">
    <div class="card">
       <h2 class="card-title">Branch Admin Info</h2>
         {!! Form::open([ 'url' => '/branch/admin' ,
                         'method' => 'post' ,
                          'id' => 'addbranchAdminInfo' ,
                          'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap' ,

         ]) !!}

         {{ csrf_field() }}
         <input type="hidden" name="branch_id" value="{{ $branch->id }}"/>

         <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="email" value="" disabled="disabled" required type="email">
                   <label class="input__label" for="email">
                     <span class="input__label-content">Email * </span>
               </label>
             </span>
             @if ($errors->has('email'))
                 <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                 </span>
             @endif
         </div>
         <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="phone" value="" disabled="disabled" required type="tel">
                   <label class="input__label" for="phone">
                     <span class="input__label-content">Phone * </span>
               </label>
             </span>
             @if ($errors->has('phone'))
                 <span class="help-block">
                     <strong>{{ $errors->first('phone') }}</strong>
                 </span>
             @endif
         </div>
         <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="username" value="" disabled="disabled" required type="text">
                   <label class="input__label" for="username">
                     <span class="input__label-content">Username  * </span>
               </label>
             </span>
             @if ($errors->has('username'))
                 <span class="help-block">
                     <strong>{{ $errors->first('username') }}</strong>
                 </span>
             @endif
         </div>

         <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
             <span class="input input--filled">
                   <input class="input__field" name="password" disabled="disabled" type="password">
                   <label class="input__label" for="password">
                     <span class="input__label-content"> Password  * </span>
               </label>
             </span>
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
         </div>
         <div class="form-group">
             <span class="input input--filled">
                   <input class="input__field" name="password_confirmation" disabled="disabled" type="password">
                   <label class="input__label" for="password_confirmation">
                     <span class="input__label-content">Rewrite Password * </span>
               </label>
             </span>
         </div>



         <div class="btns-wrap">
           <button type="submit" class="btn btn-primary btn-save d-none" data-form-target="addbranchAdminInfo">save</button>
           <button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
         </div>
         <button type="button" class="btn btn-link" data-form-target="addbranchAdminInfo"><span class="icon-edit"></span></button>
         {!! Form::close() !!}
    </div>
  </div>
  @endif
</div>



<!-- ==================== BRANCH ADVS SECTION ===================== -->



@if(Auth::user()->hasRole('sys_admin'))
<div class="card sortable ui-sortable mb-5" id="adv_sortable">
  <!-- <div class="card " id="adv_sortable"> -->
    <h2 class="card-title">Branch Ads</h2>
    <div class="row text-center">
      @if ($errors->has('link'))
          <div class="alert alert-danger" style="width:100%;">
              <strong>{{ $errors->first('link') }}</strong>
          </div>
      @endif
    </div>
    @foreach($branch->advs as $branchAd)

    {!! Form::open([ 'url' => '/branch/adv/'.$branchAd->id.'' ,
                     'method' => 'put' ,
                     'id' => 'branchAd_'.$branchAd->id.'' ,
                     'class' => 'd-flex align-items-center card-row sortable-item flex-wrap flex-xl-nowrap',
                     'enctype' => 'multipart/form-data'
    ]) !!}

    {{ csrf_field() }}


      <span class="icon-table"></span>
      <div class="form-group inline">
          <label class="inline">Upload Ad Picture</label>
          <span class="btn btn-default btn-file">
                CHOOSE FILE <input type="file" name="advPic" disabled="disabled">
          </span>
      </div>
      <div class="form-group {{ $errors->has('link'.$branchAd->id) ? ' has-error' : '' }}">
          <span class="input input--filled">
                <input class="input__field" name="{{'link'.$branchAd->id}}" value="{{ $branchAd->link }}" disabled="disabled" type="text">
                <label class="input__label" for="link">
                  <span class="input__label-content">Link</span>
                </label>
          </span>
          @if ($errors->has('link'.$branchAd->id))
              <span class="help-block">
                  <strong>{{ $errors->first('link'.$branchAd->id) }}</strong>
              </span>
          @endif
      </div>
      <div>
          <small>Active</small>


          <div class="material-switch">

             <input type="hidden" value="" name="adv_status" id="{{ 'branchActive_'.$branchAd->id }}" />
              @if($branchAd->isActive)
              <input onclick="handlebranchClick(this)"
              data-branchid="{{$branchAd->id }}"
              id="{{ 'branchActive'.$branchAd->id }}"
               type="checkbox" checked disabled="true">
              @else
              <input onclick="handlebranchClick(this)"
               id="{{ 'branchActive'.$branchAd->id }}"
               data-branchid="{{$branchAd->id }}"
               type="checkbox" disabled="true">
              @endif
              <label for="{{ 'branchActive'.$branchAd->id }}" class="label-default"></label>
          </div>

      </div>
      <div class="date-range">
      <!-- name attribute for dateFrom and dateTo is important for datepicker init
        visit http://api.jqueryui.com/datepicker/#method-setDate to check how to get or set dates -->

        <label>Start & End time</label>

        <div class="form-group">
          <span class="input">
            <span class="icon-calendar"></span>
            <input type="text" class="input__field date_from_input" id="{{'dateFrom'.$branchAd->id}}" name="dateFrom" value="{{$branchAd->startdate }}" disabled="disabled">
            <label class="input__label" for="dateFrom">
                    <span class="input__label-content"></span>
            </label>
          </span>
        </div>
        <div class="form-group">
          <span class="input">
            <span class="icon-calendar"></span>
            <input type="text" class="input__field date_from_input" id="{{'dateTo'.$branchAd->id}}" name="dateTo" value="{{$branchAd->enddate }}" disabled="disabled">
            <label class="input__label" for="dateTo">
                    <span class="input__label-content"></span>
            </label>
          </span>
        </div>
        <div class="form-group">
          @if($branchAd->enddate)
            @if( date("Y-m-d") > date('Y-m-d',strtotime($branchAd->enddate)))
            <h5 style="color:#6d202b;">EXPIRED</h5>
            @endif
          @endif
        </div>


      </div>
      <div class="btns-wrap">
        <button type="submit" class="btn btn-primary btn-save d-none" data-form-target="{{ 'branchAd_'.$branchAd->id }}">save</button>
        <button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
      </div>

      <button type="button" class="btn btn-link" data-form-target="{{ 'branchAd_'.$branchAd->id }}"><span class="icon-edit"></span></button>
      <a href="#deleteAdvModal" data-id="{{ $branchAd->id }}"
      onclick="removeAdvModal(this)"
      data-toggle="modal" class="btn btn-link btn-delete">
      <span class="icon-delete"></span></a>

    {!! Form::close() !!}
    <div class="modal fade" id="deleteAdvModal" tabindex="-1" role="dialog" aria-labelledby="deleteAdvModal" aria-hidden="true">
        <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
            <div class="modal-content">
              {!! Form::open([ 'url' => '/branch/adv/delete' ,
                               'method' => 'post' ,
              ]) !!}

              {{ csrf_field() }}
              <div class="modal-header">
                  <h5 class="modal-title">REMOVE ADVERT</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                ARE YOU SURE YOU WANT TO REMOVE THIS ADVERT ?
                 <span id="advert_name"></span>
                 <input type="hidden" name="adv_id" class="advert_record_id" value="" />
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                  <button type="submit" class="btn btn-primary">REMOVE ADVERT</button>
              </div>
              {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endforeach

    <a  id="add_new_ad">ADD NEW AD</a>
    <a  id="cancel_new_ad" style="display:none;">CANCEL</a>

    <div id="new_ad" style="display:none;">

          {!! Form::open([ 'url' => '/add/branch/adv' ,
                           'method' => 'post' ,
                           'id' => 'AddbranchAd' ,
                           'class' => 'd-flex align-items-center card-row  flex-wrap flex-xl-nowrap',
                           'enctype' => 'multipart/form-data'
          ]) !!}

          {{ csrf_field() }}
              <input type="hidden" name="branch_id" value="{{ $branch->id }}" />
               <span class="icon-table"></span>
               <div class="form-group inline">
                   <label class="inline">Upload Ad Picture</label>
                   <span class="btn btn-default btn-file">
                         CHOOSE FILE <input type="file" name="advPic"  required>
                     </span>
               </div>
               <div class="form-group">
                   <span class="input input--filled">
                         <input class="input__field" name="link" value="" type="text">
                         <label class="input__label" for="link">
                           <span class="input__label-content">Link</span>
                     </label>
                   </span>
               </div>
               <div>
                   <small>Active</small>
                   <div class="material-switch">
                     <input type="hidden" value="" name="adv_status" id="branchActiveValue" />

                       <input onclick="handleNewbranchClick(this)"
                       id="branchActive"
                       name="branchActive"
                       type="checkbox"  >

                       <label for="branchActive" class="label-default"></label>
                   </div>
               </div>
               <div class="date-range">
               <!-- name attribute for dateFrom and dateTo is important for datepicker init
                 visit http://api.jqueryui.com/datepicker/#method-setDate to check how to get or set dates -->
                 <label>Start & End time</label>

                 <div class="form-group">
                   <span class="input">
                     <span class="icon-calendar"></span>
                     <input type="text" class="input__field" name="dateFrom" value="" >
                     <label class="input__label" for="dateFrom">
                             <span class="input__label-content"></span>
                     </label>
                   </span>
                 </div>
                 <div class="form-group">
                   <span class="input">
                     <span class="icon-calendar"></span>
                     <input type="text" class="input__field" name="dateTo" value="" >
                     <label class="input__label" for="dateTo">
                             <span class="input__label-content"></span>
                     </label>
                   </span>
                 </div>
               </div>

                 <button type="submit" class="btn btn-primary btn-save " data-form-target="AddbranchAd">save</button>
                 <button type="button" id="hideNewAdvForm" class="btn btn-primary btn-cancel">cancel</button>


             {!! Form::close() !!}
      </div>
</div>
@endif
