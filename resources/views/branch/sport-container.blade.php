<!-- ===============  COURTS ================= -->
<h2 class="card-title bg-gray mb-0 pt-3 pb-3">
  <a href="{{'#sport_'.$current_service_sport->id.'_courts'}}" data-toggle="collapse">{{ $current_service_sport->sport->name }} COURTS</a>
</h2>
<div id="{{'sport_'.$current_service_sport->id.'_courts'}}" class="collapse show pt-3 pl-3 pr-3 pb-3">
  <!-- ======== GENERAL COURT SETTING FORM ======= -->
  {!! Form::open([ 'url' => '/service/sport/'.$current_service_sport->id.'' , 'method' => 'put' , 'id' => 'sport_'.$current_service_sport->id.'_general'
  , 'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap card-row' , 'enctype' => 'multipart/form-data' ])
  !!} {{ csrf_field() }}
  <div class="form-group form-group-icon order-3 order-md-1 editedInputs">
    <span class="input input--filled">
      <span class="icon-time"></span>
      <input class="input__field" name="slotDuration" value="{{$current_service_sport->slot_duration}}" disabled="disabled" type="text"
          required>
      <label class="input__label" for="slotDuration">
        <span class="input__label-content">Slot Duration (in minutes) * </span>
      </label>
    </span>
  </div>
  <div class="form-group form-group-icon order-4 order-md-2 editedInputs">
    <span class="input input--filled timepicker-range">
      <span class="icon-time"></span>
      <!-- 9:00 AM - 12:00 PM -->
      <input class="input__field timepicker-from" name="morningWTF" data-min-time="6:00 AM" data-max-time="12:00 PM" value="{{ date('H:i', strtotime($current_service_sport->morning_from)) }}"
          disabled="disabled" type="time" required>
      <input class="input__field timepicker-to" name="morningWTT" data-min-time="6:00 AM" data-max-time="12:00 PM" value="{{ date('H:i', strtotime($current_service_sport->morning_to)) }}"
          disabled="disabled" type="time" required>
      <label class="input__label" for="morningWT">
        <!-- <span class="input__label-content nonCounted">{{ date('H:i A', strtotime($current_service_sport->morning_from)) }} - {{ date('H:i A', strtotime($current_service_sport->morning_to)) }}</span> -->
        <span class="input__label-content">Morning working Time *</span>
      </label>
    </span>
  </div>
  <div class="form-group form-group-icon order-5 order-md-3 editedInputs">
    <span class="input input--filled timepicker-range">
      <span class="icon-time"></span>
      <input class="input__field timepicker-from" name="afternoonWTF" data-min-time="12:00 PM" data-max-time="6:00 PM" value="{{ date('H:i', strtotime($current_service_sport->afternoon_from)) }}"
          disabled="disabled" type="time" required>
      <input class="input__field timepicker-to" name="afternoonWTT" data-min-time="12:00 PM" data-max-time="6:00 PM" value="{{ date('H:i', strtotime($current_service_sport->afternoon_to)) }}"
          disabled="disabled" type="time" required>
      <label class="input__label" for="afternoonWT">
        <!-- <span class="input__label-content nonCounted">{{ date('H:i A', strtotime($current_service_sport->afternoon_from)) }} - {{ date('H:i A', strtotime($current_service_sport->afternoon_to)) }}</span> -->
        <span class="input__label-content ">Afternoon working Time *</span>
      </label>
    </span>
  </div>
  <div class="form-group form-group-icon order-6 order-md-4 editedInputs">
    <span class="input input--filled timepicker-range">
      <span class="icon-time"></span>
      <input class="input__field timepicker-from" name="eveningWTF" data-min-time="6:00 PM" data-max-time="11:59 PM" value="{{ date('H:i', strtotime($current_service_sport->evening_from)) }}"
          disabled="disabled" type="time" required>
      <input class="input__field timepicker-to" name="eveningWTT" data-min-time="6:00 PM" data-max-time="11:59 PM" value="{{ date('H:i', strtotime($current_service_sport->evening_to)) }}"
          disabled="disabled" type="time" required>

      <!-- <input class="input__field timepicker-from" name="eveningWTF" value="{{ date('H:i A', strtotime($current_service_sport->evening_from)) }}"
          data-min-time="6:00 PM" data-max-time="12:00 PM" disabled="disabled" type="text" required>
      <input class="input__field timepicker-to" name="eveningWTT" value="{{ date('H:i A', strtotime($current_service_sport->evening_to)) }}"
          data-min-time="6:00 PM" data-max-time="12:00 PM" disabled="disabled" type="text" required> -->
      <label class="input__label" for="eveningWT">
        <!-- <span class="input__label-content nonCounted">{{ date('H:i A', strtotime($current_service_sport->evening_from)) }} - {{ date('H:i A', strtotime($current_service_sport->evening_to)) }}</span> -->
        <span class="input__label-content">Evening working Time *</span>
      </label>
    </span>
  </div>
  <div class="form-group form-group-icon order-7 order-md-5">
    <span class="input input--filled">
      <!-- <span class="icon-time"></span> -->
      <input class="input__field pl-0" name="priceSlot" value="{{$current_service_sport->slot_price}}" disabled="disabled" type="text" required>
      <label class="input__label" for="priceSlot">
        <span class="input__label-content">Price / Slot * (EGP)</span>
      </label>
    </span>
  </div>
  <div class="btns-wrap order-2 order-md-6">
    <button type="submit" class="btn btn-primary btn-save d-none" data-form-target="{{'sport_'.$current_service_sport->id.'_general'}}">save</button>
    <button type="button" class="btn btn-primary btn-cancel d-none canButton">cancel</button>
  </div>
  <button type="button" class="btn btn-link order-1 order-md-7 pinButton" data-form-target="{{'sport_'.$current_service_sport->id.'_general'}}">
    <span class="icon-edit"></span>
  </button>
  {!! Form::close() !!}

  <!-- <script>
    (function(){
      var $pinButton = $('.pinButton');
      var $cancelButton = $('.canButton');


        // $('.counted').show();
        $('.nonCounted').hide();
      function hide(){
        $('.counted').hide();
        $('.nonCounted').show();
      }
      $pinButton.click(function(){
        hide();
      });
      $cancelButton.click(function(){
        var $counted = $(this).parent().siblings().find('.counted');
        var $nonCounted = $(this).parent().siblings().find('.nonCounted');
        $nonCounted.each(function(){
          $(this).addClass('d-none');
        });
        $counted.each(function(){
          $(this).addClass('d-block');
        });
      });
    })();
  </script> -->
  <!-- ======== END OF GENERAL COURT SETTING FORM ======= -->
  <!-- ======== SINGLE COURT SETTING ======= -->
  @foreach($current_service_sport->court as $court)
   @if($court->service_sport_id == $current_service_sport->id ) {!! Form::open([ 'url' => '/courts/'.$court->id.''
  , 'method' => 'put' , 'id' => 'sport_court_'.$court->id.'' , 'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap
  card-row' , 'enctype' => 'multipart/form-data' ]) !!} {{ csrf_field() }}
  <div class="form-group order-4 order-md-1">
    <span class="input input--filled">
      <input class="input__field" name="courtName" value="{{ $court->name }}" disabled="disabled" type="text" required>
      <label class="input__label" for="courtName">
        <span class="input__label-content">Court Name</span>
      </label>
    </span>
  </div>
  <div class="order-5 order-md-2">
    <small>Active</small>
    <div class="material-switch">
      <input type="hidden" value="" name="court_status" id="{{ 'courtActiveStatus_'.$court->id }}" /> @if($court->isActive)
      <input id="{{'courtActive'.$court->id }}" onclick="courtStatusSwitch(this)" data-courtid="{{ $court->id }}" type="checkbox"
          disabled="true" checked> @else
      <input id="{{'courtActive'.$court->id }}" onclick="courtStatusSwitch(this)" data-courtid="{{ $court->id }}" type="checkbox"
          disabled="true"> @endif
      <label for="{{'courtActive'.$court->id }}" class="label-default"></label>
    </div>
  </div>
  <div class="date-range order-6 order-md-3">
    <label>Occasionally Closing</label>
    <div class="form-group">
      <span class="input">
        <span class="icon-calendar"></span>
        <input type="text" class="input__field" name="dateFrom" placeholder="Start Date" value="{{ $court->closing_from }}" disabled="disabled">
        <label class="input__label" for="dateFrom">
          <span class="input__label-content"></span>
        </label>
      </span>
    </div>
    <div class="form-group">
      <span class="input">
        <span class="icon-calendar"></span>
        <input type="text" class="input__field" name="dateTo" placeholder="End Date" value="{{ $court->closing_to }}" disabled="disabled">
        <label class="input__label" for="dateTo">
          <span class="input__label-content"></span>
        </label>
      </span>
    </div>
  </div>
  <a href="#deleteCourtModal" data-id="{{ $court->id }}" onclick="removeCourtModal(this)" data-toggle="modal" class="btn btn-link btn-delete order-3 order-md-4">
    <span class="icon-delete"></span>
  </a>
  <div class="btns-wrap order-2 order-md-5">
    <button type="submit" class="btn btn-primary btn-save d-none" data-form-target="{{ 'sport_court_'.$court->id }}">save</button>
    <button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
  </div>
  <button type="button" class="btn btn-link order-1 order-md-6" data-form-target="{{ 'sport_court_'.$court->id }}">
    <span class="icon-edit"></span>
  </button>
  {!! Form::close() !!}
  <div class="modal fade" id="deleteCourtModal" tabindex="-1" role="dialog" aria-labelledby="deleteCourtModal" aria-hidden="true">
    <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
      <div class="modal-content">
        {!! Form::open([ 'url' => '/court/delete' , 'method' => 'post' , ]) !!} {{ csrf_field() }}
        <div class="modal-header">
          <h5 class="modal-title ml-0">REMOVE ADVERT</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ARE YOU SURE YOU WANT TO REMOVE THIS COURT ?
          <span id="court_name"></span>
          <input type="hidden" name="court_id" class="court_record_id" value="" />
        </div>
        <div class="modal-footer d-flex">
          <button type="button" class="btn btn-outline-primary border-0" data-dismiss="modal">CANCEL</button>
          <button type="submit" class="btn btn-outline-primary border-0">REMOVE COURT</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  @endif @endforeach
  <!-- ======== END OF SINGLE COURT SETTING ======= -->
  <!-- ======== ADD NEW COURT ======= -->
  <a onclick="openNewCourtForm(this)" id="{{'add_new_court_'.$current_service_sport->id}}" data-courtid="{{ $current_service_sport->id }}">ADD COURT</a>
  <div id="{{'new_court_'.$current_service_sport->id}}" style="display:none;">
    {!! Form::open([ 'url' => '/addCourt' , 'method' => 'post' , 'id' => 'add_new_court_form'.$current_service_sport->id.'' , 'class'
    => 'd-flex align-items-center flex-wrap flex-xl-nowrap card-row' , 'enctype' => 'multipart/form-data' ]) !!} {{ csrf_field()
    }}
    <input type="hidden" name="service_sport_id" value="{{ $current_service_sport->id }}" />
    <div class="form-group">
      <span class="input input--filled">
        <input class="input__field" name="court_name" value="" type="text" required>
        <label class="input__label" for="court_name">
          <span class="input__label-content">Court Name * </span>
        </label>
      </span>
    </div>
    <div>
      <small>Active</small>
      <div class="material-switch">
        <input type="hidden" value="" name="court_status" id="{{'newCourtActiveValue_'.$current_service_sport->id}}" />
        <input id="{{'newCourtActive'.$current_service_sport->id}}" onclick="newCourtStatusSwitch(this)" data-sportservice="{{ $current_service_sport->id }}"
            type="checkbox">
        <label for="{{'newCourtActive'.$current_service_sport->id}}" class="label-default"></label>
      </div>
    </div>
    <div class="date-range">
      <label>Occasionally Closing</label>
      <div class="form-group">
        <span class="input">
          <span class="icon-calendar"></span>
          <input type="text" class="input__field" name="dateFrom" placeholder="Start Date" value="">
          <label class="input__label" for="dateFrom">
            <span class="input__label-content"></span>
          </label>
        </span>
      </div>
      <div class="form-group">
        <span class="input">
          <span class="icon-calendar"></span>
          <input type="text" class="input__field" name="dateTo" placeholder="End Date" value="">
          <label class="input__label" for="dateTo">
            <span class="input__label-content"></span>
          </label>
        </span>
      </div>
    </div>
    <button type="submit" class="btn btn-primary btn-save" data-form-target="{{'add_new_court_form'.$current_service_sport->id}}">save</button>
    <button type="button" onclick="hideNewCourtForm(this)" data-courtid="{{ $current_service_sport->id }}" class="btn btn-primary btn-cancel ">cancel</button>
    {!! Form::close() !!}
  </div>
  <!-- ========END OF ADD NEW COURT ======= -->
</div>
<!-- =========== END OF COURTS ============== -->
<!-- =========== TRAINERS ============== -->
<h2 class="card-title bg-gray mb-0 mt-3 pt-3 pb-3">
  <a href="#{{'#sport_'.$current_service_sport->id.'_trainers'}}" data-toggle="collapse">{{ $current_service_sport->sport->name }} TRAINER</a>
</h2>
<div id="{{'#sport_'.$current_service_sport->id.'_trainers'}}" class="collapse show pt-3 pl-3 pr-3 pb-3">
  @foreach($current_service_sport->trainer as $trainer) @if($trainer->service_sport_id == $current_service_sport->id ) {!! Form::open([ 'url' =>
  '/trainers/'.$trainer->id.'' , 'method' => 'put' , 'id' => 'sport_trainer_'.$trainer->id.'' , 'class' => 'd-flex align-items-center
  flex-wrap flex-xl-nowrap card-row' , 'enctype' => 'multipart/form-data' ]) !!} {{ csrf_field() }}
  <div class="thumb-upload order-4 order-md-1">
    <div class="image-upload">
      <label for="{{'coachImage_'.$trainer->id}}">
        <span class="icon-upload" style="pointer-events: none"></span>
      </label>
      <input id="{{'coachImage_'.$trainer->id}}" type="file" name="avatar" disabled="disabled" />
    </div>
    <div class="thumb">
      @if(! $trainer->avatarURL || $trainer->avatarURL == "none")
      <img src="/images/user.jpg"> @else
      <img src="{{ asset('storage/app/public/').'/'.$trainer->avatarURL }}"> @endif
    </div>
  </div>
  <div class="form-group order-5 order-md-2">
    <span class="input input--filled">
      <input class="input__field" name="coachName" value="{{ $trainer->name }}" disabled="disabled" type="text" required>
      <label class="input__label" for="coachName">
        <span class="input__label-content">Coach Name *</span>
      </label>
    </span>
  </div>
  <div class="form-group order-6 order-md-3">
    <span class="input input--filled">
      <input class="input__field" name="coachPhone" value="{{ $trainer->phone }}" disabled="disabled" type="tel" required>
      <label class="input__label" for="coachPhone">
        <span class="input__label-content">Phone *</span>
      </label>
    </span>
  </div>
  <div class="form-group order-7 order-md-4">
    <span class="input input--filled">
      <input class="input__field" name="coachEmail" value="{{ $trainer->email }}" disabled="disabled" type="email" required>
      <label class="input__label" for="coachEmail">
        <span class="input__label-content">Email</span>
      </label>
    </span>
  </div>
  <div class="form-group order-8 order-md-5">
    <span class="input input--filled">
      <input class="input__field" name="coachPrice" value="{{ $trainer->price_per_session }}" disabled="disabled" type="text" required>
      <label class="input__label" for="coachPrice">
        <span class="input__label-content">Price Per Session *</span>
      </label>
    </span>
  </div>
  <div class="form-group form-group-icon order-9 order-md-6">
    <span class="input input--filled timepicker-range">
      <span class="icon-time"></span>
      <input class="input__field timepicker-from" name="coachWTF" value="{{ $trainer->daily_from }}" disabled="disabled" type="time"
          required>
      <input class="input__field timepicker-to" name="coachWTT" value="{{ $trainer->daily_to }}" disabled="disabled" type="time"
          required>
      <label class="input__label" for="coachWT">
        <span class="input__label-content">Working Time *</span>
      </label>
    </span>
  </div>
  <div class="order-10 order-md-7">
    <small>Active</small>
    <div class="material-switch">
      <input type="hidden" value="" name="trainer_status" id="{{ 'trainerActive_'.$trainer->id }}" /> @if($trainer->isActive)
      <input id="{{'trainerActive'.$trainer->id }}" onclick="trainerStatusSwitch(this)" data-trainerid="{{ $trainer->id }}" type="checkbox"
          disabled="true" checked> @else
      <input id="{{'trainerActive'.$trainer->id }}" onclick="trainerStatusSwitch(this)" data-trainerid="{{ $trainer->id }}" type="checkbox"
          disabled="true"> @endif
      <!-- <input id="{{'coachActive'.$trainer->id}}" name="trainer_status" disabled="true" type="checkbox" checked> -->
      <label for="{{'trainerActive'.$trainer->id}}" class="label-default"></label>
    </div>
  </div>
  <div class="form-group select-wrap order-11 order-md-8">

      <label for="offDays">Weekly Days Off</label>
      <select class="select" name="offDays[]" multiple disabled="disabled">
        @foreach(collect(json_decode($trainer->days_off)) as $day_off)
        <option value="{{$day_off}}" selected>{{$day_off}}</option>
        @endforeach
        <option value="Friday">Friday</option>
        <option value="Saturday">Saturday</option>
        <option value="Sunday">Sunday</option>
        <option value="Monday">Monday</option>
        <option value="Tuesday">Tuesday</option>
        <option value="Wednesday">Wednesday</option>
        <option value="Thursday">Thursday</option>
      </select>
  </div>
  <a href="#deleteTrainerModal" data-id="{{ $trainer->id }}" onclick="removeTrainerModal(this)" data-toggle="modal" class="btn btn-link btn-delete order-3 order-md-9">
    <span class="icon-delete"></span>
  </a>
  <div class="btns-wrap order-2 order-md-10">
    <button type="submit" class="btn btn-primary btn-save d-none" data-form-target="{{ 'sport_trainer_'.$trainer->id }}">save</button>
    <button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
  </div>
  <button type="button" class="btn btn-link order-1 order-md-11" data-form-target="{{ 'sport_trainer_'.$trainer->id }}">
    <span class="icon-edit"></span>
  </button>
  {!! Form::close() !!}
  <div class="modal fade" id="deleteTrainerModal" tabindex="-1" role="dialog" aria-labelledby="deleteTrainerModal" aria-hidden="true">
    <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
      <div class="modal-content">
        {!! Form::open([ 'url' => '/trainer/delete' , 'method' => 'post' , ]) !!} {{ csrf_field() }}
        <div class="modal-header">
          <h5 class="modal-title ml-0">REMOVE TRAINER</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ARE YOU SURE YOU WANT TO REMOVE THIS TRAINER ?
          <span id="trainer_name"></span>
          <input type="hidden" name="trainer_id" class="trainer_record_id" value="" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-block" data-dismiss="modal">CANCEL</button>
          <button type="submit" class="btn btn-primary btn-block">REMOVE Trainer</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  @endif @endforeach
  <!-- =========== ADD NEW TRAINER ============== -->
  <a onclick="openNewTrainerForm(this)" id="{{'add_new_trainer_'.$current_service_sport->id}}" data-trainerid="{{ $current_service_sport->id }}"
      class="text-primary text-uppercase " style="font-size:1.23846rem;font-weight:500;">ADD TRAINER</a>
  <div id="{{'new_trainer_'.$current_service_sport->id}}" style="display:none;">
    {!! Form::open([ 'url' => '/addTrainer' , 'method' => 'post' , 'id' => 'new_sport_trainer_'.$current_service_sport->id.'' , 'class'
    => 'd-flex align-items-center flex-wrap flex-xl-nowrap card-row' , 'enctype' => 'multipart/form-data' ]) !!} {{ csrf_field()
    }}
    <input type="hidden" name="service_sport_id" value="{{ $current_service_sport->id }}" />
    <div class="thumb-upload">
      <div class="image-upload">
        <label for="{{'newCoachImage_'.$current_service_sport->id}}">
          <span class="icon-upload" style="pointer-events: none"></span>
        </label>
        <input id="{{'newCoachImage_'.$current_service_sport->id}}" name="avatar" type="file" />
      </div>
      <div class="thumb">
        <img src="/images/user.jpg">
      </div>
    </div>
    <div class="form-group">
      <span class="input input--filled">
        <input class="input__field" name="trainer_name" value="" type="text" required>
        <label class="input__label" for="trainer_name">
          <span class="input__label-content">Coach Name *</span>
        </label>
      </span>
    </div>
    <div class="form-group">
      <span class="input input--filled">
        <input class="input__field" name="phone" value="" type="tel" required>
        <label class="input__label" for="phone">
          <span class="input__label-content">Phone *</span>
        </label>
      </span>
    </div>
    <div class="form-group">
      <span class="input input--filled">
        <input class="input__field" name="email" value="" type="email">
        <label class="input__label" for="email">
          <span class="input__label-content">Email</span>
        </label>
      </span>
    </div>
    <div class="form-group">
      <span class="input input--filled">
        <input class="input__field" name="price_per_session" value="" type="text" required>
        <label class="input__label" for="price_per_session">
          <span class="input__label-content">Price Per Session *</span>
        </label>
      </span>
    </div>
    <div class="form-group form-group-icon order-9 order-md-6">
      <span class="input input--filled timepicker-range">
        <span class="icon-time"></span>
        <input class="input__field timepicker-from" name="coachWTF" value="" type="time" required>
        <input class="input__field timepicker-to" name="coachWTT" value="" type="time" required>
        <label class="input__label" for="coachWT">
          <span class="input__label-content">Working Time *</span>
        </label>
      </span>
    </div>
    <div>
      <small>Active</small>
      <div class="material-switch">
        <input type="hidden" value="" name="trainer_status" id="{{ 'newTrainerActiveValue_'.$current_service_sport->id }}" />
        <input id="{{'newTrainerActive'.$current_service_sport->id}}" onclick="newTrainerStatusSwitch(this)" data-sportservice="{{ $current_service_sport->id }}"
            type="checkbox">
        <label for="{{ 'newTrainerActive'.$current_service_sport->id }}" class="label-default"></label>
      </div>
    </div>
    <div class="form-group select-wrap order-11 order-md-8">
      <label for="offDays">Weekly Days Off</label>
      <select class="select" name="offDays[]" multiple>
        <option value="Friday">Friday</option>
        <option value="Saturday">Saturday</option>
        <option value="Sunday">Sunday</option>
        <option value="Monday">Monday</option>
        <option value="Tuesday">Tuesday</option>
        <option value="Wednsday">Wednsday</option>
        <option value="Thursday">Thursday</option>
      </select>
    </div>
    <div class="btns-wrap order-2 order-md-10">
      <button type="submit" class="btn btn-primary btn-save" data-form-target="{{ 'new_sport_trainer_'.$current_service_sport->id }}">save</button>
      <button type="button" onclick="hideNewTrainerForm(this)" data-trainerid="{{ $current_service_sport->id }}" class="btn btn-primary btn-cancel">cancel</button>
    </div>

    {!! Form::close() !!}
  </div>
</div>
<!-- =========== END OF ADD NEW TRAINER ============== -->
<!-- =========== END OF TRAINERS ============== -->

<script>

  //edit forms
  (function() {
    var editBtns = $('#sport_container_card .btn[type="button"][data-form-target]');
    editBtns.each(function() {
      var editBtn = $(this),
        targetForm = $('#' + editBtn.attr('data-form-target')),
        selects = targetForm.find('select.select'),
        saveBtn = $(
          '.btn[type="submit"][data-form-target="' +
            editBtn.attr('data-form-target') +
            '"]'
        ),
        cancelBtn = saveBtn.siblings('.btn-cancel'),
        fields = targetForm.find('input[disabled]');

      editBtn.click(function(event) {
        if (editBtn.hasClass('edit-active')) {
          fields.each(function() {
            $(this).attr('disabled', 'true');
            saveBtn.addClass('d-none');
            cancelBtn.addClass('d-none');
            editBtn.removeClass('d-none');
          });
          selects.attr('disabled', 'true').trigger('chosen:updated');
        } else {
          fields.each(function() {
            $(this).removeAttr('disabled');
            saveBtn.removeClass('d-none');
            cancelBtn.removeClass('d-none');
            editBtn.addClass('d-none');
          });
          selects.removeAttr('disabled').trigger('chosen:updated');
        }
        editBtn.toggleClass('edit-active');
      });
      cancelBtn.click(function(event) {
        editBtn.click();
      });
    });
  })();


  //datepickers
  (function() {
    //daterange
    var dateRange = $('#sport_container_card .date-range');
    dateRange.each(function(index, el) {
      var dateFrom = $(this).find('input[name="dateFrom"]'),
        dateTo = $(this).find('input[name="dateTo"]'),
        dateFormat = 'yy-m-d';

      if (dateFrom.val() !== '') {
        dateFromInit = dateFrom.val();
      } else {
        dateFromInit = null;
      }

      if (dateTo.val() !== '') {
        dateToInit = dateTo.val();
      } else {
        dateToInit = null;
      }

      dateFrom
        .datepicker({
          dateFormat: dateFormat,
          nextText: '>',
          prevText: '<'
        })
        .datepicker('setDate', dateFromInit)
        .on('change', function() {
          dateTo.datepicker('option', 'minDate', getDate(this));
        });

      dateTo
        .datepicker({
          dateFormat: dateFormat,
          nextText: '>',
          prevText: '<'
        })
        .datepicker('setDate', dateToInit)
        .on('change', function() {
          dateFrom.datepicker('option', 'maxDate', getDate(this));
        });

      function getDate(element) {
        var date;
        try {
          date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
          date = null;
        }

        return date;
      }
    });
  })();

    //Timepickers
    (function() {
      var timepickersRange = $('#sport_container_card .timepicker-range');
      timepickersRange.each(function(index, el) {

        // adding class input--filled while changing picker value
        $(this).on('change', function(){
          if(!$(this).hasClass('input--filled')){
            $(this).addClass('input--filled');
          }
        });

        var timeFrom = $(this).find('input.timepicker-from'),
          timeTo = $(this).find('input.timepicker-to'),
          timeFromInit = timeFrom.val(),
          timeToInit = timeTo.val(),
          timeFromMin = timeFrom.data('min-time'),  // from input's data-min-time attribute
          timeToMin = timeTo.data('min-time'), // from input's data-min-time attribute
          timeFromMax = timeFrom.data('max-time'), // from input's data-max-time attribute
          timeToMax = timeTo.data('max-time'); // from input's data-max-time attribute

        if (timeFromMin == '') {
          timeFromMin = '12:00 AM';
        }
        if (timeFromMax == '') {
          timeFromMax = '12:00 AM';
        }
        if (timeToMin == '') {
          timeToMin = '12:00 AM';
        }
        if (timeToMax == '') {
          timeToMax = '12:00 AM';
        }

        $.timepicker.timeRange(timeFrom, timeTo, {
          // minInterval: 1000 * 60 * 60, // 1hr
          timeFormat: 'HH:mm',
          controlType: 'select',
          oneLine: true,
          start: {
            timeOnlyTitle: 'Start',
            minTime: timeFromMin,
            maxTime: timeFromMax
          }, // start picker options
          end: {
            timeOnlyTitle: 'End',
            minTime: timeToMin,
            maxTime: timeToMax
          } // end picker options
        });

        if (timeFromInit !== '') {
          timeFrom.timepicker('setDate', timeFromInit);
        }
        if (timeToInit !== '') {
          timeTo.timepicker('setDate', timeToInit);
        }


      });
    })();

  //select init
	(function() {
		var selects = $('#sport_container_card .select');
		selects.each(function(index, el) {
			el = $(this);
			el.chosen({
				width: '100%'
			});
		});
	})();

</script>
