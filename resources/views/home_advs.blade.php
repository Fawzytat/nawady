@extends('layouts.inner')
@section('content')

<div class="container-fluid breadcrumb-wrap">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Home Page adverts </li>
    </ol>
</div>

<div class="container-fluid pb-5">

	<div class="card sortable ui-sortable" id="adv_sortable">

	    <h2 class="card-title">HOMEPAGE ADS</h2>
	    <h3 class="card-title">710X525 Ad</h3>
			<div class="row text-center">
				@if ($errors->has('link'))
						<div class="alert alert-danger" style="width:100%;">
								<strong>{{ $errors->first('link') }}</strong>
						</div>
				@endif
			</div>

			@foreach($advs as $adv)

	    {!! Form::open([ 'url' => '/adv/'.$adv->id.'' ,
	                     'method' => 'put' ,
	                     'id' => 'adv_'.$adv->id.'' ,
	                     'class' => 'd-flex align-items-center card-row sortable-item flex-wrap flex-xl-nowrap',
	                     'enctype' => 'multipart/form-data'
	    ]) !!}

	    {{ csrf_field() }}


	      <span class="icon-table order-4 order-md-1"></span>
	      <div class="form-group inline order-5 order-md-2">
	          <label class="inline">Upload Ad Picture</label>
	          <span class="btn btn-default btn-file">
	                CHOOSE FILE <input type="file"  name="advPic" disabled="disabled">
	          </span>
	      </div>
	      <div class="form-group order-6 order-md-3 {{ $errors->has('link'.$adv->id) ? ' has-error' : '' }}">
	          <span class="input input--filled">
	                <input class="input__field" name="{{'link'.$adv->id}}" value="{{ $adv->link }}" disabled="disabled" type="text">
	                <label class="input__label" for="link">
	                  <span class="input__label-content">Link</span>
	                </label>
	          </span>
	          @if ($errors->has('link'.$adv->id))
	              <span class="help-block">
	                  <strong>{{ $errors->first('link'.$adv->id) }}</strong>
	              </span>
	          @endif
	      </div>
	      <div class="order-7 order-md-4">
	          <small>Active</small>


	          <div class="material-switch">

	             <input type="hidden" value="" name="adv_status" id="{{ 'advActive_'.$adv->id }}" />
	              @if($adv->isActive)
	              <input onclick="handleAdvClick(this)"
	              data-advid="{{$adv->id }}"
	              id="{{ 'advActive'.$adv->id }}"
	               type="checkbox" checked disabled="true">
	              @else
	              <input onclick="handleAdvClick(this)"
	               id="{{ 'advActive'.$adv->id }}"
	               data-advid="{{$adv->id }}"
	               type="checkbox" disabled="true">
	              @endif
	              <label for="{{ 'advActive'.$adv->id }}" class="label-default"></label>
	          </div>

	      </div>
	      <div class="date-range order-8 order-md-5">
	      <!-- name attribute for dateFrom and dateTo is important for datepicker init
	        visit http://api.jqueryui.com/datepicker/#method-setDate to check how to get or set dates -->
	        <label>Start & End time</label>

	        <div class="form-group">
	          <span class="input">
	            <span class="icon-calendar"></span>
	            <input type="text" class="input__field date_from_input" id="{{'dateFrom'.$adv->id}}" name="dateFrom" value="{{$adv->startdate }}" disabled="disabled">
	            <label class="input__label" for="dateFrom">
	                    <span class="input__label-content"></span>
	            </label>
	          </span>
	        </div>
	        <div class="form-group">
	          <span class="input">
	            <span class="icon-calendar"></span>
	            <input type="text" class="input__field date_from_input" id="{{'dateTo'.$adv->id}}" name="dateTo" value="{{$adv->enddate }}" disabled="disabled">
	            <label class="input__label" for="dateTo">
	                    <span class="input__label-content"></span>
	            </label>
	          </span>
	        </div>
					<div class="form-group">
            @if($adv->enddate)
                @if( date("Y-m-d") > date('Y-m-d',strtotime($adv->enddate)))
                <h5 style="color:#6d202b;">EXPIRED</h5>
                @endif
            @endif

					</div>
	      </div>
				<div class="btns-wrap order-1 order-md-6">
					<button type="submit" class="btn btn-primary btn-save  d-none" data-form-target="{{ 'adv_'.$adv->id }}">save</button>
					<button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
				</div>

	      <button type="button" class="btn btn-link order-2 order-md-7" data-form-target="{{ 'adv_'.$adv->id }}"><span class="icon-edit"></span></button>
	      <a href="#deleteAdvModal" data-id="{{ $adv->id }}"
	      onclick="removeAdvModal(this)"
	      data-toggle="modal" class="btn btn-link btn-delete order-3 order-md-8">
	      <span class="icon-delete"></span></a>

	    {!! Form::close() !!}
	    <div class="modal fade" id="deleteAdvModal" tabindex="-1" role="dialog" aria-labelledby="deleteAdvModal" aria-hidden="true">
	        <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
	            <div class="modal-content">
	              {!! Form::open([ 'url' => '/adv/delete' ,
	                               'method' => 'post' ,
	              ]) !!}

	              {{ csrf_field() }}
	              <div class="modal-header">
	                  <h5 class="modal-title">REMOVE ADVERT</h5>
	                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                      <span aria-hidden="true">&times;</span>
	                  </button>
	              </div>
	              <div class="modal-body">
	                ARE YOU SURE YOU WANT TO REMOVE THIS ADVERT ?
	                 <span id="advert_name"></span>
	                 <input type="hidden" name="adv_id" class="advert_record_id" value="" />
	              </div>
	              <div class="modal-footer">
	                  <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
	                  <button type="submit" class="btn btn-primary">REMOVE ADVERT</button>
	              </div>
	              {!! Form::close() !!}
	            </div>
	        </div>
	    </div>
	    @endforeach

		 <a  id="add_new_ad">ADD NEW AD</a>

	 	 <div id="new_ad" style="display:none;">

	 				 {!! Form::open([ 'url' => '/addAdv' ,
	 													'method' => 'post' ,
	 													'id' => 'AddAd' ,
	 													'class' => 'd-flex align-items-center card-row  flex-wrap flex-xl-nowrap',
	 													'enctype' => 'multipart/form-data'
	 				 ]) !!}

	 				 {{ csrf_field() }}

	 							<span class="icon-table"></span>
	 							<div class="form-group inline">
	 									<label class="inline">Upload Ad Picture</label>
	 									<span class="btn btn-default btn-file">
	 												CHOOSE FILE <input type="file" name="advPic" >
	 										</span>
	 							</div>
	 							<div class="form-group">
	 									<span class="input input--filled">
	 												<input class="input__field" name="link" value="" type="text">
	 												<label class="input__label" for="link">
	 													<span class="input__label-content">Link</span>
	 										</label>
	 									</span>

	 							</div>
	 							<div>
	 									<small>Active</small>
	 									<div class="material-switch">
	 										<input type="hidden" value="" name="adv_status" id="advActive" />

	 											<input onclick="handleNewAdvClick(this)"
	 											id="AdvActive"

	 											type="checkbox"  >

	 											<label for="AdvActive" class="label-default"></label>
	 									</div>
	 							</div>
	 							<div class="date-range">
	 							<!-- name attribute for dateFrom and dateTo is important for datepicker init
	 								visit http://api.jqueryui.com/datepicker/#method-setDate to check how to get or set dates -->
	 								<label>Start & End time</label>
	 								<div class="form-group">
	 									<span class="input">
	 										<span class="icon-calendar"></span>
	 										<input type="text" class="input__field" name="dateFrom" value="" >
	 										<label class="input__label" for="dateFrom">
	 														<span class="input__label-content"></span>
	 										</label>
	 									</span>
	 								</div>
	 								<div class="form-group">
	 									<span class="input">
	 										<span class="icon-calendar"></span>
	 										<input type="text" class="input__field" name="dateTo" value="" >
	 										<label class="input__label" for="dateTo">
	 														<span class="input__label-content"></span>
	 										</label>
	 									</span>
	 								</div>
	 							</div>

	 								<button type="submit" class="btn btn-primary btn-save " data-form-target="AddAd">save</button>
	 								<button type="button" id="hideNewAdvForm" class="btn btn-primary btn-cancel">cancel</button>


	 						{!! Form::close() !!}
	 		 </div>
	</div>

<!-- ================= SIDE BAR ADVERTS  ===================== -->

  <div class="card sortable ui-sortable" id="sidebar_adv_sortable">
    <h3 class="card-title">330X812 Ad</h3>

    <div class="row text-center">
      @if ($errors->has('link'))
          <div class="alert alert-danger" style="width:100%;">
              <strong>{{ $errors->first('link') }}</strong>
          </div>
      @endif
    </div>

    @foreach($sidebar_images as $sidbar_adv)

    {!! Form::open([ 'url' => '/sidebar/adv/'.$sidbar_adv->id.'' ,
                     'method' => 'put' ,
                     'id' => 'adv_'.$sidbar_adv->id.'' ,
                     'class' => 'd-flex align-items-center card-row sortable-item flex-wrap flex-xl-nowrap',
                     'enctype' => 'multipart/form-data'
    ]) !!}

    {{ csrf_field() }}

    <input type="hidden" name="adv_type" value="sidebar" />
      <span class="icon-table"></span>
      <div class="form-group inline">
          <label class="inline">Upload Ad Picture</label>
          <span class="btn btn-default btn-file">
                CHOOSE FILE <input type="file"  name="advPic" disabled="disabled">
          </span>
      </div>
      <div class="form-group {{ $errors->has('link'.$sidbar_adv->id) ? ' has-error' : '' }}">
          <span class="input input--filled">
                <input class="input__field" name="{{'link'.$sidbar_adv->id}}" value="{{ $sidbar_adv->link }}" disabled="disabled" type="text">
                <label class="input__label" for="link">
                  <span class="input__label-content">Link</span>
                </label>
          </span>
          @if ($errors->has('link'.$sidbar_adv->id))
              <span class="help-block">
                  <strong>{{ $errors->first('link'.$sidbar_adv->id) }}</strong>
              </span>
          @endif
      </div>
      <div>
          <small>Active</small>


          <div class="material-switch">

             <input type="hidden" value="" name="adv_status" id="{{ 'advActive_'.$sidbar_adv->id }}" />
              @if($sidbar_adv->isActive)
              <input onclick="handleAdvClick(this)"
              data-advid="{{$sidbar_adv->id }}"
              id="{{ 'advActive'.$sidbar_adv->id }}"
               type="checkbox" checked disabled="true">
              @else
              <input onclick="handleAdvClick(this)"
               id="{{ 'advActive'.$sidbar_adv->id }}"
               data-advid="{{$sidbar_adv->id }}"
               type="checkbox" disabled="true">
              @endif
              <label for="{{ 'advActive'.$sidbar_adv->id }}" class="label-default"></label>
          </div>

      </div>
      <div class="date-range">
      <!-- name attribute for dateFrom and dateTo is important for datepicker init
        visit http://api.jqueryui.com/datepicker/#method-setDate to check how to get or set dates -->
        <label>Start & End time</label>

        <div class="form-group">
          <span class="input">
            <span class="icon-calendar"></span>
            <input type="text" class="input__field date_from_input" id="{{'dateFrom'.$sidbar_adv->id}}" name="dateFrom" value="{{$sidbar_adv->startdate }}" disabled="disabled">
            <label class="input__label" for="dateFrom">
                    <span class="input__label-content"></span>
            </label>
          </span>
        </div>
        <div class="form-group">
          <span class="input">
            <span class="icon-calendar"></span>
            <input type="text" class="input__field date_from_input" id="{{'dateTo'.$sidbar_adv->id}}" name="dateTo" value="{{$sidbar_adv->enddate }}" disabled="disabled">
            <label class="input__label" for="dateTo">
                    <span class="input__label-content"></span>
            </label>
          </span>
        </div>
        <div class="form-group">
          @if($sidbar_adv->enddate)
            @if( date("Y-m-d") > date('Y-m-d',strtotime($sidbar_adv->enddate)))
            <h5 style="color:#6d202b;">EXPIRED</h5>
            @endif
          @endif
        </div>
      </div>
      <div class="btns-wrap">
        <button type="submit" class="btn btn-primary btn-save  d-none" data-form-target="{{ 'adv_'.$sidbar_adv->id }}">save</button>
        <button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
      </div>

      <button type="button" class="btn btn-link" data-form-target="{{ 'adv_'.$sidbar_adv->id }}"><span class="icon-edit"></span></button>
      <a href="#deleteAdvModal" data-id="{{ $sidbar_adv->id }}"
      onclick="removeAdvModal(this)"
      data-toggle="modal" class="btn btn-link btn-delete">
      <span class="icon-delete"></span></a>

    {!! Form::close() !!}

    @endforeach



	</div>

	<div class="card">
		<h2 class="card-title">SPONSORS LOGOS</h2>
		<div class="row">
			<div class="col-lg-6">
				<h4 class="card-title">Uploaded Logos</h4>
				<div class="img-preview">
          @foreach($logos as $logo)
          <div>
            <img src="{{ asset('storage/app/public/').'/'.$logo->url }}">
            <button type="button" data-id="{{ $logo->id }}" onclick="removeSponsorLogo(this)" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          @endforeach
				</div>
			</div>
			<div class="col-lg-6">
				<h4 class="card-title">Add New</h4>
				<div class="file-multi-input">
          {!! Form::open([ 'url' => '/updateSponsorLogos' ,
                           'method' => 'post' ,
                           'enctype' => 'multipart/form-data'
          ]) !!}

          {{ csrf_field() }}

					<div class="form-group d-inline-flex justify-content-between align-items-center inline">
				        <label class="inline">Upload Ad Picture</label>
				        <span class="btn btn-default btn-file">
				            CHOOSE FILE <input type="file" name="sponsorlogo"  class="multi" >
				        </span>
					</div>
          <div class="form-group d-inline-flex  align-items-center inline">
              <button type="submit" class="btn  btn-primary">save</button>
          </div>
          {!! Form::close() !!}
				</div>
			</div>
		</div>

	</div>

	<div class="card">
		{!! Form::open([ 'url' => '/updateWelcomeNote' ,
										 'method' => 'post' ,
		]) !!}

		{{ csrf_field() }}
		<h2 class="card-title">WELCOME NOTE</h2>
		<div class="form-group d-flex justify-content-between align-items-center single-input">
	        <span class="input">
	              <input class="input__field" value="{{ $setting->welocme_note }}" name="welcome_note" type="text">
	              <label class="input__label" for="welcome_note">
	                <span class="input__label-content">{{ $setting->welcome_note }}</span>
			      </label>
	        </span>
	        <button type="submit" class="btn btn-sm btn-primary">save</button>
		</div>
		{!! Form::close() !!}
	</div>


</div>

@endsection

@section('customScript')
<script src="/js/home_advs.js"></script>
@endsection
