@extends('layouts.inner') @section('content')

<!-- =================  BOOKING MODAL ===================== -->
@include('includes.booking-modal')
<!-- ================= BOOKING MODAL ===================== -->


<div class="container-fluid breadcrumb-wrap">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/">Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Member profile</li>
	</ol>
</div>
<section class="container-fluid pb-5">
	<div class="card-grid">
		<div class="row">
			<div class="col-lg-3">
				<div class="user-header">
					<div class="thumb-upload">
						<form method="post" enctype="multipart/form-data" id="img_thumb_upload">
							{{ csrf_field() }}
							<div class="image-upload">
								<label for="userImage">
									<span class="icon-upload" style="pointer-events: none"></span>
								</label>
								<input type="hidden" value="{{ $user->id }}" id="photo_member_id" />
								<input id="userImage" name="userImage" onChange="updateUserImage(this)" type="file" />
							</div>
							<div class="thumb">
								@if(! $user->avatarURL || $user->avatarURL == "none")
								<img src="/images/user.jpg"> @else
								<img src="{{ asset('storage/app/public/').'/'.$user->avatarURL }}"> @endif
							</div>
						</form>
					</div>
					<p>{{ $user->username }}</p>
					<small>{{ $user->address }}</small>
				</div>
				<div class="nav flex-column nav-tabs nav-tabs-vertical" role="tablist" aria-orientation="vertical">
					<a class="nav-link {{ !empty($tabName) && $tabName == 'booking' ? 'active' : '' }}" data-toggle="tab" href="#bookingHistory"
					    role="tab" aria-selected="true">BOOKING HISTORY</a>
					<a class="nav-link {{ !empty($tabName) && $tabName == 'clubs' ? 'active' : '' }}" data-toggle="tab" href="#memberClubs" role="tab"
					    aria-selected="false">MEMBER CLUBS</a>
					<a class="nav-link {{ !empty($tabName) && $tabName == 'info' ? 'active' : '' }}" data-toggle="tab" href="#memberInfo" role="tab"
					    aria-selected="false">MEMBER INFO</a>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="tab-content">

					<div class="tab-pane fade {{ !empty($tabName) && $tabName == 'booking' ? 'show active' : '' }}" id="bookingHistory" role="tabpanel">
						<div style="min-height: 650px;">
							<h2 class="card-title text-dark text-uppercase font-weight-regular my-5">booking history</h2>
							<h3 class="text-dark text-capitalize font-weight-regular">Filter Booking</h3>
							<form class="mb-5 mt-3">
								<div class="row">
									<input type="hidden" value="{{ $user->id }}" id="current_user_id" />
									<div class="col-md-3 mb-2">
										<div class="form-group">
											<select class="custom-select" id="branchSelect" name="branchSelect" placeholder="Select Branch…">
												<option value="" selected>Select Branch…</option>
												@foreach($branches as $branch)
												<option value="{{ $branch->id }}">{{ $branch->name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-md-3 mb-2">
										<div class="form-group">
											<select class="custom-select" id="courtSelect" name="courtSelect" placeholder="Select court…">
												<option value="" selected>Select Court…</option>
											</select>
										</div>
									</div>
									<div class="col-md-3 mb-2">
										<div class="form-group">
											<select class="custom-select" id="statusSelect" name="statusSelect" placeholder="Select Status…">
												<option value="" selected>Select Status…</option>
												<option value="upcoming">Upcoming</option>
												<option value="canceled">Canceled</option>
												<option value="refunded">Refunded</option>
												<option value="done">Done</option>
											</select>
										</div>
									</div>
									<div class="col-md-3 ml-auto mb-2">
										<div class="d-flex filter-btns">
											<button type="button" id="GoFilterBtn" class="btn btn-default">GO</button>
											<button type="button" id="GoClearBtn" class="btn btn-cancel">CLEAR</button>
										</div>
									</div>
								</div>
							</form>
							<table class="table table-bordered" id="current-bookings-table">
								<thead>
									<tr>
										<th>Booking ID</th>
										<th>Club Name</th>
										<th>Court Name</th>
										<th>Date</th>
										<th>Time</th>
										<th>Coach</th>
										<th>Status</th>
										<th>Total</th>
									</tr>
								</thead>
							</table>

						</div>

					</div>
					<div class="tab-pane fade {{ !empty($tabName) && $tabName == 'clubs' ? 'show active' : '' }}" id="memberClubs" role="tabpanel">
						<div class="pt-4">
							<h2 class="title">MEMBER CLUBS</h2>
						</div>

						<!-- ====================  USER CLUB ROW ========================== -->

						@foreach($branches as $branch)

						<div class="d-flex align-items-center mt-4">
							<div class="thumb f-s">
								@if(! $branch->club->logoUrl || $branch->club->logoUrl == "none")
								<img src="/images/club.png"> @else
								<img src="{{ asset('storage/app/public/').'/'.$branch->club->logoUrl }}"> @endif
							</div>
							<p>{{ $branch->club->name}} - {{ $branch->name }}</p>
						</div>
						<h3 class="title">Membership Information</h3>
						{!! Form::open([ 'url' => '/branch/member/'.$branch->member->where('user_id',$user->id)->first()->id.'' , 'method' => 'put'
						, 'id' => 'branchMembershipInfo_'.$branch->id.'' , 'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap'
						]) !!} {{ csrf_field() }}



						<input type="hidden" name="branch_id" value="{{ $branch->id }}" />
						<div class="form-group">
							<span class="input input--filled">
								<input class="input__field" name="name" value="{{ $branch->member->where('user_id',$user->id)->first()->membershipName }}"
								    disabled="disabled" required type="text">
								<label class="input__label" for="name">
									<span class="input__label-content">Name * </span>
								</label>
							</span>
						</div>
						<div class="form-group {{ $errors->has('phone'.$branch->member->where('user_id',$user->id)->first()->id) ? ' has-error' : '' }}">
							<span class="input input--filled">
								<input class="input__field" name="{{'phone'.$branch->member->where('user_id',$user->id)->first()->id}}" value="{{ $branch->member->where('user_id',$user->id)->first()->user->phone }}"
								    disabled="disabled" required type="tel">
								<label class="input__label" for="phone">
									<span class="input__label-content">Phone * </span>
								</label>
							</span>

							@if ($errors->has('phone'.$branch->member->where('user_id',$user->id)->first()->id))
							<span class="help-block">
								<strong>{{ $errors->first('phone'.$branch->member->where('user_id',$user->id)->first()->id) }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group {{ $errors->has('membershipId'.$branch->member->where('user_id',$user->id)->first()->id) ? ' has-error' : '' }}">
							<span class="input input--filled">
								<input class="input__field" name="{{'membershipId'.$branch->member->where('user_id',$user->id)->first()->id}}" value="{{ $branch->member->where('user_id',$user->id)->first()->membershipId }}"
								    disabled="disabled" required type="text">
								<label class="input__label" for="membershipId">
									<span class="input__label-content">Member ID * </span>
								</label>
							</span>
							@if ($errors->has('membershipId'.$branch->member->where('user_id',$user->id)->first()->id))
							<span class="help-block">
								<strong>{{ $errors->first('membershipId'.$branch->member->where('user_id',$user->id)->first()->id) }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group" style="display:none;">
							<span class="input">
								<input type="text" class="input__field datepicker" name="expiryDate" value="1/11/2019" disabled="disabled">
								<label class="input__label" for="expiryDate">
									<span class="input__label-content">Membership Expiry Date</span>
								</label>
							</span>
						</div>
						<div class="btns-wrap">
							<button type="submit" class="btn btn-primary btn-save d-none" data-form-target="{{'branchMembershipInfo_'.$branch->id}}">save</button>
							<button type="button" class="btn btn-primary btn-cancel d-none">cancel</button>
						</div>
						<button type="button" class="btn btn-link" data-form-target="{{'branchMembershipInfo_'.$branch->id}}">
							<span class="icon-edit"></span>
						</button>

						<a href="#deAttachMemberModal" data-memberid="{{ $branch->member->where('user_id',$user->id)->first()->id }}" data-branchid="{{ $branch->id }}"
						    onclick="deAttachMemberModal(this)" data-toggle="modal" class="btn btn-link btn-delete">
							<span class="icon-delete"></span>
						</a>
						</form>

						@endforeach

						<div class="modal fade" id="deAttachMemberModal" tabindex="-1" role="dialog" aria-labelledby="deAttachMemberModal" aria-hidden="true">
							<div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
								<div class="modal-content">
									{!! Form::open([ 'url' => '/branch/member/delete' , 'method' => 'post' , ]) !!} {{ csrf_field() }}
									<div class="modal-header">
										<h5 class="modal-title">Remove member from this branch</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										ARE YOU SURE YOU WANT TO DO THIS ACTION ?
										<span id="advert_name"></span>
										<input type="text" name="branch_id" class="member_branch_id" value="" />
										<input type="text" name="member_id" class="branch_member_id" value="" />
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
										<button type="submit" class="btn btn-primary">REMOVE BRANCH</button>
									</div>
									{!! Form::close() !!}
								</div>
							</div>
						</div>


						<!-- ==================== END USER CLUB ROW ========================== -->

					</div>

					<div class="tab-pane fade {{ !empty($tabName) && $tabName == 'info' ? 'show active' : '' }}" id="memberInfo" role="tabpanel">
						<div class="d-inline-flex justify-content-between align-items-center pt-4">
							<h2 class="title">MEMBER INFO</h2>
							<button type="button" class="btn btn-link" data-form-target="editInfo">
								<span class="icon-edit"></span>
							</button>
						</div>

						{!! Form::open([ 'url' => '/users/'.$user->id.'' , 'method' => 'put' , 'id' => 'editInfo' , 'enctype' => 'multipart/form-data'
						]) !!} {{ csrf_field() }}
						<div class="d-flex">

							<input type="hidden" name="member_id" value="{{ $user->id }}" />
							<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
								<span class="input">
									<input class="input__field" type="text" name="username" value="{{ $user->username }}" disabled="true" />
									<label class="input__label" for="username">
										<span class="input__label-content">Username</span>
									</label>
								</span>
								@if ($errors->has('username'))
								<span class="help-block">
									<strong>{{ $errors->first('username') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="d-flex">
							<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
								<span class="input">
									<input class="input__field" type="email" name="email" value="{{ $user->email }}" disabled="true" />
									<label class="input__label" for="email">
										<span class="input__label-content">Email</span>
									</label>
								</span>
								@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="d-flex">
							<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
								<span class="input">
									<input class="input__field" type="text" name="phone" value="{{ $user->phone }}" disabled="true" />
									<label class="input__label" for="phone">
										<span class="input__label-content">Phone</span>
									</label>
								</span>
								@if ($errors->has('phone'))
								<span class="help-block">
									<strong>{{ $errors->first('phone') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="d-flex">
							<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
								<span class="input">
									<input class="input__field" type="text" name="address" value="{{ $user->address }}" disabled="true" />
									<label class="input__label" for="location">
										<span class="input__label-content">Address</span>
									</label>
								</span>
								@if ($errors->has('address'))
								<span class="help-block">
									<strong>{{ $errors->first('address') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<button type="submit" class="btn btn-primary mb-3 d-none" data-form-target="editInfo">save</button>
						</form>
						{!! Form::open([ 'url' => '/reset/user/password' , 'method' => 'post' , ]) !!} {{ csrf_field() }}
						<input type="hidden" name="member_id" value="{{ $user->id }}" />
						<input type="hidden" name="user_id" value="{{ $user->id }}" />
						<a href="#" class="change-pw">CHANGE PASSWORD</a>
						<div class="d-flex @if( ! $errors->has('formShow') && !$errors->has('password')) changePassField d-none @endif ">
							<div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
								<span class="input input--filled">
									<input class="input__field" name="old_password" type="password">
									<label class="input__label" for="old_password">
										<span class="input__label-content">Old Password</span>
									</label>
								</span>
								@if($errors->has('old_password'))
								<span class="help-block">
									<strong>{{ $errors->first('old_password') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="d-flex @if( ! $errors->has('formShow') && !$errors->has('password')) changePassField d-none @endif">
							<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
								<span class="input input--filled">
									<input class="input__field" name="password" type="password">
									<label class="input__label" for="password">
										<span class="input__label-content">New Password</span>
									</label>
								</span>
								@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="d-flex @if( ! $errors->has('formShow') && !$errors->has('password')) changePassField d-none @endif">
							<div class="form-group">
								<span class="input input--filled">
									<input class="input__field" name="password_confirmation" type="password">
									<label class="input__label" for="password_confirmation">
										<span class="input__label-content">Rewrite New Password</span>
									</label>
								</span>
							</div>
						</div>
						<div class="btns-wrap @if( ! $errors->has('formShow') && !$errors->has('password')) changePassField d-none @endif">
							<button type="submit" class="btn btn-primary btn-save">save</button>
							<button type="button" class="btn btn-primary btn-cancel">cancel</button>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

<!-- these scripts will be placed in the inner layout footer -->
@section('customScript')
<script src="/js/system_member.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection