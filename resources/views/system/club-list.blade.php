@extends('layouts.inner')
@section('content')

<!-- Add Club Modal -->
<div class="modal fade" id="addClubModal" tabindex="-1" role="dialog" aria-labelledby="addClub" aria-hidden="true">
    <div class="modal-dialog modal-lg d-md-flex flex-md-column justify-content-md-center my-md-0" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ADD CLUB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="card-title">Club Info</h2>

                  {!! Form::open([ 'url' => '/addClub' ,
                                  'method' => 'post',
                                  'enctype' => 'multipart/form-data'
                  ]) !!}

                    {{ csrf_field() }}

                  <div class="d-flex align-items-center flex-wrap flex-xl-nowrap">
                    <div class="thumb-upload">
                        <div class="image-upload">
                          <label for="clubImage">
                            <span class="icon-upload" style="pointer-events: none"></span>
                        </label>
                        <input id="clubImage" type="file" name="club_avatar"   />
                        </div>
                        <div class="thumb">
                        </div>
                    </div>
                      <div class="form-group">
                          <span class="input">
                            <input class="input__field" type="text" name="club_name" required />
                            <label class="input__label" for="club_name">
                              <span class="input__label-content">Club Name *</span>
                          </label>
                          </span>
                      </div>
                      <div class="form-group inline">
                          <label class="inline" for="clubActive">Club Active</label>
                          <input type="hidden" name="club_status" id="new_Club_Active" />

                          <div class="material-switch">
                              <input id="newClubActive"
                                onclick="newClubClick(this)"
                                type="checkbox">
                              <label for="newClubActive" class="label-default"></label>
                          </div>
                      </div>
                  </div>


                <h2 class="card-title">Club First Branch</h2>
                <div class="d-flex align-items-center flex-wrap flex-xl-nowrap">
                  <div class="form-group">
                      <span class="input">
                        <input class="input__field" type="text" name="branch_name" required />
                        <label class="input__label" for="branch_name">
                          <span class="input__label-content">Branch Name *</span>
                      </label>
                      </span>
                  </div>
                  <div class="form-group inline">
                      <small>Branch Active</small>
                      <input type="hidden" name="branch_status" id="Club_Branch_Active" />

                      <div class="material-switch">
                          <input id="ClubBranchActive"
                            onclick="ClubBranchClick(this)"
                            type="checkbox" >
                          <label for="ClubBranchActive" class="label-default"></label>
                      </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">ADD CLUB</button>
            </div>
              {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="container-fluid breadcrumb-wrap">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Club listing</li>
    </ol>
</div>

<section class="container-fluid pb-5">
    <div class="section-head">
        <h1>clubs</h1>
        <a href="#addClubModal" data-toggle="modal">Add club</a>
    </div>
    <div class="accordion" id="adminClubsAccordion" data-children=".item">
      @foreach($clubs as $club)
      <div class="item">
          <a data-toggle="collapse" data-parent="#adminClubsAccordion" href="#{{ 'club_'.$club->id }}" role="button" aria-expanded="true" aria-controls="{{ 'club_'.$club->id }}">
              <div class="d-flex align-items-center">
                  <div class="thumb">
                    @if(! $club->logoUrl || $club->logoUrl == "none")
                    <img src="/images/club.png">
                    @else
                    <img src="{{ asset('storage/app/public/').'/'.$club->logoUrl }}">
                    @endif
                  </div>
                  <h2>{{ $club->name }}</h2>
              </div>
              <span>^</span>
          </a>
          <div id="{{ 'club_'.$club->id }}" class="tab-pannel collapse " role="tabpanel">
              <div class="card card">
                  <h2 class="card-title">Club Info <!-- <button type="button" class="btn btn-float" data-form-target="{{'clubInfoForm_'.$club->id }}"><span class="icon-edit"></span></button> --></h2>

                    {!! Form::open([ 'url' => '/clubs/'.$club->id.'' ,
                                    'method' => 'put' ,
                                     'id' => 'clubInfoForm_'.$club->id ,
                                     'class' => 'd-flex align-items-center flex-wrap flex-xl-nowrap' ,
                                     'enctype' => 'multipart/form-data'
                    ]) !!}

                      {{ csrf_field() }}
                      <div class="form-group">
                          <span class="input">
                                <input class="input__field" type="text" name="name" value="{{ $club->name }}" disabled="true" />
                                <label class="input__label" for="text">
                                  <span class="input__label-content"> CLUB NAME</span>
                                </label>
                          </span>
                      </div>
                      <p class="file-label">Change Club Logo</p>
                      <span class="btn btn-default btn-file">
                              Browse <input type="file" name="avatar" disabled="true">
                      </span>
                      <p>Club Active</p>
                      <div class="material-switch">
                        <input type="hidden" value="" name="club_status" id="{{ 'clubActive_'.$club->id }}" />
                        @if($club->isActive)
                        <input
                        onclick="handleclubClick(this)"
                        data-clubid="{{$club->id }}"
                        id="{{ 'switchclub_'.$club->id }}"
                         type="checkbox"
                         checked disabled="false">
                        @else
                        <input
                        onclick="handleclubClick(this)"
                        data-clubid="{{$club->id }}"
                        id="{{ 'switchclub_'.$club->id }}"
                        type="checkbox" disabled="false">
                        @endif
                          <label for="{{ 'switchclub_'.$club->id }}" class="label-default"></label>
                      </div>

                  <div class="btns-wrap">
                    <button type="submit" class="btn btn-primary btn-save d-none m-0 align-self-center" data-form-target="{{'clubInfoForm_'.$club->id }}">save</button>
                    <button type="button" class="btn btn-primary btn-cancel d-none m-0 ">cancel</button>
                  </div>
                  <button type="button" class="btn btn-link" data-form-target="{{'clubInfoForm_'.$club->id }}"><span class="icon-edit"></span></button>
                    {!! Form::close() !!}
              </div>
              <div class="card card">
                  <h2 class="card-title">Club Branches</h2>


                  @foreach($club->branches as $branch )
                  {!! Form::open([ 'url' => '/branchesBasic/'.$branch->id.'' ,
                                  'method' => 'put' ,
                                   'id' => 'branch_form'.$branch->id.'' ,
                                    'class' => 'd-flex align-items-center card-row flex-wrap flex-xl-nowrap'
                  ]) !!}
                  <!-- <form id="{{ 'branch_form'.$branch->id }}" class="d-flex align-items-center card-row"> -->
                    {{ csrf_field() }}
                      <div class="form-group">
                          <span class="input">
                                <input class="input__field" type="text" name="branch_name" value="{{ $branch->name }}" disabled="true" />
                                <label class="input__label" for="text">
                                  <span class="input__label-content">BRANCH NAME</span>
                          </label>
                          </span>
                      </div>
                      <div>
                          <small>Branch Active</small>
                          <div class="material-switch">

                             <input type="hidden" value="" name="branch_status" id="{{ 'branchActive_'.$branch->id }}" />
                              @if($branch->isActive)
                              <input onclick="handlebranchClick(this)"
                              data-branchid="{{$branch->id }}"
                              id="{{ 'branchActive'.$branch->id }}"
                               type="checkbox" checked disabled="true">
                              @else
                              <input onclick="handlebranchClick(this)"
                               id="{{ 'branchActive'.$branch->id }}"
                               data-branchid="{{$branch->id }}"
                               type="checkbox" disabled="true">
                              @endif
                              <label for="{{ 'branchActive'.$branch->id }}" class="label-default"></label>
                          </div>
                      </div>
                      <a href="/branch/{{ $branch->id }}/booking" class="btn btn-default">VIEW BRANCH</a>
                      <div class="btns-wrap">
                        <button type="submit" class="btn btn-primary btn-save d-none m-0 align-self-center" data-form-target="{{ 'branch_form'.$branch->id }}">save</button>
                        <button type="button" class="btn btn-primary btn-cancel d-none m-0">cancel</button>
                      </div>
                      <button type="button" class="btn btn-link" data-form-target="{{ 'branch_form'.$branch->id }}"><span class="icon-edit"></span></button>
                      <!-- <button type="submit" class="btn btn-primary d-none" data-form-target="{{ 'branch_form'.$branch->id }}">save</button> -->
                     <!--  <button type="button" class="btn btn-link" data-form-target="{{ 'branch_form'.$branch->id }}"><span class="icon-edit"></span></button> -->
                  <!-- </form> -->
                  {!! Form::close() !!}
                  @endforeach

                <a onclick="openNewBranchForm(this)" id="{{'add_new_branch_'.$club->id}}"
                  data-clubid="{{ $club->id }}" >ADD BRANCH</a>

                <div id="{{'newBranch_'.$club->id}}" style="display:none;">
                  {!! Form::open([ 'url' => '/addBranch' ,
                                  'method' => 'post' ,
                                   'id' => 'new_branch_form'.$club->id.'' ,
                                   'class' => 'd-flex align-items-center card-row'
                  ]) !!}

                    {{ csrf_field() }}
                    <input type="hidden" name="club_id" value="{{ $club->id }}" />

                      <div class="form-group">
                          <span class="input">
                                <input class="input__field" type="text" name="branch_name" value=""  required />
                                <label class="input__label" for="branch_name">
                                  <span class="input__label-content">  Branch Name </span>
                          </label>
                          </span>
                      </div>
                      <div>
                          <small>Branch Active</small>
                          <div class="material-switch">

                             <input type="hidden" value="" name="branch_status" id="{{ 'newBranchActive_'.$club->id }}" />

                              <input onclick="newBranchClick(this)"
                              data-clubid="{{ $club->id }}"
                              id="{{ 'newBranchActive'.$club->id }}"
                               type="checkbox"  >

                              <label for="{{ 'newBranchActive'.$club->id }}" class="label-default"></label>
                          </div>
                      </div>


                        <button type="submit" class="btn btn-primary btn-save m-0 align-self-center" data-form-target="{{ 'new_branch_form'.$club->id }}">save</button>
                        <button type="button"
                        onclick="hideNewBranchForm(this)"
                        data-clubid="{{ $club->id }}"
                         class="btn btn-primary btn-cancel m-0">cancel</button>




                  {!! Form::close() !!}
                </div>
              </div>
          </div>
      </div>
      @endforeach

    </div>
</section>


@endsection

@section('customScript')

<script src="/js/clublist.js"></script>
@endsection
