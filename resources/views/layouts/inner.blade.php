<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" type="image/png" href="/images/favicon16.png"/>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700|Work+Sans:700" rel="stylesheet">
	<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

    <!-- jQuery UI CSS -->
    <link rel="stylesheet" href="/css/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/jquery-ui-timepicker-addon.min.css">

    <!-- chosen css -->
    <link rel="stylesheet" href="/css/chosen.min.css">
    <!-- jTable css -->

    <!-- Main CSS -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/main.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    @yield('customCss')
    <title>S-Team</title>
</head>

<body class="inner">
<div class="page-preloader"></div>
<div class="page-preloader2"></div>
@include('includes.header')

  @yield('content')
    <footer class="container-fluid">
        <p>© 2017- S-Team. All Rights reserved. Designed & Developed by <a target="_blank" href="http://www.road9media.com">Road9 Media</a></p>
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="/info/terms">Terms & Conditions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/info/cancel">Cancellation & Refund Policy</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/info/privacy">Privacy Policy</a>
            </li>
        </ul>
    </footer>
    <!-- /header -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <!-- Owl Script -->
    <script src="/js/owl.carousel.min.js"></script>
    <!-- Classie -->
    <script src="/js/classie.js"></script>
    <!-- Chosen -->
    <script src="/js/chosen.jquery.min.js"></script>
    <!-- jquery Ui Script -->
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/jquery-ui-timepicker-addon.min.js"></script>
    <!-- jquery Ui touch Script -->
    <script src="/js/jQuery-ui-touch.js"></script>
    <!-- jTable -->
    <script src="/js/jquery.jtable.min.js"></script>
    <!-- main Script -->
    <script src="/js/main.js"></script>

    <script>
    $(window).on('load', function(){
    $('.page-preloader2').fadeOut(2000);
      });
    </script>

    @yield('customScript')
    @yield('vuescripts')

</body>

</html>
