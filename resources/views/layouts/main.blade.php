<!doctype html>
<html lang="en">
	<!-- this is for home page only -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/png" href="/images/favicon16.png"/>
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700|Work+Sans:700" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/main.css">
    <title>S-Team</title>
</head>
<body>
<div class="page-preloader"></div>
<div class="page-preloader2"></div>
@include('includes.header')

  @yield('content')
    <footer class="container-fluid">
        <p>© 2017- S-Team. All rights reserved. Designed & Developed by <a target="_blank" href="http://www.road9media.com">Road9 Media</a></p>
        <ul class="nav">
          <li class="nav-item">
              <a class="nav-link active" href="/info/terms">Terms & Conditions</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/info/cancel">Cancellation & Refund Policy</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/info/privacy">Privacy Policy</a>
          </li>
        </ul>
    </footer>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <!-- Owl Script -->
    <script>
   // $( document ).ready(function() {
   //     console.log( "document loaded" );
   //     $('.preloader').show();
   // });
   //
   // $( window ).on( "load", function() {
   //     console.log( "window loaded" );
   //     $('.preloader').hide();
   // });
   </script>
    <script src="/js/owl.carousel.min.js"></script>
    <!-- Classie -->
    <script src="/js/classie.js"></script>
    <!-- main Script -->
    <script src="/js/main.js"></script>
    @yield('customScript')

        <script>
        $(window).on('load', function(){
        $('.page-preloader2').fadeOut(2000);
          });
        </script>
</body>

</html>
