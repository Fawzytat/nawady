@extends('layouts.inner')
@section('customCss')

<link rel='stylesheet' href='/fullcalendar/fullcalendar.css' />
<link rel='stylesheet' href='/fullcalendar/scheduler.min.css' />

<script src='/fullcalendar/lib/moment.min.js'></script>
<script src='/fullcalendar/fullcalendar.js'></script>
<script src='/fullcalendar/scheduler.min.js'></script>

@endsection
@section('content')

<!-- ================= ADD CLUB MODAL ===================== -->
      @include('includes.verify-branch-admin')
<!-- ============ END OF  ADD CLUB MODAL ============== -->

<div class="container-fluid breadcrumb-wrap">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        @if(Auth::user()->hasRole('sys_admin'))
        <li class="breadcrumb-item"><a href="/club-list">Club Listing</a></li>
        @endif
        <li class="breadcrumb-item active" aria-current="page">Club Dashboard</li>
    </ol>
</div>
<input type="hidden" id="current_branch_id" value="{{ $branch->id }}" />
<section class="container-fluid">
    <div class="row">
        <div class="col-lg-4">
            <div class="card card-horizontal">
             <a href="{{'/club-page/'.$branch->id.'/court-booking/default'}}">
                <div class="thumb">
                    <img src="{{ asset('storage/app/public/').'/'.$branch->club->logoUrl }}">
                </div>
              </a>

                <div class="mr-auto">
                    <p>{{ $branch->club->name }}</p>
                    <small>{{ $branch->name }}</small>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="dropdown d-md-none">
              <button class="btn btn-success btn-block btn-pill-nav dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                BOOKINGS
              </button>
              <div class="dropdown-menu nav" aria-labelledby="dropdownMenuButton" role="tablist">
                <a class="dropdown-item" data-toggle="pill" href="#pills-booking">BOOKINGS</a>
                <a class="dropdown-item" data-toggle="pill" href="#pills-settings">SERVICES SETTINGS</a>
                <a class="dropdown-item" data-toggle="pill" href="#pills-info">MEMBERS INFO</a>
                <a class="dropdown-item" data-toggle="pill" href="#pills-payment">PAYMENT REPORTING</a>
                <a class="dropdown-item" data-toggle="pill" href="#pills-branch">BRANCH INFO</a>
              </div>
            </div>
            <ul class="nav nav-pills mb-3 mt-5 d-none d-md-flex" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link  {{ !empty($tabName) && $tabName == 'booking' ? 'active' : '' }}" id="pills-home-tab" data-toggle="pill" href="#pills-booking" role="tab" aria-controls="pills-home" aria-selected="true">BOOKINGS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ !empty($tabName) && $tabName == 'setting' ? 'active' : '' }}" id="pills-profile-tab" data-toggle="pill" href="#pills-settings" role="tab" aria-controls="pills-profile" aria-selected="false">SERVICES SETTINGS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ !empty($tabName) && $tabName == 'members' ? 'active' : '' }}" id="pills-contact-tab" data-toggle="pill" href="#pills-info" role="tab" aria-controls="pills-contact" aria-selected="false">MEMBERS INFO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ !empty($tabName) && $tabName == 'payment' ? 'active' : '' }}" id="pills-contact-tab" data-toggle="pill" href="#pills-payment" role="tab" aria-controls="pills-contact" aria-selected="false">PAYMENT REPORTING</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ !empty($tabName) && $tabName == 'branchinfo' ? 'active' : '' }}" id="pills-contact-tab" data-toggle="pill" href="#pills-branch" role="tab" aria-controls="pills-contact" aria-selected="false">BRANCH INFO</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade {{ !empty($tabName) && $tabName == 'booking' ? 'show active' : '' }}" id="pills-booking" role="tabpanel" aria-labelledby="pills-booking-tab">
                    @include('branch.bookings')
                </div>
                <div class="tab-pane fade {{ !empty($tabName) && $tabName == 'setting' ? 'show active' : '' }}" id="pills-settings" role="tabpanel" aria-labelledby="pills-settings-tab">
                  		@include('branch.service-setting')
                </div>
                <div class="tab-pane fade {{ !empty($tabName) && $tabName == 'members' ? 'show active' : '' }}" id="pills-info" role="tabpanel" aria-labelledby="pills-info-tab">
                      @include('branch.members-info')
                </div>
                <div class="tab-pane fade {{ !empty($tabName) && $tabName == 'payment' ? 'show active' : '' }} " id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                      @include('branch.payment-report')
                </div>
                <div class="tab-pane fade {{ !empty($tabName) && $tabName == 'branchinfo' ? 'show active' : '' }}" id="pills-branch" role="tabpanel" aria-labelledby="pills-branch-tab">
                      @include('branch.info')
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('customScript')
<script src="/js/branch/branchinfo.js"></script>
<script src="/js/branch/servicesettings.js"></script>
<script src="/js/branch/membersinfo.js"></script>
<script src="/js/branch/bookings.js"></script>
<script src="/js/branch/payment.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!-- Export btn  -->
<script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
@endsection
