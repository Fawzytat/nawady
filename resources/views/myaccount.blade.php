@extends('layouts.inner')
@section('content')

<!-- =================  BOOKING MODAL ===================== -->
       @include('includes.booking-modal')
<!-- ================= BOOKING MODAL ===================== -->
<!-- =================  BOOKING Cancel MODAL ===================== -->
       @include('includes.booking-cancel')
<!-- ================= BOOKING Cancel MODAL ===================== -->

<div class="container-fluid breadcrumb-wrap">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">My Account</li>
    </ol>
</div>
<section class="container-fluid pb-5">
    <div class="row">
        <div class="col-lg-3 mb-5">
            <div class="card card-user">a
                <div class="card-header">
                    <div class="thumb-upload">


                        <form method="post" enctype="multipart/form-data" id="img_thumb_upload">
                          {{ csrf_field() }}
            	                    <div class="image-upload">
            	                        <label for="userImage">
            	                            <span class="icon-upload" style="pointer-events: none"></span>
            	                        </label>
                                      <input type="hidden" value="{{ Auth::user()->id }}" id="photo_member_id"  />
            	                        <input id="userImage" name="userImage" onChange="updateUserImage(this)" type="file" />
            	                    </div>
            	                    <div class="thumb">
                                      @if(! Auth::user()->avatarURL || Auth::user()->avatarURL == "none")
                                      <img src="/images/user.jpg">
                                      @else
                                      <img src="{{ asset('storage/app/public/').'/'.Auth::user()->avatarURL }}">
                                      @endif
            	                    </div>
                          </form>


                    </div>
                    <p>{{ Auth::user()->username }}</p>
                    <small>{{ Auth::user()->address }}</small>
                    @if (session('status'))
                          <button type="button" class="btn btn-float @if($errors) d-none @endif" id="editMemberInfo"><span class="icon-edit"></span></button>
                    @else
                    <button type="button" class="btn btn-float" id="editMemberInfo"><span class="icon-edit"></span></button>
                    @endif

                </div>
                <div class="card-body">
                  {!! Form::open([
                    'url' => '/users/'.Auth::user()->id.'' ,
                    'method' => 'put' ,
                    'id' => 'editInfo',
                    'enctype' => 'multipart/form-data'
                    ]) !!}

                        {{ csrf_field() }}
                        <div class="d-flex">
                            <span class="icon-mail"></span>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <span class="input">
                                  @if (session('status'))
                                  <input class="input__field" type="email" name="email" value="{{ Auth::user()->email }}" @if(!$errors) disabled @endif/>
                                  @else
                                  <input class="input__field" type="email" name="email" value="{{ Auth::user()->email }}"  disabled/>
                                  @endif

                                  <label class="input__label" for="email">
                                    <span class="input__label-content">Email</span>
                                </label>
                                </span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex">
                            <span class="icon-phone"></span>
                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <span class="input">
                                  @if (session('status'))
                                  <input class="input__field" type="text" name="phone" value="{{ Auth::user()->phone }}" @if(!$errors) disabled @endif/>
                                  @else
                                  <input class="input__field" type="text" name="phone" value="{{ Auth::user()->phone }}"  disabled />
                                  @endif
                                  <label class="input__label" for="phone">
                                    <span class="input__label-content">Phone</span>
                                </label>
                                </span>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex">
                            <span class="icon-location"></span>
                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                <span class="input">
                                  @if (session('status'))
                                  <input class="input__field" type="text" name="address" value="{{ Auth::user()->address }}" @if(!$errors) disabled @endif/>
                                  @else
                                  <input class="input__field" type="text" name="address" value="{{ Auth::user()->address }}"  disabled/>
                                  @endif
                                  <label class="input__label" for="location">
                                    <span class="input__label-content">Address</span>
                                </label>
                                </span>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                </div>

                <div class="card-footer">
                  <div class="btns-wrap">
                    @if (session('status'))
                    <button type="submit" class="btn btn-save @if(! $errors) d-none @endif" id="save">save</button>
                    <button type="button" class="btn btn-cancel  @if(! $errors) d-none @endif" id="cancel">Cancel</button>
                    @else
                    <button type="submit" class="btn btn-save  d-none" id="save">save</button>
                    <button type="button" class="btn btn-cancel  d-none" id="cancel">Cancel</button>
                    @endif
                  </div>


                <!-- <div class="card-footer d-flex flex-row justify-content-center">
					<button type="submit" class="btn btn-save d-none" id="save">save</button>
					<button type="button" class="btn btn-cancel  d-none" id="cancel">Cancel</button> -->

                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();" class="btn btn-default btn-block d-block" id="logout">logout</a>


                  <!-- </form> -->
                  {!! Form::close() !!}
                </div>

                {!! Form::open([ 'url' => '/reset/user/password' ,
                                'method' => 'post' ,

                ]) !!}

                  {{ csrf_field() }}

                    <a href="#" class="change-pw">CHANGE PASSWORD</a>
                    <div class="d-flex @if( ! $errors->has('formShow') && !$errors->has('password')) d-none changePassField @endif">
                        <div class="form-group w-100 {{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <span class="input">
                                  <input class="input__field" type="password" name="old_password"  />
                                  <label class="input__label" for="old_password">
                                    <span class="input__label-content">Old Password</span>
                                  </label>
                            </span>
                            @if ($errors->has('old_password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('old_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="d-flex @if( ! $errors->has('formShow') && !$errors->has('password')) d-none changePassField @endif">
                      <div class="form-group w-100 {{ $errors->has('password') ? ' has-error' : '' }}">
            			        <span class="input input--filled">
            			              <input class="input__field" name="password"  type="password">
            			              <label class="input__label" for="password">
            			                <span class="input__label-content">New Password</span>
            					      </label>
            			        </span>
                          @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
            		    	</div>
                    </div>
                    <div class="d-flex @if( ! $errors->has('formShow') && !$errors->has('password')) d-none changePassField @endif">
                      <div class="form-group w-100">
                          <span class="input input--filled">
                                <input class="input__field" name="password_confirmation"  type="password">
                                <label class="input__label" for="password_confirmation">
                                  <span class="input__label-content">Rewrite New Password</span>
                            </label>
                          </span>
                      </div>
                    </div>
                    <div class="btns-wrap d-flex flex-row justify-content-start @if( ! $errors->has('formShow') && !$errors->has('password')) d-none changePassField @endif">
                        <button type="submit"  class="btn btn-primary btn-save">save</button>
                        <button type="button"  class="btn btn-primary btn-cancel">cancel</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-9 mb-5">
            <div class="card" style="min-height: 650px;">
                <h2 class="card-title text-dark my-5 text-uppercase font-weight-bold">booking history</h2>
                <h3 class="text-secondary text-capitalize font-weight-bold ">Filter Booking</h3>
                <form class="mt-2 mb-5">
                  <div class="row">
                    <input type="hidden" value="{{ Auth::user()->id }}" id="current_user_id"  />
                    <div class="col-md-3 mb-2">
                      <div class="form-group">
                        <select class="custom-select" id="branchSelect" name="branchSelect" placeholder="Select Branch…">
                          <option value="" selected>Select Branch…</option>
                          @foreach($branches as $branch)
                          <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                          @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-md-3 mb-2">
                      <div class="form-group">
                          <select class="custom-select" id="courtSelect" name="courtSelect" placeholder="Select court…">
                                <option value="" selected>Select Court…</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-3 mb-2">
                      <div class="form-group">
                        <select class="custom-select" id="statusSelect" name="statusSelect" placeholder="Select Status…">
                                <option value="" selected>Select Status…</option>
                                <option value="upcoming">Upcoming</option>
                                <option value="canceled">Canceled</option>
                                <option value="refunded">Refunded</option>
                                <option value="done">Done</option>
                              </select>
                        </div>
                    </div>
                    <div class="col-md-3 ml-auto mb-2">
                      <div class="d-flex filter-btns">
                          <button type="button" id="GoFilterBtn" class="btn btn-default">GO</button>
                          <button type="button" id="GoClearBtn" class="btn btn-cancel">CLEAR</button>
                            <!-- <div id="example_wrapper"></div>                     -->
                      </div>
                    </div>
                  </div>
                </form>

                <!-- Datatable -->

                <!-- <input type="hidden" id="current_month" value="{{ date('m') }}" /> -->
                <table class="table table-bordered" id="current-bookings-table">
                     <thead>
                         <tr>
                             <th>Booking ID</th>
                             <th>Club Name</th>
                             <th>Court Name</th>
                             <th>Date</th>
                             <th>Time</th>
                             <th>Coach</th>
                             <th>Status</th>
                             <th>Total</th>
                             <th></th>
                         </tr>
                     </thead>
                 </table>

            </div>
        </div>
    </div>
</section>

@endsection

@section('customScript')
<script src="/js/my-account.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!-- Export btn  -->
<!-- <script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script> -->
@endsection
