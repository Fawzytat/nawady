@extends('layouts.inner')


@section('content')
<!-- =================  LOGIN MODAL ===================== -->
      @include('includes.login_modal')
<!-- ================= END LOGIN MODAL ===================== -->
<!-- =================  REGISTER MODAL ===================== -->
      @include('includes.register_modal')
<!-- ================= END REGISTER MODAL ===================== -->
<!-- =================  FORGET PASSWORD MODAL ===================== -->
      @include('includes.forget_password')
<!-- ================= END FORGET PASSWORD MODAL ===================== -->

<section id="termsAndConditions">
  <div class="container-fluid">
    <div class="row mx-5">
      <div class="card col">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link text-uppercase font-weight-bold text-dark py-3 mr-4 {{ !empty($current_tab) && $current_tab == 'terms' ? 'active' : '' }}" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
             aria-selected="true">Terms & Conditions</a>
            <a class="nav-item nav-link text-uppercase font-weight-bold text-dark py-3 mr-4 {{ !empty($current_tab) && $current_tab == 'privacy' ? 'active' : '' }}" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
             aria-selected="false">privacy policy</a>
            <a class="nav-item nav-link text-uppercase font-weight-bold text-dark py-3 mr-4 {{ !empty($current_tab) && $current_tab == 'cancel' ? 'active' : '' }}" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact"
             aria-selected="false">cancelation & refund police</a>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade py-4 {{ !empty($current_tab) && $current_tab == 'terms' ? 'show active' : '' }}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>

            <h4 class="text-secondary mt-4">Lorem ipsum dolor sit amet.</h3>
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>

            <h4 class="text-secondary mt-4">Lorem ipsum dolor sit amet.</h3>
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>
          </div>
          <div class="tab-pane fade py-4 {{ !empty($current_tab) && $current_tab == 'privacy' ? 'show active' : '' }}" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>

            <h4 class="text-secondary mt-4">Lorem ipsum dolor sit amet.</h3>
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>

            <h4 class="text-secondary mt-4">Lorem ipsum dolor sit amet.</h3>
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>
          </div>
          <div class="tab-pane fade py-4 {{ !empty($current_tab) && $current_tab == 'cancel' ? 'show active' : '' }}" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>

            <h4 class="text-secondary mt-4">Lorem ipsum dolor sit amet.</h3>
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>
            


            <h4 class="text-secondary mt-4">Lorem ipsum dolor sit amet.</h3>
            <p class="text-secondary">
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis odio numquam aliquid suscipit voluptas doloribus, in error eius quam soluta, perspiciatis expedita quaerat vel. Omnis eligendi exercitationem dolores porro ad.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
