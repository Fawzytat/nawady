@extends('layouts.inner')


@section('content')

<section id="not-found">
  <div class="container-fluid">
    <div class="row">
      <div class="d-flex flex-column justify-content-center align-items-center" id="not-found-items">
        <h1 class="font-weight-bold text-primary">403</h1>
        <p class="text-dark font-weight-bold">Sorry , You are not Authorized to view this page</p>
        <a type="button" href="/" class="btn btn-primary btn-lg text-uppercase">go to homepage</a>
      </div>
    </div>
  </div>
</section>

@endsection
