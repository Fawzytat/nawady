
@component('mail::message')

Hello {{ $user->username }}

We received a request to change your password on Nawady Services.

@component('mail::button', ['url' => $url ])
Click here to change your password
@endcomponent

If you didn’t request a password change, you can ignore this message and continue to use your current password.
Someone probably typed in your email address by accident, <br>


Your friends at Nawady services, <br>
{{ config('app.name') }}
@endcomponent
