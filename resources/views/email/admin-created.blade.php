
@component('mail::message')

Hello {{ $user->username }}

Congratulations! confirming that your club branch
 {{$branch->club->name}} - {{$branch->name}} has been successfully added to Nawady services system.



@component('mail::panel')
You can administer your club through the following info:
@endcomponent


@component('mail::table')
|        |          |
| ------------- |:-------------:|
| URL      | {{ $url }}     |
| Username      | {{ $user->username }} |
| Password      | {{ $password }} |

@endcomponent

You can call Nawady system admin if you needed any help.


Your friends at Nawady services, <br>
{{ config('app.name') }}
@endcomponent
