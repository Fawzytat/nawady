
@component('mail::message')

Hello {{ $user->username }}

Thanks for creating an account with us.
<br><br>
To get started, please confirm your email address,
so we know your details are correct.
Then you can verify your club membership & start booking courts.

@component('mail::button', ['url' => $url ])
Confirm Email Address
@endcomponent

Your friends at Nawady services, <br>
{{ config('app.name') }}
@endcomponent
