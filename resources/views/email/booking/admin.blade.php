@component('mail::message')

Hello Club Admin,

New court booking from Nawady services system to your
{{$branch->club->name}} - {{$branch->name}} {{ $sport_name }}’s courts.




@component('mail::panel')
Booking Details:
@endcomponent



@component('mail::table')
|        |          |
| ------------- |:-------------:|
| Booking ID      | # {{ $booking->booking_id }}      |
| Sport      | {{ $sport_name }} |
| Court      | {{ $court_name }} |
| Date      | {{ $booking->day }} |
| Time      | {{ $booking->total_time }} |
@if($booking->withTrainer)
| Trainer      | {{ $trainer->name }} |
@endif
| Payment Method      | @if($booking->booking_type == "cc") Credit Card @endif @if($booking->booking_type == "cash") Cash @endif |
@if($booking->booking_type == "cash")
| Payment Refrence Code      | {{ $booking->payment_code }} |
@endif
| Total      | {{ $booking->total_price }} EGP |
@endcomponent


@if($booking->withTrainer)
<h7><i>(Make sure to inform trainer to be in court at time)</i></h7> <br />
@endif

Thanks for using Nawady services system!,<br>
{{ config('app.name') }}
@endcomponent
