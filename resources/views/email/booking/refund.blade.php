@component('mail::message')

Hello {{ $member->membershipName }},

Confirming that your refund request has been successfully processed & under progress now.

Please note that it may take from 8 to 15 working days till amount is refunded back to your credit card.

@component('mail::panel')
Cancelled request details:
@endcomponent


@component('mail::table')
|        |          |
| ------------- |:-------------:|
| Member Name      |  {{ $member->membershipName }}    |
| Booking ID      | # {{ $booking->booking_id }}      |
| Date      | {{ $booking->day }} |
| Sport      | {{ $sport_name }} |
| Court      | {{ $court_name }} |
| Time      | {{ $booking->total_time }} |
@if($booking->withTrainer)
| Trainer      | {{ $trainer->name }} |
@endif
| Payment Method      | @if($booking->booking_type == "cc") Credit Card @endif @if($booking->booking_type == "cash") Cash @endif |
@if($booking->booking_type == "cash")
| Payment Refrence Code      | {{ $booking->payment_code }} |
@endif
@if($booking->booking_type == "cc")
| Total      | {{ $booking->total_price }} EGP |
@endif
@endcomponent


Your friends at Nawady services,<br>
{{ config('app.name') }}
@endcomponent
