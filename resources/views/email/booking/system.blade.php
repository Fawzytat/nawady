@component('mail::message')

Hello {{ $member->membershipName }}  ,

Confirming that you’ve successfully booked a court on Nawady system with following details:




@component('mail::panel')
Booking Details:
@endcomponent



@component('mail::table')
|        |          |
| ------------- |:-------------:|
| Club    | {{$branch->club->name}}      |
| Branch      | {{$branch->name}}      |
| Booking ID      | # {{ $booking->booking_id }}      |
| Sport      | {{ $sport_name }} |
| Court      | {{ $court_name }} |
| Date      | {{ $booking->day }} |
| Time      | {{ $booking->total_time }} |
@if($booking->withTrainer)
| Trainer      | {{ $trainer->name }} |
@endif
| Payment Method      | @if($booking->booking_type == "cc") Credit Card @endif @if($booking->booking_type == "cash") Cash @endif |
@if($booking->booking_type == "cash")
| Payment Refrence Code      | {{ $booking->payment_code }} |
@endif
| Total      | {{ $booking->total_price }} EGP |
@endcomponent



Thanks for using Nawady services system!,<br>
{{ config('app.name') }}
@endcomponent
