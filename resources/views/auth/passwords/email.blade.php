@extends('layouts.inner')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="container" style="margin-top:5px;">
      @if (session('status'))
          <div class="alert alert-success text-center">
              {{ session('status') }}
          </div>
      @endif
    </div>
  </div>
  <div class="row">

      <div class="col-md-4 col-md-offset-2"></div>
      <div class="col-md-4 col-md-offset-2">
      <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
          {{ csrf_field() }}

          @if (! session('status'))
          
          <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <span class="input">
                <input class="input__field" type="email" name="email" value="{{ old('email') }}" required />
                <label class="input__label" for="email">
                  <span class="input__label-content">Email</span>
              </label>
              </span>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>


          <button type="submit" class="btn btn-block btn-primary" >Send Password Reset Link</button>
          @endif
      </form>

    </div>
  </div>
</div>
@endsection
