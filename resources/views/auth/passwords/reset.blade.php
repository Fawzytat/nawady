@extends('layouts.inner')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="container" style="margin-top:5px;">
      @if (session('status'))
          <div class="alert alert-success text-center">
              {{ session('status') }}
          </div>
      @endif
    </div>
  </div>
  <div class="row">
      <div class="col-md-4 col-md-offset-2"></div>
      <div class="col-md-4 col-md-offset-2">
        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <span class="input">
                  <input class="input__field" type="email" name="email" value="{{ old('email') }}" required />
                  <label class="input__label" for="email">
                    <span class="input__label-content">Email</span>
                </label>
                </span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <span class="input">
                  <input class="input__field" type="password" name="password" value="{{ old('password') }}" required />
                  <label class="input__label" for="password">
                    <span class="input__label-content">Password</span>
                </label>
                </span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <span class="input">
                  <input class="input__field" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" required />
                  <label class="input__label" for="password_confirmation">
                    <span class="input__label-content">Confirm Password</span>
                </label>
                </span>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

              <button type="submit" class="btn btn-block btn-primary" >Reset Password</button>
        </form>
      </div>
  </div>
    <!-- <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> -->
</div>
@endsection
