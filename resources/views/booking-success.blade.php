@extends('layouts.inner')

@section('content')

<div class="col-lg-6 pb-5">
  <div class="card">
    <div class="card-header bg-success">
      <span class="icon-done"></span>
      <h1>Thank you for booking</h1>
      <a href="javascript:window.print()" class="print-link">
        <span class="icon-print"></span>
      </a>
    </div>
    <div class="card-body d-print">
      <h2 class="card-title">Your booking was sccssesfully done</h2>
      @if($booking->booking_type == "cash")
      <h3 class="card-title">Please make sure to provide this Reference Payment Code: {{ $booking->payment_code }} at court.</h3>
      @endif
      <div class="booking-status">
        <p>Booking ID:<span>#{{ $booking->booking_id }}</span></p>
        <p>Club:<span>{{ $club_name }}</span></p>
        <p>Branch:<span>{{ $branch_name  }}</span></p>
        <p>Court:<span>{{ $court_name }}</span></p>
        <p>Time:<span>{{ $booking->total_time }}</span></p>
        <p>Date:<span>{{ date('D',strtotime($booking->day)) }}, {{ $booking->day }}</span></p>
        @if($booking->withTrainer)
        <p>Trainer:<span>{{ $trainer_name }}</span></p>
        @endif

        @if($booking->booking_type == "cc")
        <p>Transaction No<span>{{ $booking->payment->migs_TransactionNo }}</span></p>
        <p>Card No<span>{{ $booking->payment->migs_CardNum }}</span></p>
        <p>Receipt No<span>{{ $booking->payment->migs_ReceiptNo }}</span></p>
        @endif
        <p>Total:<span>{{ $booking->total_price }} EGP</span></p>
      </div>
      <p>You can follow your booking history from your booking page in your profile</p>
      @if($cancel_limit_in_hours != 0)
      <p>For Cancellation go to booking history and choose Cancel before the booking time by {{ $cancel_limit_in_hours }} hours</p>
      @endif
      <div class="d-flex flex-wrap justify-content-between">
        <a href="/myaccount" class="btn btn-link">GO TO BOOKING HISTORY</a>
        <a href="/" class="btn btn-link">BACK TO HOME PAGE</a>
      </div>
    </div>
  </div>
</div>


@endsection
