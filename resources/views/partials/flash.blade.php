@if (session('status'))

    <div class="alert alert-success alert-dismissable" style="">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <strong>Success!</strong> {{ session('status') }}
    </div>
@endif
