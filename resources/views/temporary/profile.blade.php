@extends('layouts.inner')
@section('content')





<!-- payments table -->
<div class="card mb-5" id="payment-reporting-card">
<div class="row mt-3 ml-3 mr-3" id="payment-reporting-card-table">
    <table>
        <thead class="card-row col-12">
            <tr class="d-flex flex-row justify-content-around align-items-start col-12" id="payment-reporting-card-table-head">
                <th>club</th>
                <th>branch</th>
                <th>month</th>
                <th>from - to</th>
                <th>total booking</th>
                <th>revenue</th>
                <th>bank fees (3%)</th>
                <th>total amount</th>
                <th>recieipt</th>
                <th>status</th>
            </tr>
        </thead>
        <tbody class="card-row col-12">
            <tr class="d-flex flex-row justify-content-around align-items-start col-12" id="payment-reporting-card-table-body">
                <th>
                    <p>wadi degla</p>
                </th>
                <th>
                    <p>maadi</p>
                </th>
                <th>
                    <a href="#">novamber 2017</a>
                </th>
                <th>
                    <p>01 Nov. 26 Nov.</p>
                </th>
                <th>
                    <p>28 booking</p>
                    <a href="#" class="sm">16 online</a>
                    <a href="#" class="sm">12 cash</a>
                </th>
                <th>
                    <p>27,030 EGP</p>
                    <p class="sm">15,515 EGP Online</p>
                    <p class="sm">11,515 EGP Cash</p>
                </th>
                <th>
                    <p>1543 EGP</p>
                </th>
                <th>
                    <p>27,030 EGP</p>
                </th>
                <th><a href="#" target="_blanck">View Receipt</a></th>
                <th>
                    <p>Paid</p>
                </th>
            </tr>

            <tr class="d-flex flex-row justify-content-around align-items-start col-12" id="payment-reporting-card-table-body">
                <th>
                    <p>wadi degla</p>
                </th>
                <th>
                    <p>maadi</p>
                </th>
                <th>
                    <a href="#">novamber 2017</a>
                </th>
                <th>
                    <p>01 Nov. 26 Nov.</p>
                </th>
                <th>
                    <p>28 booking</p>
                    <a href="#" class="sm">16 online</a>
                    <a href="#" class="sm">12 cash</a>
                </th>
                <th>
                    <p>27,030 EGP</p>
                    <p class="sm">15,515 EGP Online</p>
                    <p class="sm">11,515 EGP Cash</p>
                </th>
                <th>
                    <p>1543 EGP</p>
                </th>
                <th>
                    <p>27,030 EGP</p>
    </th>
    <!-- System admin view receipt -->
    <th>
                    <a href="#" type="button" data-toggle="modal" data-target="#viewReceiptModal">Upload Receipt</a>
    </th>

    <!-- Club admin view recepit-->
               <!-- <th><a href="#" target="_blanck">View Receipt</a></th> -->

    <!-- System admin status -->
    <th>
      <div class="form-group pl-5 pr-5">
        <select class="custom-select sysAdminStatus border-0">
          <option value="paid" class="font-weight-bold" selected>paid</option>
          <option value="due" class="font-weight-bold">Due</option>
        </select>
      </div>
    </th>
                <!-- Club admin status -->
    <!-- <th><p>Paid</p></th> -->
            </tr>
        </tbody>
    </table>
</div>
<!-- End payments table -->
</div>
@endsection
