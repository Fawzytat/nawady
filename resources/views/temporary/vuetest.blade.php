

<style>
.redColor { background: red;}
</style>

<div id="root">
  <input id="input" type="text" v-model="others"  >
  <p> ------------- </p>

  <ul>
    <li v-for="student in students" v-text="student"></li>
  </ul>

  <input type="text" v-model="newName" />
  <button v-on:click="addName" >add name</button>
<p> ------------- </p>
  <button v-bind:class="{ 'redColor' : status }" @click="toggleClass" >change color</button>
  <!-- <button @click="addName">add name</button>  The same  -->
<p> ------------- </p>

<h4> All tasks </h4>
<ul>
  <li v-for="task in tasks" v-text="task.description"></li>
</ul>
<h4> Incompleted tasks </h4>

<ul>
  <li v-for="task in incompletedTasks" v-text="task.description"></li>
</ul>
<p> ------------- </p>
<h4> tasks in component </h4>
<task-list></task-list>
</div>

<script src="https://unpkg.com/vue@2.5.13/dist/vue.js"></script>
<script src="js/trainning.js"></script>
<script src="js/firstcomponent.js"></script>
