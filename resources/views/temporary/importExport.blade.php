<html lang="en">
<head>
	<title>Import - Export Laravel 5</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
</head>


<body>
<br/>
<br/>
	<div class="container">
		<div class="panel panel-primary">
		  <!-- <div class="panel-heading">
		    <h3 class="panel-title" style="padding:12px 0px;font-size:25px;"><strong>Laravel 5.3 - import export csv or excel file into database example</strong></h3>
		  </div> -->
		  <div class="panel-body">


		  		@if ($message = Session::get('success'))
					<div class="alert alert-success" role="alert">
						{{ Session::get('success') }}
					</div>
				@endif


				@if ($message = Session::get('error'))
					<div class="alert alert-danger" role="alert">
						{{ Session::get('error') }}
					</div>
				@endif

				<?php if(!empty($errors) && count($errors)>0) : ?>
						<div class="container">
								<div class="row">
										<div class="col-sm-12 text-center ">
												<?php foreach ($errors->all() as $error) : ?>
														<p class="bg-danger"><?php echo $error ?></p>
												<?php endforeach; ?>
										</div>
								</div>
						</div>
				<?php endif; ?>
				<?php if(Session::has('message')) : ?>
						<div class="container">
								<div class="row">
										<div class="col-sm-12 text-center ">
												<p class="bg-success" id="status"><?php echo Session::get('message'); ?></p>
										</div>
								</div>
						</div>
				<?php endif; ?>

				<h3>Import File Form:</h3>
				<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 20px;"
         action="{{ URL::to('import') }}"
         class="form-horizontal" method="post"
         enctype="multipart/form-data">

					<input type="file" name="import_file"  required/>
					{{ csrf_field() }}
					<br/>

					<input name="validate" value="CHECK FILE BEFORE IMPORTING"
					type="submit"
					 class="btn btn-primary"></input>

					<input name="import" value="START IMPORTING"
					type="submit"
					 class="btn btn-primary"></input>


				</form>
				<br/>


		    	<h3>Import File From Database:</h3>
		    	<div style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 20px;">
			    	<a href="{{ url('downloadExcel/xls') }}"><button class="btn btn-success btn-lg">Download Excel xls</button></a>
					<a href="{{ url('downloadExcel/xlsx') }}"><button class="btn btn-success btn-lg">Download Excel xlsx</button></a>
					<a href="{{ url('downloadExcel/csv') }}"><button class="btn btn-success btn-lg">Download CSV</button></a>
		    	</div>


		  </div>
		</div>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script>
			(function($){
					'use strict';
					function statusUpdater() {
							var status = $('#status');
							$.ajax({
									'url': '/status',
							}).done(function(r) {
									if(r.msg==='done') {
											status.text( "The import is completed. Your data is now available for viewing ... " );
											console.log( "The import is completed. Your data is now available for viewing ... " );
									}
									else if(r.msg==='done_validation') {
											status.text( "The Validation is completed" );
											console.log( "The Validation is completed" );
									} else {
											//get the total number of imported rows
											status.text( "Status is: " + r.msg );
											console.log("Status is: " + r.msg);
											console.log( "The job is not yet done... Hold your horses, it takes a while :)" );
											statusUpdater();
									}
								})
								.fail(function() {
										status.text( "An error has occurred... We could ask Neo about what happened, but he's taken the red pill and he's at home sleeping" );
										console.log( "An error has occurred... We could ask Neo about what happened, but he's taken the red pill and he's at home sleeping" );
								});
					}
					statusUpdater();
			})(jQuery);
	</script>

</body>


</html>
