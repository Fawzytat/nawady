@extends('layouts.inner')
@section('customCss')

<link rel='stylesheet' href='/fullcalendar/fullcalendar.css' />
<link rel='stylesheet' href='/fullcalendar/scheduler.min.css' />

<script src='/fullcalendar/lib/moment.min.js'></script>
<script src='/fullcalendar/fullcalendar.js'></script>
<script src='/fullcalendar/scheduler.min.js'></script>

@endsection
@section('content')


 <!-- =================  TRAINER MODAL ===================== -->
        @include('includes.trainer')
 <!-- ================= END TRAINER MODAL ===================== -->
  <!-- =================  LOGIN MODAL ===================== -->
        @include('includes.login_modal')
  <!-- ================= END LOGIN MODAL ===================== -->
  <!-- =================  REGISTER MODAL ===================== -->
        @include('includes.register_modal')
  <!-- ================= END REGISTER MODAL ===================== -->
  <!-- ================= ADD CLUB MODAL ===================== -->
        @include('includes.verify-branch')
  <!-- ============ END OF  ADD CLUB MODAL ============== -->
  <!-- ================= SLOTS ERROR MESSAGE ===================== -->
        @include('includes.slots-error')
  <!-- ============ END OF SLOTS ERROR MESSAGE ============== -->
<section class="container-fluid pb-5">
	<div class="row">
		<div class="col-xl-8 mb-5">

<!-- ================= AFTER VERIVICATION ===================== -->
    @if( Auth::user() && count( Auth::user()->members) && count( collect($branches)) && count( collect($branches)->where('id', $branch->id ) ))

        			<div class="row pt-5">
        				<div class="col-lg-5">
        					<div class="card card-horizontal my-0 h-100">
        						<div class="thumb align-self-start f-g">
                      @if(! $branch->club->logoUrl || $branch->club->logoUrl == "none")
                      <img src="/images/club.png">
                      @else
        							<img src="{{ asset('storage/app/public/').'/'.$branch->club->logoUrl }}">
                      @endif
                    </div>
        						<div class="mr-auto">
        							<p>{{ $branch->club->name }} Club - {{ $branch->name }}</p>
        							<div class="form-group">
        		                      <label for="branch">Choose Branch</label>

        		                        <select name="branch" class="custom-select" onChange="window.location.href='/club-page/'+this.value+'/court-booking/default'">
                                      @foreach($branch->club->branches->where('isActive',1) as $club_branch)
                                        @if($club_branch->id == $branch->id)
                                        <option value="{{ $club_branch->id }}" selected>{{ $club_branch->name }}</option>
                                        @else
                                        <option  value="{{ $club_branch->id }}">{{ $club_branch->name }}</option>
                                        @endif
                                      @endforeach
        		                        </select>
        			                 </div>
        			                 <div class="d-flex">
        			                 	<p><span>{{ count($branch->member) }}</span>Joined Members</p>
        			                 	<p><span>{{ count($branch->services->first()->courts) }}</span>Courts</p>
        			                 </div>
        						</div>
        					</div>
        				</div>
        				<div class="col-lg-7">
                  @if(! Auth::user()->hasRole('sys_admin') && ! Auth::user()->hasRole('club_admin'))
        					<div class="card my-0 h-100 membership-info">
        						<h2 class="card-title card-title-sm">Membership Information</h2>
        						<div class="d-flex flex-wrap">
        							<div class="form-group">
        		                      <span class="input">
        		                        <input class="input__field" type="text" name="name" value="{{ Auth::user()->members->where('branch_id',$branch->id)->first()->membershipName }}" />
        		                        <label class="input__label" for="name">
        		                          <span class="input__label-content">Name</span>
        			                    </label>
        		                      </span>
        		                    </div>
        		                    <div class="form-group">
        		                      <span class="input">
        		                        <input class="input__field" type="text" name="phone" value="{{ Auth::user()->phone }}" />
        		                        <label class="input__label" for="phone">
        		                          <span class="input__label-content">Phone</span>
        			                    </label>
        		                      </span>
        		                    </div>
        		                    <div class="form-group">
        		                      <span class="input">
        		                        <input class="input__field" type="text" name="membershipID" value="{{ Auth::user()->members->where('branch_id',$branch->id)->first()->membershipId }}" />
        		                        <label class="input__label" for="membershipID">
        		                          <span class="input__label-content">Membership ID</span>
        			                    </label>
        		                      </span>
        		                    </div>
        		                    <div class="form-group">
        		                      <span class="input">
        		                        <input class="input__field" type="text" name="membershipDate" value="1/11/2019" />
        		                        <label class="input__label" for="membershipDate">
        		                          <span class="input__label-content">Membership Expiry Date</span>
        			                    </label>
        		                      </span>
        		                    </div>
            						</div>
            					</div>
                      @endif
            				</div>
            			</div>



  <!-- ================= END AFTER VERIVICATION ===================== -->

          @else
  <!-- ================= BEFORE VERIVICATION ===================== -->

  <div class="card card-horizontal pl-4 pr-4">
    <div class="thumb align-self-start">
      @if(! $branch->club->logoUrl || $branch->club->logoUrl == "none")
      <img src="/images/club.png">
      @else
      <img src="{{ asset('storage/app/public/').'/'.$branch->club->logoUrl }}">
      @endif
    </div>
    <div class="mr-auto">
      <p>{{ $branch->club->name }} Club - {{ $branch->name }}</p>
      <div class="form-group">
                  <label for="branch">Choose Branch</label>
                  <select name="branch" class="custom-select" onChange="window.location.href='/club-page/'+this.value+'/court-booking/default'">
                    @foreach($branch->club->branches->where('isActive',1) as $club_branch)
                      @if($club_branch->id == $branch->id)
                      <option value="{{ $club_branch->id }}" selected>{{ $club_branch->name }}</option>
                      @else
                      <option  value="{{ $club_branch->id }}">{{ $club_branch->name }}</option>
                      @endif
                    @endforeach
                  </select>
               </div>
               <div class="d-flex">
                 <p><span>{{ count($branch->member) }}</span>Joined Members</p>
                 <p><span>{{ count($branch->services) ? count($branch->services->first()->courts) : 0 }}</span>Courts</p>
               </div>
    </div>

        @if(Auth::user())
          @if(! Auth::user()->hasRole('sys_admin') && ! Auth::user()->hasRole('club_admin'))
        <a href="#memberAddClubModal" class="btn btn-primary" data-toggle="modal">Verify branch membership </a>
          @endif
        @else
        <a href="#loginModal" class="btn btn-primary" data-toggle="modal">Verify branch membership </a>
        @endif

  </div>

  <!-- ================= END BEFORE VERIVICATION ===================== -->
        @endif
  <!-- =================  SERVICES NAV  ===================== -->

  <!-- CHECKING IF MEMBER ACTIVE  -->
  @if ( Auth::user() && Auth::user()->members->where('branch_id',$branch->id)->first() && !(Auth::user()->members->where('branch_id',$branch->id)->first()->isActive) )
  <div class="card">
    <h2 class="card-title text-center"> Your account has been deactivated by Club Admin, please contact club branch for more details. </h2>
  </div>
  @else
  <!-- CHECKING IF MEMBER ACTIVE ( endif is near line 319 , search for (bazinga) )  -->


      <ul class="nav nav-subtab" role="tablist">
        @foreach($branch->services as $service)
        @if($service->isActive)
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="{{'#'.$service->short_name}}" role="tab" aria-selected="true">{{ $service->name }}</a>
        </li>
        @endif
        @endforeach
      </ul>
    <!-- =================  SERVICES NAV END ===================== -->
    <!-- =================  SERVICES TABS  ===================== -->
      <div class="tab-content">
        @foreach($branch->services as $service)
        @if($service->isActive)
        <div class="tab-pane fade active show" id="{{$service->short_name}}" role="tabpanel">

            <ul class="nav sports-subtab" role="tablist">
              @foreach($service->serviceSport->where('isActive',1) as $key => $service_sport)

                  @if(!empty($subTabName) && $subTabName == 'default')
                  <li  class=" nav-item @if($key == $service->serviceSport->where('isActive',1)->keys()->first()) active show @endif" data-toggle="tooltip" title="{{ $service_sport->sport->name }}">
                    <a href="#booking_calendar" data-service-sport-id="{{ $service_sport->id }}" class="sport_tab nav-link @if($key == $service->serviceSport->where('isActive',1)->keys()->first()) active show @endif" data-toggle="tab"  role="tab" aria-selected="false">
                  @else
                  <li class="sport_tab nav-item {{ !empty($subTabName) && $subTabName == $service_sport->id ? 'show active' : '' }}" data-toggle="tooltip" title="{{ $service_sport->sport->name }}">
                    <a href="#booking_calendar" data-service-sport-id="{{ $service_sport->id }}" class="sport_tab nav-link @if($key == $service->serviceSport->where('isActive',1)->keys()->first()) active show @endif" data-toggle="tab"  role="tab" aria-selected="false">
                  @endif

                        <span class="h5">{{ $service_sport->sport->name }}</span>

                      </a>
                    </li>

              @endforeach
            </ul>
            @endif
        </div>
        @endforeach
      </div>
    <!-- =================  SERVICES TABS END ===================== -->
      <!-- CONTROL ROOM -->
      @if($branch->services->where('short_name','court_booking')->first()->serviceSport->where('isActive',1)->first())
      <div class="row">
        <input type="hidden" id="current_branch_id" value="{{ $branch->id }}" />
        <input type="hidden" id="current_service_sport_id" value="{{ $branch->services->where('short_name','court_booking')->first()->serviceSport->where('isActive',1)->first()->id }}" />
        <input type="hidden" id="current_service_slot_duration" value="{{ $branch->services->where('short_name','court_booking')->first()->serviceSport->where('isActive',1)->first()->slot_duration_h }}" />
        <input type="hidden" id="current_service_slot_price" value="{{ $branch->services->where('short_name','court_booking')->first()->serviceSport->where('isActive',1)->first()->slot_price }}" />
        <input type="hidden" id="current_date" value="{{ date('Y-m-d') }}" />
        <input type="hidden" id="current_date_d" value="{{ date('l') }}" />
        <input type="hidden" id="selected_time_from" value="" />
        <input type="hidden" id="selected_time_to" value="" />
        <input type="hidden" id="selected_court_id" value="" />
        <input type="hidden" id="current_booking_total" value="" />
        <input type="hidden" id="selected_trainer_id" value="" />
        <input type="hidden" id="selected_trainer_price" value="" />
        <input type="hidden" id="current_user_bookings" value="" />
        <input type="hidden" id="current_no_of_slots" value="" />
      </div>
      <!-- CONTROL ROOM -->
      @endif
  <!-- =================  CALENDER CARD  ===================== -->

  @if($branch->services->where('short_name','court_booking')->first()->isActive)
    @if($branch->services->where('short_name','court_booking')->first()->serviceSport->where('isActive',1)->first())
			<div class="card " id="calender_big_container">
        <h2 class="card-title dont_clear_me" id="calender_title"></h2>
        <div class="d-flex align-items-center py-4 dont_clear_me">
          <div class="mr-auto">
            <h2 class="card-title inline">Select Day</h2>
            <ul class="nav nav-calender">
              <li class="nav-item">
                <a class="nav-link active" day-date-d="{{ date('l') }}" day-date="{{ date('Y-m-d') }}" data-toggle="tab" href="#booking_calendar" role="tab" aria-selected="true">{{ date('D') }} , {{ date('Y-m-d') }} </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" day-date-d="{{ date('l', strtotime('+1 day')) }}" day-date="{{ date('Y-m-d', strtotime('+1 day')) }}" data-toggle="tab" href="#booking_calendar" role="tab" aria-selected="false">{{ date('D', strtotime("+1 day")) }} , {{ date('Y-m-d', strtotime("+1 day")) }}</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" day-date-d="{{ date('l', strtotime('+2 day')) }}" day-date="{{ date('Y-m-d', strtotime('+2 day')) }}" data-toggle="tab" href="#booking_calendar" role="tab" aria-selected="false">{{ date('D', strtotime("+2 day")) }} , {{ date('Y-m-d', strtotime("+2 day")) }}</a>
              </li>
            </ul>
          </div>
          <div>
            <p class="price" id="price_duration"></p>
          </div>
        </div>
        <div class="tab-content">
  				<div class="tab-pane fade show active calender-wrap" id='booking_calendar'></div>
        </div>
        @if(Auth::user())
          @if(! Auth::user()->hasRole('sys_admin') && ! Auth::user()->hasRole('club_admin'))
				<div class="dont_clear_me  d-flex justify-content-between flex-wrap">
					<p class="d-none d-lg-block">
						To select a slot, click on one of the green slots. you can drag your mouse to
            select multiple successive slots. Number of successive slots allowed per booking in this club branch is
            {{ $branch->services->where('short_name','court_booking')->first()->max_duration_per_booking }} slots.
					</p>
					<p class="d-block d-lg-none d-xl-none">
						To select a slot, click & hold your finger on the slot till it is selected.
             You can drag your selection to select multiple successive slots. Number of successive slots allowed per
              booking in this club branch is
              {{ $branch->services->where('short_name','court_booking')->first()->max_duration_per_booking }} slots.
					</p>
					<div class="booking-status">
						<h2 class="card-title">Booking Status</h2>
						<p>Club:<span>{{ $branch->club->name }}</span></p>
						<p>Branch:<span>{{ $branch->name }}</span></p>
						<p>Court:<span id="booking_court_name"></span></p>
						<p>Time:<span id="booking_from_to"></span></p>
						<p>Date:<span id="booking_date">{{ date('Y-m-d') }}</span></p>
						<p>Trainer:
              <a id="reserve_btn" class="text-primary anchor_link" onclick="getAvailableTrainers(this);" >RSERVE A TRAINER</a>
              <span style="display:none;" id="booked_trainer_data"></span>
              <a style="display:none;" class="text-primary anchor_link" id="booked_trainer_clear"  onclick="clearBookedTrainer(this);" >Clear</a>
            </p>
						<p>Total:<span id="booking_total"></span></p>
					</div>
					<div class="payment">
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" id="payCashAtCourt" name="payment">
						  <label class="custom-control-label" for="payCashAtCourt">Pay Cash at Court</label>
						</div>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" id="payCC" checked name="payment">
						  <label class="custom-control-label" for="payCC">Pay Now Using Credit Card</label>
						</div>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" id="payFawry" name="payment" disabled>
						  <label class="custom-control-label" for="payFawry">Pay Using Fawry</label>
						</div>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" id="payGizera" name="payment" disabled>
						  <label class="custom-control-label" for="payGizera">Pay using Gizera card</label>
						</div>
						<div class="custom-control custom-checkbox">
						  <input type="checkbox" class="custom-control-input" id="acceptTerms">
						  <label class="custom-control-label" for="acceptTerms">Accept <a class="anchor_link text-primary" href="/info/terms" >Terms & Conditions</a></label>
						</div>
						<!-- remove data-toggle="tooltip" and title if you don't need it -->
            @if(Auth::user())
              @if(! Auth::user()->hasRole('sys_admin') && ! Auth::user()->hasRole('club_admin'))
                @if( Auth::user() && count( Auth::user()->members) && count( collect($branches)) && count( collect($branches)->where('id', $branch->id ) ))
                <a onclick="startBooking(this);" class="btn btn-primary text-white" >BOOK NOW </a>
                @else
                <a href="#memberAddClubModal" class="btn btn-primary" data-toggle="modal">BOOK NOW </a>
                @endif
              @endif
            @else
            <a href="#loginModal" class="btn btn-primary" data-toggle="modal">BOOK NOW </a>
            @endif
						<!-- <button type="button" class="btn btn-primary"  data-toggle="tooltip" title="Please Select time slots to be able to book tis court">BOOK NOW</button> -->
						<div class="d-flex flex-row-reverse flex-wrap align-items-center">
							<img src="/images/bank.png">
							<img src="/images/visa.png">
							<img src="/images/mastercard.png">
						</div>
					</div>
				</div>
        @endif
        @endif
			</div>
      <div class="card" id="calender_no_courts">
        <h2 class="card-title text-center"> There is no registerd courts under this sport </h2>
      </div>
      @endif
      @endif
  <!-- else case of check member active ( bazinga ) -->
            @endif
  <!-- Second case of check member active  -->
      <!-- ================= END CALENDER CARD  ===================== -->

		</div>

		<div class="col-xl-4 dont_clear_me">
      <!-- ============== BRANCH ADVERTS ============== -->
          @foreach($branchAdvs as $branchAdv)
            @if( $branchAdv->url && $branchAdv->url !="none")
                @if($branchAdv->enddate)
                  @if( date("Y-m-d") < date('Y-m-d',strtotime($branchAdv->enddate))  && date("Y-m-d") > date('Y-m-d',strtotime($branchAdv->startdate)))
                    <div class="ads-space ads-space-side">
                      <a target="_blank" href="{{ $branchAdv->link }}">
                        <img src="{{ asset('storage/app/public/').'/'.$branchAdv->url }}">
                      </a>
                    </div>
                  @endif
                @else
                <div class="ads-space ads-space-side">
                  <a target="_blank" href="{{ $branchAdv->link }}">
                    <img src="{{ asset('storage/app/public/').'/'.$branchAdv->url }}">
                  </a>
                </div>
                @endif
            @endif
          @endforeach

      <!-- ============== END OF  BRANCH ADVERTS ============== -->

			<div class="card dont_clear_me">
				<h2 class="card-title">Contact info</h2>
				 <div class="card-body pt-0">
                        <div class="d-flex">
                            <span class="icon-mail"></span>
                            <div class="form-group">
                                <span class="input">
                                  <input class="input__field" type="email" name="email" value="{{ $branch->email }}" disabled="true" />
                                  <label class="input__label" for="email">
                                    <span class="input__label-content">Email</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex">
                            <span class="icon-phone"></span>
                            <div class="form-group">
                                <span class="input">
                                  <input class="input__field" type="text" name="phone" value="{{ $branch->phone }}" disabled="true" />
                                  <label class="input__label" for="phone">
                                    <span class="input__label-content">Phone</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex">
                            <span class="icon-website"></span>
                            <div class="form-group">
                                <span class="input">
                                  <input class="input__field" type="text" name="website" value="{{ $branch->website }}" disabled="true" />
                                  <label class="input__label" for="website">
                                    <span class="input__label-content">Website</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex">
                            <span class="icon-location"></span>
                            <div class="form-group">
                                <span class="input">
                                  <input class="input__field" type="text" name="location" value="{{ $branch->address }}" disabled="true" />
                                  <label class="input__label" for="location">
                                    <span class="input__label-content">Address</span>
                                </label>
                                </span>
                            </div>
                        </div>

                        @if($branch->map_url)
                        <iframe
                        src="{{ $branch->map_url }}"
                        width="100%" height="300" frameborder="0" style="border:0" allowfullscreen>
                        </iframe>
                        @endif
			</div>
		</div>
	</div>

	<!-- <div class="row">
	error and succes booking
	</div> -->


</section>
@endsection
@section('customScript')
<script src="/js/register.js"></script>
<script src="/js/login.js"></script>
<script src="/js/clubpage.js"></script>
<script src="/js/calender.js"></script>

@endsection
