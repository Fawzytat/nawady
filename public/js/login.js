

      // ------  login-----------


            $('#login_form_submit').click(function(e){
            clearAndResetLoginInputs();
             var LoginForm = $("#LoginForm");
             var formData = LoginForm.serialize();
            e.preventDefault();
            $('.page-preloader').show();
                    $.ajax({
                        url:'/login',
                        type:'POST',
                        data:formData,
                        success:function(data){
                          $('.page-preloader').hide();
                            //window.location.reload();
                            window.location.href = '/home';

                        },
                        error: function (data) {
                          $('.page-preloader').hide();
                          var object = jQuery.parseJSON( data.responseText);

                          if(object.warning){
                               $("#login-username").addClass("has-error");
                               $( '#login-errors-username' ).html( object.warning );
                           }
                           else
                           {
                              appendAllLoginErrors(data);
                           }

                        }
                    });
                });


     // ------ END  login-----------

           // ------ Clear And reseting inputs  ------
            function clearAndResetLoginInputs()
            {
                $( '#login-errors-username' ).html( "" );
                $( '#login-errors-password' ).html( "" );

                $("#login-username").removeClass("has-error");
                $("#login-password").removeClass("has-error");


                }
           // ------End of Clear And reseting inputs  ------

            // ------ append all errors  ------
             function appendAllLoginErrors(data)
            {
                            //console.log(data.responseText);
                            var object = jQuery.parseJSON( data.responseText);

                            var obj = object.errors;
                            console.log(obj);

                           if(obj.username){
                                $("#login-username").addClass("has-error");
                                $( '#login-errors-username' ).html( obj.username );
                            }

                            if(obj.password){
                                $("#login-password").addClass("has-error");
                                $( '#login-errors-password' ).html( obj.password );
                            }

            }
            // ------ End of append all errors  ------

            function sendLinkAgain(thi)
            {
              $('.page-preloader').show();
              var user_id = $(thi).data("id");
              $.ajax({
                  url:'/resendVerifyEmail',
                  type:'POST',
                  data:
                  {
                    "user_id" : user_id,
                    "_token": $('meta[name="csrf-token"]').attr('content')
                  },
                  success:function(data){
                    $('.page-preloader').hide();
                      window.location.href = '/home';
                  },
                  error: function (data) {
                    $('.page-preloader').hide();
                    alert("error");
                  }
              });
            }
