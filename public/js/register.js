

      // ------  Register-----------

            $('#RegisterForm').on('submit',(function(e) {
            //$('#register-form-submit').click(function(e){

            clearAndResetInputs();
             //var RegisterForm = $("#RegisterForm");
             //var formData = RegisterForm.serialize();
             var formData = new FormData(this);
             e.preventDefault();

                    $.ajax({
                        url:'/signup',
                        type:'POST',
                        data:formData,
                        beforeSend: function() {
                          $(".page-preloader").show();
                       },
                        success:function(data){
                          $('.page-preloader').hide();
                          $('.register_success').show();
                          setTimeout(function(){// wait for 5 secs(2)
                                  location.reload(); // then reload the page.(3)
                             }, 5000);

                        },
                        cache: false,
                        contentType: false,
                        processData: false,
                        error: function (data) {
                          $('.page-preloader').hide();
                           appendAllErrors(data);

                        }
                    });
                }));


     // ------ END  Register-----------

           // ------ Clear And reseting inputs  ------
            function clearAndResetInputs()
            {
                $( '#register-errors-username' ).html( "" );
                $( '#register-errors-email' ).html( "" );
                $( '#register-errors-phone' ).html( "" );
                $( '#register-errors-password' ).html( "" );

                $("#register-username").removeClass("has-error");
                $("#register-email").removeClass("has-error");
                $("#register-phone").removeClass("has-error");
                $("#register-password").removeClass("has-error");


                }
           // ------End of Clear And reseting inputs  ------

            // ------ append all errors  ------
             function appendAllErrors(data)
            {
                            //console.log(data.responseText);
                            var object = jQuery.parseJSON( data.responseText);

                            var obj = object.errors;
                            console.log(obj);
                            console.log(obj.username);

                           if(obj.username){
                                $("#register-username").addClass("has-error");
                                $( '#register-errors-username' ).html( obj.username );
                            }

                             if(obj.phone){
                                  $("#register-phone").addClass("has-error");
                                  $( '#register-errors-phone' ).html( obj.phone );
                              }

                            if(obj.email){
                                $("#register-email").addClass("has-error");
                                $( '#register-errors-email' ).html( obj.email );
                            }
                            if(obj.password){
                                $("#register-password").addClass("has-error");
                                $( '#register-errors-password' ).html( obj.password );
                            }

            }
            // ------ End of append all errors  ------
