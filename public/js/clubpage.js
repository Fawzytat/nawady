
  $('#attach_form_submit').click(function(e){
   var attachFormBranchId = $("#attachFormBranchId");
   var attachMemberForm = $("#attachMemberForm");
   var formData = attachMemberForm.serialize();
   e.preventDefault();
   $('#second_message').hide();
   clearAndResetLoginInputs();
          $.ajax({
              url:'/checkOrAttachMember2',
              type:'POST',
              data:formData,
              success:function(data){
                   location.reload();
              },
              error: function (data) {
                var object = jQuery.parseJSON( data.responseText);
                var obj = object.errors;
                 if(obj){
                      appendAllAttachErrors(obj);
                  }
                 else {
                   $('#second_message').show();
                   $('#first_message').hide();
                 }
              }
          });
      });


      // ------ append all errors  ------
       function appendAllAttachErrors(obj)
      {
           if(obj.member_id){
                $("#attach-member-id").addClass("has-error");
                $( '#attach-errors-member-id' ).html( obj.member_id );
            }

            if(obj.member_name){
                $("#attach-member-name").addClass("has-error");
                $( '#attach-errors-member-name' ).html( obj.member_name );
            }
      }
      // ------ End of append all errors  ------

      // ------ Clear And reseting inputs  ------
       function clearAndResetLoginInputs()
       {
           $( '#attach-errors-member-id' ).html( "" );
           $( '#attach-errors-member-name' ).html( "" );

           $("#attach-member-name").removeClass("has-error");
           $("#attach-member-id").removeClass("has-error");


           }
      // ------End of Clear And reseting inputs  ------

      // ------ REPORT TO ADMIN  ------

       function reportRequestToAdmin(th)
        {
          var attachMemberForm = $("#attachMemberForm");
          var formData = attachMemberForm.serializeArray();
          console.log($(th).data('reason'));
          formData.push({ name: "reason", value: $(th).data('reason') });
              $.ajax({
                  url:'/reportRequestToAdmin',
                  type:'POST',
                  data:formData,
                  success:function(data){
                    $('#beforeReporting').hide();
                    $('#reported_message').show();
                  },
                  error: function (data) {

                  }
              });
           }
      // ------REPORT TO ADMIN ------

      function page_location_reload()
      {
        location.reload();
      }
