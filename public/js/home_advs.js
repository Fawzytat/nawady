// ------ Handle Toggle Click events   ------
function handleAdvClick(cb) {

 var status = (cb.checked);
 var advid = $(cb).data('advid');

 $('#advActive_'+advid+'').val(status);
}

function handleNewAdvClick(cb) {
var status = (cb.checked);
$('#advActive').val(status);
}

function handleNewSAdvClick(cb) {
var status = (cb.checked);
$('#sidebarAdvActive').val(status);
}

// ------ End Handle Toggle Click events  ------

// ------ Handle the sorting request   ------
  $("#adv_sortable").sortable({
  placeholder: 'sortable_placeholder',
  update : function () {
      var order = $("#adv_sortable").sortable('serialize');
      var csrf_token = $('meta[name="csrf-token"]').attr('content');
       console.log(order);

      $.ajax({
           type: 'POST',
           url: '/sortAdvs',
           data: {
             "_token": csrf_token,
             "data": order
                  },
                  success:function(d){
                    console.log(d)
                  },
                  error: function (data) {
                  }
       });
  }
  });
  // ------ END OF Handle the sorting request   ------

  // ------ Handle the sidebar sorting request   ------
    $("#sidebar_adv_sortable").sortable({
    placeholder: 'sortable_placeholder',
    update : function () {
        var order = $("#sidebar_adv_sortable").sortable('serialize');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
         console.log(order);

        $.ajax({
             type: 'POST',
             url: '/sortSidebarAdvs',
             data: {
               "_token": csrf_token,
               "data": order
                    },
                    success:function(d){
                      console.log(d)
                    },
                    error: function (data) {
                    }
         });
    }
    });
    // ------ END OF Handle the sidebar sorting request   ------


 // ------ DELETE ADV   ------

function removeAdvModal(cb) {
 var advid = $(cb).data('id');
$(".modal-body .advert_record_id").val(advid);
}

// ------END OF DELETE ADV   ------


// ------ Show and hide add new add section  ------

$("#add_new_ad").click(function(){
$(this).hide();
$('#new_ad').show();
});

$('#hideNewAdvForm').click(function(){
console.log('dfsdf')
$('#new_ad').hide();
$('#add_new_ad').show();
});

// ------ End Show and hide add new add section  ------

// ------ Show and hide add new side adv section  ------

$("#add_new_sidebar_ad").click(function(){
$(this).hide();
$('#new_sidebar_ad').show();
});

$('#hideNewSAdvForm').click(function(){

$('#new_sidebar_ad').hide();
$('#add_new_sidebar_ad').show();
});

// ------ End Show and hide add new side add section  ------
function removeSponsorLogo(btn) {
 var logo_id = $(btn).data('id');
var csrf_token = $('meta[name="csrf-token"]').attr('content');
 $.ajax({
      type: 'POST',
      url: '/removeSponsorLogo',
      data: {
        "_token": csrf_token,
        "logo_id": logo_id
             },
             success:function(d){
              window.location.reload();
             },
             error: function (data) {
             }
  });
}
