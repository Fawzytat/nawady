
  $(document).ready(function() {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
             type: 'POST',
             url: '/getCalenderData',
             data: {
               "_token": csrf_token,
               "branch_id": $("#current_branch_id").val(),
               "service_sport_id": $("#current_service_sport_id").val(),
               "day_date": $("#current_date").val()
                    },
                    success:function(data){
                      if(data.courts_count == 0)
                      {
                        $('#calender_no_courts').show();
                        $('#calender_big_container').hide();
                      }
                      else
                      {
                        $('#calender_no_courts').hide();
                        $('#calender_big_container').show();
                        $("#calender_title").html(data.calender_title);
                        $('#price_duration').html(data.price_duration);
                        $('#current_user_bookings').val(data.current_user_bookings);
                        applyCalender(data.myMinTime,data.myMaxTime,data.scrollTime,data.slotDuration,
                          data.slotLabelInterval,JSON.parse(data.courts),JSON.parse(data.dimmed_slots),
                          data.user_auth,data.user_verified,data.max_book_per_day,data.max_slots_per_booking
                         ,data.duration_allowed,data.slot_price,data.slotDuration_m,JSON.parse(data.booked_slots));
                      }
                    },
                    error: function (data) {
                    }
         });
  });

  $(".sport_tab").on('click', function (e) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var this_service_sport = $(this).data('service-sport-id');
        $("#booking_court_name").html("");
        $("#booking_from_to").html("");
        $("#booking_total").html("");
        $("#current_no_of_slots").val("");
        $("#selected_time_from").val("");
        $("#selected_time_to").val("");
        $("#current_booking_total").val("");
        $("#selected_trainer_id").val("");
        $("#reserve_btn").show();
        $("#booked_trainer_data").html("");
        $("#booked_trainer_data").hide();
        $("#booked_trainer_clear").hide();
        $("#current_service_sport_id").val(this_service_sport);
    });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var calender = $($(this).attr('href'));
        $("#calender_title").html("");
        $('#price_duration').html("");
        if($(this).attr('day-date'))
        {
          var day_date = $(this).attr('day-date');
          var day_date_d = $(this).attr('day-date-d');
          $("#current_date").val(day_date);
          $("#current_date_d").val(day_date_d);
          $("#booking_date").html("");
          $("#current_no_of_slots").val("");
          $("#selected_time_from").val("");
          $("#selected_time_to").val("");
          $("#current_booking_total").val("");
          $("#selected_trainer_id").val("");
          $('#current_user_bookings').val("");
          $("#booking_court_name").html("");
          $("#booking_from_to").html("");
          $("#booking_total").html("");
          $("#reserve_btn").show();
          $("#booked_trainer_data").html("");
          $("#booked_trainer_data").hide();
          $("#booked_trainer_clear").hide();
          $("#booking_date").html(day_date);
        }
        calender.fullCalendar( 'destroy' );

        $.ajax({
             type: 'POST',
             url: '/getCalenderData',
             data: {
               "_token": csrf_token,
               "branch_id": $("#current_branch_id").val(),
               "service_sport_id": $("#current_service_sport_id").val(),
               "day_date": $("#current_date").val()
                    },
                    success:function(data){
                      if(data.courts_count == 0)
                      {
                        $('#calender_no_courts').show();
                        $('#calender_big_container').hide();
                      }
                      else
                      {
                        $('#calender_no_courts').hide();
                        $('#calender_big_container').show();
                        $("#calender_title").html(data.calender_title);
                        $('#price_duration').html(data.price_duration);
                        $('#current_user_bookings').val(data.current_user_bookings);
                        applyCalender(data.myMinTime,data.myMaxTime,data.scrollTime,data.slotDuration,
                          data.slotLabelInterval,JSON.parse(data.courts),JSON.parse(data.dimmed_slots),
                          data.user_auth,data.user_verified,data.max_book_per_day,data.max_slots_per_booking
                         ,data.duration_allowed,data.slot_price,data.slotDuration_m,JSON.parse(data.booked_slots));
                         calender.fullCalendar( 'render' );
                      }
                    },
                    error: function (data) {
                    }
         });


    });



function applyCalender(myMinTime,myMaxTime,scrollTime,slotDuration,slotLabelInterval,
                       courts,dimmed_slots,user_auth,user_verified,
                       max_book_per_day,max_slots_per_booking,duration_allowed,slot_price,slotDuration_m,booked_slots){
  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  $('#booking_calendar').fullCalendar({
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
    aspectRatio: 4,
    height: 'parent',
    header: false,
    defaultView: 'timelineDay',
    scrollTime: scrollTime,
    slotDuration: slotDuration,
    minTime: myMinTime,
    maxTime: myMaxTime,
    selectLongPressDelay : 500,
    slotLabelInterval: slotLabelInterval,
	  selectable: true,
	  contentHeight: 'auto',
    unselectAuto: true,
    unselectCancel: ".dont_clear_me",
    selectHelper: true,
    selectOverlap :false,
    slotLabelFormat: "h(:mm)a",
    resourceLabelText: " ",
    resourceAreaWidth: "10%",
    resources : courts,
    eventSources:
        [
           {
            events: booked_slots
           },
          {
            events: dimmed_slots
          }
        ],
    eventClick: function(calEvent, jsEvent, view) {
      if (jsEvent.target.classList.contains('fc-bgevent')) {
           alert('Click Background Event Area');
       }
      alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
      alert('View: ' + view.name);
      alert(calEvent.start);
      alert(calEvent.end);
      alert('Court is : ' + calEvent.resourceId);
      $(this).css('background-color', 'red');

  },select: function(startDate, endDate, jsEvent, view, resource) {
      if(user_auth == "guest")
      {
        $('#loginModal').modal('show');
      }
      else if (user_verified == "not_verified")
      {
        $('#memberAddClubModal').modal('show');
      }
      else if (parseInt(max_book_per_day) <= parseInt($("#current_user_bookings").val()) )
      {

        $('#slotsErrorModal').modal('show');
        $('#slots_error_message').html("");
        $('#slots_error_message').html("You can't make more than "+max_book_per_day+" bookings per day");
      }
      else
      {
        var selected_duration = (endDate - startDate) / 1000 / 60 ;

        if ( selected_duration > duration_allowed )
        {
          $('#slotsErrorModal').modal('show');
          $('#slots_error_message').html("");
          $('#slots_error_message').html("Not allowed to book more than "+max_slots_per_booking+" slots per booking");
        }
        else
        {
          // alert('selected ' + startDate.format("HH:mm") + ' to ' + endDate.format("HH:mm") + ' ' + resource.id + ' ' + resource.title);
          $(".fc-highlight").css("background", "#184a9b");
          $("#booking_court_name").html("");
          $("#booking_from_to").html("");
          $("#selected_time_from").val("");
          $("#selected_time_to").val("");
          $("#current_booking_total").val("");
          $("#selected_trainer_id").val("");
          $("#booked_trainer_data").html("");
          $("#selected_trainer_price").val("");
          $("#current_no_of_slots").val("");
          $("#booked_trainer_clear").hide();
          $("#reserve_btn").show();
          $("#selected_time_from").val(startDate.format("HH:mm"));
          $("#selected_time_to").val(endDate.format("HH:mm"));
          $("#selected_court_id").val("");
          $("#selected_court_id").val(resource.id);
          $("#booking_total").html("");
          $("#booking_court_name").html(resource.title);
          $("#current_no_of_slots").val(selected_duration/slotDuration_m);
          $("#booking_from_to").html( ((endDate - startDate) / 1000 / 60 ) +' Minutes from '+ startDate.format("h(:mm)a") +' to ' + endDate.format("h(:mm)a") );
          $("#booking_total").html( (selected_duration /slotDuration_m) * slot_price );
          $("#current_booking_total").val( (selected_duration /slotDuration_m) * slot_price );

        }

      }

    }
    ,unselect: function(jsEvent, view) {
      $("#current_no_of_slots").val("");
      $("#selected_time_from").val("");
      $("#selected_time_to").val("");
      $("#current_booking_total").val("");
      $("#selected_trainer_id").val("");
      //$('#current_user_bookings').val("");
      $("#booking_court_name").html("");
      $("#booking_from_to").html("");
      $("#booking_total").html("");
      $("#reserve_btn").show();
      $("#booked_trainer_data").html("");
      $("#booked_trainer_data").hide();
      $("#booked_trainer_clear").hide();
    }

  });
}



function getAvailableTrainers(th)
{

  var service_sport_id = $('#current_service_sport_id').val();

  if($("#selected_time_from").val() == "")
  {
    $('#slotsErrorModal').modal('show');
    $('#slots_error_message').html("");
    $('#slots_error_message').html("Please Select a slot first");
  }
  else
  {
    $.ajax({
        url:'/getAvailableTrainers',
        type:'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          "service_sport_id": service_sport_id,
          "day": $("#current_date_d").val(),
          "date": $("#current_date").val(),
          "time_from": $("#selected_time_from").val(),
          "time_to": $("#selected_time_to").val()
               },
               success:function(data){
                 $('#reserveTrainer').modal('show');
                 $('#trainers_list').html(data);
               },
               error: function (data) {
               }
    });
  }


}

$(function() {
    $('body').on('click', '.fc-bgevent', function(e) {
        e.preventDefault();
        $("#current_no_of_slots").val("");
        $("#selected_time_from").val("");
        $("#selected_time_to").val("");
        $("#current_booking_total").val("");
        $("#selected_trainer_id").val("");
        //$('#current_user_bookings').val("");
        $("#booking_court_name").html("");
        $("#booking_from_to").html("");
        $("#booking_total").html("");
        $("#reserve_btn").show();
        $("#booked_trainer_data").html("");
        $("#booked_trainer_data").hide();
        $("#booked_trainer_clear").hide();
    });
});

$(function() {
    $('body').on('click', '.fc-head', function(e) {
        e.preventDefault();
        $("#current_no_of_slots").val("");
        $("#selected_time_from").val("");
        $("#selected_time_to").val("");
        $("#current_booking_total").val("");
        $("#selected_trainer_id").val("");
        //$('#current_user_bookings').val("");
        $("#booking_court_name").html("");
        $("#booking_from_to").html("");
        $("#booking_total").html("");
        $("#reserve_btn").show();
        $("#booked_trainer_data").html("");
        $("#booked_trainer_data").hide();
        $("#booked_trainer_clear").hide();
    });
});

$(function() {
    $('body').on('click', '.fc-resource-area', function(e) {
        e.preventDefault();
        $("#current_no_of_slots").val("");
        $("#selected_time_from").val("");
        $("#selected_time_to").val("");
        $("#current_booking_total").val("");
        $("#selected_trainer_id").val("");
        //$('#current_user_bookings').val("");
        $("#booking_court_name").html("");
        $("#booking_from_to").html("");
        $("#booking_total").html("");
        $("#reserve_btn").show();
        $("#booked_trainer_data").html("");
        $("#booked_trainer_data").hide();
        $("#booked_trainer_clear").hide();
    });
});



function clearBookedTrainer(th)
{
    $("#booked_trainer_clear").hide();
    var current_price = parseInt($("#booking_total").html());
    var selected_trainer_price = parseInt($("#selected_trainer_price").val());
    var no_of_slots = parseInt($("#current_no_of_slots").val());
    var new_price = current_price - (selected_trainer_price * no_of_slots );
    $("#booking_total").html("");
    $("#booking_total").html(new_price);
    $("#reserve_btn").show();
    $("#booked_trainer_data").html("");
    $("#selected_trainer_price").val("");
    $("#booked_trainer_data").hide();

}

function startBooking(th)
{
  if($("#selected_time_from").val() == "")
  {
    $('#slotsErrorModal').modal('show');
    $('#slots_error_message').html("");
    $('#slots_error_message').html("Please Select a slot first");
  }
  else
  {
    if($("#acceptTerms").prop('checked'))
    {
      if($("#payCC").prop('checked'))
      {
        $.ajax({
            url:'/setBooking',
            type:'POST',
            beforeSend: function() {
              $(".page-preloader").show();
           },
            data: {
              "_token": $('meta[name="csrf-token"]').attr('content'),
              "service_sport_id": $("#current_service_sport_id").val(),
              "date": $("#current_date").val(),
              "time_from": $("#selected_time_from").val(),
              "time_to": $("#selected_time_to").val(),
              "branch_id": $("#current_branch_id").val(),
              "service_sport_id": $("#current_service_sport_id").val(),
              "court_id": $("#selected_court_id").val(),
              "booking_total": $("#current_booking_total").val(),
              "trainer_id": $("#selected_trainer_id").val(),
              "total_time": $("#booking_from_to").html(),
              "booking_type":"cc"
                   },
                   success:function(data){
                     //$(".page-preloader").hide();
                      window.location.href = data.url;
                   },
                   error: function (data) {
                   }
        });
      }
      else
      {
        $.ajax({
            url:'/setBooking',
            type:'POST',
            data: {
              "_token": $('meta[name="csrf-token"]').attr('content'),
              "service_sport_id": $("#current_service_sport_id").val(),
              "date": $("#current_date").val(),
              "time_from": $("#selected_time_from").val(),
              "time_to": $("#selected_time_to").val(),
              "branch_id": $("#current_branch_id").val(),
              "service_sport_id": $("#current_service_sport_id").val(),
              "court_id": $("#selected_court_id").val(),
              "booking_total": $("#current_booking_total").val(),
              "trainer_id": $("#selected_trainer_id").val(),
              "total_time": $("#booking_from_to").html(),
              "booking_type":"cash"
                   },
                   beforeSend: function() {
                     $(".page-preloader").show();
                  },
                   success:function(data){
                     $(".page-preloader").hide();
                     window.location.href = '/booking/success/'+data.booking_id+'';
                   },
                   error: function (data) {
                   }
        });
      }
    }
    else
    {
      $('#slotsErrorModal').modal('show');
      $('#slots_error_message').html("");
      $('#slots_error_message').html("Please Accept Terms & Conditions first");
    }
  }


}

// destroy the calender and generate it without last row
(function(){
	addGlobalStyle = function (css) {
		var head, style;
		head = document.getElementsByTagName('head')[0];
		if (!head) {
			return;
		}
		style = document.createElement('style');
		style.type = 'text/css';
		style.innerHTML = css;
		head.appendChild(style);
	}

	//Calculate size
	var bottomDifference = $('.tab-content')[0].getBoundingClientRect().bottom - $('.fc-slats')[0].getBoundingClientRect().bottom;
	var currentHeight = $(".fc-slats > table").css("height");
	var newHeight = parseInt(currentHeight) + bottomDifference;
	//$( ".fc-slats > table" ).css( "height", newHeight );
	addGlobalStyle(".fc-slats > table {height:" + newHeight + "px}");

	//Destroy the fullcalendar
	// $('#booking_calendar').fullCalendar('destroy');
	// //Create it again, this time with the correct CSS rules on init
	// createFC();
})();





  // var todayDate = moment().startOf('day');
  // var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
  // var TODAY = todayDate.format('YYYY-MM-DD');
  // var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');
