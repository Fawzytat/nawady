
function updateUserImage(file1) {

    var member_id = $('#photo_member_id').val();
    var csrf_token = $('meta[name="csrf-token"]').attr('content');

    var file = file1.files[0];
    var formData = new FormData( $("#img_thumb_upload")[0] );
    formData.append('formData', file);

    $.ajax({
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type: "POST",
    url: '/updateUserPic/'+member_id+'',
    data: formData,

    contentType: false,
    processData: false,
    success: function (data) {
           window.location.reload();
      },
      error: function (data){
        console.log();
      }
  });
}

//----


function deAttachMemberModal(cb) {
 var memberid = $(cb).data('memberid');
 var branchid = $(cb).data('branchid');
$(".modal-body .member_branch_id").val(branchid);
$(".modal-body .branch_member_id").val(memberid);
}



$(function() {
  buildCurrentBookingsTable();
});


$('#branchSelect').change(function(){
  var branch_id = $(this).val();
  $('#courtSelect').html("");

  $.ajax({
       type: 'POST',
       url: '/getBranchCourts',
       data: {
         "_token": $('meta[name="csrf-token"]').attr('content'),
         "branch_id": branch_id
              },
              success:function(data){
                $('#courtSelect').html(data.courts_list);
              },
              error: function (data) {
              }
   });
});


 $("#GoFilterBtn").click(function(){
   var table = $('#current-bookings-table').DataTable();
   table.destroy();
   buildCurrentBookingsTable();
 });

 $("#GoClearBtn").click(function(){
   $('#branchSelect').val("");
   $('#courtSelect').val("");
   $('#statusSelect').val("");

   var table = $('#current-bookings-table').DataTable();
   table.destroy();
   buildCurrentBookingsTable();

 });

 function buildCurrentBookingsTable(){
   $('#current-bookings-table').DataTable({
     lengthChange: false,
     autoWidth: false,
     info: false,
     searching: false,
     language: {
        searchPlaceholder: 'Search related keywords…',
        search: ''
      },
     pagingType: 'numbers',
     processing: true,
     serverSide: true,
     "ajax": {
     'url': '/buildUserBookingsTable',
     'type': 'POST',
     'data':{
       "branch_id": $("#branchSelect").val(),
       "court_id": $("#courtSelect").val(),
       "user_id": $("#current_user_id").val(),
       "status": $("#statusSelect").val(),
       "_token": $('meta[name="csrf-token"]').attr('content')
        }
     },
       columns: [
           { data: 'booking_id', name: 'booking_id' },
           { data: 'court', name: 'court' },
           { data: 'club', name: 'club' },
           { data: 'date', name: 'date' },
           { data: 'time', name: 'time' },
           { data: 'trainer', name: 'trainer' },
           { data: 'status', name: 'status' },
           { data: 'total', name: 'total' }

       ]
   });
 }


 function showReceipt(btn) {
  var booking_id = $(btn).data('id');
  $.ajax({
      url:'/getBookingReceipt',
      type:'POST',
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "booking_id": booking_id,
             },
             success:function(data){
               $('#Booking_receipt_modal').modal('show');
               $('#booking_data').html(data);
             },
             error: function (data) {
             }
  });
}
