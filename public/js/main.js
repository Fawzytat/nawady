var dataSet = [
	["Adel Selim Fouad Selim", "94629564"],
	["Sarah Seef Mohamed <Mansour></Mansour>", "94629564"],
	["Fouad Khalil Ahmed Fawzy", "94629564"],
	["Moshira Ali Ahmed Saaid", "94629564"],
	["Farida Mohamed Raafat Mahrous", "94629564"]
];

$(document).ready(function() {
	//owl
	(function() {
		$(".ads-carousel").owlCarousel({
			items: 1,
			// autoHeight: true,
			center: true,
			autoplay: true,
			loop: true
		});

		$(".clubs-carousel").owlCarousel({
			items: 1,
			autoplay: false,
			loop: false
		});
	})();

	// input effects
	(function() {
		// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
		if (!String.prototype.trim) {
			(function() {
				// Make sure we trim BOM and NBSP
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, "");
				};
			})();
		}

		[].slice
			.call(document.querySelectorAll("input.input__field"))
			.forEach(function(inputEl) {
				// in case the input is already filled..
				if (inputEl.value.trim() !== "") {
					classie.add(inputEl.parentNode, "input--filled");
				}

				// events:
				inputEl.addEventListener("focus", onInputFocus);
				inputEl.addEventListener("blur", onInputBlur);
			});

		function onInputFocus(ev) {
			classie.add(ev.target.parentNode, "input--filled");
		}

		function onInputBlur(ev) {
			if (ev.target.value.trim() === "") {
				classie.remove(ev.target.parentNode, "input--filled");
			}
		}
	})();

	//switch modals
	(function() {
		var loginModal = $("#loginModal"),
			registerModal = $("#registerModal"),
			forgotPwModal = $("#forgotPwModal"),
			triggerRegisterBtn = $('a[href="#registerModal"]'),
			triggerForgotPWBtn = $('a[href="#forgotPwModal"]');

		triggerRegisterBtn.click(function(event) {
			loginModal.modal("hide");
			registerModal.modal("show");
		});

		triggerForgotPWBtn.click(function(event) {
			loginModal.modal("hide");
			forgotPwModal.modal("show");
		});
	})();

	//hamburger menu
	(function() {
		var menu = document.getElementById("slide-nav"),
			showBtn = document.getElementById("showBtn"),
			body = document.body;

		if (showBtn !== null) {
			showBtn.onclick = function() {
				classie.toggle(this, "is-active");
				classie.toggle(menu, "cbp-spmenu-open");
				classie.toggle(body, "menu-overlay");
			};
		}
	})();

	//edit forms
	(function() {
		var editBtns = $('.btn[type="button"][data-form-target]');
		editBtns.each(function() {
			var editBtn = $(this),
				targetForm = $("#" + editBtn.attr("data-form-target")), // this return form id for each edit button
				selects = targetForm.find("select.select"),
				saveBtn = $(
					'.btn[type="submit"][data-form-target="' +
						editBtn.attr("data-form-target") +
						'"]'
				),
				cancelBtn = saveBtn.siblings(".btn-cancel"),
				fields = targetForm.find("input[disabled]");
			editBtn.click(function(event) {
				if (editBtn.hasClass("edit-active")) {
					fields.each(function() {
						$(this).attr("disabled", "true");
						saveBtn.addClass("d-none");
						cancelBtn.addClass("d-none");
						editBtn.removeClass("d-none");
					});
					selects.attr("disabled", "true").trigger("chosen:updated");
				} else {
					fields.each(function() {
						$(this).removeAttr("disabled");
						saveBtn.removeClass("d-none");
						cancelBtn.removeClass("d-none");
						editBtn.addClass("d-none");
					});
					selects.removeAttr("disabled").trigger("chosen:updated");
				}
				editBtn.toggleClass("edit-active");
			});
			cancelBtn.click(function(event) {
				editBtn.click();
			});
		});
	})();

	$(document).on("change", 'input:not(.multi)[type="file"]', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input
				.val()
				.replace(/\\/g, "/")
				.replace(/.*\//, "");
		input.trigger("fileselect", [numFiles, label]);
	});

	$('input:not(.multi)[type="file"]').on("fileselect", function(
		event,
		numFiles,
		label
	) {
		var labelEl = $(this)
				.parent(".btn-file")
				.siblings("label"),
			elP = $(this)
				.parent(".btn-file")
				.siblings("p.file-label");
		labelEl.text(label);
		labelEl.addClass("text-truncate");
		elP.text(label);
		elP.addClass("text-truncate");
		// console.log(numFiles);
		// console.log(label);
	});

	$(document).on("change", '.multi[type="file"]', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input
				.val()
				.replace(/\\/g, "/")
				.replace(/.*\//, "");
		input.trigger("multiFileselect", [numFiles, label]);
	});

	$('.multi[type="file"]').on("multiFileselect", function(
		event,
		numFiles,
		label
	) {
		var el = $(this),
			selectedFiles = el[0].files,
			selectedWrap = $(".selected-wrap");

		for (var i = selectedFiles.length - 1; i >= 0; i--) {
			var fileLabel = selectedFiles[i].name,
				selected = $.parseHTML(
					'<span class="selected-file"><span>' +
						fileLabel +
						'</span><button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button></span>'
				);
			selectedWrap.removeClass("d-none").append(selected);
		}
	});

	$(document).on("click", ".img-preview .close", function() {
		$(this)
			.parent("div")
			.remove();
	});

	$(document).on("click", ".selected-file > .close", function() {
		var remainingSelected = $(this)
				.parent(".selected-file")
				.siblings(".selected-file")
				.children("span"),
			wrapper = $(this).parents(".selected-wrap"),
			inputFile = wrapper.find('input[name="remainingValues"]'),
			remaining = [];

		remainingSelected.each(function(index, el) {
			remaining.push($(this).text());
		});

		$(this)
			.parent(".selected-file")
			.remove();
		if (remaining.length == 0) {
			wrapper.addClass("d-none");
		}

		inputFile.val(remaining);
	});

	//datepickers
	(function() {
		//daterange
		var dateRange = $(".date-range");
		dateRange.each(function(index, el) {
			var dateFrom = $(this).find('input[name="dateFrom"]'),
				dateTo = $(this).find('input[name="dateTo"]'),
				dateFormat = "yy-m-d";

			if (dateFrom.val() !== "") {
				dateFromInit = dateFrom.val();
			} else {
				dateFromInit = null;
			}

			if (dateTo.val() !== "") {
				dateToInit = dateTo.val();
			} else {
				dateToInit = null;
			}

			dateFrom
				.datepicker({
					dateFormat: dateFormat,
					nextText: ">",
					prevText: "<"
				})
				.datepicker("setDate", dateFromInit)
				.on("change", function() {
					dateTo.datepicker("option", "minDate", getDate(this));
				});

			dateTo
				.datepicker({
					dateFormat: dateFormat,
					nextText: ">",
					prevText: "<"
				})
				.datepicker("setDate", dateToInit)
				.on("change", function() {
					dateFrom.datepicker("option", "maxDate", getDate(this));
				});

			function getDate(element) {
				var date;
				try {
					date = $.datepicker.parseDate(dateFormat, element.value);
				} catch (error) {
					date = null;
				}

				return date;
			}
		});
	})();

	//Timepickers
	(function() {
		var timepickersRange = $(".timepicker-range");
		timepickersRange.each(function(index, el) {
			// adding class input--filled while changing picker value
			$(this).on("change", function() {
				if (!$(this).hasClass("input--filled")) {
					$(this).addClass("input--filled");
				}
			});

			var timeFrom = $(this).find("input.timepicker-from"),
				timeTo = $(this).find("input.timepicker-to"),
				timeFromInit = timeFrom.val(),
				timeToInit = timeTo.val(),
				timeFromMin = timeFrom.data("min-time"), // from input's data-min-time attribute
				timeToMin = timeTo.data("min-time"), // from input's data-min-time attribute
				timeFromMax = timeFrom.data("max-time"), // from input's data-max-time attribute
				timeToMax = timeTo.data("max-time"); // from input's data-max-time attribute

			if (timeFromMin == "") {
				timeFromMin = "12:00 AM";
			}
			if (timeFromMax == "") {
				timeFromMax = "12:00 AM";
			}
			if (timeToMin == "") {
				timeToMin = "12:00 AM";
			}
			if (timeToMax == "") {
				timeToMax = "12:00 AM";
			}

			$.timepicker.timeRange(timeFrom, timeTo, {
				// minInterval: 1000 * 60 * 60, // 1hr
				timeFormat: "HH:mm",
				controlType: "select",
				oneLine: true,
				start: {
					timeOnlyTitle: "Start",
					minTime: timeFromMin,
					maxTime: timeFromMax
				}, // start picker options
				end: {
					timeOnlyTitle: "End",
					minTime: timeToMin,
					maxTime: timeToMax
				} // end picker options
			});

			if (timeFromInit !== "") {
				timeFrom.timepicker("setDate", timeFromInit);
			}
			if (timeToInit !== "") {
				timeTo.timepicker("setDate", timeToInit);
			}

			// var $datePicker = $('.ui-datepicker');
			// var $timePickerDiv = $datePicker.find('.ui-timepicker-div');
			// var $tPickHours = $timePickerDiv.find('.ui_tpicker_hour');
			// var $hoursSlider = $tPickHours.find('.ui_tpicker_hour_slider');
			// var $hoursSliderSelect = $('.ui-timepicker-select');
			// var $hoursSliderSelectOptions = $hoursSliderSelect.find('option');
			// var $firstOp = $hoursSliderSelectOptions;
			// console.log($firstOp.html());
		});
	})();

	//sorting
	(function() {
		var sortable = $(".sortable");
		sortable.each(function(index, el) {
			$(this).sortable({
				items: ".sortable-item",
				cancel: ".sortable > *:not(.sortable-item)",
				placeholder: "ui-state-highlight",
				forcePlaceholderSize: true,
				handle: "span.icon-table"
			});
		});
	})();

	//tweek dropdown button with pills fnc
	(function() {
		var dButtons = $(".btn-pill-nav");
		dButtons.each(function(index, el) {
			var el = $(this),
				dropdown = el.siblings(".dropdown-menu"),
				dropdownItem = dropdown.children(".dropdown-item");

			dropdownItem.click(function(event) {
				var value = $(this).text();
				el.text(value);
			});
		});
	})();

	//autocomplete
	(function() {
		// visit http://jqueryui.com/autocomplete/#remote-jsonp for dynamic datasource example
		var input = $(".search-autocomplete"),
			data = ["Mokhtar", "Fawzy", "Ingy"];

		input.autocomplete({
			source: data
		});
	})();

	//tooltip init
	(function() {
		$('a[data-toggle="pill"], a[data-toggle="tab"]').on(
			"shown.bs.tab",
			function(e) {
				var tabContainer = $(e.target.attributes.href.value),
					tooltipEl = tabContainer
						.find(".sports-subtab")
						.find('[data-toggle="tooltip"]');
				tooltipEl.tooltip({
					show: null,
					position: {
						my: "left top",
						at: "left bottom"
					},
					open: function(event, ui) {
						ui.tooltip.animate(
							{ top: ui.tooltip.position().top + 10 },
							"fast"
						);
					}
				});
			}
		);

		$('button[data-toggle="tooltip"]').tooltip();
	})();

	//select init
	(function() {
		var selects = $(".select");
		selects.each(function(index, el) {
			el = $(this);
			el.chosen({
				width: "100%"
			});
		});
	})();

	(function() {
		$(".change-pw").click(function(event) {
			event.preventDefault();
			$(this)
				.siblings(".changePassField")
				.addClass("d-block")
				.removeClass("d-none");
			$(this)
				.removeClass("d-none")
				.addClass("d-block");
			$(this).toggleClass("active");
		});
		$(".card-user .btn-cancel").click(function(e) {
			e.preventDefault();
			$(this)
				.parent()
				.siblings(".changePassField")
				.removeClass("d-block")
				.addClass("d-none");
			$(this)
				.parent()
				.removeClass("d-block")
				.addClass("d-none");
			$(".change-pw").removeClass("active");
		});
	})();
});
