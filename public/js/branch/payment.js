$(function() {
  buildPaymentTable();
});

$(".payment_year").click(function(){
    $(".payment_year").removeClass("active");
    $(this).addClass("active");
    $("#current_year").val("");
    $("#current_year").val($(this).data("year"));
    var table = $('#month-payments-table').DataTable();
    table.destroy();
    buildPaymentTable();
});


function buildPaymentTable()
{
  $('#month-payments-table').DataTable({
    lengthChange: false,
    autoWidth: false,
    info: false,
    searching: false,
    language: {
       searchPlaceholder: 'Search related keywords…',
       search: ''
     },
    pagingType: 'numbers',
    processing: true,
    serverSide: true,
    "ajax": {
    'url': '/buildMonthPaymentsTable',
    'type': 'POST',
    'data':{
      "branch_id": $("#current_branch_id").val(),
      "year": $("#current_year").val(),
      "_token": $('meta[name="csrf-token"]').attr('content')
       }
    },
      columns: [
          { data: 'club_name', name: 'club_name' },
          { data: 'month', name: 'month' },
          { data: 'total_booking', name: 'total_booking' },
          { data: 'revenue', name: 'revenue' },
          { data: 'bank_fees', name: 'bank_fees' },
          { data: 'total_refund', name: 'total_refund' },
          { data: 'total_amount', name: 'total_amount' },
          { data: 'recieipt', name: 'recieipt' },
          { data: 'status', name: 'status' }
      ]
  });
}

$(document).on('change', '.monthPaymentSelect', function(e) {

  var current_status = $(this).val();
  var optionSelected = $(".monthPaymentSelect option:selected", this);
  var valueSelected = this.value;
  var branch_id = $(this).find(':selected').attr('data-branch')
  var month = $(this).find(':selected').attr('data-month')
  var year = $(this).find(':selected').attr('data-year')
  if(valueSelected == "paid")
  {
    $('#paymentStatusModal').modal('show');
    $('#payment_branch_id').val("");
    $('#payment_branch_id').val(branch_id);
    $('#payment_month').val("");
    $('#payment_month').val(month);
    $('#payment_year').val("");
    $('#payment_year').val(year);
  }
});

function feedUploadModal(th) {
  var branch_id = $(th).data('branch');
  var month = $(th).data('month');
  var year = $(th).data('year');
  $('#payment_r_branch_id').val("");
  $('#payment_r_branch_id').val(branch_id);
  $('#payment_r_month').val("");
  $('#payment_r_month').val(month);
  $('#payment_r_year').val("");
  $('#payment_r_year').val(year);
}

function feedUploadedRecModal(th) {
  var branch_id = $(th).data('branch');
  var month = $(th).data('month');
  var year = $(th).data('year');
  $('#rec_img_container').html("");

  $.ajax({
    url: '/feedReceiptImageModal',
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "branch_id": branch_id,
      "month": month,
      "year": year,
    },
    success: function (data) {
      $('#rec_img_container').html(data);
    },
    error: function (data) {
    }
  });



}
