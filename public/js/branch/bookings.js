$(function() {
  buildCurrentBookingsTable();
$('#bookingCalender').fullCalendar( 'destroy' );
});

function triggerCalenderLoad()
{
  $('#bookingCalender').fullCalendar( 'destroy' );
  buildCurrentBookingsCalender();
  //buildCurrentBookingsCalender();
}

$('#sportSelect').change(function(){
  var service_sport_id = $(this).val();
  $('#courtSelect').html("");
  $('#coachSelect').html("");
  $.ajax({
       type: 'POST',
       url: '/getRelCourtsAndTrainers',
       data: {
         "_token": $('meta[name="csrf-token"]').attr('content'),
         "service_sport_id": service_sport_id
              },
              success:function(data){
                $('#courtSelect').html(data.courts_list);
                $('#coachSelect').html(data.trainers_list);

              },
              error: function (data) {
              }
   });
});

$('#monthSelect').change(function(){
  var current_month = $(this).val();
  $('#current_month').val(current_month);
});

 $("#GoFilterBtn").click(function(){
   var table = $('#current-bookings-table').DataTable();
   table.destroy();
   buildCurrentBookingsTable();
   $('#bookingCalender').fullCalendar( 'destroy' );
   buildCurrentBookingsCalender();

 });

 $("#GoClearBtn").click(function(){
   $('#monthSelect').val("");
   $('#sportSelect').val("");
   $('#coachSelect').val("");
   $('#statusSelect').val("");
   $('#courtSelect').val("");
   $('#paymentSelect').val("");
   $('#current_month').val("");
   var table = $('#current-bookings-table').DataTable();
   table.destroy();
   buildCurrentBookingsTable();
   $('#bookingCalender').fullCalendar( 'destroy' );
   buildCurrentBookingsCalender();

 });

 $(document).ready(function() {
     var table = $('#current-bookings-table').DataTable();
     table.buttons().container()
         .appendTo( '#example_wrapper' );

 } );

 function buildCurrentBookingsTable(){

   $('#current-bookings-table').DataTable({
     lengthChange: false,
     autoWidth: false,
     info: false,
     searching: false,
     language: {
        searchPlaceholder: 'Search related keywords…',
        search: ''
      },
     pagingType: 'numbers',
     processing: true,
     dom: 'Bfrtip',
     buttons: [
        {extend: 'excelHtml5' , text: 'EXPORT'}
     ],
     serverSide: true,
     "ajax": {
     'url': '/buildBookingsTable',
     'type': 'POST',
     'data':{
       "branch_id": $("#current_branch_id").val(),
       "service_sport_id": $("#sportSelect").val(),
       "court_id": $("#courtSelect").val(),
       "trainer_id": $("#coachSelect").val(),
       "current_month": $("#current_month").val(),
       "payment_type": $("#paymentSelect").val(),
       "status" : $("#statusSelect").val(),
       "_token": $('meta[name="csrf-token"]').attr('content')
        }
     },
       columns: [
           { data: 'booking_id', name: 'booking_id' },
           { data: 'member', name: 'member' },
           { data: 'sport', name: 'sport' },
           { data: 'court', name: 'court' },
           { data: 'date', name: 'date' },
           { data: 'time', name: 'time' },
           { data: 'trainer', name: 'trainer' },
           { data: 'payment', name: 'payment' },
           { data: 'status', name: 'status' },
           { data: 'total', name: 'total' },
           { data: 'action', name: 'action' }
       ]
   });

   var table = $('#current-bookings-table').DataTable();
   table.buttons().container()
       .appendTo( '#example_wrapper' );
 }


function buildCurrentBookingsCalender(){
  $.ajax({
       type: 'POST',
       url: '/buildBookingsCalender',
       data: {
         "_token": $('meta[name="csrf-token"]').attr('content'),
         "branch_id": $("#current_branch_id").val(),
         "service_sport_id": $("#sportSelect").val(),
         "court_id": $("#courtSelect").val(),
         "trainer_id": $("#coachSelect").val(),
         "status" : $("#statusSelect").val(),
         "current_month": $("#current_month").val(),
         "payment_type": $("#paymentSelect").val()
              },
              success:function(data){
                console.log("done");
                applyBookingsCalender(JSON.parse(data.bookings),data.day);
              },
              error: function (data) {
              }
   });
}

// $( document ).ajaxComplete(function( event, request, settings ) {
//   buildCurrentBookingsCalender();
//   $('#bookingCalender').fullCalendar('render');
// });


function applyBookingsCalender(bookings,day){
  $('#bookingCalender').fullCalendar({
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      header: {
        left: 'title',
        center: '',
        right: 'prev,next'
      },
      defaultDate: day,
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      eventSources:
          [
             {
              events: bookings
             }
          ],
    });
}

function feedCashModal(btn) {
  var booking_id = $(btn).data('id');
  $.ajax({
    url: '/feedEnterCashModal',
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "booking_id": booking_id,
    },
    success: function (data) {
      $('#cashPaymentCode').modal('show');
      $('#refrence_info').html(data);
    },
    error: function (data) {
    }
  });
}


$(document).on('change', '.refund_select', function(e) {

  var current_status = $(this).val();
  var optionSelected = $(".refund_select option:selected", this);
  var valueSelected = this.value;
  var booking_id = $(this).find(':selected').attr('data-id')
  if(valueSelected == "refund")
  {
    $('#confirmRefundModal').modal('show');
    $('#bookingToRefundId').val("");
    $('#bookingToRefundId').val(booking_id);
  }
});


$('#ConfirmPaymentCodesubmit').click(function(e){
clearAndResetInputs();
 var ConfirmPaymentCodeForm = $("#ConfirmPaymentCodeForm");
 var formData = ConfirmPaymentCodeForm.serialize();
 e.preventDefault();
        $.ajax({
            url:'/submitCashPayment',
            type:'POST',
            data:formData,
            success:function(data){
              location.reload();
            },

            error: function (data) {
               appendAllErrors(data);
            }
        });
    });

// ------ Clear And reseting inputs  ------
function clearAndResetInputs()
    {
    $('#cash-payment-error' ).html( "" );
    $("#cash-payment-code").removeClass("has-error");
    }
// ------End of Clear And reseting inputs  ------

// ------ append all errors  ------
 function appendAllErrors(data)
{
                var object = jQuery.parseJSON( data.responseText);
                var obj = object.errors;
                 if(obj.payment_code){
                      $("#cash-payment-code").addClass("has-error");
                      $( '#cash-payment-error' ).html( obj.payment_code );
                  }
}
