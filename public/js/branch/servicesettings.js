$(function(){
  var current_service_sport_id = $('#current_service_sport_id').val();
  buildServiceSportCard(current_service_sport_id);
});

function updateSPcard(cb) {
 var current_service_sport_id = $(cb).data('servicesportid');
 $('#sport_container_card').html("");
 buildServiceSportCard(current_service_sport_id);
}


function buildServiceSportCard(current_service_sport_id){
  $.ajax({
       type: 'POST',
       url: '/buildServiceSportCard',
       data: {
         "_token": $('meta[name="csrf-token"]').attr('content'),
         "current_service_sport_id": current_service_sport_id
              },
              success:function(data){
                $('#sport_container_card').html(data);
              },
              error: function (data) {
              }
   });
}

// ------ Handle Toggle Click events   ------
function handleServiceClick(cb) {
 var status = (cb.checked);
 var service_id = $(cb).data('serviceid');
 $('#serviceActive_'+service_id+'').val(status);
}
// ------ End Handle Toggle Click events  ------


// -------------- Switch service sport setting ---------------

function serviceSportSwitch(cb) {
 var csrf_token = $('meta[name="csrf-token"]').attr('content');
 var status = (cb.checked);
 var service_id = $(cb).data('serviceid');
 $.ajax({
      type: 'POST',
      url: '/service/sport/switch',
      data: {
        "_token": csrf_token,
        "service_sport_id": service_id
             },
             success:function(d){
               console.log(d)
             },
             error: function (data) {
             }
  });
}
// ------ Switch service sport -----------


// -------------- Switch Court---------------

function courtStatusSwitch(cb) {
 var csrf_token = $('meta[name="csrf-token"]').attr('content');
 var status = (cb.checked);
 var court_id = $(cb).data('courtid');
$('#courtActiveStatus_'+court_id+'').val(status);
}
// ------ End Switch Court- -----------


// -------------- Switch Court---------------

function trainerStatusSwitch(cb) {
 var csrf_token = $('meta[name="csrf-token"]').attr('content');
 var status = (cb.checked);
 var trainer_id = $(cb).data('trainerid');
$('#trainerActive_'+trainer_id+'').val(status);
}
// ------ End Switch Court- -----------


// ------ Show and hide add new add section  ------

// $("#add_new_court").click(function(){
// $(this).hide();
// $('#new_court').show();
// });

function openNewCourtForm(btn) {
 var court_id = $(btn).data('courtid');
 $(btn).hide();
$('#new_court_'+court_id+'').show();
}

function hideNewCourtForm(btn)
{
  var court_id = $(btn).data('courtid');
  $('#new_court_'+court_id+'').hide();
  $('#add_new_court_'+court_id+'').show();
}

function openNewTrainerForm(btn) {
 var trainer_id = $(btn).data('trainerid');
 $(btn).hide();
$('#new_trainer_'+trainer_id+'').show();
}

function hideNewTrainerForm(btn)
{
  var trainer_id = $(btn).data('trainerid');
  $('#new_trainer_'+trainer_id+'').hide();
  $('#add_new_trainer_'+trainer_id+'').show();
}

// ------ End Show and hide add new add section  ------

//  ------
function newCourtStatusSwitch(cb) {
 var status = (cb.checked);
 var sport_service_id = $(cb).data('sportservice');
 $('#newCourtActiveValue_'+sport_service_id).val(status);
}
//  -----------
function newTrainerStatusSwitch(cb) {
 var status = (cb.checked);
 var sport_service_id = $(cb).data('sportservice');
 $('#newTrainerActiveValue_'+sport_service_id).val(status);
}

function removeCourtModal(cb) {
 var courtid = $(cb).data('id');
$(".modal-body .court_record_id").val(courtid);
}

function removeTrainerModal(cb) {
 var trainerid = $(cb).data('id');
$(".modal-body .trainer_record_id").val(trainerid);
}
