
$(function() {
    $('#joined-members-table').DataTable({
      lengthChange: false,
      autoWidth: false,
      info: false,
      searching: true,
      language: {
         searchPlaceholder: 'Search member name/ID…',
         search: ''
       },
      pagingType: 'numbers',
      processing: true,
      serverSide: true,
        ajax: '/getJoinedMembers/getmembers/'+$("#current_branch_id").val()+'',
        columns: [
            { data: 'avatar', name: 'avatar' },
            { data: 'membershipId', name: 'membershipId' },
            { data: 'membershipName', name: 'membershipName' },
            { data: 'bookings', name: 'bookings' },
            { data: 'created_at', name: 'created_at' },
            { data: 'status', name: 'status' },
            { data: 'actions', name: 'actions' },
        ]
    });
});

$(function() {
    $('#members-table').DataTable({
        lengthChange: false,
        autoWidth: false,
        info: false,
        searching: true,
        pagingType: 'numbers',
        language: {
          searchPlaceholder: 'Search member name/ID…',
          search: ''
        },
        processing: true,
        serverSide: true,
        ajax: '/getMembers/getdata/'+$("#current_branch_id").val()+'',
        columns: [
            { data: 'status', name: 'status' },
            { data: 'membershipName', name: 'membershipName' },
            { data: 'membershipId', name: 'membershipId' },
            { data: 'edit', name: 'edit', orderable: false, searchable: false },
        ]
    });

    $('#searcMemberNameID').on( 'keyup', function () {
       //search data
    } );
});

$(function() {
    $('#branch-members-requests').DataTable({
        lengthChange: false,
        autoWidth: false,
        info: false,
        searching: false,
        pagingType: 'numbers',
        processing: true,
        serverSide: true,
        ajax: '/getMembersRequests/getrequests/'+$("#current_branch_id").val()+'',
        columns: [
            { data: 'avatar', name: 'avatar' },
            { data: 'membershipId', name: 'membershipId' },
            { data: 'membershipName', name: 'membershipName' },
            { data: 'reason', name: 'reason' },
            { data: 'actions', name: 'actions' },
        ]
    });
});


function submitAssignForm(cb) {
  var id = $(cb).data('id');
  $('#attachFormUserId').val("");
  $('#attachFormUserId').val(id);
  $('#memberAddClubModal').modal('show');
}

$('#attach_form_submit').click(function(e){
 var attachFormBranchId = $("#attachFormBranchId");
 var attachMemberForm = $("#attachMemberForm");
 var formData = attachMemberForm.serialize();
 e.preventDefault();
 $('#second_message').hide();
 clearAndResetLoginInputs();
        $.ajax({
            url:'/checkOrAttachMember3',
            type:'POST',
            data:formData,
            success:function(data){
                  console.log(data.user_id);
                 $('#memberAddClubModal').modal('hide');
                 $('#assign_member_submit_'+data.user_id+'').hide();
                 $('#assignedSuccefully_'+data.user_id+'').show();

                 // hide the assign btn and add assigned succefully
            },
            error: function (data) {
              var object = jQuery.parseJSON( data.responseText);
              var obj = object.errors;
               if(obj){
                    appendAllAttachErrors(obj);
                }
               else {
                 $('#second_message').show();
                 $('#first_message').hide();
               }
            }
        });
    });


// ------ append all errors  ------
 function appendAllAttachErrors(obj)
{
     if(obj.member_id){
          $("#attach-member-id").addClass("has-error");
          $( '#attach-errors-member-id' ).html( obj.member_id );
      }

      if(obj.member_name){
          $("#attach-member-name").addClass("has-error");
          $( '#attach-errors-member-name' ).html( obj.member_name );
      }
}
// ------ End of append all errors  ------

// ------ Clear And reseting inputs  ------
 function clearAndResetLoginInputs()
 {
     $( '#attach-errors-member-id' ).html( "" );
     $( '#attach-errors-member-name' ).html( "" );

     $("#attach-member-name").removeClass("has-error");
     $("#attach-member-id").removeClass("has-error");


     }
// ------End of Clear And reseting inputs  ------

function searchAllMembers(th)
{
  var querydata = $('#memberSearch').val();
  var branch_id = $('#current_branch_id').val();
  var csrf_token = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
      url:'/searchAllMembers',
      type:'POST',
      data: {
        "_token": csrf_token,
        "querydata": querydata,
        "branch_id": branch_id
             },
             success:function(data){
               $('#members_search_result').html(data);
             },
             error: function (data) {
             }
  });

}


$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href');
        getArticles(url);
        window.history.pushState("", "", url);
    });

    function getArticles(url) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var branch_id = $('#current_branch_id').val();
        $.ajax({
            url : url,
            type:'POST',
            data: {
              "_token": csrf_token,
              "branch_id": branch_id
                   }
        }).done(function (data) {
            $('#members_search_result').html(data);
        }).fail(function () {
            alert('Members could not be loaded.');
        });
    }
});


//================================ UPDATE SHEET MODAL =========================================

$('#sheetMemberModal').on('show.bs.modal', function(e) {

      var member_id = $(e.relatedTarget).data('id');
      var branch_id = $(e.relatedTarget).data('branch');
      var csrf_token = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                 url:'/feedUpdateMemberForm',
                 type:'POST',
                 data: {
                   "_token": csrf_token,
                   "member_id": member_id,
                   "branch_id" : branch_id
                        },
                        success:function(data){
                          $('#members_feed_result').html(data);
                        },
                        error: function (data) {
                        }
             });
  });


  $('#updateSheetMember_submit').click(function(e){

  e.preventDefault();
  clearAndResetLoginInputs();
   var updateSheetMember = $("#updateSheetMember");
   var member_id = $('#member_id').val();
   var formData = updateSheetMember.serialize();


          $.ajax({
              url:'/updateSheetMember/'+member_id+'',
              type:'PUT',
              data:formData,
              success:function(data){
                window.location.reload();

              },
              error: function (data) {
                 appendAllLoginErrors(data);
              }
          });
      });


// ------ END  login-----------

// ------ Clear And reseting inputs  ------
function clearAndResetLoginInputs()
{
    $( '#updateSheetMember-errors-membershipId' ).html( "" );
    $( '#updateSheetMember-errors-membershipName' ).html( "" );

    $("#sheet-membershipName").removeClass("has-error");
    $("#sheet-membershipId").removeClass("has-error");


    }
// ------End of Clear And reseting inputs  ------

// ------ append all errors  ------
 function appendAllLoginErrors(data)
{
                //console.log(data.responseText);
                var object = jQuery.parseJSON( data.responseText);

                var obj = object.errors;

               if(obj.membershipName){
                    $("#sheet-membershipName").addClass("has-error");
                    $( '#updateSheetMember-errors-membershipId' ).html( obj.membershipName );
                }

                if(obj.membershipId){
                    $("#sheet-membershipId").addClass("has-error");
                    $( '#updateSheetMember-errors-membershipId' ).html( obj.membershipId );
                }

}
// ------ End of append all errors  ------

//================================END OF UPDATE SHEET MODAL =========================================

// ======================= REGISTERED MEMBERS PAGE  ==============================


function confirmReqClick(row) {

 var req_id = $(row).data('reqid');
 var csrf_token = $('meta[name="csrf-token"]').attr('content');

 $.ajax({
     url:'/confirmApplication',
     type:'POST',
     data: {
       "_token": csrf_token,
       "req_id": req_id
     },
     success:function(data){
       window.location.reload();
     },
     error: function (data) {
       console.log('data.Message');
     }
 });


}

function cancelReqClick(row) {

var reqid = $(row).data('reqid');
var csrf_token = $('meta[name="csrf-token"]').attr('content');

 $.ajax({
     url:'/cancelApplication',
     type:'POST',
     data: {
       "_token": csrf_token,
       "req_id": reqid
     },
     success:function(data){
       window.location.reload();
     },
     error: function (data) {
     }
 });

}




function UpdateMemberActiveStatus(switcher) {

 var member_id = $(switcher).data('memberid');
 var csrf_token = $('meta[name="csrf-token"]').attr('content');
 console.log(member_id);

 $.ajax({
     url:'/UpdateJoinedMemberStatus',
     type:'POST',
     data: {
       "_token": csrf_token,
       "member_id": member_id
     },
     success:function(data){
       console.log('updated');
     },
     error: function (data) {
     }
 });
}

function ViewProfile(viewer) {

 var member_id = $(viewer).data('memberid');
 var csrf_token = $('meta[name="csrf-token"]').attr('content');

 window.location.href = '/member/'+member_id+'/info';
}

// ======================= SHEET MEMBERS DATATABLES  ==============================

// ======================= JOINED MEMBERS DATATABLES  ==============================
// ======================= REQUESTED MEMBERS DATATABLES  ==============================
// ======================= MEMBERS SHEET STATUS UPDATER  ==============================


// $("#btn_checker").on("click",function(){
//     $("#page-preloader").show();
// });
//
// $("#btn_importer").on("click",function(){
//     $("#page-preloader").show();
// });

$("#import_form_action" ).on("submit",function(){
      $(".page-preloader").show();
});

$('.browse_importer').on('change', function(){
  $('#btn_checker').prop("disabled", false);
 });


(function($){
    'use strict';
    function statusUpdater() {
        var status = $('#status');
        $.ajax({
            'url': '/status',
        }).done(function(r) {
            if(r.msg==='done') {
                status.text( " Nothing to do ... " );
                console.log( " Nothing to do ... " );
            }
            else if(r.msg==='done_importing') {
                status.text( "The import is completed. Your data is now available for viewing ... " );
                console.log( "The import is completed. Your data is now available for viewing ... " );
            }
            else if(r.msg==='done_validation') {

                console.log( r.validation_status );

                if(r.validation_status == "valid")
                {

                  status.text("Your sheet is valid for import! Please use the (Start Importing) button to import your members safely now to your branch.");
                   $('#btn_checker').hide();
                   $('#choose_file_div').hide();
                   $('#btn_importer').show();
                   $('#import_patch_id').val(r.patch);
                }
                else
                {
                    status.text( r.validation_status );
                   $('#errors_sheet_btn').show();
                   //$('#patch_id').val(r.patch);
                   $("#errors_sheet_btn").attr('href', '/downloaderrorssheet/'+r.patch+'');

                }

            } else {
                //get the total number of imported rows
                status.text( "Status is: " + r.msg );
                console.log("Status is: " + r.msg);
                console.log( "The job is not yet done... Hold your horses, it takes a while :)" );
                setTimeout(statusUpdater, 5000);
                //statusUpdater();
            }
          })
          .fail(function() {
              status.text( "An error has occurred... We could ask Neo about what happened, but he's taken the red pill and he's at home sleeping" );
              console.log( "An error has occurred... We could ask Neo about what happened, but he's taken the red pill and he's at home sleeping" );
          });
    }
    setTimeout(statusUpdater, 5000);
    //statusUpdater();
})(jQuery);
