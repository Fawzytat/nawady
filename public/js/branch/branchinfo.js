
      //$('#RegisterForm').on('submit',(function(e) {
      $('#BranchInfo-form-submit').click(function(e){
      e.preventDefault();
      clearAndResetInputs();
       var BranchInfo = $("#branchContactInfo");
       var BranchId = $("#BranchId").val();
       var formData = BranchInfo.serialize();
              $.ajax({
                  url:'/branches/'+BranchId ,
                  type:'PUT',
                  data:formData,
                  success:function(data){
                    console.log("success");
                    $("#BranchInfo-form-submit").addClass('d-none');
                    $("#BranchInfo-form-cancel").addClass('d-none');
                    $("#BranchInfo-form-edit").removeClass('d-none');
                  },

                  error: function (data) {
                     appendAllErrors(data);
                  }
              });
          });

// ------ END  Register-----------


 // ------ Clear And reseting inputs  ------
  function clearAndResetInputs()
  {

      $( '#BranchInfo-errors-email' ).html( "" );
      $( '#BranchInfo-errors-phone' ).html( "" );

      $("#BranchInfo-email").removeClass("has-error");
      $("#BranchInfo-phone").removeClass("has-error");



      }
 // ------End of Clear And reseting inputs  ------

  // ------ append all errors  ------
   function appendAllErrors(data)
  {
                  //console.log(data.responseText);
                  var object = jQuery.parseJSON( data.responseText);

                  var obj = object.errors;


                   if(obj.phone){
                        $("#BranchInfo-phone").addClass("has-error");
                        $( '#BranchInfo-errors-phone' ).html( obj.phone );
                    }

                  if(obj.email){
                      $("#BranchInfo-email").addClass("has-error");
                      $( '#BranchInfo-errors-email' ).html( obj.email );
                  }


  }
  // ------ End of append all errors  ------

  // ------ Handle Toggle Click events   ------
  function handlebranchClick(cb) {

   var status = (cb.checked);
   var branch_id = $(cb).data('branchid');
   console.log(branch_id);
   console.log(status);
   $('#branchActive_'+branch_id+'').val(status);
}

function handleNewbranchClick(cb) {

 var status = (cb.checked);
 console.log(status);
 $('#branchActiveValue').val(status);
}

  // ------ End Handle Toggle Click events  ------


// ------ Handle the sorting request   ------
  $("#adv_sortable").sortable({
  placeholder: 'sortable_placeholder',
  update : function () {
      var order = $("#adv_sortable").sortable('serialize');
      var csrf_token = $('meta[name="csrf-token"]').attr('content');
       console.log(order);

      $.ajax({
           type: 'POST',
           url: '/sortBranchAdvs',
           data: {
             "_token": csrf_token,
             "data": order
                  },
                  success:function(d){
                    console.log(d)
                  },
                  error: function (data) {
                  }
       });
  }
  });
  // ------ Handle the sorting request   ------


  // ------ Show and hide add new add section  ------

$("#add_new_ad").click(function(){
 $(this).hide();
 $('#new_ad').show();
});

$('#hideNewAdvForm').click(function(){
  console.log('dfsdf')
 $('#new_ad').hide();
 $('#add_new_ad').show();
});

// ------ End Show and hide add new add section  ------


function removeAdvModal(cb) {
 var advid = $(cb).data('id');
$(".modal-body .advert_record_id").val(advid);
}
