
  var app = new Vue({
    el: '#root',
    data: {
      newName : '',
      status: false,
      messages : 'welcome fdeghfdh',
      others : 'dsfdsf',
      students : ['suzy' , 'jozef' , 'ressie'] ,
      tasks : [
        { description: " First description " , completed: true },
        { description: " second description " , completed: true },
        { description: " Third description " , completed: false },
        { description: " fourth description " , completed: false },
        { description: " fifth description " , completed: false },
        { description: " 6th description " , completed: true },
        { description: " 7th description " , completed: false },
        { description: " 8th description " , completed: true },
      ]
    },
    methods :
    {
      addName(){
        this.students.push(this.newName);
        this.newName = '';
      },
      toggleClass(){
        this.status = !this.status;
      }
    },
    computed : {
      incompletedTasks(){
        return this.tasks.filter(taskaya => !taskaya.completed)
      }
    }

  })
