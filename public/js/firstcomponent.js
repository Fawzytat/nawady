Vue.component('task-list',{
  template : `
    <div>
      <task v-for="task in tasks" v-text="task.description"></task>
    </div>
  `,
  data() {
    return {
      tasks : [
        { description: " First description " , completed: true },
        { description: " second description " , completed: true },
        { description: " Third description " , completed: false },
        { description: " fourth description " , completed: false },
        { description: " fifth description " , completed: false },
        { description: " 6th description " , completed: true },
        { description: " 7th description " , completed: false },
        { description: " 8th description " , completed: true },
      ]
    }
  }
});

Vue.component('task',{
  template : '<li><slot></slot></li>'
});

new Vue({
  el : '#root'
});
