function updateUserImage(file1) {
	var member_id = $("#photo_member_id").val();
	var csrf_token = $('meta[name="csrf-token"]').attr("content");

	var file = file1.files[0];
	var formData = new FormData($("#img_thumb_upload")[0]);
	formData.append("formData", file);

	$.ajax({
		headers: {
			"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
		},
		type: "POST",
		url: "/updateUserPic/" + member_id + "",
		data: formData,

		contentType: false,
		processData: false,
		success: function(data) {
			window.location.reload();
		},
		error: function(data) {
			console.log();
		}
	});
}

$(function() {
	buildCurrentBookingsTable();
});

$("#branchSelect").change(function() {
	var branch_id = $(this).val();
	$("#courtSelect").html("");

	$.ajax({
		type: "POST",
		url: "/getBranchCourts",
		data: {
			_token: $('meta[name="csrf-token"]').attr("content"),
			branch_id: branch_id
		},
		success: function(data) {
			$("#courtSelect").html(data.courts_list);
		},
		error: function(data) {}
	});
});

$("#GoFilterBtn").click(function() {
	var table = $("#current-bookings-table").DataTable();
	table.destroy();
	buildCurrentBookingsTable();
});

$("#GoFilterBtn").click(function() {
	var table = $("#current-bookings-table").DataTable();
	table.destroy();
	buildCurrentBookingsTable();
});

$("#GoClearBtn").click(function() {
	$("#branchSelect").val("");
	$("#courtSelect").val("");
	$("#statusSelect").val("");

	var table = $("#current-bookings-table").DataTable();
	table.destroy();
	buildCurrentBookingsTable();
	//ds
});

// $(document).ready(function() {
//     var table = $('#current-bookings-table').DataTable();
//     table.buttons().container()
//         .appendTo( '#example_wrapper' );
// } );

function buildCurrentBookingsTable() {
	$("#current-bookings-table").DataTable({
		lengthChange: false,
		autoWidth: false,
		info: false,
		searching: false,
		language: {
			searchPlaceholder: "Search related keywords…",
			search: ""
		},
		pagingType: "numbers",
		processing: true,
		serverSide: true,
		ajax: {
			url: "/buildUserBookingsTable",
			type: "POST",
			data: {
				branch_id: $("#branchSelect").val(),
				court_id: $("#courtSelect").val(),
				user_id: $("#current_user_id").val(),
				status: $("#statusSelect").val(),
				_token: $('meta[name="csrf-token"]').attr("content")
			}
		},
		columns: [
			{ data: "booking_id", name: "booking_id" },
			{ data: "court", name: "court" },
			{ data: "club", name: "club" },
			{ data: "date", name: "date" },
			{ data: "time", name: "time" },
			{ data: "trainer", name: "trainer" },
			{ data: "status", name: "status" },
			{ data: "total", name: "total" },
			{ data: "action", name: "action" }
		]
	});
}



function showReceipt(btn) {
	var booking_id = $(btn).data("id");
	$.ajax({
		url: "/getBookingReceipt",
		type: "POST",
		data: {
			_token: $('meta[name="csrf-token"]').attr("content"),
			booking_id: booking_id
		},
		success: function(data) {
			$("#Booking_receipt_modal").modal("show");
			$("#booking_data").html(data);
		},
		error: function(data) {}
	});
}

function cancelBooking(btn) {
	$("#Booking_cancel_id").val("");
	var booking_id = $(btn).data("id");
	$("#Booking_cancel_modal").modal("show");
	$("#Booking_cancel_id").val(booking_id);
}
// toggle edit profile form
(function() {
    // catch dom
    var $editBtn = $("#editMemberInfo");
    var $formBtnContainer = $(".card-footer");
    var $formInputsContainer = $("#editInfo");
    var $emailInput = $formInputsContainer.find('input[name="email"]');
    var $emailVal = $emailInput.val();
    var $phoneInput = $formInputsContainer.find('input[name="phone"]');
    var $phoneVal = $phoneInput.val();
    var $addressInput = $formInputsContainer.find('input[name="address"]');
    var $addressVal = $addressInput.val();
    var $saveBtn = $formBtnContainer.find("#save");
    var $cancelBtn = $formBtnContainer.find("#cancel");
    var $logoutBtn = $formBtnContainer.find("#logout");

    $editBtn.on("click", function() {
        if (!$(this).hasClass("d-none")) {
            $(this)
                .removeClass("d-block")
                .addClass("d-none");
        }
        $emailInput.removeAttr("disabled");
        $phoneInput.removeAttr("disabled");
        $addressInput.removeAttr("disabled");
        $saveBtn
            .parent()
            .removeClass("d-none")
            .addClass("d-flex");
        $saveBtn.removeClass("d-none").addClass("d-block");
        $cancelBtn.removeClass("d-none").addClass("d-block");
        $logoutBtn.removeClass("d-block").addClass("d-none");
    });

    $cancelBtn.on("click", function() {
        $saveBtn
            .parent()
            .removeClass("d-flex")
            .addClass("d-none");
				$('.help-block').addClass('d-none');
        $editBtn.addClass("d-block").removeClass("d-none");
        $saveBtn.removeClass("d-block").addClass("d-none");
        $cancelBtn.removeClass("d-block").addClass("d-none");
        $logoutBtn.removeClass("d-none").addClass("d-block");
        $emailInput.attr("disabled", "disabled");
        $phoneInput.attr("disabled", "disabled");
        $addressInput.attr("disabled", "disabled");
        $emailInput.val($emailVal);
        $phoneInput.val($phoneVal);
        $addressInput.val($addressVal);
    });
})();
