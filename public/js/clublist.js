

function handlebranchClick(cb) {

 var status = (cb.checked);
 var branch_id = $(cb).data('branchid');
 $('#branchActive_'+branch_id+'').val(status);

}

function handleclubClick(checkbox) {

 var status = (checkbox.checked);
 var club_id = $(checkbox).data('clubid');
 $('#clubActive_'+club_id+'').val(status);

}


function openNewBranchForm(btn) {
 var clubid = $(btn).data('clubid');
 $(btn).hide();
$('#newBranch_'+clubid+'').show();
}

function hideNewBranchForm(btn)
{
  var clubid = $(btn).data('clubid');
  $('#newBranch_'+clubid+'').hide();
  $('#add_new_branch_'+clubid+'').show();
}


function newBranchClick(cb) {

 var status = (cb.checked);
 var club_id = $(cb).data('clubid');

 $('#newBranchActive_'+club_id).val(status);
}

function newClubClick(cb) {

 var status = (cb.checked);
 console.log(status);
 $('#new_Club_Active').val(status);
}

function ClubBranchClick(cb) {

 var status = (cb.checked);
 console.log(status);
 $('#Club_Branch_Active').val(status);
}
