$(document).ready(function() {
	//owl
	(function() {
		$('.ads-carousel').owlCarousel({
			items: 1,
			autoHeight: true,
			center: true,
			autoplay: true,
			loop: true
		});

		$('.clubs-carousel').owlCarousel({
			items: 1,
			autoplay: true,
			loop: true
		});
	})();

	// input effects
	(function() {
		// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
		if (!String.prototype.trim) {
			(function() {
				// Make sure we trim BOM and NBSP
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, '');
				};
			})();
		}

		[].slice
			.call(document.querySelectorAll('input.input__field'))
			.forEach(function(inputEl) {
				// in case the input is already filled..
				if (inputEl.value.trim() !== '') {
					classie.add(inputEl.parentNode, 'input--filled');
				}

				// events:
				inputEl.addEventListener('focus', onInputFocus);
				inputEl.addEventListener('blur', onInputBlur);
			});

		function onInputFocus(ev) {
			classie.add(ev.target.parentNode, 'input--filled');
		}

		function onInputBlur(ev) {
			if (ev.target.value.trim() === '') {
				classie.remove(ev.target.parentNode, 'input--filled');
			}
		}
	})();

	//switch modals
	(function() {
		var loginModal = $('#loginModal'),
			registerModal = $('#registerModal'),
			forgotPwModal = $('#forgotPwModal');
		(triggerRegisterBtn = $('a[href="#registerModal"]')),
			(triggerForgotPWBtn = $('a[href="#forgotPwModal"]'));

		triggerRegisterBtn.click(function(event) {
			loginModal.modal('hide');
			registerModal.modal('show');
		});

		triggerForgotPWBtn.click(function(event) {
			loginModal.modal('hide');
			forgotPwModal.modal('show');
		});
	})();

	//hamburger menu
	(function() {
		var menu = document.getElementById('slide-nav'),
			showBtn = document.getElementById('showBtn'),
			body = document.body;

		if (showBtn !== null) {
			showBtn.onclick = function() {
				classie.toggle(this, 'is-active');
				classie.toggle(menu, 'cbp-spmenu-open');
				classie.toggle(body, 'menu-overlay');
			};
		}
	})();

	//edit forms
	(function() {
		var editBtns = $('.btn[type="button"][data-form-target]');
		editBtns.each(function() {
			var editBtn = $(this),
				targetForm = $('#' + editBtn.attr('data-form-target')),
				saveBtn = $(
					'.btn[type="submit"][data-form-target="' +
						editBtn.attr('data-form-target') +
						'"]'
				),
				fields = targetForm.find('input[disabled]');

			editBtn.click(function(event) {
				if (editBtn.hasClass('edit-active')) {
					fields.each(function() {
						$(this).attr('disabled', 'true');
						saveBtn.addClass('d-none');
					});
				} else {
					fields.each(function() {
						$(this).removeAttr('disabled');
						saveBtn.removeClass('d-none');
					});
				}
				editBtn.toggleClass('edit-active');
			});
		});
	})();

	$(document).on('change', ':file', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input
				.val()
				.replace(/\\/g, '/')
				.replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	});

	$(':file').on('fileselect', function(event, numFiles, label) {
		console.log(numFiles);
		console.log(label);
	});
});
