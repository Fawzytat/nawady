var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var minifycss = require('gulp-uglifycss');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync.init({
    // For more options
    // @link http://www.browsersync.io/docs/options/

    // Project URL.
    proxy: 'http://localhost/steam/public',

    // `true` Automatically open the browser with BrowserSync live server.
    // `false` Stop the browser from automatically opening.
    open: false,

    // Inject CSS changes.
    // Commnet it to reload browser for every CSS change.
    injectChanges: true,

    // Use a specific port (instead of the one auto-detected by Browsersync).
    port: 8000
  });
});

gulp.task('bs-reload', function() {
  browserSync.reload();
});

gulp.task('styles', function() {
  gulp
    .src(['sass/**/*.scss'])
    .pipe(sourcemaps.init())
    .pipe(
      plumber({
        errorHandler: function(error) {
          console.log(error.message);
          this.emit('end');
        }
      })
    )
    .pipe(sass())
    .pipe(sourcemaps.write({ includeContent: false }))
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('css/'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('css/'))
    // .pipe(browserSync.reload({ stream: true }))
    // .pipe(notify({ message: 'TASK: "styles" Completed! 💯', onLast: true }));
});

gulp.task('scripts', function() {
  return (gulp
      .src('js/main.js')
      .pipe(
        plumber({
          errorHandler: function(error) {
            console.log(error.message);
            this.emit('end');
          }
        })
      )
      .pipe(rename({ suffix: '.min' }))
      .pipe(uglify())
      .pipe(gulp.dest('js/'))
      // .pipe(browserSync.reload({ stream: true }))
      // .pipe(notify({ message: 'TASK: "javascript" Completed! 💯', onLast: true })) 
    );
});

gulp.task('default', function() {
  gulp.watch('sass/**/*.scss', ['styles']);
  gulp.watch('js/main.js', ['scripts']);
  gulp.watch('*.html', ['bs-reload']);
});
